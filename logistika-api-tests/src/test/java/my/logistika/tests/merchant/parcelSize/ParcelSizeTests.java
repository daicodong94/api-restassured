package my.logistika.tests.merchant.parcelSize;

import io.qameta.allure.TmsLink;
import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.merchant.parcelSize.steps.GetParcelSizeSteps;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

public class ParcelSizeTests extends BaseTest {
    GetParcelSizeSteps getParcelSizeSteps = new GetParcelSizeSteps();


    @Test(description = "API: ParcelSize_merchant - get parcel size", groups = {"api-merchant"})
    @TmsLink(value = "TEST-001")
    public void get_parcel_size()
    {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        getParcelSizeSteps.when_get_all_parcel_size()
                .validateResponse(HttpStatus.SC_OK);
    }
    @Test(description = "API: ParcelSize_merchant - get parcel size by visible", groups = {"api-merchant"})
    @TmsLink(value = "TEST-002")
    public void get_parcel_size_by_visible()
    {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        getParcelSizeSteps.when_get_parcel_size_by_visible(Boolean.TRUE)
                .validateResponse(HttpStatus.SC_OK);
    }
    @Test(description = "API: ParcelSize_merchant - get parcel size by id", groups = {"api-merchant"})
    @TmsLink(value = "TEST-002")
    public void get_parcel_size_by_id()
    {
        // id 610a6f50b4a8a20011fa1593
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        getParcelSizeSteps.when_get_parcel_size_by_id("610a6f50b4a8a20011fa1593")
                .validateResponse(HttpStatus.SC_OK);
    }
}
