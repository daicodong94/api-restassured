package my.logistika.tests.merchant.address;

import io.qameta.allure.TmsLink;
import my.logistika.base.BaseTest;
import my.logistika.data.DataUser;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.authentication.login.steps.LoginSteps;
import my.logistika.microservices.merchant.address.models.CreateAddressRequest;
import my.logistika.microservices.merchant.address.models.CreateAddressResponse;
import my.logistika.microservices.merchant.address.steps.CreateMerchantAddressSteps;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

public class MerchantAddressTests extends BaseTest {

    private CreateMerchantAddressSteps merchantAddressSteps = new CreateMerchantAddressSteps();
    private CreateAddressResponse createAddressResponse;
    private CreateAddressRequest createAddressRequest;
    LoginSteps loginSteps = new LoginSteps();

    @Test(description = "API: address - Create a new merchant address", groups = {"api-merchant"})
    @TmsLink(value = "TEST-001")
    public void test_create_address(){
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        createAddressRequest = new CreateAddressRequest("TestName","+601111111111","testAddress","testAddress2","60000","testCity","testState","sample pickup instructions","Malaysia");
        createAddressResponse = (CreateAddressResponse) merchantAddressSteps.when_created_merchant_address(createAddressRequest)
                                .validateResponse(HttpStatus.SC_CREATED)
                                .saveResponseObject(CreateAddressResponse.class);
        merchantAddressSteps.then_verify_merchant_address(createAddressRequest,createAddressResponse);
    }
    }
