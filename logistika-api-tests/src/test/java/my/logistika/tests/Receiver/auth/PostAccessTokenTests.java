package my.logistika.tests.Receiver.auth;

import io.qameta.allure.Description;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import my.logistika.microservices.receiver.auth.steps.AccessTokenSteps;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import static my.logistika.microservices.receiver.auth.models.Constants.*;

public class PostAccessTokenTests extends AuthScriptBaseTests {
    private AccessTokenSteps accessTokenSteps = new AccessTokenSteps();

    @Test(description = "API: POST access token", groups = "regression")
    @Description("201 - Create new token successfully with valid code")
    @Story("Login with Setel Connect Web form")
    @TmsLink("RC-119")
    public void RC_119_create_new_token_successfully_with_valid_code(){
        accessTokenSteps.given_create_access_token(loginCode)
                        .then_verify_status_code(HttpStatus.SC_CREATED)
                        .then_verify_get_access_token_successfully();
    }

    @Test(description = "API: POST access token", groups = "regression")
    @Description("400 - Create new token with code is empty or null")
    @Story("Login with Setel Connect Web form")
    @TmsLink("RC-120")
    public void RC_120_create_new_token_with_code_is_empty_or_null(){
        accessTokenSteps.given_create_access_token("")
                        .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                        .then_verify_response_body_when_code_is_empty();

        accessTokenSteps.given_create_access_token(null)
                        .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                        .then_verify_response_body_when_code_is_null();
    }

    @Test(description = "API: POST access token", groups = "regression")
    @Description("400 - Create new token without code")
    @Story("Login with Setel Connect Web form")
    @TmsLink("RC-121")
    public void RC_121_create_new_token_without_code(){
        accessTokenSteps.given_create_access_token_with_empty_body()
                        .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                        .then_verify_response_body_when_code_is_null();
    }

    @Test(description = "API: POST access token", groups = "regression")
    @Description("400 - Create new token with non-existing code")
    @Story("Login with Setel Connect Web form")
    @TmsLink("RC-122")
    public void RC_122_create_new_token_with_non_existing_code(){
        accessTokenSteps.given_create_access_token(NON_EXISTING_VALUE)
                        .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                        .then_verify_returned_authorization_code_not_found();
    }

    @Test(description = "API: POST access token", groups = "regression")
    @Description("400 - Create new token with expired code")
    @Story("Login with Setel Connect Web form")
    @TmsLink("RC-123")
    public void RC_123_create_new_token_with_expired_code(){
        accessTokenSteps.given_create_access_token(EXPIRED_CODE)
                        .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                        .then_verify_returned_authorization_code_not_found();
    }

    @Test(description = "API: POST access token", groups = "regression")
    @Description("400 - Create new token with invalid type of code")
    @Story("Login with Setel Connect Web form")
    @TmsLink("RC-124")
    public void RC_124_create_new_token_with_invalid_type_of_code(){
        accessTokenSteps.given_create_access_token_with_type_of_code_is_integer(123)
                        .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                        .then_verify_response_body_when_enter_invalid_type_of_code();

        accessTokenSteps.given_create_access_token_with_type_of_code_is_boolean(true)
                        .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                        .then_verify_response_body_when_enter_invalid_type_of_code();
    }
}
