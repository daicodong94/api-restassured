package my.logistika.tests.Receiver.NextPublicHolidaysCount;
import my.logistika.base.BaseTest;
import my.logistika.microservices.receiver.NextPublicHolidaysCount.models.NextPublicHolidaysCountResponse;
import my.logistika.microservices.receiver.NextPublicHolidaysCount.steps.NextPublicHolidaysCountSteps;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import java.util.HashMap;
import java.util.Map;


public class NextPublicHolidaysCountTests extends BaseTest {

    private NextPublicHolidaysCountSteps nextHolidayCountSteps = new NextPublicHolidaysCountSteps();
    private NextPublicHolidaysCountResponse nextPublicHolidaysCountResponse;

    @Test(description = "API: Receiver - Next Public Holidays Count", groups = "regression",dataProvider = "next_public_holiday_count_data_provider")
    public void test_next_public_Holidays_count(String date,Object isSaturdayWorkingDay,Object daysToSkip,Object deliveryRegion, int expectedCount) {
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("date",date);
        if (!isSaturdayWorkingDay.equals("default")){
            qMap.put("isSaturdayWorkingDay", isSaturdayWorkingDay);
        }
        if (!daysToSkip.equals("default")) {
            qMap.put("workingDaysToSkip", daysToSkip);
        }
        if (!deliveryRegion.equals("default")){
            qMap.put("deliveryRegion", deliveryRegion);
        }
        nextPublicHolidaysCountResponse = (NextPublicHolidaysCountResponse) nextHolidayCountSteps.when_holiday_count_is_requested(qMap)
                .validateResponse(200)
                .saveResponseObject(NextPublicHolidaysCountResponse.class);
        nextHolidayCountSteps.then_expected_holidays_count_is_returned(expectedCount,nextPublicHolidaysCountResponse);
    }

   @Test(description = "API: Receiver - Next Public Holidays Count - invalid date", groups = "regression")
    public void test_next_public_Holidays_count_invalid_date() {
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("date","invalid");
        nextHolidayCountSteps.when_holiday_count_is_requested(qMap)
                .validateResponse(400);
    }

    @Test(description = "API: Receiver - Next Public Holidays Count - invalid workingDaysToSkip", groups = "regression")
    public void test_next_public_Holidays_count_invalid_workingDays() {
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("date","2021-11-03");
        qMap.put("isSaturdayWorkingDay", true);
        qMap.put("workingDaysToSkip", "invalid");
        qMap.put("deliveryRegion", "KV");
        nextHolidayCountSteps.when_holiday_count_is_requested(qMap)
                .validateResponse(400);
    }


    /*
    This is data provider which sets query parameters of different combinations and passes expected output for each combination.
    isSaturdayWorkingDay, daysToSKip and deliveryRegion are optional query parameters.
    Hence, add 'default' in the data provider when you do not want to add a parameter which is handled in test method to skip the query parameters when specified as default.
     */

    @DataProvider(name = "next_public_holiday_count_data_provider")
    public Object[][] next_public_holiday_count_data_provider() {
        final Object[][] objects = {
                {"2021-11-03", true, 0, "KV", 1},
                {"2021-11-04", false, 0, "KV", 0},
                {"2021-11-05", true, 0, "KV", 0},
                {"2021-11-05", false, 0, "KV", 2},
                {"2021-11-03", false, 0, "KV", 1},
                {"2021-11-05", true, 0, "JHR", 0},
                {"2021-11-05", false, 0, "JHR", 2},
                {"2021-11-05", false, 0, "JHR", 2},
                {"2021-11-03", true, 1, "KV", 2},
                {"2021-11-03", false, 1, "KV", 4},
                {"2021-11-03", "default", 0, "KV", 1},
                {"2021-11-03", true, "default", "KV", 1},
                {"2021-11-03", true, 0, "default", 1},
                {"2021-11-03", "default", "default", "default", 1}
        };
        return objects;
    }
}
