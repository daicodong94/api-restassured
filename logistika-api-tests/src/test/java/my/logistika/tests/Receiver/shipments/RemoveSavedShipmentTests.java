package my.logistika.tests.Receiver.shipments;

import io.qameta.allure.Description;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import my.logistika.microservices.receiver.auth.steps.AccessTokenSteps;
import my.logistika.microservices.receiver.shipments.steps.AddSavedShipmentSteps;
import my.logistika.microservices.receiver.shipments.steps.RemoveSavedShipmentSteps;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import static my.logistika.microservices.receiver.auth.models.Constants.*;


public class RemoveSavedShipmentTests extends ShipmentScriptBase {
    private AddSavedShipmentSteps addSavedShipmentSteps = new AddSavedShipmentSteps();
    private RemoveSavedShipmentSteps removeSavedShipmentSteps = new RemoveSavedShipmentSteps();
    private AccessTokenSteps accessTokenSteps = new AccessTokenSteps();

    @Test(description = "API: DELETE Remove shipment", groups = "regression")
    @Description("200 - Send request with valid shipmentId and valid access token")
    @Story("Remove from my shipments")
    @TmsLink("RC-265")
    public void RC_265_remove_shipment_with_valid_shipmendId_and_access_token() {
        accessTokenSteps.given_create_access_token(loginCode);
        String accessToken = accessTokenSteps.then_get_the_returned_access_token();
        then_verify_shipment_is_stored_to_receiver_database();
        addSavedShipmentSteps.given_post_add_saved_shipment_with_authorization(accessToken,shipmentId)
                .then_verify_status_code(HttpStatus.SC_CREATED);
        removeSavedShipmentSteps.given_delete_remove_saved_shipment_with_authorization(accessToken,shipmentId)
                          .then_verify_status_code(HttpStatus.SC_OK)
                          .then_verify_response_body_remove_saved_shipment_successfully(PHONE_NUMBER, shipmentId);
    }

    @Test(description = "API: DELETE Remove shipment", groups = "regression")
    @Description("404 - Send request with multi shipmentId and valid access token")
    @Story("Remove from my shipments")
    @TmsLink("RC-268")
    public void RC_268_remove_shipment_with_multi_shipmendId_and_valid_access_token() {
        accessTokenSteps.given_create_access_token(loginCode);
        String accessToken = accessTokenSteps.then_get_the_returned_access_token();
        then_verify_shipment_is_stored_to_receiver_database();
        addSavedShipmentSteps.given_post_add_saved_shipment_with_authorization(accessToken,shipmentId)
                .then_verify_status_code(HttpStatus.SC_CREATED);
        removeSavedShipmentSteps.given_delete_remove_saved_shipment_with_authorization_and_multi_shipmentId(accessToken,shipmentId)
                .then_verify_status_code(HttpStatus.SC_NOT_FOUND)
                .then_verify_response_body_when_saved_shipment_not_found();
    }

    @Test(description = "API: DELETE Remove shipment", groups = "regression")
    @Description("400 - Send request with the removed shipmentId and valid access token")
    @Story("Remove from my shipments")
    @TmsLink("RC-269")
    public void RC_269_remove_shipment_with_the_added_shipmendId_and_valid_access_token() {
        accessTokenSteps.given_create_access_token(loginCode);
        String accessToken = accessTokenSteps.then_get_the_returned_access_token();
        then_verify_shipment_is_stored_to_receiver_database();
        addSavedShipmentSteps.given_post_add_saved_shipment_with_authorization(accessToken,shipmentId)
                .then_verify_status_code(HttpStatus.SC_CREATED);
        removeSavedShipmentSteps.given_delete_remove_saved_shipment_with_authorization(accessToken,shipmentId)
                .then_verify_status_code(HttpStatus.SC_OK)
                .then_verify_response_body_remove_saved_shipment_successfully(PHONE_NUMBER, shipmentId);
        removeSavedShipmentSteps.given_delete_remove_saved_shipment_with_authorization(accessToken,shipmentId)
                .then_verify_status_code(HttpStatus.SC_NOT_FOUND)
                .then_verify_response_body_when_saved_shipment_not_found();
    }

    @Test(description = "API: DELETE Remove shipment", groups = "regression")
    @Description("404 - Send request with non-existing shipmentId and valid access token")
    @Story("Remove from my shipments")
    @TmsLink("RC-271")
    public void RC_271_remove_shipment_with_non_existing_shipmendId_and_valid_access_token(){
        accessTokenSteps.given_create_access_token(loginCode);
        String accessToken = accessTokenSteps.then_get_the_returned_access_token();
        removeSavedShipmentSteps.given_delete_remove_saved_shipment_with_authorization(accessToken,NON_EXISTING_VALUE)
                .then_verify_status_code(HttpStatus.SC_NOT_FOUND)
                .then_verify_response_body_when_saved_shipment_not_found();
    }

    @Test(description = "API: DELETE Remove shipment", groups = "regression")
    @Description("401 - Send request with valid shipmentId but do not send access token")
    @Story("Remove from my shipments")
    @TmsLink("RC-272")
    public void RC_272_remove_shipment_with_valid_shipmendId_without_access_token() {
        then_verify_shipment_is_stored_to_receiver_database();
        removeSavedShipmentSteps.given_delete_remove_saved_shipment_without_authorization(shipmentId)
                .then_verify_status_code(HttpStatus.SC_UNAUTHORIZED)
                .then_verify_returned_unauthorized();
    }

    @Test(description = "API: DELETE Remove shipment", groups = "regression")
    @Description("401 - Send request with valid shipmentId and non-existing access token")
    @Story("Remove from my shipments")
    @TmsLink("RC-273")
    public void RC_273_remove_shipment_with_valid_shipmendId_and_non_existing_access_token() {
        then_verify_shipment_is_stored_to_receiver_database();
        removeSavedShipmentSteps.given_delete_remove_saved_shipment_with_authorization(NON_EXISTING_VALUE,shipmentId)
                .then_verify_status_code(HttpStatus.SC_UNAUTHORIZED)
                .then_verify_returned_unauthorized();
    }

    @Test(description = "API: DELETE Remove shipment", groups = "regression")
    @Description("401 - Send request with valid shipmentId and expired access token")
    @Story("Remove from my shipments")
    @TmsLink("RC-274")
    public void RC_274_remove_shipment_with_valid_shipmendId_and_expired_access_token() {
        then_verify_shipment_is_stored_to_receiver_database();
        removeSavedShipmentSteps.given_delete_remove_saved_shipment_with_authorization(EXPIRED_ACCESS_TOKEN,shipmentId)
                .then_verify_status_code(HttpStatus.SC_UNAUTHORIZED)
                .then_verify_returned_unauthorized();
    }

    @Test(description = "API: DELETE Remove shipment", groups = "regression")
    @Description("401 - Send request with invalid both of shipmentId and access token")
    @Story("Remove from my shipments")
    @TmsLink("RC-275")
    public void RC_275_remove_shipment_with_invalid_shipmendId_and_access_token() {
        removeSavedShipmentSteps.given_delete_remove_saved_shipment_with_authorization(NON_EXISTING_VALUE,NON_EXISTING_VALUE)
                .then_verify_status_code(HttpStatus.SC_UNAUTHORIZED)
                .then_verify_returned_unauthorized();
    }
}