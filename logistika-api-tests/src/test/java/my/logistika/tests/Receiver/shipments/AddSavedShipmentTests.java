package my.logistika.tests.Receiver.shipments;

import io.qameta.allure.Description;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import my.logistika.microservices.receiver.auth.steps.AccessTokenSteps;
import my.logistika.microservices.receiver.shipments.steps.AddSavedShipmentSteps;
import my.logistika.microservices.receiver.shipments.steps.ShipmentDetailSteps;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import static my.logistika.microservices.receiver.auth.models.Constants.*;


public class AddSavedShipmentTests extends ShipmentScriptBase {
    private AddSavedShipmentSteps addSavedShipmentSteps = new AddSavedShipmentSteps();
    private AccessTokenSteps accessTokenSteps = new AccessTokenSteps();
    private ShipmentDetailSteps shipmentDetailSteps = new ShipmentDetailSteps();

    @Test(description = "API: POST Add shipment", groups = "regression")
    @Description("201 - Send request with valid shipmentId and valid access token")
    @Story("Add to my shipments")
    @TmsLink("RC-253")
    public void RC_253_add_shipment_with_valid_shipmendId_and_access_token() {
        accessTokenSteps.given_create_access_token(loginCode);
        String accessToken = accessTokenSteps.then_get_the_returned_access_token();
        then_verify_shipment_is_stored_to_receiver_database();
        addSavedShipmentSteps.given_post_add_saved_shipment_with_authorization(accessToken,shipmentId)
                          .then_verify_status_code(HttpStatus.SC_CREATED)
                          .then_verify_response_body_add_saved_shipment_successfully(PHONE_NUMBER, shipmentId);
    }

    @Test(description = "API: POST Add shipment", groups = "regression")
    @Description("404 - Send request with multi shipmentId and valid access token")
    @Story("Add to my shipments")
    @TmsLink("RC-256")
    public void RC_256_add_shipment_with_multi_shipmendId_and_valid_access_token() {
        accessTokenSteps.given_create_access_token(loginCode);
        String accessToken = accessTokenSteps.then_get_the_returned_access_token();
        then_verify_shipment_is_stored_to_receiver_database();
        addSavedShipmentSteps.given_post_add_saved_shipment_with_authorization_and_multi_shipmentId(accessToken,shipmentId)
                .then_verify_status_code(HttpStatus.SC_NOT_FOUND)
                .then_verify_response_body_when_saved_shipment_not_found();
    }

    @Test(description = "API: POST Add shipment", groups = "regression")
    @Description("400 - Send request with the added shipmentId and valid access token")
    @Story("Add to my shipments")
    @TmsLink("RC-257")
    public void RC_257_add_shipment_with_the_added_shipmendId_and_valid_access_token() {
        accessTokenSteps.given_create_access_token(loginCode);
        String accessToken = accessTokenSteps.then_get_the_returned_access_token();
        then_verify_shipment_is_stored_to_receiver_database();
        addSavedShipmentSteps.given_post_add_saved_shipment_with_authorization(accessToken,shipmentId)
                .then_verify_status_code(HttpStatus.SC_CREATED)
                .then_verify_response_body_add_saved_shipment_successfully(PHONE_NUMBER, shipmentId);
        addSavedShipmentSteps.given_post_add_saved_shipment_with_authorization(accessToken,shipmentId)
                .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                .then_verify_response_body_when_shipment_already_added();
    }

    @Test(description = "API: POST Add shipment", groups = "regression")
    @Description("404 - Send request with non-existing shipmentId and valid access token")
    @Story("Add to my shipments")
    @TmsLink("RC-259")
    public void RC_259_add_shipment_with_non_existing_shipmendId_and_valid_access_token(){
        accessTokenSteps.given_create_access_token(loginCode);
        String accessToken = accessTokenSteps.then_get_the_returned_access_token();
        addSavedShipmentSteps.given_post_add_saved_shipment_with_authorization(accessToken,NON_EXISTING_VALUE)
                .then_verify_status_code(HttpStatus.SC_NOT_FOUND)
                .then_verify_response_body_when_saved_shipment_not_found();
    }

    @Test(description = "API: POST Add shipment", groups = "regression")
    @Description("401 - Send request with valid shipmentId but do not send access token")
    @Story("Add to my shipments")
    @TmsLink("RC-260")
    public void RC_260_add_shipment_with_valid_shipmendId_without_access_token() {
        then_verify_shipment_is_stored_to_receiver_database();
        addSavedShipmentSteps.given_post_add_saved_shipment_without_authorization(shipmentId)
                .then_verify_status_code(HttpStatus.SC_UNAUTHORIZED)
                .then_verify_returned_unauthorized();
    }

    @Test(description = "API: POST Add shipment", groups = "regression")
    @Description("401 - Send request with valid shipmentId and non-existing access token")
    @Story("Add to my shipments")
    @TmsLink("RC-261")
    public void RC_261_add_shipment_with_valid_shipmendId_and_non_existing_access_token() {
        then_verify_shipment_is_stored_to_receiver_database();
        addSavedShipmentSteps.given_post_add_saved_shipment_with_authorization(NON_EXISTING_VALUE,shipmentId)
                .then_verify_status_code(HttpStatus.SC_UNAUTHORIZED)
                .then_verify_returned_unauthorized();
    }

    @Test(description = "API: POST Add shipment", groups = "regression")
    @Description("401 - Send request with valid shipmentId and expired access token")
    @Story("Add to my shipments")
    @TmsLink("RC-262")
    public void RC_262_add_shipment_with_valid_shipmendId_and_expired_access_token() {
        addSavedShipmentSteps.given_post_add_saved_shipment_with_authorization(EXPIRED_ACCESS_TOKEN,shipmentId)
                .then_verify_status_code(HttpStatus.SC_UNAUTHORIZED)
                .then_verify_returned_unauthorized();
    }

    @Test(description = "API: POST Add shipment", groups = "regression")
    @Description("401 - Send request with invalid both of shipmentId and access token")
    @Story("Add to my shipments")
    @TmsLink("RC-263")
    public void RC_263_add_shipment_with_invalid_shipmendId_and_access_token() {
        addSavedShipmentSteps.given_post_add_saved_shipment_with_authorization(NON_EXISTING_VALUE,NON_EXISTING_VALUE)
                .then_verify_status_code(HttpStatus.SC_UNAUTHORIZED)
                .then_verify_returned_unauthorized();
    }

    @Test(description = "API: POST Add shipment", groups = "regression")
    @Description("201 - Send request with the saved shipmentId of another user")
    @Story("Add to my shipments")
    @TmsLink("RC-269")
    public void RC_269_add_shipment_with_the_saved_shipmendId_of_another_user() {
        accessTokenSteps.given_create_access_token(loginCode);
        String accessToken = accessTokenSteps.then_get_the_returned_access_token();
        then_verify_shipment_is_stored_to_receiver_database();
        addSavedShipmentSteps.given_post_add_saved_shipment_with_authorization(accessToken,shipmentId)
                .then_verify_status_code(HttpStatus.SC_CREATED)
                .then_verify_response_body_add_saved_shipment_successfully(PHONE_NUMBER, shipmentId);
        prepareCodeToCheckAuthWithPhone(PHONE_NUMBER2);
        accessTokenSteps.given_create_access_token(loginCodeByPhone);
        String accessToken2 = accessTokenSteps.then_get_the_returned_access_token();
        addSavedShipmentSteps.given_post_add_saved_shipment_with_authorization(accessToken2,shipmentId)
                .then_verify_status_code(HttpStatus.SC_CREATED)
                .then_verify_response_body_add_saved_shipment_successfully(PHONE_NUMBER2, shipmentId);
    }
}