package my.logistika.tests.Receiver.auth;

import io.qameta.allure.Description;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import my.logistika.microservices.receiver.auth.steps.*;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import static my.logistika.microservices.receiver.auth.models.Constants.*;

public class PostRefreshTokenTests extends AuthScriptBaseTests {
    private AccessTokenSteps accessTokenSteps = new AccessTokenSteps();
    private RefreshTokenSteps refreshTokenSteps = new RefreshTokenSteps();

    @Test(description = "API: POST refresh token", groups = "regression")
    @Description("201 - Refresh token succesfully")
    @Story("Login with Setel Connect Web form")
    @TmsLink("RC-126")
    public void RC_126_refresh_token_successfully(){
        accessTokenSteps.given_create_access_token(loginCode);
        String refreshToken = accessTokenSteps.then_get_the_returned_refresh_token();
        refreshTokenSteps.given_create_refresh_token(refreshToken)
                         .then_verify_status_code(HttpStatus.SC_CREATED)
                         .then_verify_get_refresh_token_successfully();
    }

    @Test(description = "API: POST refresh token", groups = "regression")
    @Description("400 - Send request with refreshToken is empty or null")
    @Story("Login with Setel Connect Web form")
    @TmsLink("RC-127")
    public void RC_127_refresh_token_with_value_is_empty_or_null(){
        refreshTokenSteps.given_create_refresh_token("")
                        .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                        .then_verify_response_body_when_refresh_token_is_empty();

        refreshTokenSteps.given_create_refresh_token(null)
                        .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                        .then_verify_response_body_when_refresh_token_is_null();
    }

    @Test(description = "API: POST refresh token", groups = "regression")
    @Description("400 - Send request without refreshToken")
    @Story("Login with Setel Connect Web form")
    @TmsLink("RC-128")
    public void RC_128_refresh_token_without_value(){
        refreshTokenSteps.given_create_refresh_token_with_empty_body()
                        .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                        .then_verify_response_body_when_refresh_token_is_null();
    }

    @Test(description = "API: POST refresh token", groups = "regression")
    @Description("500 - Send request with non-existing refreshToken")
    @Story("Login with Setel Connect Web form")
    @TmsLink("RC-129")
    public void RC_129_refresh_token_with_non_existing_value(){
        refreshTokenSteps.given_create_refresh_token(NON_EXISTING_VALUE)
                         .then_verify_status_code(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                         .then_verify_returned_internal_server_error();
    }

    @Test(description = "API: POST refresh token", groups = "regression")
    @Description("500 - Send request with expired refreshToken")
    @Story("Login with Setel Connect Web form")
    @TmsLink("RC-130")
    public void RC_130_refresh_token_with_expired_value(){
        refreshTokenSteps.given_create_refresh_token(EXPIRED_REFRESH_TOKEN)
                         .then_verify_status_code(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                         .then_verify_returned_internal_server_error();
    }

    @Test(description = "API: POST refresh token", groups = "regression")
    @Description("500 - Send request with refreshToken = accessToken")
    @Story("Login with Setel Connect Web form")
    @TmsLink("RC-131")
    public void RC_131_refresh_token_with_access_token(){
        accessTokenSteps.given_create_access_token(loginCode);
        String accessToken = accessTokenSteps.then_get_the_returned_access_token();
        refreshTokenSteps.given_create_refresh_token(accessToken)
                         .then_verify_status_code(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                         .then_verify_returned_internal_server_error();
    }

    @Test(description = "API: POST refresh token", groups = "regression")
    @Description("400 - Send request with invalid type of refreshToken")
    @Story("Login with Setel Connect Web form")
    @TmsLink("RC-132")
    public void RC_132_refresh_token_with_invalid_type_of_value(){
        refreshTokenSteps.given_create_refresh_token_with_type_of_value_is_integer(123)
                         .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                         .then_verify_response_body_when_enter_invalid_type_of_refresh_token();

        refreshTokenSteps.given_create_refresh_token_with_type_of_value_is_boolean(true)
                         .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                         .then_verify_response_body_when_enter_invalid_type_of_refresh_token();
    }
    
}
