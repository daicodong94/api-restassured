package my.logistika.tests.Receiver.shipments;

import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.consignment.createShipment.models.CreateShipment;
import my.logistika.microservices.consignment.createShipment.models.CreateShipmentDTO;
import my.logistika.microservices.consignment.createShipment.models.ShipmentResponse;
import my.logistika.microservices.consignment.createShipment.steps.CreateShipmentSteps;
import my.logistika.microservices.receiver.shipments.steps.ShipmentDetailSteps;
import my.logistika.microservices.receiver.shipments.steps.ShipmentHistorySteps;
import my.logistika.tests.Receiver.auth.AuthScriptBaseTests;
import my.setel.utils.CommonUtils;
import my.setel.utils.LoggerUtils;
import org.apache.http.HttpStatus;
import org.assertj.core.api.Assertions;
import org.testng.annotations.BeforeMethod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static my.logistika.constants.APIFieldName.AUTHORIZATION_TYPE;
import static my.logistika.constants.APIFieldName.Header.HEADER_AUTHORIZATION;
import static my.logistika.microservices.receiver.shipments.models.Constants.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.fail;
import static org.awaitility.Awaitility.await;

public class ShipmentScriptBase extends AuthScriptBaseTests {
    private CreateShipmentSteps createShipmentSteps = new CreateShipmentSteps();
    private ShipmentResponse shipmentResponse;
    private ShipmentHistorySteps shipmentHistorySteps = new ShipmentHistorySteps();
    private ShipmentDetailSteps shipmentDetailSteps = new ShipmentDetailSteps();

    protected String shipmentId;
    protected String trackingToken;
    public static String merchantReferenceId;

    @BeforeMethod(alwaysRun = true)
    public void prepareNewShipment() {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        merchantReferenceId = CommonUtils.getRandomFiveCharsString();
        CreateShipmentDTO createShipmentDTO = new CreateShipmentDTO(
                merchantReferenceId,
                noOfParcels,
                parcelWeight,
                parcelLength,
                parcelWidth,
                parcelHeight,
                parcelSize,
                receiverName,
                receiverPhoneNumber,
                receiverAddress1,
                receiverAddress2,
                receiverPostalCode,
                receiverCity,
                receiverState,
                instructions,
                isReceiverOfficeAddress,
                receiverAddress,
                receiverEmail,
                merchant,
                merchantPhoneNumber,
                merchantUserIdReferenceId,
                initialPickupType,
                senderAddress1,
                senderAddress2,
                senderCountry,
                senderCity,
                senderState,
                senderPostalCode,
                senderPhoneNumber,
                workflowStatus,
                merchantUserId);
        List<CreateShipmentDTO> list = new ArrayList<>();
        list.add(createShipmentDTO);
        CreateShipment createShipmentRequest = new CreateShipment(list);
        shipmentResponse = (ShipmentResponse) createShipmentSteps.when_a_POST_request_is_made_to_shipments_endpoint(createShipmentRequest)
                .validateResponse(201)
                .saveResponseObject(ShipmentResponse.class);
        Map<String, String> expectedMap = new HashMap<>();
        expectedMap.put("workflowStatusForUser", "Pending Pickup");
        expectedMap.put("merchantReferenceId", merchantReferenceId);
        createShipmentSteps.then_shipment_must_be_created(expectedMap, shipmentResponse.getShipments().get(0));
        this.shipmentId = shipmentResponse.getShipments().get(0).getShipmentId();
        this.trackingToken = shipmentResponse.getShipments().get(0).getTrackingToken();
    }

    @Step("Verify shipment is stored to receiver database")
    public void then_verify_shipment_is_stored_to_receiver_database() {
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", TYPE_SHIPMENT_ID);
        qMap.put("value", shipmentId);
        try {
            await().
                    atMost(120, TimeUnit.SECONDS).
                    with().pollDelay(1, TimeUnit.SECONDS).pollInSameThread().
                    pollInterval(2, TimeUnit.SECONDS).
                    untilAsserted(() -> {
                        LoggerUtils.getInstance().info("executing.......");
                        Assertions.assertThat(shipmentDetailSteps.given_get_shipment_details_without_authorization(qMap)
                                .getResponse().getStatusCode()).isEqualTo(HttpStatus.SC_OK);
                    });
        } catch (org.awaitility.core.ConditionTimeoutException e) {
            fail(e.getMessage());
        }
    }
}