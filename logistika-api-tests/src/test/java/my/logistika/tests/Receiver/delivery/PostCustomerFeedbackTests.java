package my.logistika.tests.Receiver.delivery;

import com.sun.codemodel.JVar;
import groovy.util.Eval;
import io.qameta.allure.Description;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import my.logistika.microservices.receiver.delivery.models.CustomerFeedbackResponse;
import my.logistika.microservices.receiver.delivery.steps.DeliveryCustomerFeedbackSteps;
import my.setel.utils.Logger;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import my.setel.utils.LoggerUtils;

import java.util.HashMap;
import java.util.Map;

import static my.logistika.constants.APIErrorMessage.Delivery.*;
import static my.logistika.constants.APIErrorMessage.Error.*;
import static my.logistika.microservices.receiver.delivery.models.Constants.*;
import static org.jboss.netty.handler.codec.http.HttpHeaders.setHeader;

public class PostCustomerFeedbackTests extends DeliveryScriptBaseTests {
    private DeliveryCustomerFeedbackSteps deliveryCustomerFeedbackSteps = new DeliveryCustomerFeedbackSteps();
    private DeliveryScriptBaseTests deliveryScriptBaseTests = new DeliveryScriptBaseTests();


    @Test(description = "API: POST Customer feedback", groups = "regression")
    @Description("400 - Send same feedback with valid tracking token with shipment status completed and valid access token ")
    @Story("Login with Setel Connect Web form")
    @TmsLink("CR-143")
    public void CR_143_Send_the_same_feedback_with_valid_trackingtoken_shipment_status_completed(){
        String trackingtokennew = trackingToken;
        deliveryCustomerFeedbackSteps.add_customer_feedback(trackingtokennew, 10, MESSAGE)
                .then_verify_status_code(HttpStatus.SC_CREATED)
                .then_verify_message_update_feedback_successfully()
                .then_verify_statusCode_on_Response(STATUS_CODE_201);

        CustomerFeedbackResponse customerFeedbackResponse = (CustomerFeedbackResponse) deliveryCustomerFeedbackSteps.add_customer_feedback(trackingtokennew, 10, MESSAGE)
                .then_verify_error(BAD_REQUEST)
                .then_verify_error_message(BAD_REQUEST)
                .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                .then_verify_statusCode_on_Response(STATUS_CODE_400)
                .then_verify_message_update_feedback_unsuccessfully()
                .saveResponseObject(CustomerFeedbackResponse.class);
        String message = customerFeedbackResponse.getMessage();
        LoggerUtils.getInstance().info("message is " + message);
    }
    @Test(description = "API: POST Customer feedback", groups = "regression")
    @Description("400 - Send feedback with the same valid tracking token")
    @Story("Login with Setel Connect Web form")
    @TmsLink("CR-148")
    public void CR_148_Send_feedback_with_the_same_valid_tracking_token(){
        String trackingtokenNew = trackingToken;
        Map<String, Object> body = new HashMap<>();
        body.put("trackingToken", trackingtokenNew);
        body.put("rating", 10);
        body.put("feedback", "tesst");
        deliveryCustomerFeedbackSteps.add_customer_feedback_with_req_body(body)
                .then_verify_status_code(HttpStatus.SC_CREATED)
                .then_verify_message_update_feedback_successfully()
                .then_verify_statusCode_on_Response(STATUS_CODE_201);

        Map<String, Object> body1 = new HashMap<>();
        body1.put("trackingToken", trackingtokenNew);
        body1.put("rating", 10);
        body1.put("feedback", "abcdef");
        deliveryCustomerFeedbackSteps.add_customer_feedback_with_req_body(body1)
                .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                .then_verify_error(BAD_REQUEST)
                .then_verify_message_update_feedback_unsuccessfully()
                .then_verify_statusCode_on_Response(STATUS_CODE_400);
    }
    @Test(description = "API: POST Customer feedback", groups = "regression")
    @Description("400 - Send feedback with invalid value of rate")
    @Story("Login with Setel Connect Web form")
    @TmsLink("CR-149")
    public void CR_149_Send_feedback_with_invalid_value_of_rate(){
        deliveryCustomerFeedbackSteps.add_customer_feedback(trackingToken, 11, MESSAGE)
                .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                .then_verify_response_body_when_send_feedback_with_value_of_rate_greater_than_10();

        deliveryCustomerFeedbackSteps.add_customer_feedback(trackingToken, -1, MESSAGE)
                .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                .then_verify_response_body_when_send_feedback_with_value_of_rate_less_than_0();
    }
    @Test(description = "API: POST Customer feedback", groups = "regression")
    @Description("400 - Send feedback with invalid value of tracking token")
    @Story("Login with Setel Connect Web form")
    @TmsLink("CR-150")
    public void CR_150_Send_feedback_with_invalid_tracking_of_token(){
        deliveryCustomerFeedbackSteps.add_customer_feedback( "null", 10, MESSAGE)
                .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                .then_verify_response_body_when_send_feedback_with_invalid_value_of_tracking_token();

        deliveryCustomerFeedbackSteps.add_customer_feedback(null, 10, MESSAGE)
                .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                .then_verify_statusCode_on_Response(STATUS_CODE_400)
                .then_verify_error(BAD_REQUEST)
                .then_verify_message_on_response(REQUIRE_TRACKING_TOKEN);

        Map<String, Object> body = new HashMap<>();
        body.put("rating", 10);
        body.put("feedback", MESSAGE);
        deliveryCustomerFeedbackSteps.add_customer_feedback_with_req_body(body)
                .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                .then_verify_statusCode_on_Response(STATUS_CODE_400)
                .then_verify_error(BAD_REQUEST)
                .then_verify_message_on_response(REQUIRE_TRACKING_TOKEN);
    }
    @Test(description = "API: POST Customer feedback", groups = "regression")
    @Description("400 - Send feedback with valid trackingtoken not included Completed status")
    @Story("Login with Setel Connect Web form")
    @TmsLink("CR-151")
    public void CR_151_Send_feedback_with_valid_trackingtoken_not_included_Completed(){
        deliveryCustomerFeedbackSteps.add_customer_feedback(trackingToken_Status_Intransit, 10, MESSAGE)
                .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                .then_verify_error(BAD_REQUEST)
                .then_verify_error_message(INVALID_TRACKING_TOKEN)
                .then_verify_statusCode_on_Response(STATUS_CODE_400);
    }
    @Test(description = "API: POST Customer feedback", groups = "regression")
    @Description("201 - Send feedback with valid trackingtoken with shipment status Completed and dont send access token")
    @Story("Login with Setel Connect Web form")
    @TmsLink("CR-152")
    public void CR_152_Send_feednack_with_valid_trackingtoken_with_shipment_Status_completed_and_non_access_token(){
        deliveryCustomerFeedbackSteps.add_customer_feedback(trackingToken, 10, MESSAGE)
                .then_verify_status_code(HttpStatus.SC_CREATED)
                .then_verify_message_update_feedback_successfully()
                .then_verify_statusCode_on_Response(STATUS_CODE_201);
    }
    @Test(description = "API: POST Customer feedback", groups = "regression")
    @Description("201 - Send feedback with valid trackingtoken with shipment status Completed and invalid value of feedback")
    @Story("Login with Setel Connect Web form")
    @TmsLink("CR-153")
    public void CR_153_Send_feedback_with_valid_trackingtoken_with_shipment_status_completed_and_invalid_value_of_feedback() {
        deliveryCustomerFeedbackSteps.add_customer_feedback(trackingToken, 10, "null")
                .then_verify_status_code(HttpStatus.SC_CREATED)
                .then_verify_message_update_feedback_successfully()
                .then_verify_statusCode_on_Response(STATUS_CODE_201);
    }
    @Test(description = "API: POST Customer feedback", groups = "regression")
    @Description("201 - Send feedback with valid trackingtoken with shipment status Completed and invalid value of feedback")
    @Story("Login with Setel Connect Web form")
    @TmsLink("CR-153")
    public void   CR_153_Send_feedback_with_valid_trackingtoken_with_shipment_status_completed_and_invalid_value_of_feedback_empty() {
        deliveryCustomerFeedbackSteps.add_customer_feedback(trackingToken, 10, "")
                .then_verify_status_code(HttpStatus.SC_CREATED)
                .then_verify_message_update_feedback_successfully()
                .then_verify_statusCode_on_Response(STATUS_CODE_201);
    }
    @Test(description = "API: POST Customer feedback", groups = "regression")
    @Description("201 - Send feedback with valid trackingtoken with shipment status Completed and invalid value of feedback")
    @Story("Login with Setel Connect Web form")
    @TmsLink("CR-153")
    public void   CR_153_Send_feedback_with_valid_trackingtoken_with_shipment_status_completed_and_non_feedback(){
        Map<String, Object> body = new HashMap<>();
        body.put("trackingToken", trackingToken);
        body.put("rating", 10);
        deliveryCustomerFeedbackSteps.add_customer_feedback_with_req_body(body)
                .then_verify_status_code(HttpStatus.SC_CREATED)
                .then_verify_message_update_feedback_successfully()
                .then_verify_statusCode_on_Response(STATUS_CODE_201);
    }
    @Test(description = "API: POST Customer feedback", groups = "regression")
    @Description("201 - Send feedback with valid trackingtoken with shipment status Completed and valid access token")
    @Story("Login with Setel Connect Web form")
    @TmsLink("CR-154")
    public void   CR_154_Send_feedback_with_valid_trackingtoken_with_shipment_status_completed_and_valid_access_token() {
        deliveryCustomerFeedbackSteps.add_customer_feedback(trackingToken, 10, MESSAGE)
                .then_verify_status_code(HttpStatus.SC_CREATED)
                .then_verify_message_update_feedback_successfully()
                .then_verify_statusCode_on_Response(STATUS_CODE_201);
    }
}