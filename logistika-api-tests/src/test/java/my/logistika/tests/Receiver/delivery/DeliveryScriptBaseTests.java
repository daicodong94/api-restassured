package my.logistika.tests.Receiver.delivery;

import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.consignment.constants.ConsignmentApiShipmentStatus;
import my.logistika.microservices.consignment.constants.ConsignmentApiWorkFlowStatus;
import my.logistika.microservices.consignment.createShipment.models.CreateShipment;
import my.logistika.microservices.consignment.createShipment.models.CreateShipmentDTO;
import my.logistika.microservices.consignment.createShipment.models.CreateShipmentResponse;
import my.logistika.microservices.consignment.createShipment.models.ShipmentResponse;
import my.logistika.microservices.consignment.createShipment.steps.CreateShipmentSteps;
import my.logistika.microservices.consignment.getShipmentDetails.steps.GetShipmentByIdSteps;
import my.logistika.microservices.consignment.reverseShipment.steps.CreateReverseShipmentSteps;
import my.logistika.microservices.delivery.approveReturn.steps.ApproveReturnSteps;
import my.logistika.microservices.delivery.constants.PickupTypes;
import my.logistika.microservices.delivery.constants.ShipmentActions;
import my.logistika.microservices.delivery.constants.WorkFlowStatus;
import my.logistika.microservices.delivery.constants.WorkFlowStatusForUser;
import my.logistika.microservices.delivery.failedDelivery.steps.FailedDeliverySteps;
import my.logistika.microservices.delivery.getShipmentDetails.steps.GetShipmentDetailsByIdSteps;
import my.logistika.microservices.delivery.scanning.steps.UpdateShipmentStatusScanSteps;
import my.logistika.microservices.delivery.updateStatus.steps.UpdateStatusSteps;
import my.logistika.microservices.receiver.auth.steps.PrepareLoginCodeSteps;
import my.setel.utils.CommonUtils;
import my.setel.utils.LoggerUtils;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeMethod;
import my.logistika.microservices.receiver.delivery.steps.DeliveryCustomerFeedbackSteps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static my.logistika.microservices.receiver.auth.models.Constants.PHONE_NUMBER;
import static my.logistika.microservices.receiver.shipments.models.Constants.*;
import static my.logistika.microservices.receiver.shipments.models.Constants.merchantUserId;
import static my.setel.utils.CommonUtils.getRandomNumber;

public class DeliveryScriptBaseTests extends BaseTest {
    protected PrepareLoginCodeSteps prepareLoginCodeSteps = new PrepareLoginCodeSteps();
    protected String loginCode;
    protected String loginPhone;
    protected String accessToken;
    protected DeliveryCustomerFeedbackSteps deliveryCustomerFeedbackSteps = new DeliveryCustomerFeedbackSteps();
//    protected CreateShipmentSteps createShipmentSteps = new CreateShipmentSteps();
//    protected UpdateStatusSteps updateStatusSteps = new UpdateStatusSteps();
    protected ShipmentResponse shipmentResponse;
//    protected String shipmentId;
    protected String trackingToken;
    protected String loginAccesstoken;
    protected String trackingToken_Status_Intransit;
//    protected String id;
    public static String merchantReferenceId;
//    public static String workflowStatus;

    protected UpdateStatusSteps updateStatusSteps = new UpdateStatusSteps();
    protected ApproveReturnSteps approveReturnSteps = new ApproveReturnSteps();
    protected FailedDeliverySteps failedDeliverySteps = new FailedDeliverySteps();
    protected CreateShipmentSteps createShipmentSteps = new CreateShipmentSteps();
    protected GetShipmentByIdSteps getShipmentByIdSteps = new GetShipmentByIdSteps();
    protected UpdateShipmentStatusScanSteps scanSteps = new UpdateShipmentStatusScanSteps();
    protected GetShipmentDetailsByIdSteps getShipmentDetailsByIdSteps = new GetShipmentDetailsByIdSteps();
    protected CreateReverseShipmentSteps createReverseShipmentSteps = new CreateReverseShipmentSteps();


    @BeforeMethod()
    public DeliveryScriptBaseTests CreateNewShipmentwith_ShipmentStatus_Completed() throws InterruptedException {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "60000", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        CreateShipmentResponse createShipmentResponse = shipmentResponse.getShipments().get(0);
        String shipmentId = createShipmentResponse.getShipmentId();
        String id = createShipmentResponse.getId();
        this.trackingToken = createShipmentResponse.getTrackingToken();
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);
        LoggerUtils.getInstance().info("Shipment created with tracking Token " + trackingToken);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        Map<String, String> updates = new HashMap<>();
        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, updates);
        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, updates);
        scanSteps.doScan(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId, WorkFlowStatus.OUT_FOR_DELIVERY);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, updates);
        updateStatusSteps.when_a_POST_request_is_made_to_update_status_endpoint(ApiUserRegistry.OPS_SUPPORT.username, shipmentId, ShipmentActions.delivered.name(), shipmentId)
                .validateResponse(201);
        Map<String, String> updatesMap = new HashMap<>();
        updatesMap.put("workflowStatus", WorkFlowStatus.DELIVERED_TO_CUSTOMER);
        updatesMap.put("workflowStatusForUser", WorkFlowStatusForUser.COMPLETED);
        updatesMap.put("currentOwner", ApiUserRegistry.CUSTOMER_GLOBAL.userId);
        updateStatusSteps.then_workflow_status_must_be_updated_as_expected(updatesMap);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_CUSTOMER);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.COMPLETED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, updates);
        return this;
    }
    @BeforeMethod()
    public DeliveryScriptBaseTests create_shipment_with_status_Intransit() throws InterruptedException {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "60000", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        CreateShipmentResponse createShipmentResponse = shipmentResponse.getShipments().get(0);
        String shipmentId = createShipmentResponse.getShipmentId();
        String id = createShipmentResponse.getId();
        this.trackingToken_Status_Intransit = createShipmentResponse.getTrackingToken();
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        Map<String, String> updates = new HashMap<>();
        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, updates);
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);
        LoggerUtils.getInstance().info("Shipment created with tracking Token " + trackingToken_Status_Intransit);
        return this;
    }
    @BeforeMethod()
    public DeliveryScriptBaseTests prepareCodeToCheckAuth() {
        this.loginPhone = PHONE_NUMBER;
        prepareLoginCodeSteps.given_send_otp(loginPhone)
                .then_verify_status_code(HttpStatus.SC_CREATED);
        String pin = getRandomNumber(6);
        prepareLoginCodeSteps.then_verify_otp(loginPhone, pin)
                .then_verify_status_code(HttpStatus.SC_CREATED)
                .then_verify_get_otp_successfully();
        String phoneToken = prepareLoginCodeSteps.then_get_the_returned_phoneToken();
        String otpToken = prepareLoginCodeSteps.then_get_the_returned_otpToken();

        prepareLoginCodeSteps.given_drop_in(phoneToken, otpToken, loginPhone)
                .then_verify_status_code(HttpStatus.SC_CREATED);

        String loginAccessToken = prepareLoginCodeSteps.then_get_the_returned_login_access_token();
        this.loginAccesstoken = prepareLoginCodeSteps.then_get_the_returned_login_access_token();
        prepareLoginCodeSteps.given_authorize(loginAccessToken)
                .then_verify_status_code(HttpStatus.SC_CREATED);
        this.loginCode = prepareLoginCodeSteps.then_get_the_returned_code();
        deliveryCustomerFeedbackSteps.create_access_token(loginCode)
                .then_verify_status_code(HttpStatus.SC_CREATED);
        this.accessToken = deliveryCustomerFeedbackSteps.get_access_token();
        return this;
    }
}
