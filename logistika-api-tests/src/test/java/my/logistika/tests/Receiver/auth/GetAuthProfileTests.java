package my.logistika.tests.Receiver.auth;

import io.qameta.allure.Description;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import my.logistika.microservices.receiver.auth.steps.AccessTokenSteps;
import my.logistika.microservices.receiver.auth.steps.AuthProfileSteps;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import static my.logistika.microservices.receiver.auth.models.Constants.*;

public class GetAuthProfileTests extends AuthScriptBaseTests {
    private AccessTokenSteps accessTokenSteps = new AccessTokenSteps();
    private AuthProfileSteps authProfileSteps = new AuthProfileSteps();

    @Test(description = "API: GET receiver users", groups = "regression")
    @Description("200 - Fetch user data successfully")
    @Story("Login with Setel Connect Web form")
    @TmsLink("RC-134")
    public void RC_134_fetch_user_data_successfully() {
        accessTokenSteps.given_create_access_token(loginCode);
        String accessToken = accessTokenSteps.then_get_the_returned_access_token();
        authProfileSteps.given_get_auth_profile_with_authorization(accessToken)
                        .then_verify_status_code(HttpStatus.SC_OK)
                        .then_verify_get_access_token_successfully(loginPhone);
    }

    @Test(description = "API: GET receiver users", groups = "regression")
    @Description("401 - Fetch user data without access token")
    @Story("Login with Setel Connect Web form")
    @TmsLink("RC-135")
    public void RC_135_fetch_user_without_access_token() {
        authProfileSteps.given_get_auth_profile_without_authorization()
                        .then_verify_status_code(HttpStatus.SC_UNAUTHORIZED)
                        .then_verify_returned_unauthorized();
    }

    @Test(description = "API: GET receiver users", groups = "regression")
    @Description("401 - Fetch user data with non-existing access token")
    @Story("Login with Setel Connect Web form")
    @TmsLink("RC-136")
    public void RC_136_fetch_user_with_non_existing_access_token() {
        authProfileSteps.given_get_auth_profile_with_authorization(NON_EXISTING_VALUE)
                        .then_verify_status_code(HttpStatus.SC_UNAUTHORIZED)
                        .then_verify_returned_unauthorized();
    }

    @Test(description = "API: GET receiver users", groups = "regression")
    @Description("401 - Fetch user data with non-existing access token")
    @Story("Login with Setel Connect Web form")
    @TmsLink("RC-137")
    public void RC_137_fetch_user_with_non_expired_access_token() {
        authProfileSteps.given_get_auth_profile_with_authorization(EXPIRED_ACCESS_TOKEN)
                        .then_verify_status_code(HttpStatus.SC_UNAUTHORIZED)
                        .then_verify_returned_unauthorized();
    }

    @Test(description = "API: GET receiver users", groups = "regression")
    @Description("401 - Fetch user data with non-existing access token")
    @Story("Login with Setel Connect Web form")
    @TmsLink("RC-138")
    public void RC_138_fetch_user_with_refreshToken_token() {
        accessTokenSteps.given_create_access_token(loginCode);
        String refreshToken = accessTokenSteps.then_get_the_returned_refresh_token();
        authProfileSteps.given_get_auth_profile_with_authorization(refreshToken)
                        .then_verify_status_code(HttpStatus.SC_UNAUTHORIZED)
                        .then_verify_returned_unauthorized();
    }
}
