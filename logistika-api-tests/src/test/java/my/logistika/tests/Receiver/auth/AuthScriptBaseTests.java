package my.logistika.tests.Receiver.auth;

import my.logistika.base.BaseTest;
import my.logistika.microservices.receiver.auth.steps.PrepareLoginCodeSteps;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeMethod;

import static my.logistika.microservices.receiver.auth.models.Constants.*;
import static my.setel.utils.CommonUtils.getRandomNumber;

public class AuthScriptBaseTests extends BaseTest {
    protected PrepareLoginCodeSteps prepareLoginCodeSteps = new PrepareLoginCodeSteps();
    protected String loginCode;
    protected String loginPhone;
    protected String loginCodeByPhone;

    @BeforeMethod(alwaysRun = true)
    public AuthScriptBaseTests prepareCodeToCheckAuth() {
        this.loginPhone = PHONE_NUMBER;
        prepareLoginCodeSteps.given_send_otp(loginPhone)
                             .then_verify_status_code(HttpStatus.SC_CREATED);
        String pin = getRandomNumber(6);
        prepareLoginCodeSteps.then_verify_otp(loginPhone, pin)
                             .then_verify_status_code(HttpStatus.SC_CREATED)
                             .then_verify_get_otp_successfully();
        String phoneToken = prepareLoginCodeSteps.then_get_the_returned_phoneToken();
        String otpToken = prepareLoginCodeSteps.then_get_the_returned_otpToken();

        prepareLoginCodeSteps.given_drop_in(phoneToken, otpToken, loginPhone)
                             .then_verify_status_code(HttpStatus.SC_CREATED);

        String loginAccessToken = prepareLoginCodeSteps.then_get_the_returned_login_access_token();
        prepareLoginCodeSteps.given_authorize(loginAccessToken)
                             .then_verify_status_code(HttpStatus.SC_CREATED);
        this.loginCode = prepareLoginCodeSteps.then_get_the_returned_code();
        return this;
    }

    public AuthScriptBaseTests prepareCodeToCheckAuthWithPhone(String phoneNumber) {
        prepareLoginCodeSteps.given_send_otp(phoneNumber)
                .then_verify_status_code(HttpStatus.SC_CREATED);
        String pin = getRandomNumber(6);
        prepareLoginCodeSteps.then_verify_otp(phoneNumber, pin)
                .then_verify_status_code(HttpStatus.SC_CREATED)
                .then_verify_get_otp_successfully();
        String phoneToken = prepareLoginCodeSteps.then_get_the_returned_phoneToken();
        String otpToken = prepareLoginCodeSteps.then_get_the_returned_otpToken();

        prepareLoginCodeSteps.given_drop_in(phoneToken, otpToken, phoneNumber)
                .then_verify_status_code(HttpStatus.SC_CREATED);

        String loginAccessToken = prepareLoginCodeSteps.then_get_the_returned_login_access_token();
        prepareLoginCodeSteps.given_authorize(loginAccessToken)
                .then_verify_status_code(HttpStatus.SC_CREATED);
        this.loginCodeByPhone = prepareLoginCodeSteps.then_get_the_returned_code();
        return this;
    }
}
