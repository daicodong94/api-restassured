package my.logistika.tests.Receiver.shipments;

import io.qameta.allure.Description;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import my.logistika.microservices.receiver.shipments.enums.ShipmentHistoryGetFields;
import my.logistika.microservices.receiver.auth.steps.AccessTokenSteps;
import my.logistika.microservices.receiver.shipments.steps.ShipmentHistorySteps;
import my.setel.utils.CommonUtils;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static my.logistika.microservices.receiver.auth.models.Constants.*;
import static my.logistika.microservices.receiver.shipments.models.Constants.*;

public class GetShipmentHistoryTests extends ShipmentScriptBase {
    private ShipmentHistorySteps shipmentHistorySteps = new ShipmentHistorySteps();
    private AccessTokenSteps accessTokenSteps = new AccessTokenSteps();

    @Test(description = "API: GET Shipment History", groups = "regression")
    @Description("200 - Get history with valid shipmentId")
    @Story("Track shipment page with Continue with Setel")
    @TmsLink("RC-140")
    public void RC_140_get_shipment_history_with_valid_shipmentId() {
        then_verify_shipment_is_stored_to_receiver_database();
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", TYPE_SHIPMENT_ID);
        qMap.put("value", shipmentId);
        shipmentHistorySteps.given_get_shipment_history_without_authorization(qMap)
                            .then_verify_status_code(HttpStatus.SC_OK)
                            .then_verify_get_shipment_history_successfully(STATUS_PENDING_PICKUP);
    }

    @Test(description = "API: GET Shipment History", groups = "regression")
    @Description("200 - Get history with valid trackingToken")
    @Story("Track shipment page with Continue with Setel")
    @TmsLink("RC-141")
    public void RC_141_get_shipment_history_with_valid_trackingToken() {
        then_verify_shipment_is_stored_to_receiver_database();
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", TYPE_TRACKING_TOKEN);
        qMap.put("value", trackingToken);
        shipmentHistorySteps.given_get_shipment_history_without_authorization(qMap)
                            .then_verify_status_code(HttpStatus.SC_OK)
                            .then_verify_get_shipment_history_successfully(STATUS_PENDING_PICKUP);
    }

    @Test(description = "API: GET Shipment History", groups = "regression")
    @Description("200 - Get history with valid shipmentId and a selectedField")
    @Story("Track shipment page with Continue with Setel")
    @TmsLink("RC-142")
    public void RC_142_get_shipment_history_with_valid_shipmentId_and_a_selectedField() {
        List<String> selectedFields = ShipmentHistoryGetFields.getRandomValue(1);
        String selectedFieldsParam = ShipmentHistoryGetFields.toString(selectedFields);
        then_verify_shipment_is_stored_to_receiver_database();
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", TYPE_SHIPMENT_ID);
        qMap.put("value", shipmentId);
        qMap.put("selectedFields",selectedFieldsParam);
        shipmentHistorySteps.given_get_shipment_history_without_authorization(qMap)
                            .then_verify_status_code(HttpStatus.SC_OK)
                            .then_verify_get_shipment_history_successfully_with_selected_field(selectedFields,shipmentHistorySteps.mapFields());
    }

    @Test(description = "API: GET Shipment History", groups = "regression")
    @Description("200 - Get history with valid trackingToken and multi-selectedFields")
    @Story("Track shipment page with Continue with Setel")
    @TmsLink("RC-143")
    public void RC_143_get_shipment_history_with_valid_trackingToken_and_multi_selectedField() {
        int size = Integer.parseInt(CommonUtils.getRandomNumber(1));
        List<String> selectedFields = ShipmentHistoryGetFields.getRandomValue(size);
        String selectedFieldsParam = ShipmentHistoryGetFields.toString(selectedFields);
        then_verify_shipment_is_stored_to_receiver_database();
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", TYPE_TRACKING_TOKEN);
        qMap.put("value", trackingToken);
        qMap.put("selectedFields",selectedFieldsParam);
        shipmentHistorySteps.given_get_shipment_history_without_authorization(qMap)
                        .then_verify_status_code(HttpStatus.SC_OK)
                        .then_verify_get_shipment_history_successfully_with_selected_field(selectedFields,shipmentHistorySteps.mapFields());
    }

    @Test(description = "API: GET Shipment History", groups = "regression")
    @Description("400 - Get history with valid selectedField only")
    @Story("Track shipment page with Continue with Setel")
    @TmsLink("RC-145")
    public void RC_145_get_shipment_history_with_valid_selectedField_only() {
        int size = Integer.parseInt(CommonUtils.getRandomNumber(1));
        List<String> selectedFields = ShipmentHistoryGetFields.getRandomValue(size);
        String selectedFieldsParam = ShipmentHistoryGetFields.toString(selectedFields);
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("selectedFields",selectedFieldsParam);
        shipmentHistorySteps.given_get_shipment_history_without_authorization(qMap)
                            .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                            .then_verify_response_body_when_do_not_send_type_and_value();
    }

    @Test(description = "API: GET Shipment History", groups = "regression")
    @Description("404 - Get history with invalid shipmentId")
    @Story("Track shipment page with Continue with Setel")
    @TmsLink("RC-146")
    public void RC_146_get_shipment_history_with_invalid_shipmentId() {
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", TYPE_SHIPMENT_ID);
        qMap.put("value", NON_EXISTING_VALUE);
        shipmentHistorySteps.given_get_shipment_history_without_authorization(qMap)
                            .then_verify_status_code(HttpStatus.SC_NOT_FOUND)
                            .then_verify_response_body_is_shipment_not_found();
    }

    @Test(description = "API: GET Shipment History", groups = "regression")
    @Description("404 - Get history with invalid trackingToken")
    @Story("Track shipment page with Continue with Setel")
    @TmsLink("RC-147")
    public void RC_147_get_shipment_history_with_invalid_trackingToken() {
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", TYPE_TRACKING_TOKEN);
        qMap.put("value", NON_EXISTING_VALUE);
        shipmentHistorySteps.given_get_shipment_history_without_authorization(qMap)
                            .then_verify_status_code(HttpStatus.SC_NOT_FOUND)
                            .then_verify_response_body_is_shipment_not_found();
    }

    @Test(description = "API: GET Shipment History", groups = "regression")
    @Description("400 - Get history with invalid value of type field")
    @Story("Track shipment page with Continue with Setel")
    @TmsLink("RC-148")
    public void RC_148_get_shipment_history_with_invalid_value_of_type_field() {
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", "shipment");
        qMap.put("value", shipmentId);
        shipmentHistorySteps.given_get_shipment_history_without_authorization(qMap)
                            .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                            .then_verify_response_body_when_enter_invalid_value_of_type_field();
    }

    @Test(description = "API: GET Shipment History", groups = "regression")
    @Description("400 - Get history without required field")
    @Story("Track shipment page with Continue with Setel")
    @TmsLink("RC-149")
    public void RC_149_get_shipment_history_without_required_field() {
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("value", shipmentId);
        shipmentHistorySteps.given_get_shipment_history_without_authorization(qMap)
                            .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                            .then_verify_response_body_when_do_not_send_type();

        Map<String, Object> qMap2 = new HashMap<>();
        qMap2.put("type", TYPE_SHIPMENT_ID);
        shipmentHistorySteps.given_get_shipment_history_without_authorization(qMap2)
                            .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                            .then_verify_response_body_when_value_is_null();

        Map<String, Object> qMap3 = new HashMap<>();
        shipmentHistorySteps.given_get_shipment_history_without_authorization(qMap3)
                            .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                            .then_verify_response_body_when_do_not_send_type_and_value();
    }
}