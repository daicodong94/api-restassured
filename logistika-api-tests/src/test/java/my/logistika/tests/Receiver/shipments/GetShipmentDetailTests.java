package my.logistika.tests.Receiver.shipments;

import io.qameta.allure.Description;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import my.logistika.microservices.receiver.auth.steps.AccessTokenSteps;
import my.logistika.microservices.receiver.shipments.steps.ShipmentDetailSteps;
import my.setel.utils.LoggerUtils;
import org.apache.http.HttpStatus;
import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static my.logistika.microservices.receiver.auth.models.Constants.*;
import static my.logistika.microservices.receiver.shipments.models.Constants.*;
import static org.assertj.core.api.Fail.fail;
import static org.awaitility.Awaitility.await;

public class GetShipmentDetailTests extends ShipmentScriptBase {
    private ShipmentDetailSteps shipmentDetailSteps = new ShipmentDetailSteps();
    private AccessTokenSteps accessTokenSteps = new AccessTokenSteps();

    @Test(description = "API: GET Shipment Details", groups = "regression")
    @Description("200 - Get details of shipment with valid shipmentId and valid access token")
    @Story("Track shipment page with Continue with Setel")
    @TmsLink("RC-278")
    public void RC_278_get_shipment_details_with_valid_shipmentId_and_access_token() {
        accessTokenSteps.given_create_access_token(loginCode);
        String accessToken = accessTokenSteps.then_get_the_returned_access_token();
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", TYPE_SHIPMENT_ID);
        qMap.put("value", shipmentId);
        try {
            await().
                    atMost(120, TimeUnit.SECONDS).
                    with().pollDelay(1, TimeUnit.SECONDS).pollInSameThread().
                    pollInterval(2, TimeUnit.SECONDS).
                    untilAsserted(() -> {LoggerUtils.getInstance().info("executing.......");
                        Assertions.assertThat(shipmentDetailSteps.given_get_shipment_details_with_authorization(accessToken,qMap)
                                .getResponse().getStatusCode()).isEqualTo(HttpStatus.SC_OK);
                    });
        } catch (org.awaitility.core.ConditionTimeoutException e) {
            fail(e.getMessage());
        }
        shipmentDetailSteps.then_verify_get_shipment_details_successfully(shipmentId, trackingToken, STATUS_PENDING_PICKUP, receiverPhoneNumber);
    }

    @Test(description = "API: GET Shipment Details", groups = "regression")
    @Description("200 - Get details of shipment with valid trackingToken and valid access token")
    @Story("Track shipment page with Continue with Setel")
    @TmsLink("RC-279")
    public void RC_279_get_shipment_details_with_valid_trackingToken_and_access_token() {
        accessTokenSteps.given_create_access_token(loginCode);
        String accessToken = accessTokenSteps.then_get_the_returned_access_token();
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", TYPE_TRACKING_TOKEN);
        qMap.put("value", trackingToken);
        try {
            await().
                    atMost(120, TimeUnit.SECONDS).
                    with().pollDelay(1, TimeUnit.SECONDS).pollInSameThread().
                    pollInterval(2, TimeUnit.SECONDS).
                    untilAsserted(() -> {LoggerUtils.getInstance().info("executing.......");
                        Assertions.assertThat(shipmentDetailSteps.given_get_shipment_details_with_authorization(accessToken,qMap)
                                .getResponse().getStatusCode()).isEqualTo(HttpStatus.SC_OK);
                    });
        } catch (org.awaitility.core.ConditionTimeoutException e) {
            fail(e.getMessage());
        }
        shipmentDetailSteps.then_verify_get_shipment_details_successfully(shipmentId, trackingToken, STATUS_PENDING_PICKUP, receiverPhoneNumber);
    }

    @Test(description = "API: GET Shipment Details", groups = "regression")
    @Description("200 - Get details of shipment with valid trackingToken and do not send access token")
    @Story("Track shipment page with Continue with Setel")
    @TmsLink("RC-285")
    public void RC_285_get_shipment_details_with_valid_trackingToken_and_do_not_send_access_token() {
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", TYPE_TRACKING_TOKEN);
        qMap.put("value", trackingToken);
        try {
            await().
                    atMost(120, TimeUnit.SECONDS).
                    with().pollDelay(1, TimeUnit.SECONDS).pollInSameThread().
                    pollInterval(2, TimeUnit.SECONDS).
                    untilAsserted(() -> {LoggerUtils.getInstance().info("executing.......");
                        Assertions.assertThat(shipmentDetailSteps.given_get_shipment_details_without_authorization(qMap)
                                .getResponse().getStatusCode()).isEqualTo(HttpStatus.SC_OK);
                    });
        } catch (org.awaitility.core.ConditionTimeoutException e) {
            fail(e.getMessage());
        }
        shipmentDetailSteps.then_verify_get_shipment_details_successfully(shipmentId, trackingToken, STATUS_PENDING_PICKUP, receiverPhoneNumber);
    }

    @Test(description = "API: GET Shipment Details", groups = "regression")
    @Description("404 - Get details of shipment with non-existing shipmentId")
    @Story("Track shipment page with Continue with Setel")
    @TmsLink("RC-280")
    public void RC_280_get_shipment_details_with_non_existing_shipmentId() {
        accessTokenSteps.given_create_access_token(loginCode);
        String accessToken = accessTokenSteps.then_get_the_returned_access_token();
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", TYPE_SHIPMENT_ID);
        qMap.put("value", NON_EXISTING_VALUE);
        shipmentDetailSteps.given_get_shipment_details_with_authorization(accessToken, qMap)
                           .then_verify_status_code(HttpStatus.SC_NOT_FOUND)
                           .then_verify_response_body_is_shipment_not_found();
    }

    @Test(description = "API: GET Shipment Details", groups = "regression")
    @Description("404 - Get details of shipment with non-existing trackingToken")
    @Story("Track shipment page with Continue with Setel")
    @TmsLink("RC-281")
    public void RC_281_get_shipment_details_with_non_existing_trackingToken(){
        accessTokenSteps.given_create_access_token(loginCode);
        String accessToken = accessTokenSteps.then_get_the_returned_access_token();
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", TYPE_TRACKING_TOKEN);
        qMap.put("value", NON_EXISTING_VALUE);
        shipmentDetailSteps.given_get_shipment_details_with_authorization(accessToken, qMap)
                           .then_verify_status_code(HttpStatus.SC_NOT_FOUND)
                           .then_verify_response_body_is_shipment_not_found();
    }

    @Test(description = "API: GET Shipment Details", groups = "regression")
    @Description("400 - Get details of shipment without value")
    @Story("Track shipment page with Continue with Setel")
    @TmsLink("RC-282")
    public void RC_282_get_shipment_details_without_value() {
        accessTokenSteps.given_create_access_token(loginCode);
        String accessToken = accessTokenSteps.then_get_the_returned_access_token();
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", TYPE_SHIPMENT_ID);
        qMap.put("value", "");
        shipmentDetailSteps.given_get_shipment_details_with_authorization(accessToken, qMap)
                           .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                           .then_verify_response_body_when_value_is_empty();

        Map<String, Object> qMap2 = new HashMap<>();
        qMap2.put("type", TYPE_SHIPMENT_ID);
        shipmentDetailSteps.given_get_shipment_details_with_authorization(accessToken, qMap2)
                           .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                           .then_verify_response_body_when_value_is_null();
    }

    @Test(description = "API: GET Shipment Details", groups = "regression")
    @Description("400 - Get details of shipment with invalid value of type field")
    @Story("Track shipment page with Continue with Setel")
    @TmsLink("RC-284")
    public void RC_284_get_shipment_details_with_invalid_value_of_type_field(){
        accessTokenSteps.given_create_access_token(loginCode);
        String accessToken = accessTokenSteps.then_get_the_returned_access_token();
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("value", trackingToken);
        shipmentDetailSteps.given_get_shipment_details_with_authorization(accessToken,qMap)
                           .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                           .then_verify_response_body_when_do_not_send_type();

        qMap.put("type", "");
        shipmentDetailSteps.given_get_shipment_details_with_authorization(accessToken,qMap)
                           .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                           .then_verify_response_body_when_do_not_send_type();

        qMap.put("type", "trackingToken123");
        shipmentDetailSteps.given_get_shipment_details_with_authorization(accessToken,qMap)
                           .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                           .then_verify_response_body_when_enter_invalid_value_of_type_field();
    }

    @Test(description = "API: GET Shipment Details", groups = "regression")
    @Description("401 - Get details of shipment with non-existing access token")
    @Story("Track shipment page with Continue with Setel")
    @TmsLink("RC-286")
    public void RC_286_get_shipment_details_with_non_existing_access_token() {
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", TYPE_SHIPMENT_ID);
        qMap.put("value", shipmentId);
        shipmentDetailSteps.given_get_shipment_details_with_authorization(NON_EXISTING_VALUE,qMap)
                           .then_verify_status_code(HttpStatus.SC_UNAUTHORIZED)
                           .then_verify_returned_unauthorized();
    }

    @Test(description = "API: GET Shipment Details", groups = "regression")
    @Description("401 - Get details of shipment with expired access token")
    @Story("Track shipment page with Continue with Setel")
    @TmsLink("RC-287")
    public void RC_287_get_shipment_details_with_expired_access_token() {
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", TYPE_TRACKING_TOKEN);
        qMap.put("value", trackingToken);
        shipmentDetailSteps.given_get_shipment_details_with_authorization(EXPIRED_ACCESS_TOKEN,qMap)
                           .then_verify_status_code(HttpStatus.SC_UNAUTHORIZED)
                           .then_verify_returned_unauthorized();
    }

    @Test(description = "API: GET Shipment Details", groups = "regression")
    @Description("401 - Get details of shipment with invalid both of value and access token")
    @Story("Track shipment page with Continue with Setel")
    @TmsLink("RC-288")
    public void RC_288_get_shipment_details_with_invalid_value_and_access_token() {
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", TYPE_SHIPMENT_ID);
        qMap.put("value", trackingToken);
        shipmentDetailSteps.given_get_shipment_details_with_authorization(NON_EXISTING_VALUE,qMap)
                           .then_verify_status_code(HttpStatus.SC_UNAUTHORIZED)
                           .then_verify_returned_unauthorized();
    }
}