package my.logistika.tests.Receiver.shipments;

import io.qameta.allure.Description;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import my.logistika.microservices.receiver.auth.steps.AccessTokenSteps;
import my.logistika.microservices.receiver.shipments.steps.SavedShipmentSteps;
import my.logistika.tests.Receiver.auth.AuthScriptBaseTests;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static my.logistika.microservices.receiver.auth.models.Constants.*;
import static my.logistika.microservices.receiver.shipments.models.Constants.*;

public class SavedShipmentTests extends AuthScriptBaseTests {
    private SavedShipmentSteps savedShipmentSteps = new SavedShipmentSteps();
    private AccessTokenSteps accessTokenSteps = new AccessTokenSteps();

    @Test(description = "API: GET Saved shipments", groups = "regression")
    @Description("200 - Send request with valid access token, type is shipmentHistory and a valid status")
    @Story("Filter by Shipment Status")
    @TmsLink("RC-434")
    public void RC_434_get_saved_shipments_with_valid_access_token_type_is_shipmentHistory_and_valid_status(){
        accessTokenSteps.given_create_access_token(loginCode);
        String accessToken = accessTokenSteps.then_get_the_returned_access_token();
        String filterStatus = savedShipmentSteps.get_random_status_saved_history();
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", TYPE_SHIPMENT_HISTORY);
        qMap.put("status", filterStatus);
        savedShipmentSteps.given_get_saved_shipment_with_authorization(accessToken,qMap)
                          .then_verify_status_code(HttpStatus.SC_OK)
                          .then_verify_response_body_get_shipment_history_successfully_with_status(filterStatus);
    }

    @Test(description = "API: GET Saved shipments", groups = "regression")
    @Description("200 - Send request with valid access token, type is myShipment and a valid status")
    @Story("Filter by Shipment Status")
    @TmsLink("RC-431")
    public void RC_431_get_saved_shipments_with_valid_access_token_type_is_myShipment_and_valid_status(){
        accessTokenSteps.given_create_access_token(loginCode);
        String accessToken = accessTokenSteps.then_get_the_returned_access_token();
        String filterStatus = savedShipmentSteps.get_random_status();
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", TYPE_MY_SHIPMENT);
        qMap.put("status", filterStatus);
        savedShipmentSteps.given_get_saved_shipment_with_authorization(accessToken,qMap)
                          .then_verify_status_code(HttpStatus.SC_OK)
                          .then_verify_response_body_get_my_shipment_successfully_with_status(filterStatus);
    }

    @Test(description = "API: GET Saved shipments", groups = "regression")
    @Description("200 - Send request with valid all fields")
    @Story("Filter by Shipment Status")
    @TmsLink("RC-313")
    public void RC_313_get_saved_shipments_with_valid_all_fields(){
        accessTokenSteps.given_create_access_token(loginCode);
        String accessToken = accessTokenSteps.then_get_the_returned_access_token();
        Integer limit = 10;
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", TYPE_MY_SHIPMENT);
        qMap.put("limit", limit);
        savedShipmentSteps.given_get_saved_shipment_with_authorization(accessToken,qMap)
                          .then_verify_status_code(HttpStatus.SC_OK)
                          .then_verify_response_body_get_my_shipment_successfully_with_limit(limit);

        Integer offset = savedShipmentSteps.get_the_total_data() - 1;
        qMap.put("offset", offset);
        savedShipmentSteps.given_get_saved_shipment_with_authorization(accessToken,qMap)
                          .then_verify_status_code(HttpStatus.SC_OK)
                          .then_verify_response_body_get_my_shipment_successfully_with_offset_is_total_minus_one(limit,offset);
    }

    @Test(description = "API: GET Saved shipments", groups = "regression")
    @Description("200 - Send request with valid access token, type is myShipment"
    + "200 - Send request with valid access token, type is myShipment and prepared shipments have NOT passed 14 days from Completed/Cancelled/Returned status"
    + "200 - Send request with valid access token, type is myShipment and prepared shipments have passed 14 days from Completed/Cancelled/Returned status"
    + "200 - Send request with valid access token, type is myShipment and prepared shipments have passed 14 days from the latest status, not in Completed/Cancelled/Returned status")
    @Story("Filter by Shipment Status")
    @TmsLink("RC-311")
    @TmsLink("RC-314")
    @TmsLink("RC-315")
    @TmsLink("RC-316")
    public void RC_311_314_315_316_get_saved_shipments_with_access_token_type_is_myShipment(){
        accessTokenSteps.given_create_access_token(loginCode);
        String accessToken = accessTokenSteps.then_get_the_returned_access_token();
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", TYPE_MY_SHIPMENT);
        savedShipmentSteps.given_get_saved_shipment_with_authorization(accessToken,qMap)
                          .then_verify_status_code(HttpStatus.SC_OK)
                          .then_verify_response_body_get_my_shipment_successfully();
    }

    @Test(description = "API: GET Saved shipments", groups = "regression")
    @Description("200 - Send request with valid access token, type is shipmentHistory"
    + "200 - Send request with valid access token, type is shipmentHistory and prepared shipments in Completed/Cancelled/Returned status but not older 14 days"
    + "200 - Send request with valid access token, type is shipmentHistory and prepared shipments have passed 14 days from the latest status, not Completed/Cancelled/Returned status")
    @Story("Filter by Shipment Status")
    @TmsLink("RC-312")
    @TmsLink("RC-317")
    @TmsLink("RC-318")
    public void RC_312_317_318_get_saved_shipments_with_access_token_type_is_shipmentHistory(){
        accessTokenSteps.given_create_access_token(loginCode);
        String accessToken = accessTokenSteps.then_get_the_returned_access_token();
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", TYPE_SHIPMENT_HISTORY);
        savedShipmentSteps.given_get_saved_shipment_with_authorization(accessToken,qMap)
                          .then_verify_status_code(HttpStatus.SC_OK)
                          .then_verify_response_body_get_shipment_history_successfully();
    }

    @Test(description = "API: GET Saved shipments", groups = "regression")
    @Description("400 - Send request with invalid filter status")
    @Story("Filter by Shipment Status")
    @TmsLink("RC-433")
    public void RC_433_get_saved_shipments_with_invalid_filter_status(){
        accessTokenSteps.given_create_access_token(loginCode);
        String accessToken = accessTokenSteps.then_get_the_returned_access_token();
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", TYPE_MY_SHIPMENT);
        qMap.put("status", "Onhold");
        savedShipmentSteps.given_get_saved_shipment_with_authorization(accessToken,qMap)
                          .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                          .then_verify_response_body_when_enter_invalid_status();
    }

    @Test(description = "API: GET Saved shipments", groups = "regression")
    @Description("400 - Send request with invalid value of type field")
    @Story("Filter by Shipment Status")
    @TmsLink("RC-319")
    public void RC_319_get_saved_shipments_with_invalid_value_of_type_field(){
        accessTokenSteps.given_create_access_token(loginCode);
        String accessToken = accessTokenSteps.then_get_the_returned_access_token();
        Map<String, Object> qMap = new HashMap<>();
        savedShipmentSteps.given_get_saved_shipment_with_authorization(accessToken,qMap)
                          .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                          .then_verify_response_body_when_do_not_send_type();

        qMap.put("type", "");
        savedShipmentSteps.given_get_saved_shipment_with_authorization(accessToken,qMap)
                          .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                          .then_verify_response_body_when_do_not_send_type();

        qMap.put("type", "shipmentId");
        savedShipmentSteps.given_get_saved_shipment_with_authorization(accessToken,qMap)
                          .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                          .then_verify_response_body_when_enter_invalid_value_of_type_field();
    }

    @Test(description = "API: GET Saved shipments", groups = "regression")
    @Description("400 - Send request with invalid value of limit and offset")
    @Story("Filter by Shipment Status")
    @TmsLink("RC-320")
    public void RC_320_get_saved_shipments_with_invalid_value_of_limit_and_offset(){
        accessTokenSteps.given_create_access_token(loginCode);
        String accessToken = accessTokenSteps.then_get_the_returned_access_token();
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", "shipmentHistory");
        qMap.put("limit", "wyb85");
        savedShipmentSteps.given_get_saved_shipment_with_authorization(accessToken,qMap)
                          .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                          .then_verify_response_body_when_enter_invalid_limit_type();

        qMap.put("limit", 8);
        qMap.put("offset", true);
        savedShipmentSteps.given_get_saved_shipment_with_authorization(accessToken,qMap)
                          .then_verify_status_code(HttpStatus.SC_BAD_REQUEST)
                          .then_verify_response_body_when_enter_invalid_offset_type();
    }

    @Test(description = "API: GET Saved shipments", groups = "regression")
    @Description("401 - Send request with valid all fields without access token")
    @Story("Filter by Shipment Status")
    @TmsLink("RC-321")
    public void RC_321_get_saved_shipments_with_valid_all_fields_without_access_token(){
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", TYPE_MY_SHIPMENT);
        savedShipmentSteps.given_get_saved_shipment_without_authorization(qMap)
                          .then_verify_status_code(HttpStatus.SC_UNAUTHORIZED)
                          .then_verify_response_body_when_enter_expired_access_token();
    }

    @Test(description = "API: GET Saved shipments", groups = "regression")
    @Description("401 - Send request with valid all fields and non-existing access token")
    @Story("Filter by Shipment Status")
    @TmsLink("RC-322")
    public void RC_322_get_saved_shipments_with_valid_all_fields_and_non_existing_access_token(){
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", TYPE_MY_SHIPMENT);
        savedShipmentSteps.given_get_saved_shipment_with_authorization(NON_EXISTING_VALUE,qMap)
                          .then_verify_status_code(HttpStatus.SC_UNAUTHORIZED)
                          .then_verify_response_body_when_enter_expired_access_token();
    }

    @Test(description = "API: GET Saved shipments", groups = "regression")
    @Description("401 - Send request with valid all fields and expired access token")
    @Story("Filter by Shipment Status")
    @TmsLink("RC-323")
    public void RC_323_get_saved_shipments_with_valid_all_fields_and_expired_access_token(){
        Map<String, Object> qMap = new HashMap<>();
        qMap.put("type", TYPE_MY_SHIPMENT);
        savedShipmentSteps.given_get_saved_shipment_with_authorization(EXPIRED_ACCESS_TOKEN,qMap)
                          .then_verify_status_code(HttpStatus.SC_UNAUTHORIZED)
                          .then_verify_response_body_when_enter_expired_access_token();
    }
}