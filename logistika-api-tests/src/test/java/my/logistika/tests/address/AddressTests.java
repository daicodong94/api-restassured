package my.logistika.tests.address;

import io.qameta.allure.TmsLink;
import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.address.steps.CreateAddressSteps;
import my.logistika.microservices.address.steps.DeteleAddressSteps;
import my.logistika.microservices.address.models.CreateAddressRequest;
import my.logistika.microservices.address.models.CreateAddressResponse;
import my.logistika.microservices.address.steps.GetAddressSteps;
import my.logistika.microservices.address.steps.UpdateAddressSteps;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import java.util.List;

public class AddressTests extends BaseTest {
    GetAddressSteps getAddressSteps= new GetAddressSteps();
    CreateAddressSteps createAddressSteps= new CreateAddressSteps();
    DeteleAddressSteps deteleAddressSteps= new DeteleAddressSteps();
    UpdateAddressSteps updateAddressSteps= new UpdateAddressSteps();
    CreateAddressRequest createAddressRequest;
    CreateAddressResponse createAddressResponse;
    @Test(description = "API: address - Create a new  address", groups = {"api-address"})
    @TmsLink(value = "TEST-001")
    public void test_create_address()
    {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        createAddressRequest = new CreateAddressRequest("TestName","+60112681234","testAddress","testAddress2","60000","testCity","testState","Malaysia","sample pickup instructions");
        createAddressResponse = (CreateAddressResponse) createAddressSteps.when_created_address(createAddressRequest)
                .validateResponse(HttpStatus.SC_CREATED)
                .saveResponseObject(CreateAddressResponse.class);
        createAddressSteps.then_verify_address(createAddressRequest,createAddressResponse);
        System.out.println(createAddressResponse.toString());
        deteleAddressSteps.when_delete_address(createAddressResponse.getId())
                .validateResponse(HttpStatus.SC_OK);
    }
    @Test(description = "API: address - update a address", groups = {"api-address"})
    @TmsLink(value = "TEST-002")
    public void test_update_address()
    {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        createAddressRequest = new CreateAddressRequest("TestName","+60112681234","testAddress","testAddress2","60000","testCity","testState","Malaysia","sample pickup instructions");
        createAddressResponse = (CreateAddressResponse) createAddressSteps.when_created_address(createAddressRequest)
                .validateResponse(HttpStatus.SC_CREATED)
                .saveResponseObject(CreateAddressResponse.class);
        createAddressSteps.then_verify_address(createAddressRequest,createAddressResponse);

        createAddressRequest = new CreateAddressRequest("TestName2","+60112681234","testAddress","testAddress2","60000","testCity","testState","Malaysia","sample pickup instructions");
        CreateAddressResponse Response2= (CreateAddressResponse) updateAddressSteps.when_update_address(createAddressRequest,createAddressResponse.getId())
                .validateResponse(HttpStatus.SC_OK)
                .saveResponseObject(CreateAddressResponse.class);
        updateAddressSteps.then_verify_address(createAddressRequest,Response2);
        deteleAddressSteps.when_delete_address(Response2.getId())
                .validateResponse(HttpStatus.SC_OK);
    }
    @Test(description = "API: address - get list address", groups = {"api-address"})
    @TmsLink(value = "TEST-003")
    public  void test_get_list_address()
    {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        getAddressSteps.when_get_address("1","2")
                .then_verify_count_of_address(2)
                .validateResponse(HttpStatus.SC_OK);
    }
}
