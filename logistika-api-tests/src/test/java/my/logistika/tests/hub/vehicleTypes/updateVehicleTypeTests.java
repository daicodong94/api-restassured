package my.logistika.tests.hub.vehicleTypes;

import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.apiHub.vehiclesType.models.vehicleTypeRequest;
import my.logistika.microservices.apiHub.vehiclesType.models.vehicleTypeResponse;
import my.logistika.microservices.apiHub.vehiclesType.steps.vehicleTypeSteps;
import org.apache.http.HttpStatus;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class updateVehicleTypeTests extends BaseTest {
    private vehicleTypeRequest updateVehicleTypeRequest;
    private vehicleTypeResponse updateVehicleTypeResponse;
    private vehicleTypeSteps updateVehicleTypeSteps = new vehicleTypeSteps();
    private String vehicleTypeId;

    @BeforeTest(description = "Precondition: Create Vehicle Type for Test", groups = "regression")
    public void beforeTest() {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        updateVehicleTypeRequest = updateVehicleTypeSteps.given_input_vehicle_type_data("VH001",1000,100,10);
        updateVehicleTypeResponse = (vehicleTypeResponse) updateVehicleTypeSteps.when_create_vehicle_type(updateVehicleTypeRequest)
                .verifyStatusCode(HttpStatus.SC_CREATED)
                .saveResponseObject(vehicleTypeResponse.class);
        vehicleTypeId = updateVehicleTypeResponse.getVehicleTypeId();
    }

    @Test(description = "TC001_API: Hub - Update vehicle type validation", groups = "regression", dataProvider = "vehicle Type Validation Data")
    public void TC_001_Update_Vehicle_Type_Validation(String name, Integer maxVolumeLoad, Integer maxWeight, Integer seatingCapacity,Integer expectedCode,String jsonLocator,String expectedMessage){
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        updateVehicleTypeRequest = updateVehicleTypeSteps.given_input_vehicle_type_data(name,maxVolumeLoad,maxWeight,seatingCapacity);
        updateVehicleTypeSteps.when_update_vehicle_type(vehicleTypeId,updateVehicleTypeRequest)
                .verifyMessage(jsonLocator,expectedMessage)
                .verifyStatusCode(expectedCode);
    }

    @Test(description = "TC002_API: Hub - Verify Update vehicle type Success", groups = "regression")
    public void TC_002_Verify_Update_Vehicle_Type_Success() {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        updateVehicleTypeRequest = updateVehicleTypeSteps.given_input_vehicle_type_data("VH001",1000,100,10);
        updateVehicleTypeResponse = (vehicleTypeResponse) updateVehicleTypeSteps.when_update_vehicle_type(vehicleTypeId,updateVehicleTypeRequest)
                .verifyStatusCode(HttpStatus.SC_OK)
                .saveResponseObject(vehicleTypeResponse.class);
        updateVehicleTypeSteps.then_verify_create_update_vehicle_type_success(updateVehicleTypeRequest,updateVehicleTypeResponse);
    }

    @AfterTest(description ="Delete Vehicle Type Test Data")
    public void afterTest() {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        updateVehicleTypeSteps.when_delete_vehicle_type(vehicleTypeId);
    }

    @DataProvider(name = "vehicle Type Validation Data")
    public Object[][] vehicle_type_validation_data_provider() {
        final Object[][] objects = {
                {"", 1000, 10, 10, 400,"message[0]", "name should not be empty"},
                {"   ", 1000, 10, 10, 400,"message[0]", "name should not be empty"},
                {"AQA-Do-not-Delete", 100, 0, 1, 400,"message", "Duplicate fields: name"},
                {"Test AQA01", null, 0, 1, 400,"message", "Null value is not allowed"},
                {"Test AQA01", -1, 0, 1, 400,"message[0]", "maxVolumeLoad must not be less than 0"},
                {"Test AQA01", 10, null, 1, 400,"message", "Null value is not allowed"},
                {"Test AQA01", 10, -1, 1, 400,"message[0]", "maxWeight must not be less than 0"},
                {"Test AQA01", 10, 10, null, 400,"message", "Null value is not allowed"},
                {"Test AQA01", 10, 10, -1, 400,"message[0]", "seatingCapacity must not be less than 0"},
        };
        return objects;
    }
}
