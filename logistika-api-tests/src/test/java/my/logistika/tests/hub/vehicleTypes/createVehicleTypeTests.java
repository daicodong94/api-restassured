package my.logistika.tests.hub.vehicleTypes;

import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.apiHub.vehiclesType.models.vehicleTypeRequest;
import my.logistika.microservices.apiHub.vehiclesType.models.vehicleTypeResponse;
import my.logistika.microservices.apiHub.vehiclesType.steps.vehicleTypeSteps;
import org.apache.http.HttpStatus;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class createVehicleTypeTests extends BaseTest {

    private vehicleTypeRequest createVehicleTypeRequest;
    private vehicleTypeResponse createVehicleTypeResponse;
    private vehicleTypeSteps createVehicleTypeSteps = new vehicleTypeSteps();
    private String vehicleTypeId;

    @Test(description = "TC001_API: Hub - Create vehicle type validation", groups = "regression", dataProvider = "vehicle Type Validation Data")
    public void TC_001_Create_Vehicle_Type_Validation(String name, Integer maxVolumeLoad, Integer maxWeight, Integer seatingCapacity,Integer expectedCode,String jsonLocator,String expectedMessage){
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        createVehicleTypeRequest = createVehicleTypeSteps.given_input_vehicle_type_data(name,maxVolumeLoad,maxWeight,seatingCapacity);
                createVehicleTypeSteps.when_create_vehicle_type(createVehicleTypeRequest)
                .verifyMessage(jsonLocator,expectedMessage)
                .verifyStatusCode(expectedCode);
    }

    @Test(description = "TC002_API: Hub - Verify Create vehicle type success", groups = "regression")
    public void TC_002_Verify_Create_Vehicle_Type_Success() {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        createVehicleTypeRequest = createVehicleTypeSteps.given_input_vehicle_type_data("VH001",1000,100,10);
        createVehicleTypeResponse = (vehicleTypeResponse) createVehicleTypeSteps.when_create_vehicle_type(createVehicleTypeRequest)
                .verifyStatusCode(HttpStatus.SC_CREATED)
                .saveResponseObject(vehicleTypeResponse.class);
        vehicleTypeId = createVehicleTypeResponse.getVehicleTypeId();
        createVehicleTypeSteps.then_verify_create_update_vehicle_type_success(createVehicleTypeRequest,createVehicleTypeResponse);
    }

    @AfterTest(description ="Delete Vehicle Type Test Data")
    public void afterTest() {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        createVehicleTypeSteps.when_delete_vehicle_type(vehicleTypeId);
    }

    @DataProvider(name = "vehicle Type Validation Data")
    public Object[][] vehicle_type_validation_data_provider() {
        final Object[][] objects = {
                {"", 1000, 10, 10, 400,"message[0]", "name should not be empty"},
                {"   ", 1000, 10, 10, 400,"message[0]", "name should not be empty"},
                {"AQA-Do-not-Delete", 100, 0, 1, 400,"message", "Vehicle exists"},
                {"Test AQA01", null, 0, 1, 400,"message[1]", "maxVolumeLoad must be a number conforming to the specified constraints"},
                {"Test AQA01", -1, 0, 1, 400,"message[0]", "maxVolumeLoad must not be less than 0"},
                {"Test AQA01", 10, null, 1, 400,"message[1]", "maxWeight must be a number conforming to the specified constraints"},
                {"Test AQA01", 10, -1, 1, 400,"message[0]", "maxWeight must not be less than 0"},
                {"Test AQA01", 10, 10, null, 400,"message[1]", "seatingCapacity must be a number conforming to the specified constraints"},
                {"Test AQA01", 10, 10, -1, 400,"message[0]", "seatingCapacity must not be less than 0"},
        };
        return objects;
    }
}
