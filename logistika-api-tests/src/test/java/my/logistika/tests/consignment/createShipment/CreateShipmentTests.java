package my.logistika.tests.consignment.createShipment;
import io.qameta.allure.TmsLink;
import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.consignment.constants.ConsignmentApiShipmentStatus;
import my.logistika.microservices.consignment.createShipment.models.CreateShipment;
import my.logistika.microservices.consignment.createShipment.models.CreateShipmentDTO;
import my.logistika.microservices.consignment.createShipment.models.ShipmentResponse;
import my.logistika.microservices.consignment.createShipment.steps.CreateShipmentSteps;
import my.setel.utils.CommonUtils;
import org.testng.annotations.Test;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CreateShipmentTests extends BaseTest {

    private CreateShipmentSteps createShipmentSteps = new CreateShipmentSteps();
    private ShipmentResponse shipmentResponse;

    @Test(description = "API: consignment- create shipment", groups = "consignment")
    @TmsLink(value = "TEST-001")
    public void test_create_shipment() {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        String refId = CommonUtils.getRandomFiveCharsString();
        CreateShipmentDTO createShipmentDTO=  new CreateShipmentDTO(
                refId,
                "1",
                "2",
                "5",
                "4",
                "3",
                "S",
                "rName",
                "+601111111111",
                "Test dr 44",
                "",
                "60000",
                "",
                "Kuala Lumpur",
                "optional delivery instructions",
                "0",
                "",
                "",
                "Sravan Mer",
                "+60999999999",
                "",
                "Pickup",
                "address 1 mer",
                "address 2 mer",
                "",
                "",
                "Kuala Lumpur",
                "60000",
                "+609999999999",
                "",
                "795ef531-b136-4604-8e49-b7b4b1ed3229");
        List<CreateShipmentDTO> list = new ArrayList<>();
        list.add(createShipmentDTO);
        CreateShipment createShipmentRequest = new CreateShipment(list);
        shipmentResponse = (ShipmentResponse) createShipmentSteps.when_a_POST_request_is_made_to_shipments_endpoint(createShipmentRequest)
                .validateResponse(201)
                .saveResponseObject(ShipmentResponse.class);
        Map<String,String> expectedMap = new HashMap<>();
        expectedMap.put("workflowStatusForUser", ConsignmentApiShipmentStatus.PENDING_PICKUP);
        expectedMap.put("merchantReferenceId",refId);
        createShipmentSteps.then_shipment_must_be_created(expectedMap, shipmentResponse.getShipments().get(0));
    }
}
