package my.logistika.tests.consignment.shipmentAuditLog;

import io.qameta.allure.TmsLink;
import my.logistika.base.BaseTest;
import my.logistika.constants.Account_Payment;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.authentication.login.steps.LoginSetelDashBoardSteps;
import my.logistika.microservices.authentication.login.steps.LoginSteps;
import my.logistika.microservices.consignment.shipmentAdminAuditLog.steps.ShipmentAuditLogSteps;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

public class ShipmentAuditLogTests extends BaseTest {
    LoginSetelDashBoardSteps loginSteps=new LoginSetelDashBoardSteps();
    ShipmentAuditLogSteps shipmentAuditLogSteps=new ShipmentAuditLogSteps();
    @Test(description = "API: Consignment - Get Shipment Audit Log", groups = {"api-consignmentShipment"})
    @TmsLink(value = "TEST-001")
    public void Get_Shipment_Audit_Log()
    {
        loginSteps.when_userLogins(Account_Payment.USERNAME_PAYMENTS,Account_Payment.PASSWORD_PAYMENTS);
        shipmentAuditLogSteps.when_get_shipment_audit_log_by_shipment_Id("SETQDV3514","Shipment","Today","","Quan Doan Viet")
                .validateResponse(HttpStatus.SC_OK);
        System.out.println(shipmentAuditLogSteps.getJsonAsString());
    }
}
