package my.logistika.tests.routing.rushHourConfiguration;

import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.authentication.login.steps.LoginSteps;
import my.logistika.microservices.apiRouting.rushHourConfiguration.models.RushHourRequest;
import my.logistika.microservices.apiRouting.rushHourConfiguration.models.RushHourResponse;
import my.logistika.microservices.apiRouting.rushHourConfiguration.steps.CreateRushHourSteps;
import my.logistika.microservices.apiRouting.rushHourConfiguration.steps.DeleteRushHourSteps;
import my.logistika.microservices.apiRouting.rushHourConfiguration.steps.UpdateRushHourSteps;
import org.apache.http.HttpStatus;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class updateRushHourTests extends BaseTest {

    private UpdateRushHourSteps updateRushHourSteps = new UpdateRushHourSteps();
    private RushHourResponse rushHourResponse;
    private RushHourRequest rushHourRequest;
    private String rushHourId;
    LoginSteps loginSteps = new LoginSteps();

    @BeforeTest(description = "Create New RushHour for Test", groups = "Regression")
    public void beforeTest() {
        CreateRushHourSteps createRushHourSteps = new CreateRushHourSteps();
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        rushHourRequest = new RushHourRequest("New Rush Hour","05:00 PM","06:00 PM",10,"MTR080");
        rushHourResponse = (RushHourResponse) createRushHourSteps.when_created_rush_hour("New Rush Hour","05:00 PM","06:00 PM",10,"MTR080")
                    .validateResponse(HttpStatus.SC_CREATED)
                    .saveResponseObject(RushHourResponse.class);
            rushHourId = rushHourResponse.getRushHourId();
        createRushHourSteps.then_verify_create_rush_hour_success(rushHourRequest, rushHourResponse);
        }

    @Test(description = "TC001_API: routing -Unauthorized- Update rush hour failed", groups = "Routing-regression")
    public void TC_001_Update_rush_hour_failed_when_unauthorized(){
        updateRushHourSteps.when_update_rush_hour(rushHourId,"New Rush Hour","05:00 PM","06:00 PM",10,"MTR080")
                .verifyMessage("message","Unauthorized")
                .verifyStatusCode(HttpStatus.SC_UNAUTHORIZED);
    }

    @Test(description = "TC002_API: routing - Verify name field validation", groups = "regression")
    public void TC_002_Verify_name_field_validation(){
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        updateRushHourSteps.when_update_rush_hour(rushHourId,"","05:00 PM","06:00 PM",10,"MTR080")
                .verifyMessage("message[0]","name should not be empty")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .when_update_rush_hour(rushHourId,"  ","05:00 PM","06:00 PM",10,"MTR080")
                .verifyMessage("message[0]","name should not be empty")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .when_update_rush_hour(rushHourId,"RushHour1","05:00 PM","06:00 PM",10,"MTR080")
                .verifyMessage("message","Rush hour exists.")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test(description = "TC003_API: routing - Verify from field validation", groups = "regression")
    public void TC_003_Verify_from_field_validation(){
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        updateRushHourSteps.when_update_rush_hour(rushHourId,"Test123","","09:00 PM",10,"MTR080")
                .verifyMessage("message","Rush hour time is invalid.")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .when_update_rush_hour(rushHourId,"Test129","   ","09:00 PM",10,"MTR080")
                .verifyMessage("message","Rush hour time is invalid.")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .when_update_rush_hour(rushHourId,"Test123","08:00","09:00 PM",10,"MTR080")
                .verifyMessage("message[0]","from must be in this format: hh:mm AM/PM")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test(description = "TC004_API: routing - Verify till field validation", groups = "regression")
    public void TC_004_Verify_till_field_validation(){
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        updateRushHourSteps
                .when_update_rush_hour(rushHourId,"Test129","09:00 PM","",10,"MTR080")
                .verifyMessage("message","Rush hour time is invalid.")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .when_update_rush_hour(rushHourId,"Test129","09:00 PM","  ",10,"MTR080")
                .verifyMessage("message","Rush hour time is invalid.")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .when_update_rush_hour(rushHourId,"Test129","09:00 PM","10:00",10,"MTR080")
                .verifyMessage("message[0]","till must be in this format: hh:mm AM/PM")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test(description = "TC005_API: routing - Verify combine from-till field validation", groups = "regression")
    public void TC_005_Verify_combine_from_till_validation(){
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        updateRushHourSteps.when_update_rush_hour(rushHourId,"Test129","10:00 PM","09:00 PM",10,"MTR080")
                .verifyMessage("description","Rush hour till must after from")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .when_update_rush_hour(rushHourId,"Test129","09:00 PM","10:00 AM",10,"MTR080")
                .verifyMessage("description","Rush hour till must after from")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .when_update_rush_hour(rushHourId,"Test129","11:00 AM","12:00 PM",10,"MTR080")
                .verifyMessage("message","Rush hour exists.")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test(description = "TC006_API: routing - Verify buffer field validation", groups = "regression")
    public void TC_006_Verify_buffer_field_validation(){
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        updateRushHourSteps.when_update_rush_hour(rushHourId,"Test129","09:00 PM","10:00 PM",null,"MTR080")
                .verifyMessage("message[1]","Null value is not allowed")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .when_update_rush_hour(rushHourId,"Test129","09:00 PM","10:00 PM",-1,"MTR080")
                .verifyMessage("message[0]","buffer must not be less than 0")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test(description = "TC007_API: routing - Verify hub field validation", groups = "regression")
    public void TC_007_Verify_hub_field_validation(){
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        updateRushHourSteps.when_update_rush_hour(rushHourId,"Test129","09:00 PM","10:00 PM",10,"")
                .verifyMessage("description","Hub not found")
                .verifyStatusCode(HttpStatus.SC_NOT_FOUND)
                .when_update_rush_hour(rushHourId,"Test129","09:00 PM","10:00 PM",10,"  ")
                .verifyMessage("description","Hub not found")
                .verifyStatusCode(HttpStatus.SC_NOT_FOUND)
                .when_update_rush_hour(rushHourId,"Test129","11:00 AM","12:00 PM",10,"1")
                .verifyMessage("description","Hub not found")
                .verifyStatusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(description = "TC008_API: routing - Update rush hour same as existing rush hour", groups = "regression")
    public void TC_008_Verify_update_rush_hour_same_as_existing_rush_hour(){
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        updateRushHourSteps.when_update_rush_hour(rushHourId,"DO NOT DELETE- AQA Test","01:00 AM","01:00 PM",10,"MTR080")
                .verifyMessage("message","Rush hour exists")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .when_update_rush_hour(rushHourId,"Test129","12:00 AM","01:00 AM",10,"MTR080")
                .verifyMessage("message","Rush hour exists")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test(description = "TC009_API: routing - Create rush hour success", groups = "regression")
    public void TC_009_Update_rush_hour_success(){
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        rushHourRequest = new RushHourRequest("Update RH AQA","01:00 AM","02:00 AM",10,"MTR080");
        rushHourResponse = (RushHourResponse) updateRushHourSteps.when_update_rush_hour(rushHourId,"Update RH AQA","01:00 AM","02:00 AM",10,"MTR080")
                .validateResponse(HttpStatus.SC_OK)
                .saveResponseObject(RushHourResponse.class);
        updateRushHourSteps.then_verify_update_rush_hour_success(rushHourRequest, rushHourResponse);
    }

    @AfterTest(description = "Clear Test RushHour data", groups = "regression")
    public void afterTest(){
        DeleteRushHourSteps deleteRushHourSteps = new DeleteRushHourSteps();
        deleteRushHourSteps.when_delete_rush_hour(rushHourId);
    }

}
