package my.logistika.tests.routing.plan;

import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.apiRouting.plan.createPlan.steps.createPlanSteps;
import my.logistika.microservices.apiRouting.plan.createPlan.steps.deletePlanSteps;
import my.logistika.microservices.authentication.login.steps.LoginSteps;
import my.logistika.ultils.DateCalculator;
import org.apache.http.HttpStatus;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class createPlanTests extends BaseTest {
    private createPlanSteps createPlanSteps = new createPlanSteps();
    private DateCalculator dateCalculator = new DateCalculator();
    LoginSteps loginSteps = new LoginSteps();
    private String planId;

    @Test(description = "TC001_API: routing - Create plan ", groups = "Routing-regression")
    public void TC_001_verify_hubCode_validation() {
        loginSteps.when_userLogin(ApiUserRegistry.ROUTING_DELIVERY);
        createPlanSteps.when_created_plan("", dateCalculator.tomorrow().toString(), false)
                .verifyMessage("message", "Hub not found.")
                .verifyStatusCode(HttpStatus.SC_NOT_FOUND)
                .when_created_plan("  ", dateCalculator.tomorrow().toString(), false)
                .verifyMessage("message", "Hub not found.")
                .verifyStatusCode(HttpStatus.SC_NOT_FOUND)
                .when_created_plan("Abc1234", dateCalculator.tomorrow().toString(), false)
                .verifyMessage("message", "Hub not found.")
                .verifyStatusCode(HttpStatus.SC_NOT_FOUND)
                .when_created_plan("Abc1234", dateCalculator.tomorrow().toString(), false)
                .verifyMessage("message", "Hub not found.")
                .verifyStatusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(description = "TC002_API: routing - Create plan-Date validation ", groups = "Routing-regression")
    public void TC_002_verify_date_validation() {
        loginSteps.when_userLogin(ApiUserRegistry.ROUTING_DELIVERY);
        createPlanSteps
                .when_created_plan("JJN054", "", false)
                .verifyMessage("message[0]", "date must be after today.")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .when_created_plan("JJN054", "01-01-2022", false)
                .verifyMessage("message[1]", "date must be in this format: YYYY-MM-DD")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .when_created_plan("JJN054", dateCalculator.today().toString(), false)
                .verifyMessage("message[0]", "date must be after today.")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .when_created_plan("JJN054", dateCalculator.getDate(-1).toString(), false)
                .verifyMessage("message[0]", "date must be after today.")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .when_created_plan("JJN054", dateCalculator.getDate(5).toString(), false)
                .verifyStatusCode(HttpStatus.SC_CREATED);
    }

    @Test(description = "TC003_API: routing - Create plan - withZone validation ", groups = "Routing-regression")
    public void TC_003_create_plan_failed_with_hubCode_has_no_data_in_resource_planning() {
        loginSteps.when_userLogin(ApiUserRegistry.ROUTING_DELIVERY);
        createPlanSteps.when_created_plan("LDC081", dateCalculator.tomorrow().toString(), false)
                .verifyMessage("message", "Must be more than 1 shipment.")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test(description = "TC004_API: routing - Verify create plan success", groups = "Routing-regression")
    public void TC_004_create_plan_success() {
        loginSteps.when_userLogin(ApiUserRegistry.ROUTING_DELIVERY);
         planId = createPlanSteps.when_created_plan("JJN054",dateCalculator.tomorrow().toString(),false)
                .verifyStatusCode(HttpStatus.SC_CREATED)
                .getJsonValue("planId");
    }

    @AfterTest(description = "Delete plan data", groups = "Routing-regression")
    public void AfterTest() {
        deletePlanSteps deletePlan = new deletePlanSteps();
        deletePlan.when_deleted_plan(planId);
    }

}
