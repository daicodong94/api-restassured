package my.logistika.tests.routing.rushHourConfiguration;

import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.authentication.login.steps.LoginSteps;
import my.logistika.microservices.apiRouting.rushHourConfiguration.models.RushHourRequest;
import my.logistika.microservices.apiRouting.rushHourConfiguration.models.RushHourResponse;
import my.logistika.microservices.apiRouting.rushHourConfiguration.steps.CreateRushHourSteps;
import my.logistika.microservices.apiRouting.rushHourConfiguration.steps.DeleteRushHourSteps;
import org.apache.http.HttpStatus;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class createRushHourTests extends BaseTest {

    private CreateRushHourSteps createRushHourSteps = new CreateRushHourSteps();
    private RushHourResponse createRushHourResponse;
    private RushHourRequest createRushHourRequest;
    private  String rushHourId;
    LoginSteps loginSteps = new LoginSteps();

    @Test(description = "TC001_API: routing -Unauthorized- Create rush hour failed", groups = "Routing-regression")
    public void TC_001_Create_rush_hour_failed_when_unauthorized(){
          createRushHourSteps.when_created_rush_hour("New Rush Hour","05:00 PM","06:00 PM",10,"MTR080")
                                  .verifyMessage("message","Unauthorized")
                                  .verifyStatusCode(HttpStatus.SC_UNAUTHORIZED);
    }

    @Test(description = "TC002_API: routing - Verify name field validation", groups = "regression")
    public void TC_002_Verify_name_field_validation(){
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        createRushHourSteps.when_created_rush_hour("","05:00 PM","06:00 PM",10,"MTR080")
                 .verifyMessage("message[0]","name should not be empty")
                 .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
              .when_created_rush_hour("  ","05:00 PM","06:00 PM",10,"MTR080")
                 .verifyMessage("message[0]","name should not be empty")
                 .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
              .when_created_rush_hour("RushHour1","05:00 PM","06:00 PM",10,"MTR080")
                 .verifyMessage("message","Rush hour exists.")
                 .verifyStatusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test(description = "TC003_API: routing - Verify from field validation", groups = "regression")
    public void TC_003_Verify_from_field_validation(){
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
     createRushHourSteps
                .when_created_rush_hour("Test123","","09:00 PM",10,"MTR080")
                .verifyMessage("message","Rush hour time is invalid.")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .when_created_rush_hour("Test129","   ","09:00 PM",10,"MTR080")
                .verifyMessage("message","Rush hour time is invalid.")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .when_created_rush_hour("Test123","08:00","09:00 PM",10,"MTR080")
                .verifyMessage("message[0]","from must be in this format: hh:mm AM/PM")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test(description = "TC004_API: routing - Verify till field validation", groups = "regression")
    public void TC_004_Verify_till_field_validation(){
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        createRushHourSteps
                .when_created_rush_hour("Test129","09:00 PM","",10,"MTR080")
                .verifyMessage("message","Rush hour time is invalid.")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .when_created_rush_hour("Test129","09:00 PM","  ",10,"MTR080")
                .verifyMessage("message","Rush hour time is invalid.")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .when_created_rush_hour("Test129","09:00 PM","10:00",10,"MTR080")
                .verifyMessage("message[0]","till must be in this format: hh:mm AM/PM")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test(description = "TC005_API: routing - Verify combine from-till field validation", groups = "regression")
    public void TC_005_Verify_combine_from_till_validation(){
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        createRushHourSteps
                .when_created_rush_hour("Test129","10:00 PM","09:00 PM",10,"MTR080")
                .verifyMessage("description","Rush hour till must after from")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .when_created_rush_hour("Test129","09:00 PM","10:00 AM",10,"MTR080")
                .verifyMessage("description","Rush hour till must after from")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .when_created_rush_hour("Test129","11:00 AM","12:00 PM",10,"MTR080")
                .verifyMessage("message","Rush hour exists.")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test(description = "TC006_API: routing - Verify buffer field validation", groups = "regression")
    public void TC_006_Verify_buffer_field_validation(){
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        createRushHourSteps
                .when_created_rush_hour("Test129","09:00 PM","10:00 PM",null,"MTR080")
                .verifyMessage("message[1]","buffer must be an integer number")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .when_created_rush_hour("Test129","09:00 PM","10:00 PM",-1,"MTR080")
                .verifyMessage("message[0]","buffer must not be less than 0")
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test(description = "TC007_API: routing - Verify hub field validation", groups = "regression")
    public void TC_007_Verify_hub_field_validation(){
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        createRushHourSteps
                .when_created_rush_hour("Test129","09:00 PM","10:00 PM",10,"")
                .verifyMessage("description","Hub not found")
                .verifyStatusCode(HttpStatus.SC_NOT_FOUND)
                .when_created_rush_hour("Test129","09:00 PM","10:00 PM",10,"  ")
                .verifyMessage("description","Hub not found")
                .verifyStatusCode(HttpStatus.SC_NOT_FOUND)
                .when_created_rush_hour("Test129","11:00 AM","12:00 PM",10,"1")
                .verifyMessage("description","Hub not found")
                .verifyStatusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test(description = "TC008_API: routing - Create rush hour success", groups = "regression")
    public void TC_008_Create_rush_hour_success(){
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        createRushHourRequest = new RushHourRequest("New Rush Hour","05:00 PM","06:00 PM",10,"MTR080");
        createRushHourResponse = (RushHourResponse) createRushHourSteps.when_created_rush_hour("New Rush Hour","05:00 PM","06:00 PM",10,"MTR080")
                                .validateResponse(HttpStatus.SC_CREATED)
                                .saveResponseObject(RushHourResponse.class);
            rushHourId = createRushHourResponse.getRushHourId();
        createRushHourSteps.then_verify_create_rush_hour_success(createRushHourRequest, createRushHourResponse);
    }

    @AfterTest(description = "Clear Test RushHour data", groups = "regression")
    public void afterTest(){
        DeleteRushHourSteps deleteRushHourSteps = new DeleteRushHourSteps();
        deleteRushHourSteps.when_delete_rush_hour(rushHourId);
    }

    }
