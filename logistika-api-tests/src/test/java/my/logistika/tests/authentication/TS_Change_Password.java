package my.logistika.tests.authentication;

import io.qameta.allure.TmsLink;
import my.logistika.base.BaseTest;
import my.logistika.data.DataUser;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.authentication.change_password.steps.ChangePasswordSteps;
import org.testng.annotations.Test;

public class TS_Change_Password extends BaseTest {

    ChangePasswordSteps change_password_steps= new ChangePasswordSteps();
    @Test(description = "API: ChangePassword - change Password Successfully", groups = {"authentication"})
    @TmsLink(value = "TEST-001")
    public void TC001_changePasswordSuccessfully()
    {
        change_password_steps.whenChangePassword(ApiUserRegistry.MERCHANT.username, ApiUserRegistry.MERCHANT.password,"Lucikhuong2!","Lucikhuong2!")
                .verifyStatusCode(200)
                .verifyData("SUCCESS");
        initReqSpec();
        DataUser user= new DataUser();
        user.username=ApiUserRegistry.MERCHANT.username;
        user.password="Lucikhuong2!";
        loginSteps.login_to_support(user);
        change_password_steps.whenChangePassword(user.username,user.password,ApiUserRegistry.MERCHANT.password, ApiUserRegistry.MERCHANT.password)
                .verifyData("SUCCESS");
    }
    @Test(description = "API: ChangePassword - enter Incorrect Password", groups = {"authentication"})
    @TmsLink(value = "TEST-002")
    public void TC002_enterIncorrectPassword()
    {
        change_password_steps.whenChangePassword(ApiUserRegistry.MERCHANT.username, "incorrectPass","Lucikhuong2!","Lucikhuong2!")
                .verifyStatusCode(400)
                .verifyMessage("The password entered is incorrect")
                .verifyError("Bad Request");
    }
    @Test(description = "API: ChangePassword - enter Incorrect Username", groups = {"authentication"})
    @TmsLink(value = "TEST-003")
    public void TC003_enterIncorrectUsername()
    {
        change_password_steps.whenChangePassword( "IncorrectUsername",ApiUserRegistry.MERCHANT.password,"Lucikhuong2!","Lucikhuong2!")
                .verifyStatusCode(400)
                .verifyMessage("[username must be an email]")
                .verifyError("Bad Request");
    }
    @Test(description = "API: ChangePassword - enter Invalid New Password", groups = {"authentication"})
    @TmsLink(value = "TEST-004")
    public void TC004_enterInvalidNewPassword()
    {
        change_password_steps.whenChangePassword(ApiUserRegistry.MERCHANT.username, ApiUserRegistry.MERCHANT.password,"Lucig2!","Lucig2!")
                .verifyStatusCode(400)
                .verifyMessage("[newPassword must be 8 or more characters and must include uppercase, lowercase, and special character from !@#$%^&, newPassword must be longer than or equal to 8 characters]")
                .verifyError("Bad Request")
                .whenChangePassword(ApiUserRegistry.MERCHANT.username, ApiUserRegistry.MERCHANT.password,"Lucigaaaa","Lucigaaaa")
                .verifyStatusCode(400)
                .verifyMessage("[newPassword must be 8 or more characters and must include uppercase, lowercase, and special character from !@#$%^&]")
                .verifyError("Bad Request");

    }
    @Test(description = "API: ChangePassword - enter Confirm Password Does Not Match New Password", groups = {"authentication"})
    @TmsLink(value = "TEST-005")
    public void TC005_enterConfirmPasswordDoesNotMatchNewPassword()
    {
        change_password_steps.whenChangePassword(ApiUserRegistry.MERCHANT.username, ApiUserRegistry.MERCHANT.password,"Lucikhuong2!","Lucikhuong3!")
                .verifyStatusCode(400)
                .verifyMessage("[confirmPassword must be equal to newPassword]")
                .verifyError("Bad Request");
    }


}
