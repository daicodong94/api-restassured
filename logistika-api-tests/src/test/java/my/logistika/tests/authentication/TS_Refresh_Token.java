package my.logistika.tests.authentication;

import io.qameta.allure.TmsLink;
import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.authentication.refesh_token.steps.RefreshTokenSteps;
import org.testng.annotations.Test;

public class TS_Refresh_Token extends BaseTest {
    RefreshTokenSteps token_steps = new RefreshTokenSteps();

    @Test(description = "API: RefreshToken - successful Refresh Token", groups = {"authentication"})
    @TmsLink(value = "TEST-001")
    public void TC001_successfulRefreshToken() {

        token_steps.whenRefreshToken(ApiUserRegistry.MERCHANT)
                .verifyStatusCode(201)
                .verifyUserID("20e1e748-178f-4aa3-bdbb-7ed9bc4b63b6");
    }

    @Test(description = "API: RefreshToken - enter Invalid Refresh Token", groups = {"authentication"})
    @TmsLink(value = "TEST-002")
    public void TC002_enterInvalidRefreshToken() {
        token_steps.whenRefreshToken("invalidToken")
                .verifyStatusCode(400)
                .verifyMessage("Invalid Refresh Token");
    }
}
