package my.logistika.tests.authentication;

import io.qameta.allure.TmsLink;
import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.authentication.fogot_password.steps.ForgotPasswordSteps;
import org.testng.annotations.Test;

public class TS_Forgot_Password  extends BaseTest {
    ForgotPasswordSteps forgot_password_steps = new ForgotPasswordSteps() ;
    @Test(description = "API: ForgotPassword - enter Invalid Email", groups = {"authentication"})
    @TmsLink(value = "TEST-001")
    public void TC001_enterInvalidEmail()
    {
        forgot_password_steps.whenForgotPassword("huongkhuong")
                .verifyStatusCode(400)
                .verifyMessage("[username must be an email]")
                .verifyError("Bad Request");
    }
    @Test(description = "API: ForgotPassword - enter Email Do Not Exist In DB", groups = {"authentication"})
    @TmsLink(value = "TEST-002")
    public void TC002_enterEmailDoNotExistInDB()
    {
        forgot_password_steps.whenForgotPassword("huongkhuong@gmail.com")
                .verifyStatusCode(400)
                .verifyMessage("Sorry, we didn't recognise this email address")
                .verifyError("Bad Request");
    }
    @Test(description = "API: ForgotPassword - enter Email Exist In DB", groups = {"authentication"})
    @TmsLink(value = "TEST-003")
    public void TC003_enterEmailExistInDB()
    {
        forgot_password_steps.whenForgotPassword(ApiUserRegistry.MERCHANT.username)
                .verifyStatusCode(201)
                .verifyMessage("Check your email for reset instructions");
    }
}
