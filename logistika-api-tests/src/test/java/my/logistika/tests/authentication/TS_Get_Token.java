package my.logistika.tests.authentication;
import io.qameta.allure.TmsLink;
import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.authentication.token.steps.TokenSteps;
import org.testng.annotations.Test;

public class TS_Get_Token extends BaseTest {
    TokenSteps token_steps= new TokenSteps();

    @Test(description = "API: Get token - UnAuthorization", groups = {"authentication"})
    @TmsLink(value = "TEST-001")
    public void TC001_UnAuthorization()
    {
        token_steps.whenGetToken()
                .verifyStatusCode(401)
                .verifyMessage("Unauthorized");

    }
    @Test(description = "API: Get token - UnAuthorization", groups = {"authentication"})
    @TmsLink(value = "TEST-002")
    public void TC002_Authorization()
    {
        loginSteps.login_to_support(ApiUserRegistry.MERCHANT);
        token_steps.whenGetToken()
                .verifyStatusCode(200)
                .verifyMessage("Authorized");

    }
}
