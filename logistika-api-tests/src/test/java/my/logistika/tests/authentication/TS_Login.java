package my.logistika.tests.authentication;

import io.qameta.allure.TmsLink;
import my.logistika.base.BaseTest;
import my.logistika.data.DataUser;
import my.logistika.data.user.ApiUserRegistry;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
public class TS_Login extends BaseTest {

    @Test(description = "API: Login - do Not Enter Password", groups = {"authentication"})
    @TmsLink(value = "TEST-001")
    public void TC001_doNotEnterPassword() {
        DataUser user = new DataUser();
        user.username="huongkhuong@sotatek.com";
        user.password=null;
        loginSteps.when_Login(user)
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .verifyByField("error","Bad Request")
                .verifyMessage("[password should not be empty]");
    }

    @Test(description = "API: Login - enter Wrong Password", groups = {"authentication"})
    @TmsLink(value = "TEST-002")
    public void TC002_enterWrongPassword() {
        DataUser user = new DataUser();
        user.username="huong.khuong@sotatek.com";
        user.password="Logita1@";
        loginSteps.when_Login(user)
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .verifyByField("error","Bad Request")
                .printMessage()
                .verifyMessage("Incorrect username or password.")

        ;
    }

    @Test(description = "API: Login - enter Password With Length Less Than 8", groups = {"authentication"})
    @TmsLink(value = "TEST-003")
    public void TC003_enterPasswordWithLengthLessThan8() {
        DataUser user = new DataUser();
        user.username="huongkhuong@sotatek.com";
        user.password="Logit@";
        loginSteps.when_Login(user)
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .verifyByField("error","Bad Request")
                .verifyMessage("Incorrect username or password.");
    }

    @Test(description = "API: Login - login Successfully", groups = {"authentication"})
    @TmsLink(value = "TEST-004")
    public void TC004_loginSuccessfully() {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT)
                .verifyStatusCode(HttpStatus.SC_CREATED)
                .verifyByField("token_type","Bearer")
               // .verifyByField("user_id","20e1e748-178f-4aa3-bdbb-7ed9bc4b63b6")
        ;
    }

    @Test(description = "API: Login - enter Do Not Exist Email", groups = {"authentication"})
    @TmsLink(value = "TEST-005")
    public void TC005_enterDoNotExistEmail() {
        DataUser user = new DataUser();
        user.username="anyemai@gmail.com";
        user.password="Logit@123";
        loginSteps.when_Login(user)
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .verifyMessage("Incorrect username or password.")
                .verifyByField("error","Bad Request");
    }

    @Test(description = "API: Login - do Not Enter Email And Password", groups = {"authentication"})
    @TmsLink(value = "TEST-006")
    public void TC006_doNotEnterEmailAndPassword() {
        DataUser user = new DataUser();
        user.username=null;
        user.password=null;
        loginSteps.when_Login(user)
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .verifyMessage("[username must be an email, username should not be empty, password should not be empty]")
                ;
    }
    @Test(description = "API: Login - enter Invalid Email", groups = {"authentication"})
    @TmsLink(value = "TEST-007")
    public void TC007_enterInvalidEmail()
    {
        DataUser user = new DataUser();
        user.username="anyemaigmail.com";
        user.password="Logit@123";
        loginSteps.when_Login(user)
                .verifyStatusCode(400)
                .verifyStatusCode(HttpStatus.SC_BAD_REQUEST)
                .printMessage()
                .verifyMessage("[username must be an email]")
                ;
    }
}
