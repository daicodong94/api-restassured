package my.logistika.tests.authentication;

import io.qameta.allure.TmsLink;
import my.logistika.base.BaseTest;
import my.logistika.data.DataUser;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.authentication.change_email.steps.ChangeEmailSteps;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TS_Change_Email extends BaseTest {
    ChangeEmailSteps change_email_steps = new ChangeEmailSteps();
    @BeforeClass
    public  void BeforeTest(){
        loginSteps.login_to_support(ApiUserRegistry.MERCHANT);
    }

    @Test(description = "API: ChangeEmail - enter Invalid New Email", groups = {"authentication"})
    @TmsLink(value = "TEST-001")
    public void TC001_enterInvalidNewEmail() {
        change_email_steps.whenResetEmail(ApiUserRegistry.MERCHANT.username, "huongkhuong")
                .verifyStatusCode(400)
                .verifyMessage("newEmail must be an email")
                .whenResetEmail(ApiUserRegistry.MERCHANT.username, "huongkhuong@gmail")
                .verifyStatusCode(400)
                .verifyMessage("newEmail must be an email")
                .whenResetEmail(ApiUserRegistry.MERCHANT.username, "huongkhuong1gmail.com")
                .verifyStatusCode(400)
                .verifyMessage("newEmail must be an email");
    }

    @Test(description = "API: ChangeEmail - enter User name Do Not Exist In DB", groups = {"authentication"})
    @TmsLink(value = "TEST-002")
    public void TC002_enterUsernameDoNotExistInDB() {
        change_email_steps.whenResetEmail("huongkhong1@gmail.com", "huongkhuong.it@gmail.com")
                .verifyStatusCode(400)
                .verifyMessage("username is not exist");
    }
    @Test(description = "API: ChangeEmail - successful Change Email", groups = {"authentication"})
    @TmsLink(value = "TEST-003")
    public void TC003_successfulChangeEmail() {
        change_email_steps.whenResetEmail(ApiUserRegistry.MERCHANT.username, "huongkhuong.it@gmail.com")
                .verifyStatusCode(200)
                .verifyMessage("Email has changed successfully");
        DataUser user = new DataUser();
        user.username="huongkhuong.it@gmail.com";
        user.password=ApiUserRegistry.MERCHANT.password;
        loginSteps.login_to_support(user);
        change_email_steps.whenResetEmail(user.username,ApiUserRegistry.MERCHANT.username)
                .verifyStatusCode(200)
                .verifyMessage("Email has changed successfully");
    }

}
