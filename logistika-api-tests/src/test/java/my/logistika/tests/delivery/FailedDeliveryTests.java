package my.logistika.tests.delivery;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.TmsLink;
import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.consignment.constants.ConsignmentApiShipmentStatus;
import my.logistika.microservices.consignment.constants.ConsignmentApiWorkFlowStatus;
import my.logistika.microservices.consignment.createShipment.models.CreateShipmentResponse;
import my.logistika.microservices.consignment.createShipment.models.ShipmentResponse;
import my.logistika.microservices.consignment.createShipment.steps.CreateShipmentSteps;
import my.logistika.microservices.consignment.getShipmentDetails.steps.GetShipmentByIdSteps;
import my.logistika.microservices.delivery.constants.PickupTypes;
import my.logistika.microservices.delivery.constants.WorkFlowStatus;
import my.logistika.microservices.delivery.constants.WorkFlowStatusForUser;
import my.logistika.microservices.delivery.failedDelivery.steps.FailedDeliverySteps;
import my.logistika.microservices.delivery.scanning.steps.UpdateShipmentStatusScanSteps;
import my.setel.utils.LoggerUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.HashMap;
import java.util.Map;

import static my.logistika.microservices.delivery.constants.FailedDeliveryReason.INCORRECT_ADDRESS;

@Epic("Failed Delivery")
@Feature("An LMD should be able to update a failed delivery attempt So shipment status is updated")
public class FailedDeliveryTests extends BaseTest {

    private final FailedDeliverySteps failedDeliverySteps = new FailedDeliverySteps();
    private final CreateShipmentSteps createShipmentSteps = new CreateShipmentSteps();
    private final UpdateShipmentStatusScanSteps scanSteps = new UpdateShipmentStatusScanSteps();
    private final GetShipmentByIdSteps getShipmentByIdSteps = new GetShipmentByIdSteps();

    @BeforeMethod(alwaysRun = true)
    void scanning_pre_conditions(){
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
    }

    @Test(description = "API delivery: failed delivery attempt",groups = "regression")
    @TmsLink("COR-415")
    public void test_failed_delivery_endpoint() throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse)createShipmentSteps.createShipment("60000","60000",PickupTypes.PICKUP,WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        CreateShipmentResponse createShipmentResponse = shipmentResponse.getShipments().get(0);
        String shipmentId = createShipmentResponse.getShipmentId();
        String id = createShipmentResponse.getId();
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        scanSteps.doScan(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId, WorkFlowStatus.OUT_FOR_DELIVERY);
        failedDeliverySteps.when_a_delivery_attempt_is_failed(ApiUserRegistry.LMD_DJY052_KV.username,shipmentId,INCORRECT_ADDRESS).validateStatusCode(201);
        failedDeliverySteps.then_workflowstatus_must_be_failed_delivery_attempt();
        Map<String, String> expectedUpdates = new HashMap<>();
        expectedUpdates.put("shipmentId", shipmentId);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.FAILED_DELIVERY_ATTEMPT);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.DELIVERY_FAILED);
        expectedUpdates.put("failedDeliveryReason", INCORRECT_ADDRESS);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);
    }
}
