package my.logistika.tests.delivery;

import io.qameta.allure.TmsLink;
import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.delivery.mmdRouteName.steps.MMDRouteNameSteps;
import my.setel.utils.CommonUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MMDRouteNameTests extends BaseTest {

    private final MMDRouteNameSteps mmdRouteNameSteps = new MMDRouteNameSteps();

    @BeforeMethod(alwaysRun = true)
    void setup() {
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
    }

    @Test(description = "API Delivery: create MMD route name", groups = "regression")
    @TmsLink("COR-416")
    public void create_mmd_route_name_test() {
        String routeName = CommonUtils.getRandomFiveCharsString();
        mmdRouteNameSteps.when_an_mmd_route_name_is_created(routeName)
                .then_route_name_should_be_created(routeName);
    }

    @Test(description = "API Delivery: update MMD route name", groups = "regression")
    @TmsLink("COR-417")
    public void update_mmd_route_name_test() {
        String routeName = CommonUtils.getRandomFiveCharsString();

        String id = mmdRouteNameSteps.when_an_mmd_route_name_is_created(routeName).
                then_route_name_should_be_created(routeName);
        String updatedRouteName = routeName+"_updated";
        mmdRouteNameSteps
                .when_a_PUT_request_is_made_to_route_name(id,updatedRouteName)
                .then_a_route_name_must_be_updated(id,updatedRouteName);
    }

    @Test(description = "API Delivery: delete MMD route name", groups = "regression")
    @TmsLink("COR-418")
    public void delete_mmd_route_name_test() {
        String routeName = CommonUtils.getRandomFiveCharsString();
        String id = mmdRouteNameSteps
                .when_an_mmd_route_name_is_created(routeName)
                .then_route_name_should_be_created(routeName);
        mmdRouteNameSteps.when_a_delete_request_is_made_to_mmd_route_name(id).then__route_name_must_be_deleted(id);
    }

    @Test(description = "API Delivery: GET MMD route names", groups = "regression")
    @TmsLink("COR-416")
    public void get_mmd_route_name_test() {
        mmdRouteNameSteps
                .when_a_GET_request_is_made_to_MMD_route_names_endpoint()
                .then_list_of_route_names_must_be_returned();
    }

    @Test(description = "API Delivery: update MMD route name on invalid id must be failed - negative path", groups = "regression")
    @TmsLink("COR-423")
    public void update_invalid_mmd_route_name_test() {
        mmdRouteNameSteps
                .when_a_PUT_request_is_made_to_route_name("invalid","updatedRouteName")
                .validateResponse(400);
        mmdRouteNameSteps.when_a_PUT_request_is_made_to_route_name("623c830d41f3654c89ec784b","updatedRouteName")
                .validateResponse(404);
    }

    @Test(description = "API Delivery: delete invalid MMD route name - negative path", groups = "regression")
    @TmsLink("COR-424")
    public void delete_invalid_mmd_route_name_test() {
        mmdRouteNameSteps.when_a_delete_request_is_made_to_mmd_route_name("623c830d41f3654c89ec784b")
                .validateResponse(404);
        mmdRouteNameSteps.when_a_delete_request_is_made_to_mmd_route_name("invalid")
                .validateResponse(400);

    }


}
