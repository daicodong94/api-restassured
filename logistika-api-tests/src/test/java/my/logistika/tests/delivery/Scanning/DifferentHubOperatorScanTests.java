package my.logistika.tests.delivery.Scanning;

import io.qameta.allure.TmsLink;
import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.consignment.createShipment.models.CreateShipmentResponse;
import my.logistika.microservices.consignment.createShipment.models.ShipmentResponse;
import my.logistika.microservices.consignment.createShipment.steps.CreateShipmentSteps;
import my.logistika.microservices.consignment.getShipmentDetails.steps.GetShipmentByIdSteps;
import my.logistika.microservices.delivery.approveReturn.steps.ApproveReturnSteps;
import my.logistika.microservices.delivery.constants.Hubs;
import my.logistika.microservices.delivery.constants.PickupTypes;
import my.logistika.microservices.delivery.constants.WorkFlowStatus;
import my.logistika.microservices.delivery.constants.WorkFlowStatusForUser;
import my.logistika.microservices.delivery.deliveredToCustomer.steps.DeliveredToCustomerSteps;
import my.logistika.microservices.delivery.failedDelivery.steps.FailedDeliverySteps;
import my.logistika.microservices.delivery.scanning.models.UpdateShipmentStatusScanResponse;
import my.logistika.microservices.delivery.scanning.steps.UpdateShipmentStatusScanSteps;
import my.setel.utils.LoggerUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class DifferentHubOperatorScanTests extends BaseTest {

    private final UpdateShipmentStatusScanSteps scanSteps = new UpdateShipmentStatusScanSteps();
    private final CreateShipmentSteps createShipmentSteps = new CreateShipmentSteps();

    @BeforeMethod(alwaysRun = true)
    void scanning_pre_conditions() {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
    }

    @Test(description = "API Delivery: any HO should be able to scan a shipment", groups = "regression")
    public void test_any_HO_scans_a_shipment_of_diff_hub() throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "60000", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        CreateShipmentResponse createShipmentResponse = shipmentResponse.getShipments().get(0);
        String shipmentId = createShipmentResponse.getShipmentId();
        String id = shipmentResponse.getShipments().get(0).getId();
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        assert createShipmentResponse.getPickupStation().equals(Hubs.DJY052.name());
        UpdateShipmentStatusScanResponse scanResponse = (UpdateShipmentStatusScanResponse) scanSteps.doScan(ApiUserRegistry.HO_TDY053_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION)
                .saveResponseObject(UpdateShipmentStatusScanResponse.class);
        assert scanResponse.getWorkflowStatus().equals(WorkFlowStatus.DELIVERED_TO_STATION);
        assert scanResponse.getWorkflowStatusForUser().equals(WorkFlowStatusForUser.IN_TRANSIT);
        assert scanResponse.getCurrentHub().equals(Hubs.TDY053.name());
        assert scanResponse.getPickupStation().equals(Hubs.TDY053.name());
        assert scanResponse.getCurrentLocation().equals("PETRONAS Taman Daya");
    }

}
