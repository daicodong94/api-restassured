package my.logistika.tests.delivery;

import io.qameta.allure.*;
import lombok.extern.java.Log;
import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.consignment.constants.ConsignmentApiShipmentStatus;
import my.logistika.microservices.consignment.constants.ConsignmentApiWorkFlowStatus;
import my.logistika.microservices.consignment.createShipment.models.CreateShipmentResponse;
import my.logistika.microservices.consignment.createShipment.models.ShipmentResponse;
import my.logistika.microservices.consignment.createShipment.steps.CreateShipmentSteps;
import my.logistika.microservices.consignment.getShipmentDetails.models.DetailShipmentResponseDto;
import my.logistika.microservices.consignment.getShipmentDetails.steps.GetShipmentByIdSteps;
import my.logistika.microservices.consignment.reverseShipment.steps.CreateReverseShipmentSteps;
import my.logistika.microservices.delivery.approveReturn.steps.ApproveReturnSteps;
import my.logistika.microservices.delivery.constants.PickupTypes;
import my.logistika.microservices.delivery.constants.WorkFlowStatus;
import my.logistika.microservices.delivery.constants.WorkFlowStatusForUser;
import my.logistika.microservices.delivery.deliveredToCustomer.steps.DeliveredToCustomerSteps;
import my.logistika.microservices.delivery.failedDelivery.steps.FailedDeliverySteps;
import my.logistika.microservices.delivery.getShipmentDetails.steps.GetShipmentDetailsByIdSteps;
import my.logistika.microservices.delivery.scanning.steps.UpdateShipmentStatusScanSteps;
import my.setel.utils.LoggerUtils;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.xml.soap.Detail;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.fail;
import static org.awaitility.Awaitility.await;

@Epic("Reverse shipment")
@Feature("Ops team should approve return on three failed delivery attempts")
public class ApproveReturnTests extends BaseTest {

    private final FailedDeliverySteps failedDeliverySteps = new FailedDeliverySteps();
    private final UpdateShipmentStatusScanSteps scanSteps = new UpdateShipmentStatusScanSteps();
    private final DeliveredToCustomerSteps deliveredToCustomerSteps = new DeliveredToCustomerSteps();
    private final CreateShipmentSteps createShipmentSteps = new CreateShipmentSteps();
    private final GetShipmentByIdSteps getShipmentByIdSteps = new GetShipmentByIdSteps();
    private final CreateReverseShipmentSteps createReverseShipmentSteps = new CreateReverseShipmentSteps();
    private final GetShipmentDetailsByIdSteps getShipmentDetailsByIdSteps = new GetShipmentDetailsByIdSteps();

    @BeforeMethod(alwaysRun = true)
    void setup() {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
    }

    @Test(description = "API delivery: Approve return")
    @Description("API: Delivery | Approve return negative path test | Reverse shipment should not be created when shipment status is not equal to Pending return")
    @Story("Reverse shipment approval by Ops")
    public void test_approve_return_does_not_work_at_other_workflow_status() throws Exception {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "60000", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        CreateShipmentResponse createShipmentResponse = shipmentResponse.getShipments().get(0);
        String shipmentId = createShipmentResponse.getShipmentId();
        String id = createShipmentResponse.getId();
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        assert createReverseShipmentSteps.when_a_post_request_is_made_to_create_reverse_shipment(shipmentId)
                .validateResponse(HttpStatus.SC_BAD_REQUEST).getJsonValue("message").equals("Not a delivery unsuccessful shipment");
    }

    @Test(description = "API delivery: Approve return")
    @Description("API: Delivery | Approve return negative path test | Reverse shipment should not be created when shipment status is not equal to Pending return")
    @Story("Reverse shipment approval by Ops")
    public void test_approve_return_does_not_work_when_out_for_delivery() throws Exception {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "60000", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        CreateShipmentResponse createShipmentResponse = shipmentResponse.getShipments().get(0);
        String shipmentId = createShipmentResponse.getShipmentId();
        String id = createShipmentResponse.getId();
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        scanSteps.doScan(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId, WorkFlowStatus.OUT_FOR_DELIVERY);
        assert createReverseShipmentSteps.when_a_post_request_is_made_to_create_reverse_shipment(shipmentId)
                .validateResponse(HttpStatus.SC_BAD_REQUEST).getJsonValue("message").equals("Not a delivery unsuccessful shipment");
    }

    @Test(description = "API delivery: Approve return")
    @Description("API: Delivery | Approve return negative path test | Reverse shipment should not be created when shipment status is Failed Delivery")
    @Story("Reverse shipment approval by Ops")
    public void test_approve_return_does_not_work_when_shipment_is_at_failed_delivery_status() throws Exception {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "60000", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        CreateShipmentResponse createShipmentResponse = shipmentResponse.getShipments().get(0);
        String shipmentId = createShipmentResponse.getShipmentId();
        String id = createShipmentResponse.getId();
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        scanSteps.doScan(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId, WorkFlowStatus.OUT_FOR_DELIVERY);
        failedDeliverySteps.fail_delivery_by_LMD(createShipmentResponse);
        assert createReverseShipmentSteps.when_a_post_request_is_made_to_create_reverse_shipment(shipmentId)
                .validateResponse(HttpStatus.SC_BAD_REQUEST).getJsonValue("message").equals("Not a delivery unsuccessful shipment");
    }


    @Test(description = "API delivery: approve return", groups = "regression")
    @Description("API: Delivery | Approve return action should create a reverse shipment")
    @Story("Reverse shipment approval by Ops")
    @TmsLink("COR-423")
    public void test_approve_return_same_pickup_delivery_station() throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "60000", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        CreateShipmentResponse createShipmentResponse = shipmentResponse.getShipments().get(0);
        String shipmentId = createShipmentResponse.getShipmentId();
        String id = createShipmentResponse.getId();
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        scanSteps.doScan(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId, WorkFlowStatus.OUT_FOR_DELIVERY);
        failedDeliverySteps.fail_delivery_by_LMD(createShipmentResponse);
        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        scanSteps.doScan(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId, WorkFlowStatus.OUT_FOR_DELIVERY);
        failedDeliverySteps.fail_delivery_by_LMD(createShipmentResponse);
        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        scanSteps.doScan(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId, WorkFlowStatus.OUT_FOR_DELIVERY);
        failedDeliverySteps.fail_delivery_by_LMD(createShipmentResponse);
        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERY_UNSUCCESSFUL);
        Map<String, String> updatesMap = new HashMap<>();
        updatesMap.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERY_UNSUCCESSFUL);
        updatesMap.put("workflowStatusForUser", ConsignmentApiShipmentStatus.PENDING_RETURN);

        getShipmentByIdSteps
                .then_shipment_details_must_be_updated_on_api_consignment(id, updatesMap).validateStatusCode(200);
        assert createReverseShipmentSteps.when_a_post_request_is_made_to_create_reverse_shipment(shipmentId)
                .validateResponse(201).getJsonValue("shipmentId").equals(shipmentId + "-RV");
        assert createReverseShipmentSteps.getJsonValue("workflowStatusForUser").equalsIgnoreCase(ConsignmentApiShipmentStatus.CREATED);
        assert createReverseShipmentSteps.getJsonValue("workflowStatus").equalsIgnoreCase(ConsignmentApiWorkFlowStatus.CREATED);

        String reverse_id = createReverseShipmentSteps.getJsonValue("id");

        updatesMap.clear();
        updatesMap.put("shipmentId",shipmentId+"-RV");
        updatesMap.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        updatesMap.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        try {
            await().
                    atMost(20, TimeUnit.SECONDS).
                    with().pollDelay(5, TimeUnit.SECONDS).pollInSameThread().
                    pollInterval(2, TimeUnit.SECONDS).
                    untilAsserted(() -> {
                        int sc = getShipmentByIdSteps
                                .then_shipment_details_must_be_returned_on_ADMIN_api_consignment(reverse_id, updatesMap).getResponse().getStatusCode();
                        LoggerUtils.getInstance().info(String.valueOf(sc));
                        LoggerUtils.getInstance().info(String.valueOf(sc == 200));
                        assert sc == 200;
                    });
        } catch (org.awaitility.core.ConditionTimeoutException e) {
            fail(e.getMessage());
        }
        updatesMap.put("shipmentId",shipmentId+"-RV");
        updatesMap.put("workflowStatus", WorkFlowStatus.DELIVERED_TO_STATION);
        updatesMap.put("workflowStatusForUser", WorkFlowStatusForUser.IN_TRANSIT);
        getShipmentDetailsByIdSteps.then_shipment_details_must_be_returned_as_expected(shipmentId+"-RV",updatesMap);


    }

    @Test(description = "API delivery: Returned to Merchant", groups = "regression")
    @Description("API: Delivery | Approve return action should create a reverse shipment")
    @Story("Reverse Shipment approval by Ops")
    @TmsLink("COR-423")
    public void test_delivered_To_Merchant_return_same_pickup_delivery_station() throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "60000", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        CreateShipmentResponse createShipmentResponse = shipmentResponse.getShipments().get(0);
        String shipmentId = createShipmentResponse.getShipmentId();
        String id = shipmentResponse.getShipments().get(0).getId();
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        Map<String, String> expectedUpdates = new HashMap<>();
        expectedUpdates.put("shipmentId", shipmentId);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);

        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId, WorkFlowStatus.OUT_FOR_DELIVERY);

        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        failedDeliverySteps.fail_delivery_by_LMD(createShipmentResponse);

        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.FAILED_DELIVERY_ATTEMPT);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.DELIVERY_FAILED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);

        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.DELIVERY_FAILED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId, WorkFlowStatus.OUT_FOR_DELIVERY);

        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        failedDeliverySteps.fail_delivery_by_LMD(createShipmentResponse);

        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.FAILED_DELIVERY_ATTEMPT);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.DELIVERY_FAILED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);

        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.DELIVERY_FAILED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId, WorkFlowStatus.OUT_FOR_DELIVERY);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);
        failedDeliverySteps.fail_delivery_by_LMD(createShipmentResponse);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.FAILED_DELIVERY_ATTEMPT);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.DELIVERY_FAILED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);
        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERY_UNSUCCESSFUL);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERY_UNSUCCESSFUL);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.PENDING_RETURN);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);
        assert createReverseShipmentSteps.when_a_post_request_is_made_to_create_reverse_shipment(shipmentId)
                .validateResponse(201).getJsonValue("shipmentId").equals(shipmentId + "-RV");
        assert createReverseShipmentSteps.getJsonValue("workflowStatusForUser").equalsIgnoreCase(ConsignmentApiShipmentStatus.CREATED);
        assert createReverseShipmentSteps.getJsonValue("workflowStatus").equalsIgnoreCase(ConsignmentApiWorkFlowStatus.CREATED);

        String reverse_id = createReverseShipmentSteps.getJsonValue("id");

        expectedUpdates.clear();
        expectedUpdates.put("shipmentId",shipmentId+"-RV");
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        try {
            await().
                    atMost(20, TimeUnit.SECONDS).
                    with().pollDelay(5, TimeUnit.SECONDS).pollInSameThread().
                    pollInterval(2, TimeUnit.SECONDS).
                    untilAsserted(() -> {
                        int sc = getShipmentByIdSteps
                                .then_shipment_details_must_be_returned_on_ADMIN_api_consignment(reverse_id, expectedUpdates).getResponse().getStatusCode();
                        LoggerUtils.getInstance().info(String.valueOf(sc));
                        LoggerUtils.getInstance().info(String.valueOf(sc == 200));
                        assert sc == 200;
                    });
        } catch (org.awaitility.core.ConditionTimeoutException e) {
            fail(e.getMessage());
        }
        expectedUpdates.put("shipmentId",shipmentId+"-RV");
        expectedUpdates.put("workflowStatus", WorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatusForUser", WorkFlowStatusForUser.IN_TRANSIT);
        getShipmentDetailsByIdSteps.then_shipment_details_must_be_returned_as_expected(shipmentId+"-RV",expectedUpdates);

        scanSteps.doScan(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId + "-RV", WorkFlowStatus.OUT_FOR_DELIVERY);
        try {
            await().
                    atMost(10, TimeUnit.SECONDS).
                    with().pollDelay(5, TimeUnit.SECONDS).pollInSameThread().
                    pollInterval(2, TimeUnit.SECONDS).
                    untilAsserted(() -> {
                        assertThat(deliveredToCustomerSteps.when_user_clicks_delivered_to_customer_button(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId + "-RV", "testPath", "true")
                                .getResponse().getStatusCode()).isEqualTo(HttpStatus.SC_CREATED);
                    });
        } catch (org.awaitility.core.ConditionTimeoutException e) {
            fail(e.getMessage());
        }
        Map<String, String> expected = new HashMap<>();
        expected.put("shipmentId", shipmentId + "-RV");
        expected.put("workflowStatus", WorkFlowStatus.RETURNED_TO_MERCHANT);
        expected.put("workflowStatusForUser", WorkFlowStatusForUser.RETURNED);
        deliveredToCustomerSteps.then_workflow_must_be_updated(expected);
        expectedUpdates.put("shipmentId", shipmentId);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.RETURNED_TO_MERCHANT);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.RETURNED);
        getShipmentByIdSteps
                .then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates).validateStatusCode(200);
    }
}
