package my.logistika.tests.delivery;

import io.qameta.allure.TmsLink;
import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.authentication.login.steps.LoginSteps;
import my.logistika.microservices.consignment.constants.ConsignmentApiShipmentStatus;
import my.logistika.microservices.consignment.createShipment.models.ShipmentResponse;
import my.logistika.microservices.consignment.createShipment.steps.CreateShipmentSteps;
import my.logistika.microservices.consignment.getShipmentDetails.steps.GetShipmentDetailsByTrackingToken;
import my.logistika.microservices.delivery.constants.PickupTypes;
import my.logistika.microservices.delivery.constants.WorkFlowStatus;
import my.logistika.microservices.delivery.constants.WorkFlowStatusForUser;
import my.logistika.microservices.delivery.scanning.steps.UpdateShipmentStatusScanSteps;
import my.setel.utils.LoggerUtils;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class EstimatedDeliveryDateTests extends BaseTest {
    private final LoginSteps loginSteps = new LoginSteps();
    private final CreateShipmentSteps createShipmentSteps = new CreateShipmentSteps();
    private final UpdateShipmentStatusScanSteps scanSteps = new UpdateShipmentStatusScanSteps();
    private final GetShipmentDetailsByTrackingToken getShipmentDetailsByTrackingToken = new GetShipmentDetailsByTrackingToken();

    @BeforeMethod(alwaysRun = true)
    void scanning_pre_conditions() {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
    }

    @Test(description = "API delivery: Estimated delivery date must be populated when shipment status is changed to In Transit", groups = "regression")
    @TmsLink("COR-475")
    public void test_estimated_delivery_date() throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "60000", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        String shipmentId = shipmentResponse.getShipments().get(0).getShipmentId();
        String trackingToken = shipmentResponse.getShipments().get(0).getTrackingToken();
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        Map<String, String> expectedUpdates = new HashMap<>();
        expectedUpdates.put("shipmentId", shipmentId);
        expectedUpdates.put("workflowStatus", WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        expectedUpdates.put("workflowStatusForUser", WorkFlowStatusForUser.IN_TRANSIT);
        LocalDate ld = LocalDate.now(DateTimeZone.forTimeZone(TimeZone.getTimeZone("Asia/Kuala_Lumpur")));
        LocalDate expectedDate = null;

        if (ld.getDayOfWeek() == 6) {
            expectedDate = ld.plusDays(2);
        } else {
            expectedDate=ld.plusDays(1);
        }
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM YYYY");
        String dt = sdf.format(expectedDate.toDate());
        LoggerUtils.getInstance().info(dt);
        expectedUpdates.put("estimatedDeliveryDate", dt);
        scanSteps.then_shipment_details_must_be_updated(expectedUpdates);
        expectedUpdates.clear();
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        expectedUpdates.put("estimatedDelivery", dt);
        getShipmentDetailsByTrackingToken.then_shipment_details_must_be_updated_on_consignment_service(shipmentId, trackingToken, expectedUpdates);
    }

    @Test(description = "API delivery: Estimated delivery date must be created when ABX shipment status is changed to In Transit", groups = "regression")
    @TmsLink("COR-476")
    public void test_estimated_delivery__date_standard_delivery_HO_scan() throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "30020", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        String shipmentId = shipmentResponse.getShipments().get(0).getShipmentId();
        String trackingToken = shipmentResponse.getShipments().get(0).getTrackingToken();
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        Map<String, String> expectedUpdates = new HashMap<>();
        expectedUpdates.put("shipmentId", shipmentId);
        expectedUpdates.put("workflowStatus", WorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatusForUser", WorkFlowStatusForUser.IN_TRANSIT);
        LocalDate ld = LocalDate.now(DateTimeZone.forTimeZone(TimeZone.getTimeZone("Asia/Kuala_Lumpur")));
        LocalDate fromDate = null;
        LocalDate toDate = null;
        switch (ld.getDayOfWeek()){
            case 1 :
            case 2 :
            case 7 :
                fromDate= ld.plusDays(2);
                toDate = ld.plusDays(4);
                break;
            case 3 :
            case 4 :
                fromDate= ld.plusDays(2);
                toDate = ld.plusDays(5);
                break;
            case 5 :
            case 6 :
                fromDate=ld.plusDays(3);
                toDate = ld.plusDays(5);
                break;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("MMM d YYYY");
        String from = sdf.format(fromDate.toDate());

        String to = sdf.format(toDate.toDate());
        String expectedDate = from + " - " + to;
        expectedUpdates.put("estimatedDeliveryDate", expectedDate);
        scanSteps.then_shipment_details_must_be_updated(expectedUpdates);
        expectedUpdates.clear();
        expectedUpdates.put("estimatedDelivery",expectedDate);
        expectedUpdates.put("shipmentId",shipmentId);
        getShipmentDetailsByTrackingToken
                .then_shipment_details_must_be_updated_on_consignment_service(shipmentId, trackingToken, expectedUpdates);
    }

    @Test(description = "API delivery: Estimated delivery date must be created when ABX shipment status is changed to In Transit", groups = "regression")
    @TmsLink("COR-476")
    public void test_estimated_delivery_date_standard_delivery() throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "30020", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        String shipmentId = shipmentResponse.getShipments().get(0).getShipmentId();
        String trackingToken = shipmentResponse.getShipments().get(0).getTrackingToken();
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        Map<String, String> expectedUpdates = new HashMap<>();
        expectedUpdates.put("shipmentId", shipmentId);
        expectedUpdates.put("workflowStatus", WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        expectedUpdates.put("workflowStatusForUser", WorkFlowStatusForUser.IN_TRANSIT);
        LocalDate ld = LocalDate.now(DateTimeZone.forTimeZone(TimeZone.getTimeZone("Asia/Kuala_Lumpur")));
        LocalDate fromDate = null;
        LocalDate toDate = null;
        switch (ld.getDayOfWeek()){
            case 1 :
            case 2 :
            case 7 :
                fromDate= ld.plusDays(2);
                toDate = ld.plusDays(4);
                break;
            case 3 :
            case 4 :
                fromDate= ld.plusDays(2);
                toDate = ld.plusDays(5);
                break;
            case 5 :
            case 6 :
                fromDate=ld.plusDays(3);
                toDate = ld.plusDays(5);
                break;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("MMM d YYYY");
        String from = sdf.format(fromDate.toDate());

        String to = sdf.format(toDate.toDate());
        String expectedDate = from + " - " + to;
        expectedUpdates.put("estimatedDeliveryDate", expectedDate);
        scanSteps.then_shipment_details_must_be_updated(expectedUpdates);
        expectedUpdates.clear();
        expectedUpdates.put("estimatedDelivery",expectedDate);
        expectedUpdates.put("shipmentId",shipmentId);
        getShipmentDetailsByTrackingToken
                .then_shipment_details_must_be_updated_on_consignment_service(shipmentId, trackingToken, expectedUpdates);
    }


}
