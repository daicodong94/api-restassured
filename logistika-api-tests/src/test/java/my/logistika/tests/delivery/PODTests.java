package my.logistika.tests.delivery;
import io.qameta.allure.TmsLink;
import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.authentication.login.steps.LoginSteps;
import my.logistika.microservices.consignment.createShipment.models.ShipmentResponse;
import my.logistika.microservices.consignment.createShipment.steps.CreateShipmentSteps;
import my.logistika.microservices.delivery.constants.Images;
import my.logistika.microservices.delivery.constants.PickupTypes;
import my.logistika.microservices.delivery.constants.WorkFlowStatusForUser;
import my.logistika.microservices.delivery.constants.*;
import my.logistika.microservices.delivery.proofOfDelivery.steps.PodConfirmSteps;
import my.logistika.microservices.delivery.proofOfDelivery.steps.PodUploadSteps;
import my.setel.utils.LoggerUtils;
import org.testng.annotations.Test;
import java.io.IOException;


public class PODTests extends BaseTest {

    private final PodConfirmSteps podConfirmSteps = new PodConfirmSteps();
    private final LoginSteps loginSteps = new LoginSteps();
    private final CreateShipmentSteps createShipmentSteps = new CreateShipmentSteps();
    PodUploadSteps podUploadSteps = new PodUploadSteps();

    @Test(description = "POD images must be uploaded successfully", groups = "regression")
    @TmsLink("COR-440")
    public void pod_upload_and_confirm_test() throws IOException {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("99998", "99998", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        String shipmentId = shipmentResponse.getShipments().get(0).getShipmentId();
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);

        String token_admin = loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL).getJsonValue("id_token");
        String imageURLBody = podUploadSteps.when_user_uploads_a_pod_image(PODType.image.name(), Images.IMAGE, token_admin).asString();
        LoggerUtils.getInstance().info(imageURLBody);

        String signURLBody = podUploadSteps.when_user_uploads_a_pod_image(PODType.signature.name(), Images.SIGNATURE, token_admin).asString();
        LoggerUtils.getInstance().info(signURLBody);

        podConfirmSteps.when_image_and_signature_urls_confirmed(shipmentId, imageURLBody, signURLBody)
                .then_images_should_be_successfully_uploaded(signURLBody, imageURLBody);
    }


    @Test(description = "pod upload - negative path test")
    @TmsLink("COR-444")
    public void test_pod_upload_negative_path_test() {
        String token_admin = loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL).getJsonValue("id_token");
        assert podUploadSteps.when_user_uploads_a_pod_image("test",Images.SIGNATURE,token_admin).getStatusCode() == 400;
    }
}
