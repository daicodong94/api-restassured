package my.logistika.tests.delivery;
import io.qameta.allure.TmsLink;
import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.authentication.login.steps.LoginSteps;
import my.logistika.microservices.consignment.createShipment.models.ShipmentResponse;
import my.logistika.microservices.consignment.createShipment.steps.CreateShipmentSteps;
import my.logistika.microservices.delivery.constants.*;
import my.logistika.microservices.delivery.deliveredToCustomer.steps.DeliveredToCustomerSteps;
import my.logistika.microservices.delivery.getShipmentDetails.models.GetShipmentDetailsResponse;
import my.logistika.microservices.delivery.getShipmentDetails.steps.GetShipmentDetailsByIdSteps;
import my.logistika.microservices.delivery.getShipmentDetails.steps.GetShipmentsSteps;
import my.logistika.microservices.delivery.scanning.steps.UpdateShipmentStatusScanSteps;
import my.setel.utils.LoggerUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetShipmentTests extends BaseTest {

    private final GetShipmentDetailsByIdSteps getShipmentDetailsByIdSteps = new GetShipmentDetailsByIdSteps();
    private final UpdateShipmentStatusScanSteps scanSteps = new UpdateShipmentStatusScanSteps();
    private final CreateShipmentSteps createShipmentSteps = new CreateShipmentSteps();
    private final DeliveredToCustomerSteps deliveredToCustomerSteps = new DeliveredToCustomerSteps();
    private final LoginSteps loginSteps = new LoginSteps();
    private final GetShipmentsSteps getShipmentsSteps = new GetShipmentsSteps();

    @BeforeMethod(alwaysRun = true)
    public void setup(){
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
    }

    @Test(description = "API delivery: list of shipments must be returned based on the page num and per page input",groups = "regression")
    @TmsLink("COR-426")
    public void get_shipments_list(){
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        List<GetShipmentDetailsResponse> shipmentList = getShipmentsSteps.when_a_get_request_is_made_on_shipments_endpoint(2,20).saveResponseListObject(GetShipmentDetailsResponse[].class);
        assert shipmentList.size() != 0;
        assert shipmentList.size() == 20;
    }

    @Test(description = "API delivery: shipment details must be returned on GET request - Happy path",groups = "regression")
    @TmsLink("COR-425")
    public void get_shipment_details_test() throws InterruptedException {
        String hub = Hubs.DJY052.name();
        String postalCode = "60000";
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000","60000", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        String shipmentId = shipmentResponse.getShipments().get(0).getShipmentId();
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        Map<String,String> expectedUpdates = new HashMap<>();
        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        expectedUpdates.put("workflowStatus",WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        expectedUpdates.put("workflowStatusForUser",WorkFlowStatusForUser.IN_TRANSIT);
        getShipmentDetailsByIdSteps
                .then_shipment_details_must_be_returned_as_expected(shipmentId,expectedUpdates);
        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatus",WorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatusForUser",WorkFlowStatusForUser.IN_TRANSIT);
        getShipmentDetailsByIdSteps
                .then_shipment_details_must_be_returned_as_expected(shipmentId,expectedUpdates);
        scanSteps.doScan(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId, WorkFlowStatus.OUT_FOR_DELIVERY);
        expectedUpdates.put("workflowStatus",WorkFlowStatus.OUT_FOR_DELIVERY);
        expectedUpdates.put("workflowStatusForUser",WorkFlowStatusForUser.OUT_FOR_DELIVERY);
        getShipmentDetailsByIdSteps
                .then_shipment_details_must_be_returned_as_expected(shipmentId,expectedUpdates);

        deliveredToCustomerSteps.when_user_clicks_delivered_to_customer_button(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId, "path", "false");
        expectedUpdates.put("shipmentId", shipmentId);
        expectedUpdates.put("workflowStatus", WorkFlowStatus.DELIVERED_TO_CUSTOMER);
        expectedUpdates.put("workflowStatusForUser", WorkFlowStatusForUser.COMPLETED);
        expectedUpdates.put("shippingProvider", "Logistika");
        expectedUpdates.put("deliveryRegion", "KV");
        expectedUpdates.put("parcelSize", ParcelSize.S.name());
        expectedUpdates.put("pickupStation", hub);
        expectedUpdates.put("deliveryStation", hub);
        expectedUpdates.put("currentHub", hub);
        expectedUpdates.put("nextHub", "");
        expectedUpdates.put("initialPickupType", PickupTypes.PICKUP);
        expectedUpdates.put("qrCode", shipmentId);
        expectedUpdates.put("deliveredTo", "Customer");
        expectedUpdates.put("deliveryOption", "Delivery");
        expectedUpdates.put("deliveryAddressType", "High Rise");
        expectedUpdates.put("signature", "path");
        expectedUpdates.put("proofOfDelivery", "path");
        expectedUpdates.put("currentLocation", "PETRONAS Damansara Jaya 2");
        expectedUpdates.put("senderPostalCode", postalCode);
        expectedUpdates.put("receiverPostalCode", postalCode);
        expectedUpdates.put("isLineHaulShipment", "false");
        expectedUpdates.put("isReceiverOfficeAddress", "false");

        getShipmentDetailsByIdSteps
                .then_shipment_details_must_be_returned_as_expected(shipmentId,expectedUpdates);
    }


    @Test(description = "API delivery: get shipment endpoint should return 404 when invalid id is given as input",groups = "regression")
    @TmsLink("COR-431")
    public void get_shipment_details_negative_path_test() throws InterruptedException {

        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        getShipmentDetailsByIdSteps
                .when_a_get_request_is_made_on_shipments_endpoint("invalid")
                .validateResponse(404);

    }


}
