package my.logistika.tests.delivery;
import my.logistika.base.BaseTest;
import my.logistika.constants.DateFormats;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.authentication.login.steps.LoginSteps;
import my.logistika.microservices.consignment.createShipment.models.ShipmentResponse;
import my.logistika.microservices.consignment.createShipment.steps.CreateShipmentSteps;
import my.logistika.microservices.delivery.constants.PickupTypes;
import my.logistika.microservices.delivery.constants.WorkFlowStatus;
import my.logistika.microservices.delivery.constants.WorkFlowStatusForUser;
import my.logistika.microservices.delivery.scanning.steps.UpdateShipmentStatusScanSteps;
import my.logistika.microservices.delivery.shippingProviderUpdates.models.ABXShipmentUpdatesReqBodyDto;
import my.logistika.microservices.delivery.shippingProviderUpdates.steps.ABXShipmentUpdatesSteps;
import my.setel.utils.CommonUtils;
import my.setel.utils.LoggerUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class ABXShipmentUpdatesTests extends BaseTest {

    private final ABXShipmentUpdatesSteps abxShipmentUpdates = new ABXShipmentUpdatesSteps();
    private final CreateShipmentSteps createShipmentSteps = new CreateShipmentSteps();
    private final UpdateShipmentStatusScanSteps scanSteps = new UpdateShipmentStatusScanSteps();
    private final LoginSteps loginSteps = new LoginSteps();
    private ABXShipmentUpdatesReqBodyDto abxShipmentUpdatesReqBodyDto;

    @BeforeMethod(alwaysRun = true)
    public void authorize(){
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
    }

    @DataProvider(name = "abx_workflow_status")
    public Object[][] abx_workflow_status() {
        final Object[][] objects = {
                {"Transferred to courier","010", "Shipment picked up"},
                {"Out for delivery","045", "Out for delivery"},
                {"Delivered to customer","POD", "Delivery successful"}
        };
        return objects;
    }

    @Test(description = "API Delivery: ABX | 010, 045, POD", groups = "regression",dataProvider ="abx_workflow_status")
    public void test_abx_updates_on_api_delivery(String TSshipmentStatus,String abxStatusCode,String abxShipmentStatus) throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "30020", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        String shipmentId = shipmentResponse.getShipments().get(0).getShipmentId();
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId,WorkFlowStatus.DELIVERED_TO_STATION);
        scanSteps.doScan(ApiUserRegistry.MMD_DJY052_TDY053_KV.username, shipmentId,WorkFlowStatus.PICKED_UP_FROM_STATION);
        scanSteps.doScan(ApiUserRegistry.HO_TDY053_KV.username, shipmentId,WorkFlowStatus.DELIVERED_TO_STATION);
        String location = CommonUtils.getRandomFiveCharsString(5);
        String abxRefNumber= CommonUtils.getRandomFiveCharsString(5);
        Date date = new Date();
        DateFormats.sdfYMDHMS.setTimeZone(TimeZone.getTimeZone("Asia/Kuala_Lumpur"));
        String abxStatusUpdatedDate = DateFormats.sdfYMDHMS.format(date);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DateFormats.sdfYMDHMS.toPattern());
        LocalDateTime datetime =LocalDateTime.parse(abxStatusUpdatedDate,formatter);
        datetime=datetime.minusHours(4);
        String abxStatusDate=datetime.format(formatter);
        abxShipmentUpdatesReqBodyDto = new ABXShipmentUpdatesReqBodyDto(shipmentId,TSshipmentStatus,abxShipmentStatus,location,abxStatusCode,abxRefNumber,abxStatusDate, abxStatusUpdatedDate);
        abxShipmentUpdates.when_a_post_request_is_made_to_shipping_provider_updates(abxShipmentUpdatesReqBodyDto).validateStatusCode(201);
    }

    @Test(description = "API Delivery: ABX | 010_045_POD", groups = "regression")
    public void test_abx_updates_on_api_delivery_010_045_POD() throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "30020", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        String shipmentId = shipmentResponse.getShipments().get(0).getShipmentId();

        Date date = new Date();
        DateFormats.sdfYMDHMS.setTimeZone(TimeZone.getTimeZone("Asia/Kuala_Lumpur"));
        String abxStatusUpdatedDate = DateFormats.sdfYMDHMS.format(date);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DateFormats.sdfYMDHMS.toPattern());
        LocalDateTime datetime =LocalDateTime.parse(abxStatusUpdatedDate,formatter);
        datetime=datetime.minusHours(4);
        String abxStatusDate=datetime.format(formatter);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId,WorkFlowStatus.DELIVERED_TO_STATION);
        scanSteps.doScan(ApiUserRegistry.MMD_DJY052_TDY053_KV.username, shipmentId,WorkFlowStatus.PICKED_UP_FROM_STATION);
        scanSteps.doScan(ApiUserRegistry.HO_TDY053_KV.username, shipmentId,WorkFlowStatus.DELIVERED_TO_STATION);
        abxShipmentUpdatesReqBodyDto = new ABXShipmentUpdatesReqBodyDto(shipmentId,"Transferred to courier", "Shipment picked up",CommonUtils.getRandomFiveCharsString(9),"010",CommonUtils.getRandomFiveCharsString(5),abxStatusDate, abxStatusUpdatedDate);
        abxShipmentUpdates.when_a_post_request_is_made_to_shipping_provider_updates(abxShipmentUpdatesReqBodyDto).validateStatusCode(201);
        abxShipmentUpdatesReqBodyDto = new ABXShipmentUpdatesReqBodyDto(shipmentId,"Out for delivery", "Out for delivery",CommonUtils.getRandomFiveCharsString(9),"045",CommonUtils.getRandomFiveCharsString(5),abxStatusDate,abxStatusUpdatedDate);
        abxShipmentUpdates.when_a_post_request_is_made_to_shipping_provider_updates(abxShipmentUpdatesReqBodyDto).validateStatusCode(201);
        abxShipmentUpdatesReqBodyDto = new ABXShipmentUpdatesReqBodyDto(shipmentId,"Delivered to Customer", "Delivery successful",CommonUtils.getRandomFiveCharsString(9),"POD",CommonUtils.getRandomFiveCharsString(5),abxStatusDate,abxStatusUpdatedDate);
        abxShipmentUpdates.when_a_post_request_is_made_to_shipping_provider_updates(abxShipmentUpdatesReqBodyDto).validateStatusCode(201);
    }
}
