package my.logistika.tests.delivery;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.TmsLink;
import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.consignment.constants.ConsignmentApiShipmentStatus;
import my.logistika.microservices.consignment.constants.ConsignmentApiWorkFlowStatus;
import my.logistika.microservices.consignment.createShipment.models.CreateShipmentResponse;
import my.logistika.microservices.consignment.createShipment.models.ShipmentResponse;
import my.logistika.microservices.consignment.createShipment.steps.CreateShipmentSteps;
import my.logistika.microservices.consignment.getShipmentDetails.steps.GetShipmentByIdSteps;
import my.logistika.microservices.delivery.constants.ParcelSize;
import my.logistika.microservices.delivery.constants.PickupTypes;
import my.logistika.microservices.delivery.constants.WorkFlowStatus;
import my.logistika.microservices.delivery.constants.WorkFlowStatusForUser;
import my.logistika.microservices.delivery.deliveredToCustomer.steps.DeliveredToCustomerSteps;
import my.logistika.microservices.delivery.scanning.steps.UpdateShipmentStatusScanSteps;
import my.setel.utils.LoggerUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

@Epic("Proof of delivery")
@Feature("A Last mile driver should be able to update the Receiver's signature and proof of delivery to complete the delivery")
public class DeliveredToCustomerTests extends BaseTest {

    DeliveredToCustomerSteps deliveredToCustomerSteps = new DeliveredToCustomerSteps();
    CreateShipmentSteps createShipmentSteps = new CreateShipmentSteps();
    UpdateShipmentStatusScanSteps scanSteps = new UpdateShipmentStatusScanSteps();
    GetShipmentByIdSteps getShipmentByIdSteps = new GetShipmentByIdSteps();

    @BeforeMethod(alwaysRun = true)
    void scanning_pre_conditions() {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
    }

    @Test(description = "A last mile driver should be able to complete the delivery by uploading proof of delivery and receiver's signature", groups = "regression")
    @TmsLink("COR-414")
    public void test_delivered_to_customer_by_LMD() throws InterruptedException {
        String hub = "DJY052";
        String postalCode = "60000";
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "60000", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        CreateShipmentResponse createShipmentResponse = shipmentResponse.getShipments().get(0);
        String shipmentId = createShipmentResponse.getShipmentId();
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);

        String id = createShipmentResponse.getId();
        Map<String, String> updatesMap = new HashMap<>();

        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        updatesMap.put("shipmentId", shipmentId);
        updatesMap.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        updatesMap.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, updatesMap);
      //  updatesMap.put("updatedBy", "DJY052 FMD Admin");
       // updatesMap.put("updatedByUserId", "61a5ce0f0cfac8b217b30f92");
        getShipmentByIdSteps.then_shipment_details_must_be_returned_on_ADMIN_api_consignment(id, updatesMap);

        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        updatesMap.clear();
        updatesMap.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        updatesMap.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, updatesMap);
        updatesMap.put("updatedBy", "DJY052 HO 1");
        updatesMap.put("updatedByUserId", "61a5ce1844d1d5ea2319dc74");
        getShipmentByIdSteps.then_shipment_details_must_be_returned_on_ADMIN_api_consignment(id, updatesMap);

        scanSteps.doScan(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId, WorkFlowStatus.OUT_FOR_DELIVERY);
        updatesMap.clear();
        updatesMap.put("workflowStatus", ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        updatesMap.put("workflowStatusForUser", ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, updatesMap);
        updatesMap.put("updatedBy", "DJY052 LMD ADMIN");
        updatesMap.put("updatedByUserId", "61a5ce0a54ee386c4c5b2067");
        getShipmentByIdSteps.then_shipment_details_must_be_returned_on_ADMIN_api_consignment(id, updatesMap);
        deliveredToCustomerSteps.when_user_clicks_delivered_to_customer_button(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId, "path", "false");
        Map<String, String> expected = new HashMap<>();
        expected.put("shipmentId", shipmentId);
        expected.put("workflowStatus", WorkFlowStatus.DELIVERED_TO_CUSTOMER);
        expected.put("workflowStatusForUser", WorkFlowStatusForUser.COMPLETED);
        expected.put("shippingProvider", "Logistika");
        expected.put("deliveryRegion", "KV");
        expected.put("parcelSize", ParcelSize.S.name());
        expected.put("pickupStation", hub);
        expected.put("deliveryStation", hub);
        expected.put("currentHub", hub);
        expected.put("nextHub", "");
        expected.put("initialPickupType", PickupTypes.PICKUP);
        expected.put("qrCode", shipmentId);
        expected.put("deliveredTo", "Customer");
        expected.put("deliveryOption", "Delivery");
        expected.put("deliveryAddressType", "High Rise");
        expected.put("signature", "path");
        expected.put("proofOfDelivery", "path");
        expected.put("currentLocation", "PETRONAS Damansara Jaya 2");
        expected.put("senderPostalCode", postalCode);
        expected.put("receiverPostalCode", postalCode);
        expected.put("currentOwner", "61a5ce1b44d1d5625919dc75");
        expected.put("assignedDriver", "61a5ce0a54ee386c4c5b2067");
        expected.put("isLineHaulShipment", "false");
        expected.put("isReceiverOfficeAddress", "false");
        assert !deliveredToCustomerSteps.then_workflow_must_be_updated(expected)
                .getJsonValue("lastMileCompletedOn").isEmpty();
        updatesMap.clear();
        updatesMap.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_CUSTOMER);
        updatesMap.put("workflowStatusForUser", ConsignmentApiShipmentStatus.COMPLETED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updatesMap);
        updatesMap.put("updatedBy", "DJY052 LMD Admin");
        updatesMap.put("updatedByUserId", "61a5ce0a54ee386c4c5b2067");
        getShipmentByIdSteps.then_shipment_details_must_be_returned_on_ADMIN_api_consignment(id, updatesMap);
    }
}
