package my.logistika.tests.delivery;

import io.qameta.allure.TmsLink;
import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.consignment.constants.ConsignmentApiShipmentStatus;
import my.logistika.microservices.consignment.constants.ConsignmentApiWorkFlowStatus;
import my.logistika.microservices.consignment.createShipment.models.ShipmentResponse;
import my.logistika.microservices.consignment.createShipment.steps.CreateShipmentSteps;
import my.logistika.microservices.consignment.getShipmentDetails.steps.GetShipmentByIdSteps;
import my.logistika.microservices.delivery.constants.PickupTypes;
import my.logistika.microservices.delivery.constants.WorkFlowStatus;
import my.logistika.microservices.delivery.constants.WorkFlowStatusForUser;
import my.logistika.microservices.delivery.deliveredToCustomer.steps.DeliveredToCustomerSteps;
import my.logistika.microservices.delivery.scanning.steps.UpdateShipmentStatusScanSteps;
import my.setel.utils.LoggerUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class ShipmentUpdatedEventTests extends BaseTest {
    private final CreateShipmentSteps createShipmentSteps = new CreateShipmentSteps();
    private final UpdateShipmentStatusScanSteps scanSteps = new UpdateShipmentStatusScanSteps();
    private final GetShipmentByIdSteps getShipmentByIdSteps = new GetShipmentByIdSteps();
    private final DeliveredToCustomerSteps deliveredToCustomerSteps = new DeliveredToCustomerSteps();

    @BeforeMethod(alwaysRun = true)
    public void setup() {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
    }

    @Test(description = "API Delivery: Publish shipment updates to rabbitMQ", groups = "regression")
    @TmsLink(value = "COR-455")
    public void test_workflow_updates_to_sender() throws InterruptedException {
        String FMH = "DJY052";
        String IChub = "TDY053";
        String LMH = "TPA062";
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "68000", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        String shipmentId = shipmentResponse.getShipments().get(0).getShipmentId();
        String id = shipmentResponse.getShipments().get(0).getId();
        LoggerUtils.getInstance().info("id is " + id);
        LoggerUtils.getInstance().info("shipment Id is " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan("FMD_" + FMH.toUpperCase() + "_admin@logistika.com", shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        Map<String, String> expectedUpdates = new HashMap<>();
        expectedUpdates.put("shipmentId", shipmentId);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan("HO_" + FMH.toUpperCase() + "_1@logistika.com", shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan("MMD_KV_S1_F_1@logistika.com", shipmentId, WorkFlowStatus.PICKED_UP_FROM_STATION);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan("HO_" + IChub.toUpperCase() + "_1@logistika.com", shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatus",  ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan("MMD_KV_S2_R_1@logistika.com", shipmentId, WorkFlowStatus.PICKED_UP_FROM_STATION);
        expectedUpdates.put("workflowStatus",ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_STATION);
        expectedUpdates.put("workflowStatusForUser",ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan("HO_" + LMH.toUpperCase() + "_1@logistika.com", shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan("LMD_" + LMH.toUpperCase() + "_admin@logistika.com", shipmentId, WorkFlowStatus.OUT_FOR_DELIVERY);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);
        deliveredToCustomerSteps.when_user_clicks_delivered_to_customer_button(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId, "path", "false");
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_CUSTOMER);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.COMPLETED);
        expectedUpdates.put("proofOfDelivery","path");
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

    }

}
