package my.logistika.tests.delivery.Scanning;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.TmsLink;
import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.consignment.constants.ConsignmentApiShipmentStatus;
import my.logistika.microservices.consignment.constants.ConsignmentApiWorkFlowStatus;
import my.logistika.microservices.consignment.createShipment.models.ShipmentResponse;
import my.logistika.microservices.consignment.createShipment.steps.CreateShipmentSteps;
import my.logistika.microservices.consignment.getShipmentDetails.steps.GetShipmentByIdSteps;
import my.logistika.microservices.delivery.constants.Courier;
import my.logistika.microservices.delivery.constants.PickupTypes;
import my.logistika.microservices.delivery.constants.WorkFlowStatus;
import my.logistika.microservices.delivery.constants.WorkFlowStatusForUser;
import my.logistika.microservices.delivery.scanning.models.UpdateShipmentStatusScanRequest;
import my.logistika.microservices.delivery.scanning.steps.UpdateShipmentStatusScanSteps;
import my.setel.utils.CommonUtils;
import my.setel.utils.LoggerUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;


@Epic("Scan barcode functionality for fulfilment partner app")
@Feature("A delivery partner or an Hub operator should be able to scan a shipping label and transit the workflow")
public class ScanningTests extends BaseTest {
    private final CreateShipmentSteps createShipmentSteps = new CreateShipmentSteps();
    private final UpdateShipmentStatusScanSteps scanSteps = new UpdateShipmentStatusScanSteps();
    private final GetShipmentByIdSteps getShipmentByIdSteps = new GetShipmentByIdSteps();
    private UpdateShipmentStatusScanRequest scanRequest;

    @BeforeMethod(alwaysRun = true)
    void scanning_pre_conditions() {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
    }

    @Test(description = "API Delivery: Update shipment status on scan when there are multiple IC Hubs", groups = "smoke")
    public void test_update_shipment_status_on_scan_multiple_ICHubs() throws InterruptedException {
        String FMH = "DJY052";
        String LMH = "TEST011";
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "99999", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        String shipmentId = shipmentResponse.getShipments().get(0).getShipmentId();
        LoggerUtils.getInstance().info("Shipment created with shipment ID " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan("FMD_" + FMH.toUpperCase() + "_admin@logistika.com", shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        scanSteps.doScan("HO_" + FMH.toUpperCase() + "_1@logistika.com", shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        scanSteps.doScan("MMD_KV_S1_F_1@logistika.com", shipmentId, WorkFlowStatus.PICKED_UP_FROM_STATION);
        scanSteps.doScan("HO_TDY053_1@logistika.com", shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        scanSteps.doScan("MMD_KV_S2_R_1@logistika.com", shipmentId, WorkFlowStatus.PICKED_UP_FROM_STATION);
        scanSteps.doScan("HO_TPA062_1@logistika.com", shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        scanSteps.doScan("MMD_PNG_S1_R_1@logistika.com", shipmentId, WorkFlowStatus.PICKED_UP_FROM_STATION);
        scanSteps.doScan("HO_TEST010_1@logistika.com", shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        scanSteps.doScan("MMD_TEST_S1_R_1@logistika.com", shipmentId, WorkFlowStatus.PICKED_UP_FROM_STATION);
        scanSteps.doScan("HO_" + LMH.toUpperCase() + "_1@logistika.com", shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        scanSteps.doScan("LMD_TEST011_bike_1@logistika.com", shipmentId, WorkFlowStatus.OUT_FOR_DELIVERY);
    }

    @Test(description = "API Delivery: Update shipment status on scan", groups = "regression", dataProvider = "same_first_mile_hub_last_mile_hub")
    public void test_update_shipment_status_on_scan_same_pickup_delivery_station(String postalCode, String hub, String pickupType, String workFlowStatusforUser) throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment(postalCode, postalCode, pickupType, workFlowStatusforUser)
                .saveResponseObject(ShipmentResponse.class);
        String shipmentId = shipmentResponse.getShipments().get(0).getShipmentId();
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan("FMD_" + hub.toUpperCase() + "_admin@logistika.com", shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        scanSteps.doScan("HO_" + hub.toUpperCase() + "_1@logistika.com", shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        scanSteps.doScan("LMD_" + hub.toUpperCase() + "_admin@logistika.com", shipmentId, WorkFlowStatus.OUT_FOR_DELIVERY);
    }

    @Test(description = "API Delivery: Update shipment status on scan when First mile hub and Last mile hub are different", groups = "api", dataProvider = "different_first_mile_hub_last_mile_hub")
    @TmsLink(value = "COR-409")
    public void test_update_shipment_status_on_scan_different_pickup_delivery_station(String senderCode, String FMH, String receiverPostalCode, String LMH) throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment(senderCode, receiverPostalCode, PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        String shipmentId = shipmentResponse.getShipments().get(0).getShipmentId();
        LoggerUtils.getInstance().info("Shipment has been created with " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan("FMD_" + FMH.toUpperCase() + "_admin@logistika.com", shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        scanSteps.doScan("HO_" + FMH.toUpperCase() + "_1@logistika.com", shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        scanSteps.doScan("MMD_KV_S1_F_1@logistika.com", shipmentId, WorkFlowStatus.PICKED_UP_FROM_STATION);
        scanSteps.doScan("HO_" + LMH.toUpperCase() + "_1@logistika.com", shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        scanSteps.doScan("LMD_" + LMH.toUpperCase() + "_admin@logistika.com", shipmentId, WorkFlowStatus.OUT_FOR_DELIVERY);
    }

    @Test(description = "API Delivery: Update shipment status on scan when courier is abx", groups = "regression", dataProvider = "abx_postal_codes")
    public void test_update_shipment_status_on_scan_abx(String senderCode, String FMH, String receiverPostalCode, String LMH) throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment(senderCode, receiverPostalCode, PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        String shipmentId = shipmentResponse.getShipments().get(0).getShipmentId();
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan("FMD_" + FMH.toUpperCase() + "_admin@logistika.com", shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        scanSteps.doScan("HO_" + FMH.toUpperCase() + "_1@logistika.com", shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        scanSteps.doScan("MMD_KV_S1_F_1@logistika.com", shipmentId, WorkFlowStatus.PICKED_UP_FROM_STATION);
        scanSteps.doScan("HO_" + LMH.toUpperCase() + "_1@logistika.com", shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
    }


    @Test(description = "API Delivery: Update shipment status on scan when shipment goes through IC HUB", groups = "regression", dataProvider = "IC_HUB_SHIPMENTS")
    public void test_update_shipment_status_on_scan_single_IC_Hub(String senderCode, String FMH, String receiverPostalCode, String LMH, String IChub) throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment(senderCode, receiverPostalCode, PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
               .saveResponseObject(ShipmentResponse.class);
        String shipmentId = shipmentResponse.getShipments().get(0).getShipmentId();
        LoggerUtils.getInstance().info("shipment Id is " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan("FMD_" + FMH.toUpperCase() + "_admin@logistika.com", shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        scanSteps.doScan("HO_" + FMH.toUpperCase() + "_1@logistika.com", shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        scanSteps.doScan("MMD_KV_S1_F_1@logistika.com", shipmentId, WorkFlowStatus.PICKED_UP_FROM_STATION);
        scanSteps.doScan("HO_" + IChub.toUpperCase() + "_1@logistika.com", shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        scanSteps.doScan("MMD_KV_S2_R_1@logistika.com", shipmentId, WorkFlowStatus.PICKED_UP_FROM_STATION);
        scanSteps.doScan("HO_" + LMH.toUpperCase() + "_1@logistika.com", shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        scanSteps.doScan("LMD_" + LMH.toUpperCase() + "_admin@logistika.com", shipmentId, WorkFlowStatus.OUT_FOR_DELIVERY);
    }


    @Test(description = "API Delivery: Update shipment status on scan when shipment goes through Linehaul", groups = "regression", dataProvider = "LINEHAUL_SHIPMENTS")
    public void test_update_shipment_status_on_scan_Linehaul(String senderCode, String FMH, String receiverPostalCode, String LMH, String IChub1) throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse)createShipmentSteps.createShipment(senderCode, receiverPostalCode, PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        String shipmentId = shipmentResponse.getShipments().get(0).getShipmentId();
        String id = shipmentResponse.getShipments().get(0).getId();
        LoggerUtils.getInstance().info("shipment Id is " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan("FMD_" + FMH.toUpperCase() + "_admin@logistika.com", shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        Map<String,String> updates = new HashMap<>();
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);

        scanSteps.doScan("HO_" + FMH.toUpperCase() + "_1@logistika.com", shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);

        scanSteps.doScan(ApiUserRegistry.MMD_DJY052_TDY053_KV.username, shipmentId,WorkFlowStatus.PICKED_UP_FROM_STATION);
        assert scanSteps.getJsonValue("isLineHaulShipment").equalsIgnoreCase("false");

        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_STATION);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);


        scanSteps.doScan("HO_" + IChub1.toUpperCase() + "_1@logistika.com", shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);

        scanSteps.doScan(ApiUserRegistry.MMD_TDY053_LDC081.username,shipmentId,WorkFlowStatus.TRANSFERRED_TO_COURIER);

        assert scanSteps.getJsonValue("isLineHaulShipment").equalsIgnoreCase("true");
        assert scanSteps.getJsonValue("shippingProvider").equals(Courier.logistikaLinehaul.name());

        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.TRANSFERRED_TO_COURIER);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);

        scanSteps.doScan("HO_LDC081_1@logistika.com", shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);

        scanSteps.doScan(ApiUserRegistry.MMD_LDC081_MTR080.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_STATION);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_STATION);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);

        scanSteps.doScan("HO_" + LMH.toUpperCase() + "_1@logistika.com", shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);

        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);

        scanSteps.doScan("LMD_" + LMH.toUpperCase() + "_admin@logistika.com", shipmentId, WorkFlowStatus.OUT_FOR_DELIVERY);

        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);
    }

    @Test(description = "API Delivery: Update shipment status on scan - negative path", groups = "regression", dataProvider = "same_first_mile_hub_last_mile_hub")
    public void test_update_shipment_status_on_scan_negative_path(String postalCode, String hub, String pickupType, String workFlowStatusForUser) throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse)createShipmentSteps.createShipment(postalCode, postalCode, pickupType, workFlowStatusForUser)
                .saveResponseObject(ShipmentResponse.class);
        String shipmentId = shipmentResponse.getShipments().get(0).getShipmentId();
        LoggerUtils.getInstance().info("shipment Id is " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan("FMD_" + hub.toUpperCase() + "_admin@logistika.com", shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        scanRequest = new UpdateShipmentStatusScanRequest("HO_" + hub.toUpperCase() + "_1@logistika.com", CommonUtils.getRandomFiveCharsString(9));
        assert scanSteps.when_POST_request_is_made_on_scannning_endpoint(scanRequest)
                .validateResponse(404).getJsonValue("description").equals("Shipment does not found.");
    }

    @Test(description = "API Delivery: Update shipment status on scan - should not allow other hubs to scan", groups = "regression", dataProvider = "same_first_mile_hub_last_mile_hub")
    public void test_update_shipment_status_on_scan_negative_path_invalid_email(String postalCode, String hub, String pickupType, String workFlowStatusForUser) throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse)createShipmentSteps.createShipment(postalCode, postalCode, pickupType, workFlowStatusForUser)
                .saveResponseObject(ShipmentResponse.class);
        String shipmentId = shipmentResponse.getShipments().get(0).getShipmentId();
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan("FMD_" + hub.toUpperCase() + "_admin@logistika.com", shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        scanRequest = new UpdateShipmentStatusScanRequest("HO_TDY053_1@logistika.com", shipmentId);
        scanSteps.when_POST_request_is_made_on_scannning_endpoint(scanRequest)
                .validateResponse(400);
    }

    @DataProvider(name = "same_first_mile_hub_last_mile_hub")
    public Object[][] same_pickup_delivery_hub() {
        final Object[][] objects = {
                {"60000", "DJY052", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP},
        };
        return objects;
    }

    @DataProvider(name = "different_first_mile_hub_last_mile_hub")
    public Object[][] different_pickup_delivery_hub() {
        final Object[][] objects = {
                {"60000", "DJY052", "51700", "TDY053"}
        };
        return objects;
    }

    @DataProvider(name = "abx_postal_codes")
    public Object[][] abx_postal_codes() {
        final Object[][] objects = {
                {"60000", "DJY052", "30020", "TDY053"},
        };
        return objects;
    }

    @DataProvider(name = "IC_HUB_SHIPMENTS")
    public Object[][] ic_hub_shipments() {
        final Object[][] objects = {
                {"60000", "DJY052", "50000", "TPA062", "TDY053"}
        };
        return objects;
    }

    @DataProvider(name = "LINEHAUL_SHIPMENTS")
    public Object[][] linehaul_shipments() {
        final Object[][] objects = {
                {"60000", "DJY052", "81300", "MTR080", "TDY053"}
        };
        return objects;
    }
}
