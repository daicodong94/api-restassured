package my.logistika.tests.delivery;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.TmsLink;
import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.authentication.login.steps.LoginSteps;
import my.logistika.microservices.consignment.constants.ConsignmentApiShipmentStatus;
import my.logistika.microservices.consignment.constants.ConsignmentApiWorkFlowStatus;
import my.logistika.microservices.consignment.createShipment.models.CreateShipmentResponse;
import my.logistika.microservices.consignment.createShipment.models.ShipmentResponse;
import my.logistika.microservices.consignment.createShipment.steps.CreateShipmentSteps;
import my.logistika.microservices.consignment.getShipmentDetails.steps.GetShipmentByIdSteps;
import my.logistika.microservices.delivery.changeDeliveryOption.steps.ChangeDeliveryOptionSteps;
import my.logistika.microservices.delivery.constants.DeliveryOption;
import my.logistika.microservices.delivery.constants.PickupTypes;
import my.logistika.microservices.delivery.constants.WorkFlowStatus;
import my.logistika.microservices.delivery.constants.WorkFlowStatusForUser;
import my.logistika.microservices.delivery.deliveredToCustomer.steps.DeliveredToCustomerSteps;
import my.logistika.microservices.delivery.failedDelivery.steps.FailedDeliverySteps;
import my.logistika.microservices.delivery.scanning.steps.UpdateShipmentStatusScanSteps;
import my.logistika.microservices.receiver.delivery.steps.DeliveryTypeSteps;
import my.setel.utils.LoggerUtils;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static my.logistika.microservices.delivery.constants.FailedDeliveryReason.INCORRECT_ADDRESS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.fail;
import static org.awaitility.Awaitility.await;


@Epic("Change delivery type")
@Feature("Change delivery type from delivery to self collect and self collect to delivery ")
public class ChangeDeliveryOptionTests extends BaseTest {

    private ChangeDeliveryOptionSteps changeDeliveryOptionSteps = new ChangeDeliveryOptionSteps();
    private CreateShipmentSteps createShipmentSteps = new CreateShipmentSteps();
    private UpdateShipmentStatusScanSteps scanningSteps = new UpdateShipmentStatusScanSteps();
    private FailedDeliverySteps failedDeliverySteps = new FailedDeliverySteps();
    private DeliveredToCustomerSteps deliveredToCustomerSteps = new DeliveredToCustomerSteps();
    private GetShipmentByIdSteps getShipmentByIdSteps = new GetShipmentByIdSteps();
    private DeliveryTypeSteps deliveryTypeSteps = new DeliveryTypeSteps();

    /*
    API Delivery: Ops team should be able to change delivery option when shipment is in transit, ready for collection, failed delivery, out for delivery
     */
    @BeforeMethod(alwaysRun = true)
    void scanning_pre_conditions() {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
    }

    @Test(description = "API Delivery : Change delivery type to self collect from delivery and vice versa", groups = "regression")
    @TmsLink("COR-412")
    public void test_change_delivery_option() throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "60000", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        CreateShipmentResponse createShipmentResponse = shipmentResponse.getShipments().get(0);
        String shipmentId = createShipmentResponse.getShipmentId();
        String id = createShipmentResponse.getId();
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        Map<String, String> updatesMap = new HashMap<>();
        scanningSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        updatesMap.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        updatesMap.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, updatesMap);
        assert changeDeliveryOptionSteps
                .given_a_shipment_in_transit()
                .when_a_post_request_is_made_to_change_delivery(ApiUserRegistry.OPS_SUPPORT.username, shipmentId, DeliveryOption.SELF_COLLECT)
                .then_delivery_type_must_be_changed(DeliveryOption.SELF_COLLECT)
                .getJsonValue("workflowStatus").equals(WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        updatesMap.put("shipmentId", shipmentId);
        updatesMap.put("deliveryOption", "Self collect");
        getShipmentByIdSteps
                .then_shipment_details_must_be_updated_on_api_consignment(id, updatesMap);
        assert changeDeliveryOptionSteps.when_a_post_request_is_made_to_change_delivery(ApiUserRegistry.OPS_SUPPORT.username, shipmentId, DeliveryOption.DELIVERY)
                .then_delivery_type_must_be_changed(DeliveryOption.DELIVERY)
                .getJsonValue("workflowStatus").equals(WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        updatesMap.put("deliveryOption", "delivery");
        getShipmentByIdSteps
                .then_shipment_details_must_be_updated_on_api_consignment(id, updatesMap);
    }

    @Test(description = "API Delivery: complete self collect delivery", groups = "regression")
    @TmsLink("COR-412")
    public void test_complete_self_collect_delivery() throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "60000", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        CreateShipmentResponse createShipmentResponse = shipmentResponse.getShipments().get(0);
        String shipmentId = createShipmentResponse.getShipmentId();
        String id = createShipmentResponse.getId();
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanningSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        Map<String, String> updates = new HashMap<>();
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, updates);
        changeDeliveryOptionSteps
                .given_a_shipment_in_transit()
                .when_a_post_request_is_made_to_change_delivery(ApiUserRegistry.OPS_SUPPORT.username, shipmentId, DeliveryOption.SELF_COLLECT)
                .then_delivery_type_must_be_changed(DeliveryOption.SELF_COLLECT);
        updates.put("deliveryOption", "self collect");
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, updates);
        scanningSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.READY_FOR_COLLECTION);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.READY_FOR_COLLECTION);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.READY_FOR_COLLECTION);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, updates);
        deliveredToCustomerSteps.when_user_clicks_delivered_to_customer_button(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, "testPath", "false");
        Map<String, String> expected = new HashMap<>();
        expected.put("shipmentId", shipmentId);
        expected.put("workflowStatus", WorkFlowStatus.DELIVERED_TO_CUSTOMER);
        expected.put("workflowStatusForUser", WorkFlowStatusForUser.COMPLETED);
        deliveredToCustomerSteps.then_workflow_must_be_updated(expected);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_CUSTOMER);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.COMPLETED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, updates);

    }


    @Test(description = "API Delivery: Change delivery option when delivery is failed", groups = "regression")
    @TmsLink("COR-412")
    public void test_change_delivery_option_when_status_is_failed_delivery() throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "60000", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        CreateShipmentResponse createShipmentResponse = shipmentResponse.getShipments().get(0);
        String shipmentId = createShipmentResponse.getShipmentId();
        String id = createShipmentResponse.getId();
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);

        scanningSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        Map<String, String> updates = new HashMap<>();
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, updates);

        scanningSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);

        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, updates);

        scanningSteps.doScan(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId, WorkFlowStatus.OUT_FOR_DELIVERY);

        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, updates);

        failedDeliverySteps.when_a_delivery_attempt_is_failed(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId, INCORRECT_ADDRESS)
                .then_workflowstatus_must_be_failed_delivery_attempt();

        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.FAILED_DELIVERY_ATTEMPT);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.DELIVERY_FAILED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, updates);

        assert changeDeliveryOptionSteps
                .when_a_post_request_is_made_to_change_delivery(ApiUserRegistry.OPS_SUPPORT.username, shipmentId, DeliveryOption.SELF_COLLECT)
                .then_delivery_type_must_be_changed(DeliveryOption.SELF_COLLECT)
                .getJsonValue("workflowStatus")
                .equals(WorkFlowStatus.FAILED_DELIVERY_ATTEMPT);
        scanningSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.READY_FOR_COLLECTION);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.READY_FOR_COLLECTION);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.READY_FOR_COLLECTION);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, updates);
        deliveredToCustomerSteps.when_user_clicks_delivered_to_customer_button(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, "testPath", "false");
        // Mark as delivered to customer for HO uses delivered to customer api
        Map<String, String> expected = new HashMap<>();
        expected.put("shipmentId", shipmentId);
        expected.put("workflowStatus", WorkFlowStatus.DELIVERED_TO_CUSTOMER);
        expected.put("workflowStatusForUser", WorkFlowStatusForUser.COMPLETED);
        deliveredToCustomerSteps.then_workflow_must_be_updated(expected);
    }


    @Test(description = "API Delivery: Change delivery option when receiver asks for self collect", groups = "regression")
    @TmsLink("COR-472")
    public void test_change_delivery_option_from_receiver() throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "60000", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        CreateShipmentResponse createShipmentResponse = shipmentResponse.getShipments().get(0);
        String shipmentId = createShipmentResponse.getShipmentId();
        String id = createShipmentResponse.getId();
        String trackingToken = createShipmentResponse.getTrackingToken();
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        Map<String, String> updatesMap = new HashMap<>();
        scanningSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        updatesMap.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        updatesMap.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, updatesMap);
        try {
            await().
                    atMost(20, TimeUnit.SECONDS).
                    with().pollDelay(5, TimeUnit.SECONDS).pollInSameThread().
                    pollInterval(2, TimeUnit.SECONDS).
                    untilAsserted(() -> {
                        assertThat(deliveryTypeSteps.when_a_receiver_updates_delivery_type("Self collect", "trackingToken", trackingToken)
                                .getResponse().getStatusCode()).isEqualTo(HttpStatus.SC_OK);

                    });
        } catch (org.awaitility.core.ConditionTimeoutException e) {
            fail(e.getMessage());
        }

        deliveryTypeSteps.then_delivery_type_must_be_updated_on_receiver();
        updatesMap.put("shipmentId", shipmentId);
        updatesMap.put("deliveryOption", "Self collect");
        updatesMap.put("workflowStatus", ConsignmentApiWorkFlowStatus.READY_FOR_COLLECTION);
        updatesMap.put("workflowStatusForUser", ConsignmentApiShipmentStatus.READY_FOR_COLLECTION);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanningSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.READY_FOR_COLLECTION);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, updatesMap);
        deliveryTypeSteps.when_a_receiver_updates_delivery_type("Delivery", "trackingToken", trackingToken)
                .then_delivery_type_must_be_updated_on_receiver();
        updatesMap.clear();
        updatesMap.put("shipmentId", shipmentId);
        updatesMap.put("deliveryOption", "delivery");
        updatesMap.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, updatesMap);
    }


}