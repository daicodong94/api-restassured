package my.logistika.tests.delivery;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.TmsLink;
import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.consignment.constants.ConsignmentApiShipmentStatus;
import my.logistika.microservices.consignment.constants.ConsignmentApiWorkFlowStatus;
import my.logistika.microservices.consignment.createShipment.models.CreateShipmentResponse;
import my.logistika.microservices.consignment.createShipment.models.ShipmentResponse;
import my.logistika.microservices.consignment.createShipment.steps.CreateShipmentSteps;
import my.logistika.microservices.consignment.getShipmentDetails.steps.GetShipmentByIdSteps;
import my.logistika.microservices.consignment.reverseShipment.steps.CreateReverseShipmentSteps;
import my.logistika.microservices.delivery.approveReturn.steps.ApproveReturnSteps;
import my.logistika.microservices.delivery.constants.PickupTypes;
import my.logistika.microservices.delivery.constants.ShipmentActions;
import my.logistika.microservices.delivery.constants.WorkFlowStatus;
import my.logistika.microservices.delivery.constants.WorkFlowStatusForUser;
import my.logistika.microservices.delivery.failedDelivery.steps.FailedDeliverySteps;
import my.logistika.microservices.delivery.getShipmentDetails.steps.GetShipmentDetailsByIdSteps;
import my.logistika.microservices.delivery.scanning.steps.*;
import my.logistika.microservices.delivery.updateStatus.steps.UpdateStatusSteps;
import my.setel.utils.LoggerUtils;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.fail;
import static org.awaitility.Awaitility.await;

@Epic("Update shipment status")
@Feature("An Ops support employee should be able to update the shipment status via manual triggers")
public class UpdateStatusTests extends BaseTest {

    private final UpdateStatusSteps updateStatusSteps = new UpdateStatusSteps();
    private final ApproveReturnSteps approveReturnSteps = new ApproveReturnSteps();
    private final FailedDeliverySteps failedDeliverySteps = new FailedDeliverySteps();
    private final CreateShipmentSteps createShipmentSteps = new CreateShipmentSteps();
    private final  GetShipmentByIdSteps getShipmentByIdSteps = new GetShipmentByIdSteps();
    private final UpdateShipmentStatusScanSteps scanSteps = new UpdateShipmentStatusScanSteps();
    private final GetShipmentDetailsByIdSteps getShipmentDetailsByIdSteps = new GetShipmentDetailsByIdSteps();
    private final CreateReverseShipmentSteps createReverseShipmentSteps = new CreateReverseShipmentSteps();


    @BeforeMethod(alwaysRun = true)
    void given_user_is_authorized() {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
    }

    @Test(description = "API delivery: update status - delivered to customer", groups = "regression")
    @TmsLink(value = "COR-433")
    public void test_update_status_delivered() throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "60000", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        CreateShipmentResponse createShipmentResponse = shipmentResponse.getShipments().get(0);
        String shipmentId = createShipmentResponse.getShipmentId();
        String id = createShipmentResponse.getId();
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        Map<String, String> updates = new HashMap<>();
        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);
        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);
        scanSteps.doScan(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId, WorkFlowStatus.OUT_FOR_DELIVERY);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);
        failedDeliverySteps.fail_delivery_by_LMD(createShipmentResponse);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.FAILED_DELIVERY_ATTEMPT);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.DELIVERY_FAILED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);
        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.DELIVERY_FAILED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);
        scanSteps.doScan(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId, WorkFlowStatus.OUT_FOR_DELIVERY);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);
        failedDeliverySteps.fail_delivery_by_LMD(createShipmentResponse);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.FAILED_DELIVERY_ATTEMPT);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.DELIVERY_FAILED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);
        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.DELIVERY_FAILED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);
        scanSteps.doScan(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId, WorkFlowStatus.OUT_FOR_DELIVERY);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);
        failedDeliverySteps.fail_delivery_by_LMD(createShipmentResponse);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.FAILED_DELIVERY_ATTEMPT);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.DELIVERY_FAILED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);
        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERY_UNSUCCESSFUL);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERY_UNSUCCESSFUL);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.PENDING_RETURN);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);
        updateStatusSteps.when_a_POST_request_is_made_to_update_status_endpoint(ApiUserRegistry.OPS_SUPPORT.username, shipmentId, ShipmentActions.delivered.name(), shipmentId)
                .validateResponse(201);
        Map<String, String> updatesMap = new HashMap<>();
        updatesMap.put("workflowStatus", WorkFlowStatus.DELIVERED_TO_CUSTOMER);
        updatesMap.put("workflowStatusForUser", WorkFlowStatusForUser.COMPLETED);
        updatesMap.put("currentOwner", ApiUserRegistry.CUSTOMER_GLOBAL.userId);
        updateStatusSteps.then_workflow_status_must_be_updated_as_expected(updatesMap);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_CUSTOMER);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.COMPLETED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);
    }

    @Test(description = "API delivery: update status - unsuccessful", groups = "regression")
    @TmsLink(value = "COR-434")
    public void test_update_status_unsuccessful() throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "60000", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        CreateShipmentResponse createShipmentResponse = shipmentResponse.getShipments().get(0);
        String shipmentId = createShipmentResponse.getShipmentId();
        String id = createShipmentResponse.getId();
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        Map<String, String> updates = new HashMap<>();
        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        try {
            await().
                    atMost(10, TimeUnit.SECONDS).
                    with().pollDelay(1, TimeUnit.SECONDS).pollInSameThread().
                    pollInterval(2, TimeUnit.SECONDS).
                    untilAsserted(() -> {
                        assertThat(updateStatusSteps.when_a_POST_request_is_made_to_update_status_endpoint(ApiUserRegistry.OPS_SUPPORT.username, shipmentId, ShipmentActions.unsuccessfull.name(), shipmentId)
                                .getResponse().getStatusCode()).isEqualTo(HttpStatus.SC_CREATED);
                    });
        } catch (org.awaitility.core.ConditionTimeoutException e) {
            fail(e.getMessage());
        }
        Map<String, String> updatesMap = new HashMap<>();
        updatesMap.put("workflowStatus", WorkFlowStatus.DELIVERY_UNSUCCESSFUL);
        updatesMap.put("workflowStatusForUser", WorkFlowStatusForUser.PENDING_RETURN);
        updateStatusSteps.then_workflow_status_must_be_updated_as_expected(updatesMap);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERY_UNSUCCESSFUL);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.PENDING_RETURN);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);
    }

    @Test(description = "API delivery: update status - lost", groups = "regression")
    @TmsLink(value = "COR-431")
    public void test_update_status_lost_done() throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "60000", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        CreateShipmentResponse createShipmentResponse = shipmentResponse.getShipments().get(0);
        String shipmentId = createShipmentResponse.getShipmentId();
        String id = createShipmentResponse.getId();
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        try {
            await().
                    atMost(10, TimeUnit.SECONDS).
                    with().pollDelay(1, TimeUnit.SECONDS).pollInSameThread().
                    pollInterval(2, TimeUnit.SECONDS).
                    untilAsserted(() -> {
                        assertThat(updateStatusSteps.when_a_POST_request_is_made_to_update_status_endpoint(ApiUserRegistry.OPS_SUPPORT.username, shipmentId, ShipmentActions.lost.name(), shipmentId)
                                .getResponse().getStatusCode()).isEqualTo(HttpStatus.SC_CREATED);
                    });
        } catch (org.awaitility.core.ConditionTimeoutException e) {
            fail(e.getMessage());
        }
        Map<String, String> updatesMap = new HashMap<>();
        updatesMap.put("workflowStatus", WorkFlowStatus.LOST);
        updatesMap.put("workflowStatusForUser", WorkFlowStatusForUser.ON_HOLD);
        updateStatusSteps.then_workflow_status_must_be_updated_as_expected(updatesMap);

        Map<String, String> updates = new HashMap<>();
        updates.put("workflowStatus", "Lost");
        updates.put("workflowStatusForUser", "On hold");
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);

        updateStatusSteps.when_a_POST_request_is_made_to_update_status_endpoint(ApiUserRegistry.OPS_SUPPORT.username, shipmentId, ShipmentActions.done.name(), shipmentId)
                .validateResponse(201);
        updatesMap.put("workflowStatus", WorkFlowStatus.CANCELLED_BY_OPS);
        updatesMap.put("workflowStatusForUser", WorkFlowStatusForUser.CANCELLED);
        updateStatusSteps.then_workflow_status_must_be_updated_as_expected(updatesMap);

        updates.put("workflowStatus", WorkFlowStatus.CANCELLED_BY_OPS);
        updates.put("workflowStatusForUser", WorkFlowStatusForUser.CANCELLED);

        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);
    }

    @Test(description = "API delivery: update status - damaged", groups = "regression")
    @TmsLink(value = "COR-435")
    public void test_update_status_damaged_done() throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "60000", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        CreateShipmentResponse createShipmentResponse = shipmentResponse.getShipments().get(0);
        String shipmentId = createShipmentResponse.getShipmentId();
        String id = createShipmentResponse.getId();
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        try {
            await().
                    atMost(10, TimeUnit.SECONDS).
                    with().pollDelay(1, TimeUnit.SECONDS).pollInSameThread().
                    pollInterval(2, TimeUnit.SECONDS).
                    untilAsserted(() -> {
                        assertThat(updateStatusSteps.when_a_POST_request_is_made_to_update_status_endpoint(ApiUserRegistry.OPS_SUPPORT.username, shipmentId, ShipmentActions.damaged.name(), shipmentId)
                                .getResponse().getStatusCode()).isEqualTo(HttpStatus.SC_CREATED);
                    });
        } catch (org.awaitility.core.ConditionTimeoutException e) {
            fail(e.getMessage());
        }
        Map<String, String> updatesMap = new HashMap<>();
        updatesMap.put("workflowStatus", WorkFlowStatus.DAMAGED);
        updatesMap.put("workflowStatusForUser", WorkFlowStatusForUser.ON_HOLD);
        updateStatusSteps.then_workflow_status_must_be_updated_as_expected(updatesMap);

        Map<String, String> updates = new HashMap<>();
        updates.put("workflowStatus", "damaged");
        updates.put("workflowStatusForUser", "On hold");
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);

        updateStatusSteps.when_a_POST_request_is_made_to_update_status_endpoint(ApiUserRegistry.OPS_SUPPORT.username, shipmentId, ShipmentActions.done.name(), shipmentId)
                .validateResponse(201);
        updatesMap.put("workflowStatus", WorkFlowStatus.CANCELLED_BY_OPS);
        updatesMap.put("workflowStatusForUser", WorkFlowStatusForUser.CANCELLED);
        updateStatusSteps.then_workflow_status_must_be_updated_as_expected(updatesMap);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.CANCELLED_BY_OPS);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.CANCELLED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);
    }


    @Test(description = "API delivery: update status - cancel", groups = "regression")
    @TmsLink(value = "COR-410,COR-430")
    public void test_update_status_cancel() throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "60000", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        CreateShipmentResponse createShipmentResponse = shipmentResponse.getShipments().get(0);
        String id = createShipmentResponse.getId();
        String shipmentId = createShipmentResponse.getShipmentId();
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        Map<String, String> updates = new HashMap<>();
        //updates.put("workflowStatus", "Pickup request by Merchant");
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.PENDING_PICKUP);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);
        try {
            await().
                    atMost(10, TimeUnit.SECONDS).
                    with().pollDelay(1, TimeUnit.SECONDS).pollInSameThread().
                    pollInterval(2, TimeUnit.SECONDS).
                    untilAsserted(() -> {
                        assertThat(updateStatusSteps.when_a_POST_request_is_made_to_update_status_endpoint(ApiUserRegistry.OPS_SUPPORT.username, shipmentId, ShipmentActions.cancel.name(), shipmentId)
                                .getResponse().getStatusCode()).isEqualTo(HttpStatus.SC_CREATED);
                    });
        } catch (org.awaitility.core.ConditionTimeoutException e) {
            fail(e.getMessage());
        }
        Map<String, String> updatesMap = new HashMap<>();
        updatesMap.put("workflowStatus", WorkFlowStatus.SHIPMENT_CANCELLED);
        updatesMap.put("workflowStatusForUser", WorkFlowStatusForUser.CANCELLED);
        updateStatusSteps.then_workflow_status_must_be_updated_as_expected(updatesMap);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.SHIPMENT_CANCELLED);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.CANCELLED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);
    }

    @Test(description = "API delivery: update status - lost/damage , done on reverse shipment", groups = "regression", dataProvider = "lost-damage-data")
    @TmsLink(value = "COR-432")
    public void test_reverse_shipment_trigger_DONE(Object action, Object workFlowStatus) throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "60000", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        CreateShipmentResponse createShipmentResponse = shipmentResponse.getShipments().get(0);
        String shipmentId = createShipmentResponse.getShipmentId();
        String id = createShipmentResponse.getId();
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        scanSteps.doScan(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId, WorkFlowStatus.OUT_FOR_DELIVERY);
        failedDeliverySteps.fail_delivery_by_LMD(createShipmentResponse);
        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        scanSteps.doScan(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId, WorkFlowStatus.OUT_FOR_DELIVERY);
        failedDeliverySteps.fail_delivery_by_LMD(createShipmentResponse);
        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        scanSteps.doScan(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId, WorkFlowStatus.OUT_FOR_DELIVERY);
        failedDeliverySteps.fail_delivery_by_LMD(createShipmentResponse);

        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERY_UNSUCCESSFUL);
        
        Map<String,String> updatesMap = new HashMap<>();
        updatesMap.put("shipmentId", shipmentId);
        updatesMap.put("workflowStatus", WorkFlowStatus.DELIVERY_UNSUCCESSFUL);
        updatesMap.put("workflowStatusForUser", WorkFlowStatusForUser.PENDING_RETURN);

        getShipmentDetailsByIdSteps.then_shipment_details_must_be_returned_as_expected(shipmentId,updatesMap);
        updatesMap.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERY_UNSUCCESSFUL);
        updatesMap.put("workflowStatusForUser", ConsignmentApiShipmentStatus.PENDING_RETURN);

        getShipmentByIdSteps.then_shipment_details_must_be_returned_on_ADMIN_api_consignment(id,updatesMap);

        assert createReverseShipmentSteps.when_a_post_request_is_made_to_create_reverse_shipment(shipmentId)
                .validateResponse(201).getJsonValue("shipmentId").equals(shipmentId + "-RV");
        assert createReverseShipmentSteps.getJsonValue("workflowStatusForUser").equalsIgnoreCase(ConsignmentApiShipmentStatus.CREATED);
        assert createReverseShipmentSteps.getJsonValue("workflowStatus").equalsIgnoreCase(ConsignmentApiWorkFlowStatus.CREATED);

        String reverse_id = createReverseShipmentSteps.getJsonValue("id");

        updatesMap.clear();
        updatesMap.put("shipmentId",shipmentId+"-RV");
        updatesMap.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        updatesMap.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        try {
            await().
                    atMost(20, TimeUnit.SECONDS).
                    with().pollDelay(5, TimeUnit.SECONDS).pollInSameThread().
                    pollInterval(2, TimeUnit.SECONDS).
                    untilAsserted(() -> {
                        int sc = getShipmentByIdSteps
                                .then_shipment_details_must_be_returned_on_ADMIN_api_consignment(reverse_id, updatesMap).getResponse().getStatusCode();
                        LoggerUtils.getInstance().info(String.valueOf(sc));
                        LoggerUtils.getInstance().info(String.valueOf(sc == 200));
                        assert sc == 200;
                    });
        } catch (org.awaitility.core.ConditionTimeoutException e) {
            fail(e.getMessage());
        }
       
        updatesMap.put("shipmentId",shipmentId+"-RV");
        updatesMap.put("workflowStatus", WorkFlowStatus.DELIVERED_TO_STATION);
        updatesMap.put("workflowStatusForUser", WorkFlowStatusForUser.IN_TRANSIT);
        getShipmentDetailsByIdSteps.then_shipment_details_must_be_returned_as_expected(shipmentId+"-RV",updatesMap);
        updatesMap.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        updatesMap.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps
                .then_shipment_details_must_be_updated_on_api_consignment(reverse_id, updatesMap);
        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId + "-RV", WorkFlowStatus.OUT_FOR_DELIVERY);
        updateStatusSteps.when_a_POST_request_is_made_to_update_status_endpoint(ApiUserRegistry.OPS_SUPPORT.username, shipmentId+"-RV", action.toString(), shipmentId)
                .validateResponse(201);

        updatesMap.put("workflowStatus", workFlowStatus.toString());
        updatesMap.put("workflowStatusForUser", WorkFlowStatusForUser.ON_HOLD);
        updateStatusSteps.then_workflow_status_must_be_updated_as_expected(updatesMap);
        updatesMap.clear();
        updatesMap.put("workflowStatus", workFlowStatus.toString());
        updatesMap.put("workflowStatusForUser", ConsignmentApiShipmentStatus.ON_HOLD);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updatesMap);
        updateStatusSteps.when_a_POST_request_is_made_to_update_status_endpoint(ApiUserRegistry.OPS_SUPPORT.username,shipmentId+"-RV", ShipmentActions.done.name(), shipmentId)
                .validateResponse(201);
        updatesMap.clear();
        updatesMap.put("workflowStatus", WorkFlowStatus.CANCELLED_BY_OPS);
        updatesMap.put("workflowStatusForUser", WorkFlowStatusForUser.CANCELLED);
        updateStatusSteps.then_workflow_status_must_be_updated_as_expected(updatesMap);
        updatesMap.clear();
        updatesMap.put("workflowStatus", ConsignmentApiWorkFlowStatus.CANCELLED_BY_OPS);
        updatesMap.put("workflowStatusForUser", ConsignmentApiShipmentStatus.CANCELLED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updatesMap);
    }

    @DataProvider(name = "lost-damage-data")
    public Object[][] lost_damage_data() {
        final Object[][] objects = {{ShipmentActions.lost, WorkFlowStatus.LOST},
                {ShipmentActions.damaged, WorkFlowStatus.DAMAGED}};
        return objects;
    }
}
