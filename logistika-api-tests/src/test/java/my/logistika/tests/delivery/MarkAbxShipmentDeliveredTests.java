package my.logistika.tests.delivery;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.TmsLink;
import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.authentication.login.steps.LoginSteps;
import my.logistika.microservices.consignment.constants.ConsignmentApiShipmentStatus;
import my.logistika.microservices.consignment.constants.ConsignmentApiWorkFlowStatus;
import my.logistika.microservices.consignment.createShipment.models.CreateShipmentResponse;
import my.logistika.microservices.consignment.createShipment.models.ShipmentResponse;
import my.logistika.microservices.consignment.createShipment.steps.CreateShipmentSteps;
import my.logistika.microservices.consignment.getShipmentDetails.steps.GetShipmentByIdSteps;
import my.logistika.microservices.delivery.constants.PickupTypes;
import my.logistika.microservices.delivery.constants.WorkFlowStatus;
import my.logistika.microservices.delivery.constants.WorkFlowStatusForUser;
import my.logistika.microservices.delivery.markABXdeliveredToCustomer.steps.MarkABXDeliveredToCustomerSteps;
import my.logistika.microservices.delivery.scanning.steps.UpdateShipmentStatusScanSteps;
import my.setel.utils.LoggerUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;


@Epic("ABX manual triggers")
@Feature("An Ops support employee should be able to complete abx delivery via manual trigger")
public class MarkAbxShipmentDeliveredTests extends BaseTest {
    private final LoginSteps loginSteps = new LoginSteps();
    private final MarkABXDeliveredToCustomerSteps markABXDeliveredToCustomerSteps = new MarkABXDeliveredToCustomerSteps();
    private final UpdateShipmentStatusScanSteps scanSteps = new UpdateShipmentStatusScanSteps();
    private final CreateShipmentSteps createShipmentSteps = new CreateShipmentSteps();
    private final GetShipmentByIdSteps getShipmentByIdSteps =  new GetShipmentByIdSteps();

    @BeforeMethod(alwaysRun = true)
    public void setup() {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
    }

    @Test(description = "API Delivery: ABX Mark as Delivered to Customer", groups = "regression")
    @TmsLink(value = "COR-401")
    public void test_abx_mark_as_delivered_to_customer() throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse)createShipmentSteps.createShipment("60000","30020",PickupTypes.PICKUP,WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        CreateShipmentResponse createShipmentResponse = shipmentResponse.getShipments().get(0);
        String shipmentId = createShipmentResponse.getShipmentId();
        String id  = createShipmentResponse.getId();
        LoggerUtils.getInstance().info("current shipment id is " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        scanSteps.doScan(ApiUserRegistry.MMD_DJY052_TDY053_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_STATION);
        scanSteps.doScan(ApiUserRegistry.HO_TDY053_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        markABXDeliveredToCustomerSteps.when_Ops_support_clicks_Mark_as_delivered_to_customer(ApiUserRegistry.OPS_SUPPORT.username, shipmentId)
                .then_shipment_workflow_status_must_be_set_to_completed();
        Map<String,String> expectedUpdates = new HashMap<>();
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_CUSTOMER);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.COMPLETED);
        getShipmentByIdSteps.when_a_GET_request_is_made_to_shipments_endpoint(id).then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);
    }
}
