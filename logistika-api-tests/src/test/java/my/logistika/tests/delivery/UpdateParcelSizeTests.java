package my.logistika.tests.delivery;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.TmsLink;
import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.consignment.constants.ConsignmentApiShipmentStatus;
import my.logistika.microservices.consignment.constants.ConsignmentApiWorkFlowStatus;
import my.logistika.microservices.consignment.createShipment.models.CreateShipmentDTO;
import my.logistika.microservices.consignment.createShipment.models.ShipmentResponse;
import my.logistika.microservices.consignment.createShipment.steps.CreateShipmentSteps;
import my.logistika.microservices.consignment.getShipmentDetails.steps.GetShipmentByIdSteps;
import my.logistika.microservices.delivery.constants.ParcelSize;
import my.logistika.microservices.delivery.constants.PickupTypes;
import my.logistika.microservices.delivery.constants.WorkFlowStatus;
import my.logistika.microservices.delivery.constants.WorkFlowStatusForUser;
import my.logistika.microservices.delivery.scanning.steps.UpdateShipmentStatusScanSteps;
import my.logistika.microservices.delivery.updateParcelSize.models.UpdateParcelSizeReqBodyDto;
import my.logistika.microservices.delivery.updateParcelSize.steps.UpdateParcelSizeSteps;
import my.setel.utils.CommonUtils;
import my.setel.utils.LoggerUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/*
validate the parcel size on consignment api when api-delivery starts publishing shipment updated events
 */

@Epic("Update shipment details")
@Feature("An Hub operator should be able to update the parcel size")
public class UpdateParcelSizeTests extends BaseTest {

    private final UpdateParcelSizeSteps updateParcelSizeSteps = new UpdateParcelSizeSteps();
    private final CreateShipmentSteps createShipmentSteps = new CreateShipmentSteps();
    private final UpdateShipmentStatusScanSteps scanningSteps = new UpdateShipmentStatusScanSteps();
    private final GetShipmentByIdSteps getShipmentByIdSteps = new GetShipmentByIdSteps();

    @DataProvider(name = "roles")
    public static Object[][] roles() {
        return new Object[][]{{ApiUserRegistry.HO_DJY052_KV.username}/*{ApiUserRegistry.OPS_SUPPORT.username}*/};
    }

    @BeforeMethod(alwaysRun = true)
    void given_user_is_authorized(){
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
    }


    @Test(description = "API delivery: update parcel size",groups = "regression",dataProvider = "roles")
    @TmsLink("COR-429")
    public void test_update_parcel_size(String role) throws InterruptedException {
        String refId = CommonUtils.getRandomFiveCharsString(9);
        String pickupType= PickupTypes.PICKUP;
        String RPC="60000";
        String SPC="60000";
        String parcelSize = my.logistika.microservices.delivery.constants.ParcelSize.S.name();
        CreateShipmentDTO createShipmentDTO = new CreateShipmentDTO(
                refId, "1", "2", "5", "4", "3", parcelSize, "SHIVA",
                "+601111111111", "Test dr 44", "", RPC, "",
                "Kuala Lumpur", "optional delivery instructions", "0", "", "",
                "Sravan Mer", "+60999999999", "", pickupType,
                "address 1 mer", "address 2 mer", "", "", "Kuala Lumpur",
                SPC, "+609999999999", "", ApiUserRegistry.MERCHANT.userId);
         ShipmentResponse shipmentResponse = (ShipmentResponse)createShipmentSteps.createShipment("60000","60000",PickupTypes.PICKUP,WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        String shipmentId =  shipmentResponse.getShipments().get(0).getShipmentId();
        String id = shipmentResponse.getShipments().get(0).getId();
        LoggerUtils.getInstance().info("shipment created with id " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanningSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId,WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        Map<String, String> updates = new HashMap<>();
        updates.put("parcelSize",parcelSize);
        updates.put("workflowStatus",ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        updates.put("workflowStatusForUser",ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);
        scanningSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        updates.put("parcelSize",parcelSize);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,updates);
        updateParcelSizeSteps.when_a_post_request_is_made_to_update_parcel_size(new UpdateParcelSizeReqBodyDto(ApiUserRegistry.HO_DJY052_KV.username,shipmentId, ParcelSize.M.name(),"Parcel size is M"))
                .validateResponse(201);
        Map<String,String> expectedMap = new LinkedHashMap<>();
        expectedMap.put("parcelSize",ParcelSize.M.name());
        expectedMap.put("shipmentId",shipmentId);
        updateParcelSizeSteps.then_parcel_size_must_be_updated_as_expected(expectedMap);
        updateParcelSizeSteps.when_a_post_request_is_made_to_update_parcel_size(new UpdateParcelSizeReqBodyDto(ApiUserRegistry.HO_DJY052_KV.username,shipmentId, ParcelSize.S.name(),"Parcel size is small"))
                .validateResponse(201);
        expectedMap.clear();
        expectedMap.put("parcelSize",ParcelSize.S.name());
        expectedMap.put("shipmentId",shipmentId);
        expectedMap.put("parcelSizeRemark","Parcel size is small");
        updateParcelSizeSteps.then_parcel_size_must_be_updated_as_expected(expectedMap);
    }
}
