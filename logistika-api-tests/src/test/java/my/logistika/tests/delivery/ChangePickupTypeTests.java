package my.logistika.tests.delivery;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.TmsLink;
import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.consignment.constants.ConsignmentApiShipmentStatus;
import my.logistika.microservices.consignment.constants.ConsignmentApiWorkFlowStatus;
import my.logistika.microservices.consignment.createShipment.models.CreateShipmentResponse;
import my.logistika.microservices.consignment.createShipment.models.ShipmentResponse;
import my.logistika.microservices.consignment.createShipment.steps.CreateShipmentSteps;
import my.logistika.microservices.consignment.getShipmentDetails.steps.GetShipmentByIdSteps;
import my.logistika.microservices.delivery.changePickupType.steps.ChangePickupTypeSteps;
import my.logistika.microservices.delivery.constants.PickupTypes;
import my.logistika.microservices.delivery.constants.WorkFlowStatus;
import my.logistika.microservices.delivery.constants.WorkFlowStatusForUser;
import my.logistika.microservices.delivery.scanning.steps.UpdateShipmentStatusScanSteps;
import my.setel.utils.LoggerUtils;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.fail;
import static org.awaitility.Awaitility.await;

@Epic("Ops : Change Pickup type")
@Feature("An Ops support employee should be able to change pickup type from admin portal")
public class ChangePickupTypeTests extends BaseTest {

    private final ChangePickupTypeSteps changePickupTypeSteps = new ChangePickupTypeSteps();
    private final CreateShipmentSteps createShipmentSteps = new CreateShipmentSteps();
    private final UpdateShipmentStatusScanSteps scanSteps = new UpdateShipmentStatusScanSteps();
    private final GetShipmentByIdSteps getShipmentByIdSteps = new GetShipmentByIdSteps();

    @BeforeMethod(alwaysRun = true)
    void scanning_pre_conditions() {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
    }

    @Test(description = "API delivery: change pickup type", groups = "regression", dataProvider = "pickup-types")
    @TmsLink("COR-429")
    public void change_Pickup_Type_tests(String initialPickupType, String initialShipmentStatus, String pickupType, String workFlowStatus, String shipmentStatus) throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "60000", initialPickupType, initialShipmentStatus)
                .saveResponseObject(ShipmentResponse.class);
        CreateShipmentResponse createShipmentResponse = shipmentResponse.getShipments().get(0);
        String shipmentId = createShipmentResponse.getShipmentId();
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);
        assert createShipmentResponse.getInitialPickupType().equals(initialPickupType);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        try {
            await().
                    atMost(10, TimeUnit.SECONDS).
                    with().pollDelay(1, TimeUnit.SECONDS).pollInSameThread().
                    pollInterval(2, TimeUnit.SECONDS).
                    untilAsserted(() -> {
                        LoggerUtils.getInstance().info("executing.......");
                        assertThat(changePickupTypeSteps.when_Ops_support_changes_the_pickup_type(ApiUserRegistry.OPS_SUPPORT.username, shipmentId, pickupType)
                                .getResponse().getStatusCode()).isEqualTo(HttpStatus.SC_CREATED);
                    });
        } catch (org.awaitility.core.ConditionTimeoutException e) {
            fail(e.getMessage());
        }
        changePickupTypeSteps.then_workflowStatus_should_be_changed_as_expected(workFlowStatus, shipmentStatus, pickupType);
    }

    @Test(description = "API delivery: Do not allow to change pickup type when the shipment is in transit", groups = "regression")
    public void change_Pickup_Type_test_negative_path() throws InterruptedException {
        String initialPickupType = PickupTypes.PICKUP;
        String pickupType = "decideLater";
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "60000", initialPickupType, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        CreateShipmentResponse createShipmentResponse = shipmentResponse.getShipments().get(0);
        String shipmentId = createShipmentResponse.getShipmentId();
        String id = createShipmentResponse.getId();
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);
        assert createShipmentResponse.getInitialPickupType().equals(initialPickupType);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        Map<String, String> updates = new HashMap<>();
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, updates);
        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        updates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        updates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, updates);
        try {
            await().
                    atMost(10, TimeUnit.SECONDS).
                    with().pollDelay(1, TimeUnit.SECONDS).pollInSameThread().
                    pollInterval(2, TimeUnit.SECONDS).
                    untilAsserted(() -> {
                        LoggerUtils.getInstance().info("executing.......");
                        assert changePickupTypeSteps.when_Ops_support_changes_the_pickup_type(ApiUserRegistry.OPS_SUPPORT.username, shipmentId, pickupType)
                                .validateResponse(400).getJsonValue("message").equals("Validation error");
                    });
        } catch (org.awaitility.core.ConditionTimeoutException e) {
            fail(e.getMessage());
        }


    }

    @Test(description = "API delivery: Do not allow the same pickup type", groups = "regression")
    public void test_same_pickuptype_update() throws InterruptedException {
        String initialPickupType = PickupTypes.PICKUP;
        String pickupType = "pickup";
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "60000", initialPickupType, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        CreateShipmentResponse createShipmentResponse = shipmentResponse.getShipments().get(0);
        String shipmentId = createShipmentResponse.getShipmentId();
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);
        assert createShipmentResponse.getInitialPickupType().equals(initialPickupType);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        try {
            await().
                    atMost(10, TimeUnit.SECONDS).
                    with().pollDelay(1, TimeUnit.SECONDS).pollInSameThread().
                    pollInterval(2, TimeUnit.SECONDS).
                    untilAsserted(() -> {
                        LoggerUtils.getInstance().info("executing.......");
                        assertThat(changePickupTypeSteps.when_Ops_support_changes_the_pickup_type(ApiUserRegistry.OPS_SUPPORT.username, shipmentId, pickupType)
                                .getResponse().getStatusCode()).isEqualTo(HttpStatus.SC_BAD_REQUEST);
                    });
        } catch (org.awaitility.core.ConditionTimeoutException e) {
            fail(e.getMessage());
        }
    }


    @DataProvider(name = "pickup-types")
    public Object[][] pickup_types() {
        final Object[][] objects = {
                {PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP, "decideLater", WorkFlowStatus.CREATED, "created"},
                {PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP, "dropOff", WorkFlowStatus.DROPOFF_REQUEST_BY_MERCHANT, "pendingDropOff"},
                {PickupTypes.DROP_OFF, WorkFlowStatusForUser.PENDING_DROP_OFF, "pickup", WorkFlowStatus.PICKUP_REQUEST_BY_MERCHANT, "pendingPickup"},
                {PickupTypes.DROP_OFF, WorkFlowStatusForUser.PENDING_DROP_OFF, "decideLater", WorkFlowStatus.CREATED, "created"},
                {PickupTypes.DECIDE_LATER, WorkFlowStatusForUser.CREATED, "pickup", WorkFlowStatus.PICKUP_REQUEST_BY_MERCHANT, "pendingPickup"},
                {PickupTypes.DECIDE_LATER, WorkFlowStatusForUser.CREATED, "dropOff", WorkFlowStatus.DROPOFF_REQUEST_BY_MERCHANT, "pendingDropOff"}
        };
        return objects;
    }


}
