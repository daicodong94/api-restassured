package my.logistika.tests.delivery;

import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.authentication.login.steps.LoginSteps;
import my.logistika.microservices.consignment.createShipment.models.ShipmentResponse;
import my.logistika.microservices.consignment.createShipment.steps.CreateShipmentSteps;
import my.logistika.microservices.delivery.constants.Hubs;
import my.logistika.microservices.delivery.constants.PickupTypes;
import my.logistika.microservices.delivery.constants.WorkFlowStatus;
import my.logistika.microservices.delivery.constants.WorkFlowStatusForUser;
import my.logistika.microservices.delivery.getShipmentDetails.steps.GetShipmentDetailsByIdSteps;
import my.logistika.microservices.delivery.scanning.steps.UpdateShipmentStatusScanSteps;
import my.logistika.microservices.delivery.updateShipment.models.UpdateShipmentDto;
import my.logistika.microservices.delivery.updateShipment.steps.UpdateShipmentSteps;
import my.setel.utils.LoggerUtils;
import org.testng.annotations.Test;


import static my.logistika.microservices.delivery.constants.FailedDeliveryReason.INCORRECT_ADDRESS;

public class UpdateShipmentTests extends BaseTest {

    private final UpdateShipmentSteps updateShipmentSteps = new UpdateShipmentSteps();
    private final CreateShipmentSteps createShipmentSteps = new CreateShipmentSteps();
    private final UpdateShipmentStatusScanSteps scanSteps = new UpdateShipmentStatusScanSteps();
    private final LoginSteps loginSteps = new LoginSteps();

    @Test(description = "API delivery: Update shipment details", groups = "regression")
    public void update_shipment_test() throws InterruptedException {
        String hub = Hubs.DJY052.name();
        String postalCode = "60000";
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "60000", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        String shipmentId = shipmentResponse.getShipments().get(0).getShipmentId();
        LoggerUtils.getInstance().info("Shipment created with shipment Id " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);

        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        UpdateShipmentDto updateShipmentDto = new UpdateShipmentDto(true, "shipment remark", INCORRECT_ADDRESS, 1, "Customer", "Office", "ETA", "TDY053", "DJY052",
                "61a5cdd344d1d55b9919dc55",
                "61a5ce0a54ee386c4c5b2067");

        updateShipmentSteps.when_a_PUT_request_is_made_to_shipment_endpoint(shipmentId, updateShipmentDto)
                .validateResponse(200);

        assert updateShipmentSteps.getJsonValue("oddSize").contains("true");
        assert updateShipmentSteps.getJsonValue("shipmentRemark").contains("shipment remark");
        assert updateShipmentSteps.getJsonValue("failedDeliveryReason").contains(INCORRECT_ADDRESS);
        assert updateShipmentSteps.getJsonValue("failedDeliveryAttempt").contains("1");
        assert updateShipmentSteps.getJsonValue("deliveredTo").contains("Customer");
        assert updateShipmentSteps.getJsonValue("deliveryAddressType").contains("Office");
        assert updateShipmentSteps.getJsonValue("currentHub").contains("TDY053");
        assert updateShipmentSteps.getJsonValue("nextHub").contains("DJY052");
        assert updateShipmentSteps.getJsonValue("assignedDriver").contains("61a5cdd344d1d55b9919dc55");
        assert updateShipmentSteps.getJsonValue("currentOwner").contains("61a5ce0a54ee386c4c5b2067");
        assert updateShipmentSteps.getJsonValue("estimatedDeliveryDate").contains("ETA");
    }


    @Test(description = "API delivery: Update shipment details - Negative path")
    public void update_shipment_test_negative_path() throws InterruptedException {
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        UpdateShipmentDto updateShipmentDto = new UpdateShipmentDto(true, "shipment remark", INCORRECT_ADDRESS, 1, "Customer", "Office", "ETA", "TDY053", "DJY052", "61a5cdd344d1d55b9919dc55", "61a5ce0a54ee386c4c5b2067");
        updateShipmentSteps.when_a_PUT_request_is_made_to_shipment_endpoint("InvalidshipmentId", updateShipmentDto)
                .validateResponse(404);
    }
}
