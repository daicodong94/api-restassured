package my.logistika.tests.shippingProvider;
import lombok.extern.java.Log;
import my.logistika.base.BaseTest;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.consignment.constants.ConsignmentApiShipmentStatus;
import my.logistika.microservices.consignment.constants.ConsignmentApiWorkFlowStatus;
import my.logistika.microservices.consignment.createShipment.models.ShipmentResponse;
import my.logistika.microservices.consignment.createShipment.steps.CreateShipmentSteps;
import my.logistika.microservices.consignment.getShipmentDetails.steps.GetShipmentByIdSteps;
import my.logistika.microservices.delivery.constants.PickupTypes;
import my.logistika.microservices.delivery.constants.WorkFlowStatus;
import my.logistika.microservices.delivery.constants.WorkFlowStatusForUser;
import my.logistika.microservices.delivery.deliveredToCustomer.steps.DeliveredToCustomerSteps;
import my.logistika.microservices.delivery.scanning.steps.UpdateShipmentStatusScanSteps;
import my.logistika.microservices.shippingProvider.constants.ABXEndpointCodes;
import my.logistika.microservices.shippingProvider.constants.ABXStatus;
import my.logistika.microservices.shippingProvider.constants.ABXStatusCodes;
import my.logistika.microservices.shippingProvider.models.*;
import my.logistika.microservices.shippingProvider.steps.ShippingProverABXStatusSteps;
import my.logistika.tests.delivery.Scanning.ScanningTests;
import my.setel.utils.CommonUtils;
import my.setel.utils.Logger;
import my.setel.utils.LoggerUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import java.util.HashMap;
import java.util.Map;
import java.lang.String;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.fail;
import static org.awaitility.Awaitility.await;


public class ShippingProviderTests extends BaseTest {

    private final ShippingProverABXStatusSteps shippingProverABXStatusSteps = new ShippingProverABXStatusSteps();
    private ABXShipmentStatusBodyDto abxRequest;
    private ABXShipmentStatusResponse abxResponse;
    private ABXShipmentErrorResponse abxErrorResponse;
    private GetShipmentByIdSteps getShipmentByIdSteps = new GetShipmentByIdSteps();
    private ScanningTests scanningTests = new ScanningTests();
    private CreateShipmentSteps createShipmentSteps = new CreateShipmentSteps();
    private UpdateShipmentStatusScanSteps scanSteps = new UpdateShipmentStatusScanSteps();
    private DeliveredToCustomerSteps deliveredToCustomerSteps = new DeliveredToCustomerSteps();

    @BeforeMethod(alwaysRun = true)
    void scanning_pre_conditions() {
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
    }


    @Test(description = "API: shipping provider - ABX shipment - E2E", groups = "smoke", dataProvider = "abx_postal_codes_hubs")
    public void test_abx_transitions(String senderPostalCode, String firstMileHub, String receiverPostalCode, String lastMileHub) throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment(senderPostalCode, receiverPostalCode, PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        String shipmentId = shipmentResponse.getShipments().get(0).getShipmentId();
        String id = shipmentResponse.getShipments().get(0).getId();
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        Map<String, String> expectedUpdates = new HashMap<>();
        scanSteps.doScan("FMD_" + firstMileHub.toUpperCase() + "_admin@logistika.com", shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        expectedUpdates.put("shipmentId", shipmentId);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);
        scanSteps.doScan("HO_" + firstMileHub.toUpperCase() + "_1@logistika.com", shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);
        scanSteps.doScan("MMD_KV_S1_F_1@logistika.com", shipmentId, WorkFlowStatus.PICKED_UP_FROM_STATION);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);
        scanSteps.doScan("HO_" + lastMileHub.toUpperCase() + "_1@logistika.com", shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);
        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.SHIPMENT_PICKED_UP_010, ABXStatus.SHIPMENT_PICKED_UP, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus",ConsignmentApiWorkFlowStatus.TRANSFERRED_TO_COURIER);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);
        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.ARRIVED_AT_ORIGIN_STATION_101, ABXStatus.ARRIVED_AT_ORIGIN_STATION, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);
        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.ARRIVED_AT_HUB_TRANSIT_STATION_102, ABXStatus.ARRIVED_AT_HUB_TRANSIT_STATION, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);
        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.ARRIVED_AT_DESTINATION_STATION_103, ABXStatus.ARRIVED_AT_DESTINATION_STATION, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);
        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.OUT_FOR_DELIVERY_045, ABXStatus.OUT_FOR_DELIVERY, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);
        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.DELIVERY_SUCCESSFULLY_POD, ABXStatus.DELIVERY_SUCCESSFULLY, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_CUSTOMER);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.COMPLETED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);
    }

    @Test(description = "API: shipping provider - ABX shipment - E2E", groups = "regression", dataProvider = "failCodes")
    public void test_abx_transitions_delivery_unsuccessful(String code, String reason) throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "30020", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        String shipmentId = shipmentResponse.getShipments().get(0).getShipmentId();
        LoggerUtils.getInstance().info("current shipment id is " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        String id = shipmentResponse.getShipments().get(0).getId();
        Map<String,String> expectedUpdates = new HashMap<>();
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);
        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);

        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan(ApiUserRegistry.MMD_DJY052_TDY053_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_STATION);

        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan(ApiUserRegistry.HO_TDY053_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.SHIPMENT_PICKED_UP_010, ABXStatus.SHIPMENT_PICKED_UP, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.TRANSFERRED_TO_COURIER);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.ARRIVED_AT_ORIGIN_STATION_101, ABXStatus.ARRIVED_AT_ORIGIN_STATION, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.ARRIVED_AT_HUB_TRANSIT_STATION_102, ABXStatus.ARRIVED_AT_HUB_TRANSIT_STATION, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.ARRIVED_AT_DESTINATION_STATION_103, ABXStatus.ARRIVED_AT_DESTINATION_STATION, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.OUT_FOR_DELIVERY_045, ABXStatus.OUT_FOR_DELIVERY, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, code, reason, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);

        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.FAILED_DELIVERY_ATTEMPT);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.DELIVERY_FAILED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.DELIVERY_SUCCESSFULLY_POD, ABXStatus.DELIVERY_SUCCESSFULLY, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_CUSTOMER);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.COMPLETED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

    }

    @Test(description = "API: shipping provider - ABX shipment - Failure to Delivery scenarios", groups = "regression", dataProvider = "abx-fail-code-combinations")
    public void test_abx_transitions_045_to_060XX_to_045_060XX_090(String code1, String desc1, String code2, String desc2) throws InterruptedException {
        LoggerUtils.getInstance().info("running test 010_101_102_103_045_" + code1 + "_045_" + code2 + "_POD");
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "30020", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        String shipmentId = shipmentResponse.getShipments().get(0).getShipmentId();
        String id = shipmentResponse.getShipments().get(0).getId();
        LoggerUtils.getInstance().info("current shipment id is " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        Map<String,String> expectedUpdates = new HashMap<>();
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan(ApiUserRegistry.MMD_DJY052_TDY053_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_STATION);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan(ApiUserRegistry.HO_TDY053_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);


        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.SHIPMENT_PICKED_UP_010, ABXStatus.SHIPMENT_PICKED_UP, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.TRANSFERRED_TO_COURIER);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.ARRIVED_AT_ORIGIN_STATION_101, ABXStatus.ARRIVED_AT_ORIGIN_STATION, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.TRANSFERRED_TO_COURIER);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.ARRIVED_AT_HUB_TRANSIT_STATION_102, ABXStatus.ARRIVED_AT_HUB_TRANSIT_STATION, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);


        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.ARRIVED_AT_DESTINATION_STATION_103, ABXStatus.ARRIVED_AT_DESTINATION_STATION, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.OUT_FOR_DELIVERY_045, ABXStatus.OUT_FOR_DELIVERY, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, code1, desc1, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.FAILED_DELIVERY_ATTEMPT);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.DELIVERY_FAILED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.OUT_FOR_DELIVERY_045, ABXStatus.OUT_FOR_DELIVERY, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, code2, desc2, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);

        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.FAILED_DELIVERY_ATTEMPT);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.DELIVERY_FAILED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.DELIVERY_SUCCESSFULLY_POD, ABXStatus.DELIVERY_SUCCESSFULLY, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_CUSTOMER);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.COMPLETED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

    }


    @Test(description = "API: shipping provider - ABX shipment - Failure to Delivery scenarios", groups = "regression", dataProvider = "failCodes")
    public void test_abx_transitions_customer_change_address_for_delivery(String code, String description) throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "30020", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        String shipmentId = shipmentResponse.getShipments().get(0).getShipmentId();
        String id = shipmentResponse.getShipments().get(0).getId();
        LoggerUtils.getInstance().info("current shipment id is " + shipmentId);

        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);

        Map<String, String> expectedUpdates = new HashMap<>();
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan(ApiUserRegistry.MMD_DJY052_TDY053_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_STATION);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan(ApiUserRegistry.HO_TDY053_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.SHIPMENT_PICKED_UP_010, ABXStatus.SHIPMENT_PICKED_UP, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.TRANSFERRED_TO_COURIER);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.ARRIVED_AT_ORIGIN_STATION_101, ABXStatus.ARRIVED_AT_ORIGIN_STATION, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.TRANSFERRED_TO_COURIER);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.ARRIVED_AT_HUB_TRANSIT_STATION_102, ABXStatus.ARRIVED_AT_HUB_TRANSIT_STATION, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.TRANSFERRED_TO_COURIER);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.ARRIVED_AT_DESTINATION_STATION_103, ABXStatus.ARRIVED_AT_DESTINATION_STATION, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.TRANSFERRED_TO_COURIER);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.OUT_FOR_DELIVERY_045, ABXStatus.OUT_FOR_DELIVERY, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);


        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, code, description, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);

        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.FAILED_DELIVERY_ATTEMPT);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.DELIVERY_FAILED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);


        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.OUT_FOR_DELIVERY_045, ABXStatus.OUT_FOR_DELIVERY, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, code, description, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.FAILED_DELIVERY_ATTEMPT);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.DELIVERY_FAILED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.OUT_FOR_DELIVERY_045, ABXStatus.OUT_FOR_DELIVERY, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.CHANGE_ADDRESS_060_08, ABXStatus.CHANGE_ADDRESS, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.FAILED_DELIVERY_ATTEMPT);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.DELIVERY_FAILED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.ON_THE_WAY_TO_NEW_ADDRESS_090, ABXStatus.DELIVERY_SUCCESSFULLY, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.SHIPMENT_PICKED_UP_010_1, ABXStatus.SHIPMENT_PICKED_UP, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);


        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.ARRIVED_AT_ORIGIN_STATION_101_1, ABXStatus.ARRIVED_AT_ORIGIN_STATION, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.ARRIVED_AT_HUB_TRANSIT_STATION_102_1, ABXStatus.ARRIVED_AT_HUB_TRANSIT_STATION, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.ARRIVED_AT_DESTINATION_STATION_103_1, ABXStatus.ARRIVED_AT_DESTINATION_STATION, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.OUT_FOR_DELIVERY_045_1, ABXStatus.OUT_FOR_DELIVERY, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, code, description, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.FAILED_DELIVERY_ATTEMPT);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.DELIVERY_FAILED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.DELIVERY_SUCCESSFULLY_POD, ABXStatus.DELIVERY_SUCCESSFULLY, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_CUSTOMER);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.COMPLETED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

    }


    @Test(description = "API: shipping provider - ABX shipment - valid status codes", groups = "regression", dataProvider = "abx_valid_status_data_provider")
    public void test_shipping_provider_api_valid_abx_codes(String abxStatusCode, String status_description) {
        shippingProverABXStatusSteps.call_abx_webhook_url(CommonUtils.getRandomFiveCharsString(9), abxStatusCode, status_description, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
    }

    @Test(description = "API: shipping provider - ABX shipment invalid status code", groups = "regression")
    public void test_shipping_provider_api_invalid_abx_code() {
        shippingProverABXStatusSteps.call_abx_webhook_url(CommonUtils.getRandomFiveCharsString(9), "222", ABXStatus.DELIVERY_SUCCESSFULLY, ABXEndpointCodes.FAILURE_CODE, ABXEndpointCodes.FAILURE_MESSAGE);
    }

    @Test(description = "API: shipping provider - ABX shipment invalid date", groups = "regression")
    public void test_shipping_provider_api_invalid_date() {
        abxRequest = new ABXShipmentStatusBodyDto(new ABXShipmentStatusReqDTO(new ABXShipmentStatusDTO(RandomStringUtils.random(13), ABXStatusCodes.SHIPMENT_PICKED_UP_010, ABXStatus.SHIPMENT_PICKED_UP, "randomString", "randomString", "ref123", "location")));
        abxErrorResponse = (ABXShipmentErrorResponse) shippingProverABXStatusSteps.when_updated_shipment_status(abxRequest)
                .validateResponse(HttpStatus.SC_BAD_REQUEST)
                .saveResponseObject(ABXShipmentErrorResponse.class);
        shippingProverABXStatusSteps.then_verify_error_response(abxErrorResponse);
    }


    @Test(description = "API: shipping provider - ABX shipment - delivered to station to out for delivery", groups = "regression", dataProvider = "abx_postal_codes_hubs")
    public void test_abx_transitions_delivered_to_station_to_Out_for_delivery(String senderPostalCode, String firstMileHub, String receiverPostalCode, String lastMileHub) throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment(senderPostalCode, receiverPostalCode, PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        String shipmentId = shipmentResponse.getShipments().get(0).getShipmentId();
        String id = shipmentResponse.getShipments().get(0).getId();
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan("FMD_" + firstMileHub.toUpperCase() + "_admin@logistika.com", shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        Map<String,String> expectedUpdates = new HashMap<>();
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan("HO_" + firstMileHub.toUpperCase() + "_1@logistika.com", shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan("MMD_KV_S1_F_1@logistika.com", shipmentId, WorkFlowStatus.PICKED_UP_FROM_STATION);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan("HO_" + lastMileHub.toUpperCase() + "_1@logistika.com", shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.OUT_FOR_DELIVERY_045, ABXStatus.OUT_FOR_DELIVERY, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.DELIVERY_SUCCESSFULLY_POD, ABXStatus.DELIVERY_SUCCESSFULLY, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_CUSTOMER);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.COMPLETED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);
    }


    @Test(description = "API: shipping provider - ABX shipment - delivered to station to delivered to customer", groups = "regression", dataProvider = "abx_postal_codes_hubs")
    public void test_abx_transitions_delivered_to_station_to_delivered_to_customer(String senderPostalCode, String firstMileHub, String receiverPostalCode, String lastMileHub) throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment(senderPostalCode, receiverPostalCode, PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        String shipmentId = shipmentResponse.getShipments().get(0).getShipmentId();
        String id = shipmentResponse.getShipments().get(0).getId();
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);
        scanSteps.doScan("FMD_" + firstMileHub.toUpperCase() + "_admin@logistika.com", shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        Map<String,String> expectedUpdates = new HashMap<>();
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,expectedUpdates);
        scanSteps.doScan("HO_" + firstMileHub.toUpperCase() + "_1@logistika.com", shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,expectedUpdates);
        scanSteps.doScan("MMD_KV_S1_F_1@logistika.com", shipmentId, WorkFlowStatus.PICKED_UP_FROM_STATION);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,expectedUpdates);
        scanSteps.doScan("HO_" + lastMileHub.toUpperCase() + "_1@logistika.com", shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,expectedUpdates);
        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.DELIVERY_SUCCESSFULLY_POD, ABXStatus.DELIVERY_SUCCESSFULLY, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_CUSTOMER);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.COMPLETED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id,expectedUpdates);
    }

    @Test(description = "API: shipping provider - ABX shipment - reverse shipment", groups = "regression", dataProvider = "failCodes")
    public void test_abx_transitions_customer_reverse_shipment(String code, String description) throws InterruptedException {
        ShipmentResponse shipmentResponse = (ShipmentResponse) createShipmentSteps.createShipment("60000", "30020", PickupTypes.PICKUP, WorkFlowStatusForUser.PENDING_PICKUP)
                .saveResponseObject(ShipmentResponse.class);
        String shipmentId = shipmentResponse.getShipments().get(0).getShipmentId();
        String id = shipmentResponse.getShipments().get(0).getId();
        LoggerUtils.getInstance().info("Shipment created with shipment ID " + shipmentId);
        loginSteps.when_userLogin(ApiUserRegistry.ADMIN_PORTAL);

        scanSteps.doScan(ApiUserRegistry.FMD_DJY052_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        Map<String, String> expectedUpdates = new HashMap<>();
        expectedUpdates.put("shipmentId", shipmentId);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_MERCHANT);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan(ApiUserRegistry.MMD_DJY052_TDY053_KV.username, shipmentId, WorkFlowStatus.PICKED_UP_FROM_STATION);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.PICKED_UP_FROM_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan(ApiUserRegistry.HO_TDY053_KV.username, shipmentId, WorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatus", ConsignmentApiWorkFlowStatus.DELIVERED_TO_STATION);
        expectedUpdates.put("workflowStatusForUser", ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.SHIPMENT_PICKED_UP_010, ABXStatus.SHIPMENT_PICKED_UP, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus",ConsignmentApiWorkFlowStatus.TRANSFERRED_TO_COURIER);
        expectedUpdates.put("workflowStatusForUser",ConsignmentApiShipmentStatus.IN_TRANSIT);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.ARRIVED_AT_ORIGIN_STATION_101, ABXStatus.ARRIVED_AT_ORIGIN_STATION, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.ARRIVED_AT_HUB_TRANSIT_STATION_102, ABXStatus.ARRIVED_AT_HUB_TRANSIT_STATION, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.ARRIVED_AT_DESTINATION_STATION_103, ABXStatus.ARRIVED_AT_DESTINATION_STATION, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        try {
            await().
                    atMost(20, TimeUnit.SECONDS).
                    with().pollDelay(5, TimeUnit.SECONDS).pollInSameThread().
                    pollInterval(2, TimeUnit.SECONDS).
                    untilAsserted(() -> {
                        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.OUT_FOR_DELIVERY_045, ABXStatus.OUT_FOR_DELIVERY, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
                    });
        } catch (org.awaitility.core.ConditionTimeoutException e) {
            fail(e.getMessage());
        }
        expectedUpdates.put("workflowStatus",ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        expectedUpdates.put("workflowStatusForUser",ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.WRONG_ADDRESS_060_01, ABXStatus.WRONG_ADDRESS, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus",ConsignmentApiWorkFlowStatus.FAILED_DELIVERY_ATTEMPT);
        expectedUpdates.put("workflowStatusForUser",ConsignmentApiShipmentStatus.DELIVERY_FAILED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);


        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.OUT_FOR_DELIVERY_045, ABXStatus.OUT_FOR_DELIVERY, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus",ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        expectedUpdates.put("workflowStatusForUser",ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.CANNOT_CONTACT_VIA_PHONE_060_02, ABXStatus.CANNOT_CONTACT_VIA_PHONE, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus",ConsignmentApiWorkFlowStatus.FAILED_DELIVERY_ATTEMPT);
        expectedUpdates.put("workflowStatusForUser",ConsignmentApiShipmentStatus.DELIVERY_FAILED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.OUT_FOR_DELIVERY_045, ABXStatus.OUT_FOR_DELIVERY, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus",ConsignmentApiWorkFlowStatus.OUT_FOR_DELIVERY);
        expectedUpdates.put("workflowStatusForUser",ConsignmentApiShipmentStatus.OUT_FOR_DELIVERY);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.CHANGE_ADDRESS_060_08, ABXStatus.CHANGE_ADDRESS, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus",ConsignmentApiWorkFlowStatus.FAILED_DELIVERY_ATTEMPT);
        expectedUpdates.put("workflowStatusForUser",ConsignmentApiShipmentStatus.DELIVERY_FAILED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.BACK_TO_SHIPPER_091, ABXStatus.BACK_TO_SHIPPER, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus",ConsignmentApiWorkFlowStatus.DELIVERY_UNSUCCESSFUL);
        expectedUpdates.put("workflowStatusForUser",ConsignmentApiShipmentStatus.PENDING_RETURN);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.SHIPMENT_PICKED_UP_010_1, ABXStatus.SHIPMENT_PICKED_UP, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.ARRIVED_AT_ORIGIN_STATION_101_1, ABXStatus.ARRIVED_AT_ORIGIN_STATION, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);


        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.ARRIVED_AT_HUB_TRANSIT_STATION_102_1, ABXStatus.ARRIVED_AT_HUB_TRANSIT_STATION, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);


        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.ARRIVED_AT_DESTINATION_STATION_103_1, ABXStatus.ARRIVED_AT_DESTINATION_STATION, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.OUT_FOR_DELIVERY_045_1, ABXStatus.OUT_FOR_DELIVERY, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        shippingProverABXStatusSteps.call_abx_webhook_url(shipmentId, ABXStatusCodes.RETURN_TO_ORIGIN_112, ABXStatus.RETURN_TO_ORIGIN, ABXEndpointCodes.SUCCESS_CODE, ABXEndpointCodes.SUCCESS_MESSAGE);
        expectedUpdates.put("workflowStatus",ConsignmentApiWorkFlowStatus.DELIVERY_UNSUCCESSFUL);
        expectedUpdates.put("workflowStatusForUser",ConsignmentApiShipmentStatus.PENDING_RETURN);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);

        scanSteps.doScan("MMD_KV_S1_R_1@logistika.com", shipmentId+"-RV", WorkFlowStatus.PICKED_UP_FROM_STATION);
        scanSteps.doScan(ApiUserRegistry.HO_DJY052_KV.username, shipmentId+"-RV", WorkFlowStatus.DELIVERED_TO_STATION);
        scanSteps.doScan(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId+"-RV", WorkFlowStatus.OUT_FOR_DELIVERY);
        deliveredToCustomerSteps.when_user_clicks_delivered_to_customer_button(ApiUserRegistry.LMD_DJY052_KV.username, shipmentId + "-RV", "testPath", "true");
        Map<String, String> expected = new HashMap<>();
        expected.put("shipmentId", shipmentId + "-RV");
        expected.put("workflowStatus", WorkFlowStatus.RETURNED_TO_MERCHANT);
        expected.put("workflowStatusForUser", WorkFlowStatusForUser.RETURNED);
        deliveredToCustomerSteps.then_workflow_must_be_updated(expected);
        expectedUpdates.put("workflowStatus",ConsignmentApiWorkFlowStatus.RETURNED_TO_MERCHANT);
        expectedUpdates.put("workflowStatusForUser",ConsignmentApiShipmentStatus.RETURNED);
        getShipmentByIdSteps.then_shipment_details_must_be_updated_on_api_consignment(id, expectedUpdates);
    }
    @DataProvider(name = "abx-fail-code-combinations")
    public Object[][] abx_fail_code_combinations() {
        String[][] objects = {
                {ABXStatusCodes.WRONG_ADDRESS_060_01, ABXStatus.WRONG_ADDRESS, ABXStatusCodes.WRONG_ADDRESS_060_01, ABXStatus.WRONG_ADDRESS}
                /*{ABXStatusCodes.CANNOT_CONTACT_VIA_PHONE_060_02, ABXStatus.CANNOT_CONTACT_VIA_PHONE, ABXStatusCodes.CANNOT_CONTACT_VIA_PHONE_060_02, ABXStatus.CANNOT_CONTACT_VIA_PHONE},
                {ABXStatusCodes.CONSIGNEE_REFUSED_PACKAGE_060_03, ABXStatus.CONSIGNEE_REFUSED_PACKAGE, ABXStatusCodes.WRONG_ADDRESS_060_01, ABXStatus.WRONG_ADDRESS},
                {ABXStatusCodes.CONSIGNEE_REFUSED_PACKAGE_060_03, ABXStatus.CONSIGNEE_REFUSED_PACKAGE, ABXStatusCodes.CONSIGNEE_REFUSED_PACKAGE_060_03, ABXStatus.CONSIGNEE_REFUSED_PACKAGE},
                {ABXStatusCodes.CUSTOMER_NOT_HOME_060_04, ABXStatus.CUSTOMER_NOT_HOME, ABXStatusCodes.CUSTOMER_NOT_HOME_060_04, ABXStatus.CUSTOMER_NOT_HOME},
                {ABXStatusCodes.PACKAGE_DAMAGED_060_05, ABXStatus.PACKAGE_DAMAGED, ABXStatusCodes.PACKAGE_DAMAGED_060_05, ABXStatus.PACKAGE_DAMAGED},
                {ABXStatusCodes.POSTPONE_DELIVERY_060_06, ABXStatus.POSTPONE_DELIVERY, ABXStatusCodes.REFUSED_COD_060_07, ABXStatus.REFUSED_COD},
                {ABXStatusCodes.REFUSED_COD_060_07, ABXStatus.REFUSED_COD, ABXStatusCodes.REFUSED_COD_060_07, ABXStatus.REFUSED_COD},
                {ABXStatusCodes.CHANGE_ADDRESS_060_08, ABXStatus.CHANGE_ADDRESS, ABXStatusCodes.CHANGE_ADDRESS_060_08, ABXStatus.CHANGE_ADDRESS},
                {ABXStatusCodes.DELIVERY_UNSUCCESSFUL_060_99, ABXStatus.DELIVERY_UNSUCCESSFUL, ABXStatusCodes.DELIVERY_UNSUCCESSFUL_060_99, ABXStatus.DELIVERY_UNSUCCESSFUL}*/};
        return objects;
    }

    @DataProvider(name = "failCodes")
    public Object[][] abx_fail_codes() {
        String[][] objects = {
                {ABXStatusCodes.WRONG_ADDRESS_060_01, ABXStatus.WRONG_ADDRESS},
                {ABXStatusCodes.CANNOT_CONTACT_VIA_PHONE_060_02, ABXStatus.CANNOT_CONTACT_VIA_PHONE},
                {ABXStatusCodes.CONSIGNEE_REFUSED_PACKAGE_060_03, ABXStatus.CONSIGNEE_REFUSED_PACKAGE},
                {ABXStatusCodes.CUSTOMER_NOT_HOME_060_04, ABXStatus.CUSTOMER_NOT_HOME},
                {ABXStatusCodes.PACKAGE_DAMAGED_060_05, ABXStatus.PACKAGE_DAMAGED},
                {ABXStatusCodes.POSTPONE_DELIVERY_060_06, ABXStatus.POSTPONE_DELIVERY},
                {ABXStatusCodes.REFUSED_COD_060_07, ABXStatus.REFUSED_COD},
                {ABXStatusCodes.CHANGE_ADDRESS_060_08, ABXStatus.CHANGE_ADDRESS},
                {ABXStatusCodes.DELIVERY_UNSUCCESSFUL_060_99, ABXStatus.DELIVERY_UNSUCCESSFUL}
        };
        return objects;
    }

    @DataProvider(name = "abx_valid_status_data_provider")
    public Object[][] abx_valid_status_data_provider() {
        Object[][] objects = {
                {ABXStatusCodes.SHIPMENT_PICKED_UP_010, ABXStatus.SHIPMENT_PICKED_UP},
                {ABXStatusCodes.ARRIVED_AT_ORIGIN_STATION_101, ABXStatus.ARRIVED_AT_ORIGIN_STATION},
                {ABXStatusCodes.ARRIVED_AT_HUB_TRANSIT_STATION_102, ABXStatus.ARRIVED_AT_HUB_TRANSIT_STATION},
                {ABXStatusCodes.ARRIVED_AT_DESTINATION_STATION_103, ABXStatus.ARRIVED_AT_DESTINATION_STATION},
                {ABXStatusCodes.OUT_FOR_DELIVERY_045, ABXStatus.OUT_FOR_DELIVERY},
                {ABXStatusCodes.DELIVERY_SUCCESSFULLY_POD, ABXStatus.DELIVERY_SUCCESSFULLY},
                //{"RCV", "Received at location"},
                {ABXStatusCodes.WRONG_ADDRESS_060_01, ABXStatus.WRONG_ADDRESS},
                {ABXStatusCodes.CANNOT_CONTACT_VIA_PHONE_060_02, ABXStatus.CANNOT_CONTACT_VIA_PHONE},
                {ABXStatusCodes.CONSIGNEE_REFUSED_PACKAGE_060_03, ABXStatus.CONSIGNEE_REFUSED_PACKAGE},
                {ABXStatusCodes.CUSTOMER_NOT_HOME_060_04, ABXStatus.CUSTOMER_NOT_HOME},
                {ABXStatusCodes.PACKAGE_DAMAGED_060_05, ABXStatus.PACKAGE_DAMAGED},
                {ABXStatusCodes.POSTPONE_DELIVERY_060_06, ABXStatus.POSTPONE_DELIVERY},
                {ABXStatusCodes.REFUSED_COD_060_07, ABXStatus.REFUSED_COD},
                {ABXStatusCodes.CHANGE_ADDRESS_060_08, ABXStatus.CHANGE_ADDRESS},
                {ABXStatusCodes.DELIVERY_UNSUCCESSFUL_060_99, ABXStatus.DELIVERY_UNSUCCESSFUL},
                {ABXStatusCodes.ON_THE_WAY_TO_NEW_ADDRESS_090, ABXStatus.ON_THE_WAY_TO_NEW_ADDRESS},
                {ABXStatusCodes.BACK_TO_SHIPPER_091, ABXStatus.BACK_TO_SHIPPER},
                {ABXStatusCodes.SHIPMENT_PICKED_UP_010_1, ABXStatus.SHIPMENT_PICKED_UP},
                {ABXStatusCodes.ARRIVED_AT_ORIGIN_STATION_101_1, ABXStatus.ARRIVED_AT_ORIGIN_STATION},
                {ABXStatusCodes.ARRIVED_AT_HUB_TRANSIT_STATION_102_1, ABXStatus.ARRIVED_AT_HUB_TRANSIT_STATION},
                {ABXStatusCodes.ARRIVED_AT_DESTINATION_STATION_103_1, ABXStatus.ARRIVED_AT_DESTINATION_STATION},
                {ABXStatusCodes.OUT_FOR_DELIVERY_045_1, ABXStatus.OUT_FOR_DELIVERY},
                {ABXStatusCodes.RETURN_TO_ORIGIN_112, ABXStatus.RETURN_TO_ORIGIN},
                {ABXStatusCodes.LOST_113, ABXStatus.LOST},
                {ABXStatusCodes.DAMAGE_114, ABXStatus.DAMAGE}
        };
        return objects;
    }

    @DataProvider(name = "abx_postal_codes_hubs")
    public Object[][] abx_postal_codes_hubs() {
        final Object[][] objects = {
                {"60000", "DJY052", "30020", "TDY053"},
        };
        return objects;
    }

}