package my.logistika.listener;

import io.qameta.allure.Attachment;
import my.setel.utils.LogHelper;
import org.apache.commons.io.FileUtils;
import org.testng.*;

import java.io.File;
import java.io.IOException;


public class ApiTestListener extends TestListenerAdapter{
    private static final LogHelper logger = LogHelper.getInstance();

    @Override
    public void onTestStart(ITestResult result) {
        logger.info("Test class started: " + result.getTestClass().getName());
        logger.info("Test started: " + result.getName());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        logger.info("Test SUCCESS: " + result.getName());
    }

    @Override
    public void onTestFailure(ITestResult result) {
        logger.info("Test FAILED: " + result.getName());
    }

    @Attachment(value = "TEST HTML LOG", type = "text/plain")
    private byte[] appendLogToAllure(File file) {
        try {
            return FileUtils.readFileToByteArray(file);
        } catch (IOException ignored) {
        }
        return null;
    }
}
