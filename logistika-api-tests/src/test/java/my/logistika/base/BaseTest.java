package my.logistika.base;

import com.epam.reportportal.service.tree.ItemTreeReporter;
import com.epam.reportportal.service.tree.TestItemTree;
import com.epam.reportportal.testng.ReportPortalTestNGListener;
import com.epam.reportportal.testng.TestNGService;
import com.epam.ta.reportportal.ws.model.FinishTestItemRQ;
import com.epam.ta.reportportal.ws.model.attribute.ItemAttributesRQ;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import my.logistika.constants.EndPoints;
import my.logistika.constants.Url;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.listener.ApiTestListener;
import my.logistika.microservices.authentication.login.models.LoginRequest;
import my.logistika.microservices.authentication.login.steps.LoginSteps;
//import my.setel.config.ReportPortalManager;
import my.setel.core.BaseApi;
import my.setel.httprequest.HttpRequestClientFactory;
import my.setel.utils.AllureUtils;
import my.setel.utils.LoggerUtils;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import static com.epam.reportportal.testng.TestNGService.ITEM_TREE;

@Listeners({ApiTestListener.class, ReportPortalTestNGListener.class})
public class BaseTest {

    protected LoginSteps loginSteps = new LoginSteps();

    @BeforeSuite(alwaysRun = true)
    @Parameters({"env"})
    public void preconditions(@Optional("staging") String env) throws IOException {
        if (System.getProperty("logistikaAPI.baseEnv") == null) {
            System.setProperty("logistikaAPI.baseEnv", env);
        }
//        ReportPortalManager.setReportPortal(false);
        new ApiUserRegistry();
        new my.logistika.constants.Url(System.getProperty("logistikaAPI.baseEnv"));
        BaseApi.initReqSpec();
        BaseApi.setBaseUrl(my.logistika.constants.Url.API);
    }

    @BeforeClass(alwaysRun = true)
    public void initReqSpec() {
        BaseApi.initReqSpec();
        BaseApi.setBaseUrl(my.logistika.constants.Url.API);
    }

    @AfterSuite(alwaysRun = true)
    public void postconditions() {
        AllureUtils.addAllureProperties();
        RestAssured.reset();
    }


    private void sendFinishRequest(TestItemTree.TestItemLeaf testResultLeaf, ITestResult result) {
        FinishTestItemRQ finishTestItemRQ = new FinishTestItemRQ();
        String epicValue = getClassAnnotation(result, Epic.class);
        String featureValue = getClassAnnotation(result, Feature.class);
        switch (result.getStatus()) {
            case ITestResult.SUCCESS:
                finishTestItemRQ.setStatus("PASSED");
                break;
            case ITestResult.FAILURE:
                finishTestItemRQ.setStatus("FAILED");
                finishTestItemRQ.setDescription(result.getThrowable().getMessage());
                break;
            case ITestResult.SKIP:
                finishTestItemRQ.setStatus("SKIPPED");
                break;
            default:
                break;
        }
        Set<ItemAttributesRQ> itemAttributesRQSet = new HashSet<>();
        itemAttributesRQSet.add(new ItemAttributesRQ("epic",epicValue));
        itemAttributesRQSet.add(new ItemAttributesRQ("feature",featureValue));
        itemAttributesRQSet.add(new ItemAttributesRQ("classPath",result.getTestClass().getName()));
        itemAttributesRQSet.add(new ItemAttributesRQ("debug",finishTestItemRQ.getStatus()));
        finishTestItemRQ.setAttributes(itemAttributesRQSet);
        finishTestItemRQ.setEndTime(Calendar.getInstance().getTime());
        ItemTreeReporter.finishItem(TestNGService.getReportPortal().getClient(), finishTestItemRQ, ITEM_TREE.getLaunchId(), testResultLeaf)
                .cache()
                .blockingGet();
    }

    private <T> String getClassAnnotation(ITestResult result, Class<T> clazz) {
        try {
            Class c = result.getTestClass().getRealClass();
            if (c.isAnnotationPresent(clazz)) {
                Class<? extends Annotation> a = c.getAnnotation(clazz).annotationType();
                Method m = clazz.getDeclaredMethod("value");
                return m.invoke(c.getAnnotation(clazz)).toString();
            }
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return "";
    }


}
