package my.logistika.constants.enums;

public enum DateTimeEnum {
    TODAY,
    YESTERDAY,
    TOMORROW,
    LAST_7_DAY,
    LAST_10_DAY,
    LAST_30_DAY;
}
