package my.logistika.constants;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;

public class DateFormats {

    public static final SimpleDateFormat dtFormatYMD = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat sdfYMDHMS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final SimpleDateFormat sdfYMDHMS2 = new SimpleDateFormat("yyyyMMddHHmmss");
    public static final SimpleDateFormat sdfYMD = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat sdfYMDTZ = new SimpleDateFormat ("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    public static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static final DateTimeFormatter dtfhms = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    public static final SimpleDateFormat sdfTimeZone = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    public static final DateTimeFormatter dtfYMDTZ = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    public static final DateTimeFormatter dtfhm = DateTimeFormatter.ofPattern("dd/MM/yyyy, HH:mm");
}
