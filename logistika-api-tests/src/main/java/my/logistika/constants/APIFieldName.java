package my.logistika.constants;

public class APIFieldName {

    public static class Response {
        public static final String ERROR_CODE = "errorCode";
        public static final String ERROR = "error";
        public static final String STATUS_CODE = "statusCode";
        public static final String ACCESS_TOKEN = "access_token";
        public static final String REFRESH_TOKEN = "refresh_token";
        public static final String TOKEN_TYPE = "token_type";
        public static final String EXPIRES_IN = "expires_in";
        public static final String OTP = "otp";
        public static final String OTP_TOKEN = "otpToken";
        public static final String PHONE_TOKEN = "phoneToken";
        public static final String WALLET_STATUS = "walletStatus";
        public static final String CODE = "code";
        public static final String IDENTIFIER = "identifier";
        public static final String USER_ID = "userId";
        public static final String MESSAGE = "message";
    }

    public static class Header {
        public static final String HEADER_AUTHORIZATION = "authorization";
        public static final String HEADER_OTP_TOKEN = "otp-token";
        public static final String HEADER_PHONE_TOKEN = "phone-token";
    }

    public static final String AUTHORIZATION_TYPE = "Bearer";
}
