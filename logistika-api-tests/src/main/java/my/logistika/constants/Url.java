package my.logistika.constants;

import com.google.gson.JsonObject;
import my.setel.utils.JsonUtils;

public class Url {
    public static String API;
    public Url(String env) {
        JsonObject map;
        map = JsonUtils.readJsonFile("url.json");
        JsonObject environment = map.getAsJsonObject(env);
        API = environment.get("API").getAsString();
    }
}