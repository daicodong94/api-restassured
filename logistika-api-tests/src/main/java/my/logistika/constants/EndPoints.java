package my.logistika.constants;

import my.setel.core.BaseApi;

public class EndPoints {

    public static class Authapi {
        
        private final static String BASE_URI = "/api/authenticate";
        public final static String URL_LOGIN=BASE_URI+"/login";

        public final static String URL_LOGIN_SETEL_DASHBOARD="https://api.dev.logistika.com.my/api/authenticate/login";
        public final static String URL_LOGOUT=BASE_URI+"/logout";
        public final static String URL_FORGOT_PASSWORD=BASE_URI+"/forgot-password";
        public final static String URL_RESET_PASSWORD=BASE_URI+"/reset-password";
        public final static String URL_CHANGE_PASSWORD=BASE_URI+"/change-password";
        public final static String URL_CHANGE_EMAIL=BASE_URI+"/change-email";
        public final static String URL_REFRESH_TOKEN=BASE_URI+"/refresh-token";
        public final static String URL_TOKEN=BASE_URI+"/token";
    }

    public static class MerchantApi{
        private final static String BASE_URI ="/api/merchant";
        public final static String URL_ADDRESS = BASE_URI+"/address";

        public final  static  String parcelSizeId= "parcelSizeId";

        public final static String URL_PARCEL_SIZE = BASE_URI+"/parcel-size";
        public final static String URL_UPDATE_PARCEL_SIZE=BASE_URI+"/parcel-size/{"+ parcelSizeId+"}";

    }

    public final static class UrlUpdateAddress
    {
        public final static String id="id";
        private final static String BASE_URI ="/api/merchant";
        public  final  static String URL_UPDATE_ADDRESS=BASE_URI+"/address/{"+ id+"}";
    }
    public static class AddressApi{
        private final static String BASE_URL="api/address";
        public final static String id="id";
        public final static String URL_ADDRESS=BASE_URL+"/receiver";
        public final static String URL_UPDATE_ADDRESS=BASE_URL+"/receiver/{"+id+"}";
    }

    public static class ShippingProviderApi{

        private final static String BASE_URI="/api/shippingprovider";
        public final static String ABX_SHIPMENT_STATUS = BASE_URI+"/abx/shipment_status";

    }

    public static class ReceiverApi{
        private final static String BASE_URI="/api/receiver";
        public static final String NEXT_PUBLIC_HOLIDAYS_COUNT = BASE_URI+"/next-public-holidays-count";
        public static final String URL_ACCESS_TOKEN = BASE_URI+"/auth/access-token";
        public static final String URL_REFRESH_TOKEN = BASE_URI+"/auth/refresh-token";
        public static final String URL_AUTH_PROFILE = BASE_URI+"/auth/profile";
        public static final String URL_DELIVERY_TYPE= BASE_URI+"/delivery-type";
        public static final String URL_DELIVERY_CUSTOMER_FEEDBACK= BASE_URI+"/customer-feedback";
        public static final String URL_GET_SHIPMENT_DETAILS = BASE_URI+"/shipments/details";
        public static final String URL_GET_SHIPMENT_HISTORY = BASE_URI+"/shipment-history";
        public static final String URL_GET_SAVED_SHIPMENT = BASE_URI+"/saved-shipments";
        public static final String URL_POST_ADD_SAVED_SHIPMENT = BASE_URI+"/saved-shipments/add/{shipmentId}";
        public static final String URL_DELETE_REMOVE_SAVED_SHIPMENT = BASE_URI+"/saved-shipments/remove/{shipmentId}";
    }

    public static class deliveryApi{
        private static final String BASE_URI="/api/delivery";
        public static final String URL_SCANNING =BASE_URI+"/scanning";
        public static final String URL_SHIPMENT_DELIVERED=BASE_URI+"/delivered-to-customer";
        public static final String URL_FAILED_DELIVERY=BASE_URI+"/failed-delivery";
        public static final String URL_UPDATE_STATUS=BASE_URI+"/update-status";
        public static final String URL_CHANGE_PICKUP_TYPE = BASE_URI+"/change-pickup-type";
        public static final String URL_CHANGE_DELIVERY_OPTION=BASE_URI+"/change-delivery-option";
        public static final String URL_APPROVE_RETURN=BASE_URI+"/approve-return";
        public static final String URL_ABX_SHIPMENT_UPDATES =BASE_URI+"/abx/abx-shipment-updates";
        public static final String URL_UPDATE_PARCEL_SIZE= BASE_URI+"/update-parcel-size";
        public static final String URL_ASSIGN_DRIVER=BASE_URI+"/assign-driver";
        public static final String MARK_ABX_SHIPMENT_DELIVERED=BASE_URI+"/mark-abx-shipment-delivered";
        public static final String MMD_ROUTE_NAME=BASE_URI+"/mmd-route-name";
        public static final String MMD_ROUTE_NAME_ID=BASE_URI+"/mmd-route-name/{id}";
        public static final String URL_GET_SHIPMENT_DETAILS_BY_ID=BASE_URI+"/admin/shipments/{shipmentId}";
        public static final String URL_GET_SHIPMENTS=BASE_URI+"/admin/shipments";
        public static final String URL_UPDATE_SHIPMENTS = BASE_URI+"/admin/shipments/";
        public static final String URL_UPLOAD_POD=BASE_URI+"/admin/shipments/upload-pod";
        public static final String URL_CONFIRM_POD=BASE_URI+"/admin/shipments/confirm-pod";

    }

    public static class consignmentApi {

        private static final String BASE_URI="/api/consignment";
        public static final String URL_CREATE_SHIPMENT=BASE_URI+"/shipments";
        public static final String URL_GET_SHIPMENT_BY_ID=BASE_URI+"/shipments/{id}";
        public static final String URL_GET_SHIPMENT_DETAILS=BASE_URI+"/shipments/details";
        public static final String URL_ADMIN_GET_SHIPMENT_BY_ID=BASE_URI+"/admin/shipments/{id}";
        public static final String URL_CREATE_REVERSE_SHIPMENT= BASE_URI+"/admin/shipments/{id}/reverse";

        public static final String URL_GET_SHIPMENT_AUDIT_LOG_BY_SHIPMENT_ID= BASE_URI+"/api/consignment/admin/shipment-audit-logs/shipmentId";
    }

    public static class RoutingApi {
        public final static String RUSH_HOUR_BASE_URI ="/api/routing/configuration/";
        public final static String CREATE_RUSH_HOUR = RUSH_HOUR_BASE_URI +"rush-hour";
        public final static String REARRANGE_SEQUENCES = "/api/routing/plans/rearrange-run";
        public final static String CREATE_PLAN = "/api/routing/plans";
    }

    public static class HubApi {
        public final static String API_HUB_BASE_URI ="/api/hubs/";
        public final static String VEHICLE_TYPE = API_HUB_BASE_URI+"vehicle-types/";
        public final static String GET_VEHICLE_TYPE = VEHICLE_TYPE+"detail-by-name/";
    }

    public static class sandBoxSetelApi {
        public final static String SANDBOX_SETEL_URI = "https://api.staging2.setel.my/api";
        public final static String URL_SEND_OTP = SANDBOX_SETEL_URI+"/idp/sessions/drop-in/send-otp";
        public final static String URL_VERIFY_OTP = SANDBOX_SETEL_URI+"/idp/sessions/drop-in/verify-otp";
        public final static String URL_DROPIN = SANDBOX_SETEL_URI+"/idp/sessions/drop-in";
        public final static String URL_AUTHORIZE = SANDBOX_SETEL_URI+"/connect/oauth/authorize";
    }
}
