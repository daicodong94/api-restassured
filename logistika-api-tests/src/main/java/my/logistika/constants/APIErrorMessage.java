package my.logistika.constants;

public class APIErrorMessage {

    public static class Error {
        public static final String AUTHORIZATION_CODE_NOT_FOUND = "Authorization code not found";
        public static final String AUTHORIZATION_CODE_EXPIRED = "Authorization code expired";
        public static final String BAD_REQUEST = "Bad Request";
        public static final String NOT_FOUND = "Not Found";
        public static final String INTERNAL_SERVER_ERROR = "Internal Server Error";
        public static final String UNAUTHORIZED = "User must be authorized";
        public static final String JWT_EXPIRED = "jwt expired";
        public static final String ERROR_CODE_500 = "500";
        public static final String ERROR_CODE_401 = "401";
        public static final String ERROR_CODE_400 = "400";
        public static final String ERROR_CODE_404 = "404";
        public static final Integer STATUS_CODE_500 = 500;
        public static final Integer STATUS_CODE_400 = 400;
        public static final Integer STATUS_CODE_401 = 401;
        public static final Integer STATUS_CODE_404 = 404;
        public static final Integer STATUS_CODE_201 = 201;
    }

    public static class Shipment {
        public static final String REQUIRE_CODE = "code should not be empty";
        public static final String REQUIRE_CODE_TYPE = "code must be a string";
        public static final String REQUIRE_REFRESH_TOKEN = "refreshToken should not be empty";
        public static final String REQUIRE_REFRESH_TOKEN_TYPE = "refreshToken must be a string";
        public static final String REQUIRE_VALUE = "value should not be empty";
        public static final String REQUIRE_VALUE_TYPE = "value must be a string";
        public static final String REQUIRE_TYPE = "type should not be empty";
        public static final String REQUIRE_VALUE_OF_TYPE = "type must be a valid enum value";
        public static final String SHIPMENT_NOT_FOUND = "Shipment not found";
        public static final String SAVED_SHIPMENT_NOT_FOUND = "Saved shipment not found";
        public static final String SHIPMENT_ALREADY_BEEN_ADDED = "Shipment's already been added";
        public static final String INVALID_STATUS = "each value in status must be a valid enum value";
        public static final String INVALID_LIMIT_TYPE = "limit must be a number string";
        public static final String INVALID_OFFSET_TYPE = "offset must be a number string";
    }
    public static class Delivery {
        public static final String INVALID_TRACKING_TOKEN = "Invalid tracking token";
        public static final String NON_EXISTING_VALID_SHIPMENT_STATUS = "Feedback already submited!";
        public static final String UPDATE_FEEDBACK_SUCCESSFULLY = "Customer Rating Updated!";
        public static final String UPDATE_FEEDBACK_UNSUCCESSFULLY = "Customer Feedback cannot be updated";
        public static final String REQUIRE_RATE_GREATER_THAN_10 = "rating must not be greater than 10";
        public static final String REQUIRE_RATE_LESS_THAN_0 = "rating must not be less than 0";
        public static final String REQUIRE_TRACKING_TOKEN = "trackingToken should not be empty";
        public static final String REQUIRE_TRACKING_TOKEN_TYPE = "tracking token must be a string";
    }
}
