package my.logistika.microservices.address.steps;

import io.qameta.allure.Step;
import io.restassured.response.Response;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.address.models.CreateAddressRequest;
import my.logistika.microservices.address.models.CreateAddressResponse;
import my.setel.core.BaseApi;

public class DeteleAddressSteps extends BaseApi {
    @Step("create merchant address")
    public DeteleAddressSteps when_delete_address(String id ){
       // Response res=
                sendDelete(EndPoints.AddressApi.URL_UPDATE_ADDRESS,"id",id);
        //System.out.println(res);
        return this;
    }

    @Step("verify merchant address")
    public DeteleAddressSteps then_verify_address(CreateAddressRequest expected, CreateAddressResponse actual){

        assert expected.getName().equals(actual.getName());
        assert expected.getPhone_number().equals(actual.getPhone_number());
        assert expected.getCity().equals(actual.getCity());
        assert expected.getState().equals(actual.getState());
        assert expected.getAddress1().equals(actual.getAddress1());
        assert expected.getAddress2().equals(actual.getAddress2());
        assert expected.getPostcode().equals(actual.getPostcode());
        assert expected.getPickup_instruction().equals(actual.getPickup_instruction());
        assert expected.getCountry().equals(actual.getCountry());
        return this;

    }
}
