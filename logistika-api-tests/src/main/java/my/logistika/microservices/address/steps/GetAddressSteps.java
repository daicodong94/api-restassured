package my.logistika.microservices.address.steps;

import io.qameta.allure.Step;
import io.restassured.response.Response;
import my.logistika.constants.EndPoints;
import my.setel.core.BaseApi;

import java.util.HashMap;

import java.util.List;
import java.util.Map;

public class GetAddressSteps extends BaseApi {
    Response res;

    @Step("get parcel size by visible")
    public GetAddressSteps when_get_address(String start, String limit)
    {
        Map<String,Object> map = new HashMap<>();
        map.put("start",start);
        map.put("limit",limit);
        res=sendGet(EndPoints.AddressApi.URL_ADDRESS,map);
        return  this;
    }
    @Step("verify count of address")
    public GetAddressSteps then_verify_count_of_address(Integer num)
    {
        List<Object> obs= res.as(List.class);
        assert num.equals(obs.size());
        return this;
    }
}
