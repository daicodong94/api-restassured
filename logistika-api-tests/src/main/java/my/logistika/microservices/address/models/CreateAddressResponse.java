package my.logistika.microservices.address.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreateAddressResponse {
    String name,phone_number,address1,address2,postcode,city,state,country,pickup_instruction,id,createdAt,updatedAt;
}
