package my.logistika.microservices.address.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreateAddressRequest {
     String name;
     String phone_number;
     String address1;
     String address2;
     String postcode;
     String city;
     String state;
     String country;
     String pickup_instruction;

}
