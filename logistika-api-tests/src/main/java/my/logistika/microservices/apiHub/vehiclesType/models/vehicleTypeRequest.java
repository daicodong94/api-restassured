package my.logistika.microservices.apiHub.vehiclesType.models;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class vehicleTypeRequest {
        public String name;
        public Integer maxVolumeLoad;
        public Integer maxWeight;
        public Integer seatingCapacity;
    }
