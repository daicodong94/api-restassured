package my.logistika.microservices.apiHub.vehiclesType.steps;

import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.apiHub.vehiclesType.models.vehicleTypeRequest;
import my.logistika.microservices.apiHub.vehiclesType.models.vehicleTypeResponse;
import my.setel.core.BaseApi;

public class vehicleTypeSteps extends BaseApi {

    @Step("Input data")
    public vehicleTypeRequest given_input_vehicle_type_data(String name,Integer maxVolumeLoad,Integer maxWeight, Integer seatingCapacity) {
        vehicleTypeRequest vehicleTypeRequestBody = new vehicleTypeRequest(name,maxVolumeLoad,maxWeight,seatingCapacity);
        return vehicleTypeRequestBody;
    }

    @Step("Create vehicle Type")
    public vehicleTypeSteps when_create_vehicle_type(vehicleTypeRequest vehicleTypeRequestBody) {
        sendPost(EndPoints.HubApi.VEHICLE_TYPE,vehicleTypeRequestBody);
        return this;
    }

    @Step("Verify create vehicle type success")
    public vehicleTypeSteps then_verify_create_update_vehicle_type_success(vehicleTypeRequest expected, vehicleTypeResponse actual) {
        assert expected.getName().equals(actual.getName());
        assert expected.getMaxVolumeLoad().equals(actual.getMaxVolumeLoad());
        assert expected.getMaxWeight().equals(actual.getMaxWeight());
        assert expected.getSeatingCapacity().equals(actual.getSeatingCapacity());
        return this;
    }

    @Step("Update vehicle type")
    public vehicleTypeSteps when_update_vehicle_type(String vehicleTypeId,vehicleTypeRequest vehicleTypeRequestBody) {
        sendPut(EndPoints.HubApi.VEHICLE_TYPE+vehicleTypeId,vehicleTypeRequestBody);
        return this;
    }

    @Step("Delete vehicle type")
    public vehicleTypeSteps when_delete_vehicle_type(String vehicleTypeId) {
        sendDelete(EndPoints.HubApi.VEHICLE_TYPE + vehicleTypeId);
        return this;
    }

    @Step("Verify message correct")
    public vehicleTypeSteps verifyMessage(String jsonLocator, String message) {
        assert getJsonValue(jsonLocator).equalsIgnoreCase(message);
        return this;
    }

    @Step("Verify status code correct")
    public vehicleTypeSteps verifyStatusCode(int code) {
        validateStatusCode(code);
        return this;
    }
}
