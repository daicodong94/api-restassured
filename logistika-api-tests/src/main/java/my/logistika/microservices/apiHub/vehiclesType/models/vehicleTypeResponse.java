package my.logistika.microservices.apiHub.vehiclesType.models;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class vehicleTypeResponse {
    public String name;
    public Integer maxVolumeLoad;
    public Integer maxWeight;
    public Integer seatingCapacity;
    public String vehicleTypeId;
    public String createdAt;
    public String updatedAt;
}
