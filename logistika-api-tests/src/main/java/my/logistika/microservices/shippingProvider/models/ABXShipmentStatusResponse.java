package my.logistika.microservices.shippingProvider.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ABXShipmentStatusResponse {
    ABXShipmentResponseBodyStatus res;
}

