package my.logistika.microservices.shippingProvider.constants;

public class ABXStatusCodes {

    public static final String SHIPMENT_PICKED_UP_010 = "010";
    public static final String ARRIVED_AT_ORIGIN_STATION_101 = "101";
    public static final String ARRIVED_AT_HUB_TRANSIT_STATION_102 = "102";
    public static final String ARRIVED_AT_DESTINATION_STATION_103 = "103";
    public static final String OUT_FOR_DELIVERY_045 = "045";
    public static final String DELIVERY_SUCCESSFULLY_POD = "POD";
    public static final String WRONG_ADDRESS_060_01 = "060.01";
    public static final String CANNOT_CONTACT_VIA_PHONE_060_02 = "060.02";
    public static final String CONSIGNEE_REFUSED_PACKAGE_060_03 = "060.03";
    public static final String CUSTOMER_NOT_HOME_060_04 = "060.04";
    public static final String PACKAGE_DAMAGED_060_05 = "060.05";
    public static final String POSTPONE_DELIVERY_060_06 = "060.06";
    public static final String REFUSED_COD_060_07 = "060.07";
    public static final String CHANGE_ADDRESS_060_08 = "060.08";
    public static final String DELIVERY_UNSUCCESSFUL_060_99 = "060.99";
    public static final String ON_THE_WAY_TO_NEW_ADDRESS_090 = "090";
    public static final String BACK_TO_SHIPPER_091 = "091";
    public static final String RETURN_TO_ORIGIN_112 = "112";
    public static final String LOST_113 = "113";
    public static final String DAMAGE_114 = "114";
    public static final String SHIPMENT_PICKED_UP_010_1 = "010.1";
    public static final String ARRIVED_AT_ORIGIN_STATION_101_1 = "101.1";
    public static final String ARRIVED_AT_HUB_TRANSIT_STATION_102_1 = "102.1";
    public static final String ARRIVED_AT_DESTINATION_STATION_103_1 = "103.1";
    public static final String OUT_FOR_DELIVERY_045_1 = "045.1";
}
