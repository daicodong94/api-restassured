package my.logistika.microservices.shippingProvider.steps;

import com.google.gson.Gson;
import io.qameta.allure.Step;
import my.logistika.constants.DateFormats;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.shippingProvider.models.*;
import my.setel.core.BaseApi;
import my.setel.utils.CommonUtils;
import my.setel.utils.LoggerUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.HttpStatus;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class ShippingProverABXStatusSteps extends BaseApi {

    @Step("update shipment status")
    public ShippingProverABXStatusSteps when_updated_shipment_status(ABXShipmentStatusBodyDto request){
        sendPost(EndPoints.ShippingProviderApi.ABX_SHIPMENT_STATUS,request);
        return this;
    }

    @Step("then verify status response")
    public ShippingProverABXStatusSteps then_verify_response(ABXShipmentStatusResponse response, Map<String,String> expected){
       assert response.getRes().getStatus().getStatus_code().equals(expected.get("status_code"));
       assert response.getRes().getStatus().getStatus_desc().equalsIgnoreCase(expected.get("status_desc"));
       return this;
    }

    @Step("then verify error response")
    public ShippingProverABXStatusSteps then_verify_error_response(ABXShipmentErrorResponse abxErrorResponse){
        assert abxErrorResponse.getError().equalsIgnoreCase("Bad Request");
        return this;
    }

    @Step("When abx status is posted succesfully")
    public ShippingProverABXStatusSteps call_abx_webhook_url(String shipmentId, String abxStatusCode, String status_description, String returnStatus, String result) {
        Date date = new Date();
        DateFormats.sdfYMDHMS.setTimeZone(TimeZone.getTimeZone("Asia/Kuala_Lumpur"));
        String abxStatusUpdatedDate = DateFormats.sdfYMDHMS.format(date);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DateFormats.sdfYMDHMS.toPattern());
        LocalDateTime datetime =LocalDateTime.parse(abxStatusUpdatedDate,formatter);
        datetime=datetime.minusHours(4);
        String abxStatusDate=datetime.format(formatter);
        ABXShipmentStatusBodyDto abxRequest = new ABXShipmentStatusBodyDto(new ABXShipmentStatusReqDTO(new ABXShipmentStatusDTO(shipmentId, abxStatusCode, status_description,
                abxStatusDate,abxStatusUpdatedDate,CommonUtils.getRandomFiveCharsString(5),CommonUtils.getRandomFiveCharsString(8))));
        LoggerUtils.getInstance().info(new Gson().toJson(abxRequest));
        ABXShipmentStatusResponse abxResponse = (ABXShipmentStatusResponse) when_updated_shipment_status(abxRequest)
                .validateResponse(HttpStatus.SC_CREATED)
                .saveResponseObject(ABXShipmentStatusResponse.class);
        Map<String, String> expectedMap = new HashMap<>();
        expectedMap.put("status_code", returnStatus);
        expectedMap.put("status_desc", result);
        then_verify_response(abxResponse, expectedMap);
        return this;
    }
}
