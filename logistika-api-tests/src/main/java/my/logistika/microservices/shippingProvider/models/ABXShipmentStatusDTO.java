package my.logistika.microservices.shippingProvider.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ABXShipmentStatusDTO {
    String con_no;
    String status_code;
    String status_desc;
    String status_date;
    String update_date;
    String ref_no;
    String location;
}
