package my.logistika.microservices.shippingProvider.constants;

public class ABXStatus {

    public static final String SHIPMENT_PICKED_UP = "Shipment picked up";
    public static final String ARRIVED_AT_ORIGIN_STATION = "Arrived at Origin station";
    public static final String ARRIVED_AT_HUB_TRANSIT_STATION = "Arrived at Hub/Transit station";
    public static final String ARRIVED_AT_DESTINATION_STATION = "Arrived at destination station";
    public static final String OUT_FOR_DELIVERY = "Out for delivery";
    public static final String DELIVERY_SUCCESSFULLY = "Delivery successfully";
    public static final String WRONG_ADDRESS = "Delivery unsuccessful due to Wrong Address";
    public static final String CANNOT_CONTACT_VIA_PHONE = "Delivery unsuccessful due to Cannot contact via phone";
    public static final String CONSIGNEE_REFUSED_PACKAGE = "Delivery unsuccessful due to Consignee refused the package";
    public static final String CUSTOMER_NOT_HOME = "Delivery unsuccessful due to Customer not in/home, office closed";
    public static final String PACKAGE_DAMAGED = "Delivery unsuccessful due to Package damaged";
    public static final String POSTPONE_DELIVERY = "Delivery unsuccessful due to Consignee asked to postpone delivery";
    public static final String REFUSED_COD = "Delivery unsuccessful due to Consignee refused to pay COD";
    public static final String CHANGE_ADDRESS = "Delivery unsuccessful due to change address";
    public static final String DELIVERY_UNSUCCESSFUL = "Delivery unsuccessful, pending for action";
    public static final String ON_THE_WAY_TO_NEW_ADDRESS = "On the way to new shipper address";
    public static final String BACK_TO_SHIPPER = "On the way back to shipper";
    public static final String RETURN_TO_ORIGIN = "Undelivered shipment returns to origin";
    public static final String LOST = "Lost";
    public static final String DAMAGE = "Damage";
}
