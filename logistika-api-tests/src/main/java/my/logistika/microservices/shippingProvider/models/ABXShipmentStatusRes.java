package my.logistika.microservices.shippingProvider.models;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ABXShipmentStatusRes {
    String status_code;
    String status_desc;
}
