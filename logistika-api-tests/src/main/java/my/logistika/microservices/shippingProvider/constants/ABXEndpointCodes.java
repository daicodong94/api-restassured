package my.logistika.microservices.shippingProvider.constants;

public class ABXEndpointCodes {

    public static final String SUCCESS_CODE="000";
    public static final String SUCCESS_MESSAGE="successful";
    public static final String FAILURE_CODE="005";
    public static final String FAILURE_MESSAGE="Invalid status code";
}
