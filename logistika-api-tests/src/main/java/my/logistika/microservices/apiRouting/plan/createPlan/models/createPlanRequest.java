package my.logistika.microservices.apiRouting.plan.createPlan.models;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class createPlanRequest {
    public String hubCode;
    public String date;
    public Boolean withZone;
}
