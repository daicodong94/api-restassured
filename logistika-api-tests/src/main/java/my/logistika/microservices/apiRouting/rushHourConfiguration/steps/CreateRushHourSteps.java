package my.logistika.microservices.apiRouting.rushHourConfiguration.steps;

import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.apiRouting.rushHourConfiguration.models.RushHourRequest;
import my.logistika.microservices.apiRouting.rushHourConfiguration.models.RushHourResponse;
import my.setel.core.BaseApi;

public class CreateRushHourSteps extends BaseApi {

    @Step("Create rush hour")
    public CreateRushHourSteps when_created_rush_hour(String name,String from, String till, Integer buffer,String hubCode){
        RushHourRequest inputRushHourData = new RushHourRequest(name,from,till,buffer,hubCode);
        sendPost(EndPoints.RoutingApi.CREATE_RUSH_HOUR,inputRushHourData);
        return this;
    }

    @Step("Verify create rush hour success")
    public CreateRushHourSteps then_verify_create_rush_hour_success(RushHourRequest expected, RushHourResponse actual) {
        assert expected.getName().equals(actual.getName());
        assert expected.getTill().equals(actual.getTill());
        assert expected.getBuffer().equals(actual.getBuffer());
        assert expected.getHubCode().equals(actual.getHubCode());
        return  this;
    }
    @Step("Verify message")
    public CreateRushHourSteps verifyMessage(String jsonLocator,String message) {
        assert getJsonValue(jsonLocator).equalsIgnoreCase(message);
        return this;
    }

    @Step("Verify statusCode")
    public CreateRushHourSteps verifyStatusCode(int code) {
             validateStatusCode(code);
        return this;
    }
}

