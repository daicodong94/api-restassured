package my.logistika.microservices.apiRouting.plan.createPlan.steps;

import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.apiRouting.plan.createPlan.models.createPlanRequest;
import my.setel.core.BaseApi;

public class createPlanSteps extends BaseApi {

    @Step("When send request")
    public createPlanSteps when_created_plan(String hubCode, String date, Boolean withZone) {
        createPlanRequest inputCreatePlanData = new createPlanRequest(hubCode,date,withZone);
        sendPost(EndPoints.RoutingApi.CREATE_PLAN,inputCreatePlanData);
        return this;
    }

    @Step("Verify message")
    public createPlanSteps verifyMessage(String jsonLocator, String message) {
        assert getJsonValue(jsonLocator).equalsIgnoreCase(message);
        return this;
    }

    @Step("Verify statusCode")
    public createPlanSteps verifyStatusCode(int code) {
        validateStatusCode(code);
        return this;
    }
}
