package my.logistika.microservices.apiRouting.rushHourConfiguration.models;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RushHourRequest {

    public String name;
    public String from;
    public String till;
    public Integer buffer;
    public String hubCode;

}