package my.logistika.microservices.apiRouting.plan.createPlan.steps;

import my.logistika.constants.EndPoints;
import my.setel.core.BaseApi;

public class deletePlanSteps extends BaseApi {
    public deletePlanSteps when_deleted_plan(String planID) {
        sendDelete(EndPoints.RoutingApi.CREATE_PLAN+planID);
        return this;
    }
}
