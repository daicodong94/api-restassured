package my.logistika.microservices.apiRouting.rushHourConfiguration.steps;

import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.apiRouting.rushHourConfiguration.models.RushHourRequest;
import my.logistika.microservices.apiRouting.rushHourConfiguration.models.RushHourResponse;
import my.setel.core.BaseApi;

public class UpdateRushHourSteps extends BaseApi {
    @Step("Update Rush Hour")
    public UpdateRushHourSteps when_update_rush_hour(String rushHourId,String name,String from, String till, Integer buffer,String hubCode){
        RushHourRequest inputRushHourData = new RushHourRequest(name,from,till,buffer,hubCode);
        sendPut(EndPoints.RoutingApi.RUSH_HOUR_BASE_URI+rushHourId,inputRushHourData);
        return this;
    }

    @Step("Verify update rush hour success")
    public UpdateRushHourSteps then_verify_update_rush_hour_success(RushHourRequest expected, RushHourResponse actual) {
        assert expected.getName().equals(actual.getName());
        assert expected.getTill().equals(actual.getTill());
        assert expected.getBuffer().equals(actual.getBuffer());
        assert expected.getHubCode().equals(actual.getHubCode());
        return  this;
    }

    @Step("Verify message")
    public UpdateRushHourSteps verifyMessage(String jsonLocator,String message) {
        assert getJsonValue(jsonLocator).equalsIgnoreCase(message);
        return this;
    }

    @Step("Verify statusCode")
    public UpdateRushHourSteps verifyStatusCode(int code) {
        validateStatusCode(code);
        return this;
    }
}
