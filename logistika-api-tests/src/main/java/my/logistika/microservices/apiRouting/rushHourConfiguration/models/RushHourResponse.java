package my.logistika.microservices.apiRouting.rushHourConfiguration.models;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RushHourResponse {
    public String name;
    public String from;
    public String till;
    public Integer buffer;
    public String hubCode;
    public String rushHourId;
    public String createdAt;
    public String updatedAt;
}
