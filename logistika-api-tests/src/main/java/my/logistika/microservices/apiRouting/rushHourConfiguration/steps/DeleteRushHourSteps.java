package my.logistika.microservices.apiRouting.rushHourConfiguration.steps;

import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.setel.core.BaseApi;

public class DeleteRushHourSteps extends BaseApi {
    @Step("Delete rush hour")
    public DeleteRushHourSteps when_delete_rush_hour(String rushHourId){
        sendDelete(EndPoints.RoutingApi.RUSH_HOUR_BASE_URI+rushHourId);
        return this;
    }
}
