package my.logistika.microservices.delivery.getShipmentDetails.steps;
import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.setel.core.BaseApi;
import java.util.HashMap;
import java.util.Map;

public class GetShipmentsSteps extends BaseApi {

    @Step("when a get request is made to shipments endpoint")
    public GetShipmentsSteps when_a_get_request_is_made_on_shipments_endpoint(int page, int perPage){
        Map<String,Object> queryParam = new HashMap<>();
        queryParam.put("page",page);
        queryParam.put("perPage",perPage);
        sendGet(EndPoints.deliveryApi.URL_GET_SHIPMENTS,queryParam);
        return this;
    }
}
