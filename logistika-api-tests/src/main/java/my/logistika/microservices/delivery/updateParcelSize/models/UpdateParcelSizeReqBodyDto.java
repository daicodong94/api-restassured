package my.logistika.microservices.delivery.updateParcelSize.models;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UpdateParcelSizeReqBodyDto {
   String  loggedInUserEmail;
   String  shipmentId;
   String  parcelSize;
   String  parcelSizeRemark;
}
