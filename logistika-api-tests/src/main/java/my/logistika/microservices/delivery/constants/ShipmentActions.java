package my.logistika.microservices.delivery.constants;

public enum ShipmentActions {
    cancel, lost, damaged, delivered, unsuccessfull, done, returned
}
