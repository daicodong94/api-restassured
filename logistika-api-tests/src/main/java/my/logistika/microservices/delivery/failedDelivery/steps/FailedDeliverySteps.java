package my.logistika.microservices.delivery.failedDelivery.steps;
import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.consignment.createShipment.models.CreateShipmentResponse;
import my.logistika.microservices.delivery.constants.WorkFlowStatus;
import my.logistika.microservices.delivery.constants.WorkFlowStatusForUser;
import my.logistika.microservices.delivery.failedDelivery.models.ShipmentDeliveryFailedReqBodyDto;
import my.setel.core.BaseApi;
import my.setel.utils.LoggerUtils;

import java.util.logging.Logger;

import static my.logistika.microservices.delivery.constants.FailedDeliveryReason.INCORRECT_ADDRESS;

public class FailedDeliverySteps extends BaseApi{

    @Step("When a delivery attempt is failed")
    public FailedDeliverySteps when_a_delivery_attempt_is_failed(String LMD, String shipmentId,String failedDeliveryReason){
        sendPost(EndPoints.deliveryApi.URL_FAILED_DELIVERY, new ShipmentDeliveryFailedReqBodyDto(LMD,shipmentId,failedDeliveryReason ));
        return this;
    }

    @Step("Then a shipment workflow status must be failed delivery attempt")
    public FailedDeliverySteps then_workflowstatus_must_be_failed_delivery_attempt(){
        validateStatusCode(201);
        assert getJsonValue("workflowStatus").equalsIgnoreCase(WorkFlowStatus.FAILED_DELIVERY_ATTEMPT);
        assert getJsonValue("workflowStatusForUser").equalsIgnoreCase(WorkFlowStatusForUser.DELIVERY_FAILED);
        return this;
    }

    @Step("When a delivery attempt is succesfully failed")
    public FailedDeliverySteps fail_delivery_by_LMD(CreateShipmentResponse createShipmentResponse) throws InterruptedException{
        when_a_delivery_attempt_is_failed(ApiUserRegistry.LMD_DJY052_KV.username,createShipmentResponse.getShipmentId(),INCORRECT_ADDRESS)
                .then_workflowstatus_must_be_failed_delivery_attempt();
        return this;
    }
}
