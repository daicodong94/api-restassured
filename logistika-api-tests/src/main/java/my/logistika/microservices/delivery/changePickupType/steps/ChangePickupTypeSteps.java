package my.logistika.microservices.delivery.changePickupType.steps;
import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.delivery.changePickupType.models.ChangePickupTypeReqBodyDto;
import my.setel.core.BaseApi;

public class ChangePickupTypeSteps extends BaseApi {


    @Step("when Ops support changes the pickup type")
    public ChangePickupTypeSteps when_Ops_support_changes_the_pickup_type(String email, String shipmentId, String pickupType) {
        sendPost(EndPoints.deliveryApi.URL_CHANGE_PICKUP_TYPE, new ChangePickupTypeReqBodyDto(email, shipmentId, pickupType));
        return this;
    }

    @Step("then workflow status should be changed as expected")
    public ChangePickupTypeSteps then_workflowStatus_should_be_changed_as_expected(String workFlowStatus, String workFlowStatusForUser, String pickupType) {
         assert getJsonValue("workflowStatus").equalsIgnoreCase(workFlowStatus) &&
                getJsonValue("workflowStatusForUser").equalsIgnoreCase(workFlowStatusForUser) &&
                getJsonValue("initialPickupType").equalsIgnoreCase(pickupType);
         return this;
    }
}
