package my.logistika.microservices.delivery.proofOfDelivery.steps;
import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.delivery.proofOfDelivery.models.PodConfirmDto;
import my.setel.core.BaseApi;


public class PodConfirmSteps extends BaseApi {


    @Step("when image and signature URLS are confirmed")
    public PodConfirmSteps when_image_and_signature_urls_confirmed(String shipmentId,String imageURLBody, String signatureURLBody){
        sendPost(EndPoints.deliveryApi.URL_CONFIRM_POD, new PodConfirmDto(shipmentId,imageURLBody,signatureURLBody));
        return this;
    }

    @Step("then images should be successfully uploaded")
    public PodConfirmSteps then_images_should_be_successfully_uploaded(String signature, String pod){
        assert getJsonValue("signature").equals(signature);
        assert getJsonValue("proofOfDelivery").equals(pod);
        return this;
    }


}
