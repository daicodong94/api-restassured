package my.logistika.microservices.delivery.mmdRouteName.steps;
import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.delivery.mmdRouteName.models.CreateRouteNameRequestDto;
import my.logistika.microservices.delivery.mmdRouteName.models.RouteNameModel;
import my.setel.core.BaseApi;
import org.testng.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MMDRouteNameSteps extends BaseApi {

    @Step("When an MMD route name is created")
    public MMDRouteNameSteps when_an_mmd_route_name_is_created(String routeName){
        sendPost(EndPoints.deliveryApi.MMD_ROUTE_NAME,new CreateRouteNameRequestDto(routeName));
        return this;
    }

    @Step("Then route name should be created")
    public String then_route_name_should_be_created(String routeName){
        this.validateResponse(201);
        List<RouteNameModel> routeNameModelList = when_a_GET_request_is_made_to_MMD_route_names_endpoint()
                .saveResponseListObject(RouteNameModel[].class);
        String id = null;
        for (RouteNameModel r : routeNameModelList){
            if (r.getRouteName().equals(routeName)){
               id=r.get_id();
               break;
            }
            if (routeNameModelList.indexOf(r)==routeNameModelList.size()+1){
                Assert.fail("route name not found");
                }
            }
        return id;
    }

    @Step("When a GET request is made to mmd route names endpoint")
    public  MMDRouteNameSteps when_a_GET_request_is_made_to_MMD_route_names_endpoint(){
        sendGet(EndPoints.deliveryApi.MMD_ROUTE_NAME);
        return this;
    }

    @Step("Then list of route names must be returned")
    public MMDRouteNameSteps then_list_of_route_names_must_be_returned(){
        validateResponse(200);
         List<RouteNameModel> routeNameModelList = saveResponseListObject(RouteNameModel[].class);
         assert routeNameModelList.size()!=0;
         return this;
    }

    @Step("When a PUT request is made to mmd route name")
    public MMDRouteNameSteps when_a_PUT_request_is_made_to_route_name(String id, String updatedRouteName){
        Map<String,String> pathParamMap = new HashMap<>();
        pathParamMap.put("id",id);
        sendPut(EndPoints.deliveryApi.MMD_ROUTE_NAME_ID,new CreateRouteNameRequestDto(updatedRouteName),pathParamMap);
        return this;
    }

    @Step("Then a route name must be updated")
    public MMDRouteNameSteps then_a_route_name_must_be_updated(String id, String updatedRouteName){
        this.validateResponse(200);
        List<RouteNameModel> routeNameModelList = when_a_GET_request_is_made_to_MMD_route_names_endpoint()
                .saveResponseListObject(RouteNameModel[].class);
        for (RouteNameModel r : routeNameModelList){
            if (r.get_id().equals(id)){
                assert r.getRouteName().equals(updatedRouteName);
                break;
            }
            if (routeNameModelList.indexOf(r)==routeNameModelList.size()+1){
                Assert.fail("route name not updated");
            }
        }
        return this;
    }

    @Step("When a DELETE request is made to mmd route name")
    public MMDRouteNameSteps when_a_delete_request_is_made_to_mmd_route_name(String id){
        sendDelete(EndPoints.deliveryApi.MMD_ROUTE_NAME_ID,"id",id);
        return this;
    }

    @Step("Then a route name must be deleted")
    public MMDRouteNameSteps then__route_name_must_be_deleted(String id){
        validateResponse(200);
        List<RouteNameModel> routeNameModelList = when_a_GET_request_is_made_to_MMD_route_names_endpoint()
                .saveResponseListObject(RouteNameModel[].class);
        assert routeNameModelList.size()!=0;
        for (RouteNameModel r : routeNameModelList){
            if (r.get_id().equals(id)){
                Assert.fail("route name with given id is not deleted");
            }
        }
        return this;
    }
}
