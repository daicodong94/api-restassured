package my.logistika.microservices.delivery.scanning.models;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UpdateShipmentStatusScanRequest {

    String loggedInUserEmail;
    String qrCode;
}
