package my.logistika.microservices.delivery.proofOfDelivery.steps;

import io.qameta.allure.Step;
import io.restassured.response.Response;
import io.restassured.specification.AuthenticationSpecification;
import my.logistika.constants.EndPoints;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.authentication.login.steps.LoginSteps;
import my.setel.core.BaseApi;
import my.setel.httprequest.HttpRequest;
import my.setel.httprequest.HttpRequestClientFactory;

import java.io.File;

import static io.restassured.RestAssured.given;

public class PodUploadSteps extends BaseApi {

    @Step("when user uploads a POd image")
    public Response when_user_uploads_a_pod_image(String imageType, String imagePath, String token) {
        return given().when()
                .header("authorization", token)
                .multiPart(new File(imagePath))
                .formParam("podType", imageType)
                .post(EndPoints.deliveryApi.URL_UPLOAD_POD).thenReturn();
    }

}
