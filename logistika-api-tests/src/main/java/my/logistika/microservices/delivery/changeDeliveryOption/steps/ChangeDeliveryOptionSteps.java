package my.logistika.microservices.delivery.changeDeliveryOption.steps;
import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.consignment.constants.ConsignmentApiShipmentStatus;
import my.logistika.microservices.delivery.changeDeliveryOption.models.ChangeDeliveryOptionReqBodyDto;
import my.logistika.microservices.delivery.constants.WorkFlowStatusForUser;
import my.setel.core.BaseApi;
import my.setel.utils.LoggerUtils;

public class ChangeDeliveryOptionSteps extends BaseApi {

    @Step("Given a shipment in transit")
    public ChangeDeliveryOptionSteps given_a_shipment_in_transit(){
        LoggerUtils.getInstance().info("current shipment status is "+getJsonValue("workflowStatusForUser"));
        assert getJsonValue("workflowStatusForUser").equals(ConsignmentApiShipmentStatus.IN_TRANSIT);
        return this;
    }

    @Step("When a Post request is made to change delivery")
    public ChangeDeliveryOptionSteps when_a_post_request_is_made_to_change_delivery(String email,String shipmentId, String deliveryOption ){
        sendPost(EndPoints.deliveryApi.URL_CHANGE_DELIVERY_OPTION, new ChangeDeliveryOptionReqBodyDto(email,shipmentId,deliveryOption));
        return this;
    }

    @Step("Then the delivery type must be changed")
    public ChangeDeliveryOptionSteps then_delivery_type_must_be_changed(String deliveryOption){
        assert getJsonValue("deliveryOption").equals(deliveryOption);
        return this;
    }




}
