package my.logistika.microservices.delivery.proofOfDelivery.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PodConfirmDto {

    String shipmentId;
    String podImageUrl;
    String podSignatureUrl;

}
