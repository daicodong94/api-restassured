package my.logistika.microservices.delivery.approveReturn.steps;
import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.delivery.approveReturn.models.ApproveReturnReqBodyDto;
import my.logistika.microservices.delivery.constants.WorkFlowStatus;
import my.logistika.microservices.delivery.constants.WorkFlowStatusForUser;
import my.setel.core.BaseApi;

public class ApproveReturnSteps extends BaseApi {

    @Step("When an Ops support employee clicks approve return button")
    public ApproveReturnSteps when_approve_return_trigger_is_clicked(String email,String shipmentId){
        sendPost(EndPoints.deliveryApi.URL_APPROVE_RETURN,new ApproveReturnReqBodyDto( email, shipmentId));
        return this;
    }

    @Step("Then return shipment is created")
    public ApproveReturnSteps then_a_return_shipment_is_created(String shipmentId){
       assert getJsonValue("shipmentId").equals(shipmentId+"-RV");
       assert getJsonValue("workflowStatus").equalsIgnoreCase(WorkFlowStatus.DELIVERED_TO_STATION);
       assert getJsonValue("workflowStatusForUser").equalsIgnoreCase(WorkFlowStatusForUser.IN_TRANSIT);
       assert getJsonValue("qrCode").equalsIgnoreCase(shipmentId+"-RV");
       return this;
    }
}
