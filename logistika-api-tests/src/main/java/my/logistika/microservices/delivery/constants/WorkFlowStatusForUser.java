package my.logistika.microservices.delivery.constants;

public class WorkFlowStatusForUser {
    public static final String CREATED="Created";
    public static final String PENDING_DROP_OFF="Pending drop Off";
    public static final String PENDING_PICKUP ="Pending Pickup";
    public static final String PENDING_RETURN ="pendingReturn";
    public static final String IN_TRANSIT="inTransit";
    public static final String DELIVERY_FAILED="deliveryFailed";
    public static final String COMPLETED="completed";
    public static final String ON_HOLD="onHold";
    public static final String CANCELLED="cancelled";
    public static final String RETURNED="returned";
    public static final String READY_FOR_COLLECTION="readyForCollection";
    public static final String OUT_FOR_DELIVERY="outForDelivery";
}
