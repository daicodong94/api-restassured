package my.logistika.microservices.delivery.constants;

public class DeliveryOption {

    public static final String DELIVERY="delivery";
    public static final String SELF_COLLECT="selfCollect";
}
