package my.logistika.microservices.delivery.scanning.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UpdateShipmentStatusScanResponse {

    public String rowId;
    public Integer shipmentRowId;
    public String shipmentId;
    public String workflowStatus;
    public String workflowStatusForUser;
    public String shippingProvider;
    public String deliveryRegion;
    public String parcelSize;
    public String pickupStation;
    public String deliveryStation;
    public String currentHub;
    public String initialPickupType;
    public String currentOwner;
    public String assignedDriver;
    public String assignedDriverPhoneNumber;
    public String senderPostalCode;
    public String receiverPostalCode;
    public String currentLocation;
    public String deliveryOption;
    public String qrCode;
    public Boolean isLineHaulShipment;
    public String instructions;
    public String receiverAddress;
    public String receiverAddress1;
    public String receiverAddress2;
    public String receiverCity;
    public String receiverCountry;
    public String receiverEmail;
    public String receiverName;
    public String receiverPhoneNumber;
    public String receiverState;
    public String senderAddress1;
    public String senderAddress2;
    public String senderCity;
    public String senderCountry;
    public String senderName;
    public String senderPhoneNumber;
    public String senderState;
    public Boolean isReceiverOfficeAddress;
    public String pickupVehicleType;
}