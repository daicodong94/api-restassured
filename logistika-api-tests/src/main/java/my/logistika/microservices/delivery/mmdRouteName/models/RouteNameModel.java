package my.logistika.microservices.delivery.mmdRouteName.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RouteNameModel{
    String _id;
    String routeName;
    String createdAt;
    String updatedAt;
}
