package my.logistika.microservices.delivery.changeDeliveryOption.models;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ChangeDeliveryOptionReqBodyDto {
     String loggedInUserEmail;
     String shipmentId;
     String deliveryOption;
}
