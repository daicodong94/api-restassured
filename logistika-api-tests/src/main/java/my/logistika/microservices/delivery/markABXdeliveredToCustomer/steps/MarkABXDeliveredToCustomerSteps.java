package my.logistika.microservices.delivery.markABXdeliveredToCustomer.steps;
import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.delivery.markABXdeliveredToCustomer.models.MarkABXShipmentDeliveredReqBody;
import my.setel.core.BaseApi;
import java.util.Map;

public class MarkABXDeliveredToCustomerSteps extends BaseApi {

    @Step("When Ops support team clicks ABX Mark as delivered to customer trigger")
    public MarkABXDeliveredToCustomerSteps when_Ops_support_clicks_Mark_as_delivered_to_customer(String email, String shipmentId){
        sendPost(EndPoints.deliveryApi.MARK_ABX_SHIPMENT_DELIVERED,new MarkABXShipmentDeliveredReqBody(email,shipmentId));
        return this;
    }

    @Step("then shipment workflow status must be expected")
    public MarkABXDeliveredToCustomerSteps then_shipment_workflow_status_must_be_set_to_completed() {
        validateStatusCode(201);
        return this;
    }
}
