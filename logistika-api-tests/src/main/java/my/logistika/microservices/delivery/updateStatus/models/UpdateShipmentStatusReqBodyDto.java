package my.logistika.microservices.delivery.updateStatus.models;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UpdateShipmentStatusReqBodyDto {
    String loggedInUserEmail;
    String shipmentId;
    String action;
    String remark;
}
