package my.logistika.microservices.delivery.constants;

public class WorkFlowStatus {
    public static final String CREATED="created";
    public static final String PICKUP_REQUEST_BY_MERCHANT="pickupRequestByMerchant";
    public static final String PICKED_UP_FROM_MERCHANT="pickedUpFromMerchant";
    public static final String DELIVERED_TO_STATION="deliveredToStation";
    public static final String OUT_FOR_DELIVERY="outForDelivery";
    public static final String DELIVERED_TO_CUSTOMER="deliveredToCustomer";
    public static final String PICKED_UP_FROM_STATION="pickedUpFromStation";
    public static final String TRANSFERRED_TO_COURIER="transferredToCourier";
    public static final String FAILED_DELIVERY_ATTEMPT ="failedDeliveryAttempt";
    public static final String DELIVERY_UNSUCCESSFUL = "deliveryUnsuccessful";
    public static final String DROPOFF_REQUEST_BY_MERCHANT="dropOffRequestByMerchant";
    public static final String LOST="lost";
    public static final String DAMAGED="damaged";
    public static final String SHIPMENT_CANCELLED= "shipmentCancelled";
    public static final String CANCELLED_BY_OPS="cancelledByOps";
    public static final String RETURNED_TO_MERCHANT="returnedToMerchant";
    public static final String READY_FOR_COLLECTION="readyForCollection";
}
