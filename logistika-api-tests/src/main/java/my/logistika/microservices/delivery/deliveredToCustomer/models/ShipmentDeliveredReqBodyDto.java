package my.logistika.microservices.delivery.deliveredToCustomer.models;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ShipmentDeliveredReqBodyDto {
       String loggedInUserEmail;
       String shipmentId;
       String proofOfDeliveryPic;
       String proofOfDeliverySign;
       String deliveredTo;
       String deliveryAddressType;
       String isReverseShipment;
}
