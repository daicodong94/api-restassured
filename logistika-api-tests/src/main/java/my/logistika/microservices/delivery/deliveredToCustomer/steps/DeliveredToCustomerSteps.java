package my.logistika.microservices.delivery.deliveredToCustomer.steps;
import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.delivery.constants.WorkFlowStatus;
import my.logistika.microservices.delivery.constants.WorkFlowStatusForUser;
import my.logistika.microservices.delivery.deliveredToCustomer.models.ShipmentDeliveredReqBodyDto;
import my.setel.core.BaseApi;
import my.setel.utils.LoggerUtils;

import java.util.Map;

public class
DeliveredToCustomerSteps extends BaseApi {

    @Step("when user clicks delivered to customer button")
    public DeliveredToCustomerSteps when_user_clicks_delivered_to_customer_button(String email, String shipmentId, String podPath, String isReverseShipment){
        sendPost(EndPoints.deliveryApi.URL_SHIPMENT_DELIVERED, new ShipmentDeliveredReqBodyDto(email, shipmentId,podPath,podPath,"Customer","High Rise",isReverseShipment));
        return this;
    }

    @Step("then workflow status must be updated")
    public DeliveredToCustomerSteps then_workflow_must_be_updated(Map<String,String> expectedUpdates){

        if (expectedUpdates.size() == 0) {
            throw new AssertionError("expected map is empty. It should contain elements to be validated");
        }
        for (Map.Entry<String, String> entry : expectedUpdates.entrySet()) {
            if (!getJsonValue(entry.getKey()).equalsIgnoreCase(entry.getValue())){
                LoggerUtils.getInstance().error("expected value for "+entry.getKey() +"is "+ entry.getValue() + "while actual value is "+ getJsonValue(entry.getKey()) );
            }
            assert getJsonValue(entry.getKey()).equalsIgnoreCase(entry.getValue());
        }
        return this;
    }
}
