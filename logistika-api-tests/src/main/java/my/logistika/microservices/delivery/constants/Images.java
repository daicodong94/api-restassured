package my.logistika.microservices.delivery.constants;

public class Images {

    public static final String SIGNATURE = "src/test/resources/images/signature-2.png";
    public static final String IMAGE="src/test/resources/images/image-2.jpg";
}
