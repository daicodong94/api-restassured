package my.logistika.microservices.delivery.constants;

public class PickupTypes {

    public static final String PICKUP = "Pickup";
    public static final String DROP_OFF="Drop Off";
    public static final String DECIDE_LATER="Decide later";
}
