package my.logistika.microservices.delivery.getShipmentDetails.steps;

import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.setel.core.BaseApi;
import my.setel.utils.LoggerUtils;
import org.testng.Assert;

import java.util.Map;

public class GetShipmentDetailsByIdSteps extends BaseApi {

    @Step("When a GET request is made on shipments endpoint")
    public GetShipmentDetailsByIdSteps when_a_get_request_is_made_on_shipments_endpoint(String shipmentId){
        sendGet(EndPoints.deliveryApi.URL_GET_SHIPMENT_DETAILS_BY_ID,"shipmentId",shipmentId);
        return this;
    }

    @Step("shipment details must be returned when a GET request is made to shipments endpoint")
    public GetShipmentDetailsByIdSteps then_shipment_details_must_be_returned_as_expected(String shipmentId,Map<String,String> expectedUpdates){
        if (expectedUpdates.size() == 0) {
            throw new AssertionError("expected map is empty. It should contain elements to be validated");
        }
        long timeout = System.currentTimeMillis() + 30000;
        OUTER: while (System.currentTimeMillis() < timeout) {
            sendGet(EndPoints.deliveryApi.URL_GET_SHIPMENT_DETAILS_BY_ID, "shipmentId", shipmentId);
            validateStatusCode(200);
            int i=0;
            for (Map.Entry<String, String> entry : expectedUpdates.entrySet()) {
                if (!getJsonValue(entry.getKey()).equalsIgnoreCase(entry.getValue())) {
                    if (System.currentTimeMillis() >= timeout) {
                        LoggerUtils.getInstance().info(getJsonAsString());
                        Assert.assertEquals(getJsonValue(entry.getKey()),entry.getValue());
                    } else {
                        break;
                    }
                } else {
                    i++;
                    if (i==expectedUpdates.size()){
                        LoggerUtils.getInstance().info(getJsonAsString());
                        break OUTER;
                    }
                }
            }
        }
        return this;
    }
}
