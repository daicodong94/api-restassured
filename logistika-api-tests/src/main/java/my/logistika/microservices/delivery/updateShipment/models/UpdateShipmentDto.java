package my.logistika.microservices.delivery.updateShipment.models;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateShipmentDto {

    Boolean oddSize;
    String shipmentRemark;
    String failedDeliveryReason;
    Integer failedDeliveryAttempt;
    String deliveredTo;
    String deliveryAddressType;
    String estimatedDeliveryDate;
    String currentHub;
    String nextHub;
    String assignedDeliveryPartner;
    String currentOwner;

}
