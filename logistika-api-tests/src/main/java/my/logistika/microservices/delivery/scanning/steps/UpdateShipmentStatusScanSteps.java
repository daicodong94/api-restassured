package my.logistika.microservices.delivery.scanning.steps;
import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.delivery.scanning.models.UpdateShipmentStatusScanRequest;
import my.logistika.microservices.delivery.scanning.models.UpdateShipmentStatusScanResponse;
import my.setel.core.BaseApi;
import my.setel.utils.Logger;
import my.setel.utils.LoggerUtils;
import org.testng.Assert;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.fail;
import static org.awaitility.Awaitility.await;

public class UpdateShipmentStatusScanSteps extends BaseApi {

    @Step("When POST request is made on scanning endpoint")
    public UpdateShipmentStatusScanSteps when_POST_request_is_made_on_scannning_endpoint(UpdateShipmentStatusScanRequest scanRequest) {
        sendPost(EndPoints.deliveryApi.URL_SCANNING, scanRequest);
        return this;
    }

    @Step("Then workflow status must be updated")
    public UpdateShipmentStatusScanSteps then_workflow_status_must_be_updated(Map<String, String> expected, UpdateShipmentStatusScanResponse actual) {
        LoggerUtils.getInstance().info("current workflow status is " + actual.getWorkflowStatus());
        validateStatusCode(201);
        assert expected.get("workflowStatus").equals(actual.getWorkflowStatus());
        assert expected.get("shipmentId").equals(actual.getShipmentId());
        return this;
    }

    @Step("then shipment details status must be updated on api-delivery")
    public UpdateShipmentStatusScanSteps then_shipment_details_must_be_updated(Map<String, String> expectedUpdates) {
        for (Map.Entry<String, String> entry : expectedUpdates.entrySet()) {
            Assert.assertEquals(getJsonValue(entry.getKey()), entry.getValue());
        }
        return this;
    }

    @Step("when shipping label is successfully scanned with given user")
    public UpdateShipmentStatusScanSteps doScan(String userEmail, String shipmentId, String workFlowStatus) throws InterruptedException {
        UpdateShipmentStatusScanRequest scanRequest = new UpdateShipmentStatusScanRequest(userEmail, shipmentId);
        try {
            await().
                    atMost(30, TimeUnit.SECONDS).
                    with().pollDelay(1, TimeUnit.SECONDS).pollInSameThread().
                    pollInterval(2, TimeUnit.SECONDS).
                    untilAsserted(() -> {
                        LoggerUtils.getInstance().info("scanning the label using "+userEmail+" role");
                        UpdateShipmentStatusScanResponse response = (UpdateShipmentStatusScanResponse) when_POST_request_is_made_on_scannning_endpoint(scanRequest).saveResponseObject(UpdateShipmentStatusScanResponse.class);
                        LoggerUtils.getInstance().info(getJsonAsString());
                        assertThat(response.getWorkflowStatus()).isEqualTo(workFlowStatus);
                    });
        } catch (org.awaitility.core.ConditionTimeoutException e) {
            fail(e.getMessage());
        }
        return this;
    }
}
