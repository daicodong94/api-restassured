package my.logistika.microservices.delivery.updateStatus.steps;
import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.delivery.scanning.models.UpdateShipmentStatusScanRequest;
import my.logistika.microservices.delivery.updateStatus.models.UpdateShipmentStatusReqBodyDto;
import my.setel.core.BaseApi;
import java.util.Map;

public class UpdateStatusSteps extends BaseApi {

    @Step("when a post request is made to update status endpoint")
    public UpdateStatusSteps when_a_POST_request_is_made_to_update_status_endpoint(String email, String shipmentId, String action,String qrCode){
        sendPost(EndPoints.deliveryApi.URL_UPDATE_STATUS,new UpdateShipmentStatusReqBodyDto( email,shipmentId, action,qrCode));
        return this;
    }

    @Step("then shipment workflow status must be changed as expected")
    public UpdateStatusSteps then_workflow_status_must_be_updated_as_expected(Map<String,String> expectedUpdates){
        for (Map.Entry<String, String> entry : expectedUpdates.entrySet()) {
            assert getJsonValue(entry.getKey()).equalsIgnoreCase(entry.getValue());
        }
        return this;
    }
}
