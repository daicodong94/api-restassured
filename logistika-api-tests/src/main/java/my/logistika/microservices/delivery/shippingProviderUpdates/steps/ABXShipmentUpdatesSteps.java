package my.logistika.microservices.delivery.shippingProviderUpdates.steps;
import com.google.gson.Gson;
import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.delivery.shippingProviderUpdates.models.ABXShipmentUpdatesReqBodyDto;
import my.setel.core.BaseApi;
import my.setel.utils.CommonUtils;
import my.setel.utils.JsonUtils;
import my.setel.utils.Logger;
import my.setel.utils.LoggerUtils;

public class ABXShipmentUpdatesSteps extends BaseApi {

    @Step("when a post request is made to shipping provider updates endpoint")
    public ABXShipmentUpdatesSteps when_a_post_request_is_made_to_shipping_provider_updates(ABXShipmentUpdatesReqBodyDto abxShipmentUpdatesReqBodyDto){
       LoggerUtils.getInstance().info("request body -----> " + new Gson().toJson(abxShipmentUpdatesReqBodyDto));
       sendPost(EndPoints.deliveryApi.URL_ABX_SHIPMENT_UPDATES,abxShipmentUpdatesReqBodyDto);
        return this;
    }

    @Step("then shipment status must be updated")
    public ABXShipmentUpdatesSteps then_shipment_status_must_be_updated(String workFlowStatus, String shipmentStatus, String location){
        assert getJsonValue("workflowStatus").equalsIgnoreCase(workFlowStatus);
        assert getJsonValue("workflowStatusForUser").equalsIgnoreCase(shipmentStatus);
        assert getJsonValue("currentLocation").equalsIgnoreCase(location);
        return this;
    }
}
