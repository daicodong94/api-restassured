package my.logistika.microservices.delivery.approveReturn.models;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ApproveReturnReqBodyDto {
    String loggedInUserEmail;
    String shipmentId;
}
