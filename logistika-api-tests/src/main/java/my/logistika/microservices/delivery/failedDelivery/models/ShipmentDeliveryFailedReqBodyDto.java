package my.logistika.microservices.delivery.failedDelivery.models;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ShipmentDeliveryFailedReqBodyDto {

        String loggedInUserEmail;
        String shipmentId;
        String failedDeliveryReason;

}
