package my.logistika.microservices.delivery.mmdRouteName.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreateRouteNameResponseDto  {

    String id;
    String statusCode;
}
