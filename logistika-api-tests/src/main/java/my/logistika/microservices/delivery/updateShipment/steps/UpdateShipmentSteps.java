package my.logistika.microservices.delivery.updateShipment.steps;

import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.delivery.deliveredToCustomer.steps.DeliveredToCustomerSteps;
import my.logistika.microservices.delivery.updateShipment.models.UpdateShipmentDto;
import my.setel.core.BaseApi;
import my.setel.utils.LoggerUtils;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class UpdateShipmentSteps extends BaseApi {

    @Step("when a PUT request is made to shipments endpoint")
    public UpdateShipmentSteps when_a_PUT_request_is_made_to_shipment_endpoint(String shipmentId, UpdateShipmentDto updateShipmentDto){
       sendPut(EndPoints.deliveryApi.URL_UPDATE_SHIPMENTS+shipmentId,updateShipmentDto);
        return this;
    }
}
