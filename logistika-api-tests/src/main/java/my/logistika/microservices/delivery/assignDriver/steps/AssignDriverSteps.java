package my.logistika.microservices.delivery.assignDriver.steps;

import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.delivery.assignDriver.models.AssignDriverReqBodyDto;
import my.setel.core.BaseApi;

public class AssignDriverSteps extends BaseApi {

    @Step("When an Ops team member changes FMD or LMD")
    public void when_FMD_or_LMD_is_changed(String email,String shipmentId, String assignedDriver){
        sendPost(EndPoints.deliveryApi.URL_ASSIGN_DRIVER,new AssignDriverReqBodyDto(email,shipmentId, assignedDriver));
    }
}
