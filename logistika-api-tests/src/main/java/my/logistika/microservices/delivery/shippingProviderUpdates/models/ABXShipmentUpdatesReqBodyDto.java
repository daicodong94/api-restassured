package my.logistika.microservices.delivery.shippingProviderUpdates.models;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ABXShipmentUpdatesReqBodyDto {
  String shipmentId;
  String shipmentStatus;
  String abxShipmentStatus;
  String location;
  String abxStatusCode;
  String abxRefNumber;
  String abxStatusDate;
  String abxStatusUpdatedDate;
}
