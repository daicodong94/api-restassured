package my.logistika.microservices.delivery.mmdRouteName.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreateRouteNameRequestDto {
    String routeName;
}
