package my.logistika.microservices.delivery.markABXdeliveredToCustomer.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import my.setel.core.BaseApi;
import org.testng.annotations.DataProvider;

@Data
@AllArgsConstructor
public class MarkABXShipmentDeliveredReqBody {
    String loggedInUserEmail;
    String shipmentId;
}
