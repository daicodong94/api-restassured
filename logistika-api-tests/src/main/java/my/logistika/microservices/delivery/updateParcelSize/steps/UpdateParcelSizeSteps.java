package my.logistika.microservices.delivery.updateParcelSize.steps;

import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.delivery.updateParcelSize.models.UpdateParcelSizeReqBodyDto;
import my.setel.core.BaseApi;
import my.setel.utils.LoggerUtils;

import java.util.Iterator;
import java.util.Map;

public class UpdateParcelSizeSteps extends BaseApi {

    @Step("when a post request is made to update parcel size endpoint")
    public UpdateParcelSizeSteps when_a_post_request_is_made_to_update_parcel_size(UpdateParcelSizeReqBodyDto updateParcelSizeReqBodyDto){
        sendPost(EndPoints.deliveryApi.URL_UPDATE_PARCEL_SIZE,updateParcelSizeReqBodyDto);
        return this;
    }

    @Step("Then parcel size must be updated as expected")
    public UpdateParcelSizeSteps then_parcel_size_must_be_updated_as_expected(Map<String,String> expected) {
        for (Map.Entry<String, String> entry : expected.entrySet()) {
            assert getJsonValue(entry.getKey()).equalsIgnoreCase(entry.getValue());
        }
        return this;
    }

}
