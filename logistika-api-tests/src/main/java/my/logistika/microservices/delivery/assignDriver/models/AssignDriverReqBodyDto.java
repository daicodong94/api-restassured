package my.logistika.microservices.delivery.assignDriver.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AssignDriverReqBodyDto {
        String loggedInUserEmail;
        String shipmentId;
        String assignedDriver;
    }
