package my.logistika.microservices.delivery.changePickupType.models;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ChangePickupTypeReqBodyDto {
    String loggedInUserEmail;
    String shipmentId;
    String pickupType;
}
