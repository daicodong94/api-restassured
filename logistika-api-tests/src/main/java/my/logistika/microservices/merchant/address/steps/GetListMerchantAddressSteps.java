package my.logistika.microservices.merchant.address.steps;
import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.setel.core.BaseApi;
import static org.testng.Assert.assertEquals;

public class GetListMerchantAddressSteps extends BaseApi {
    @Step("Get list address")
    public GetListMerchantAddressSteps whenGetListAddress()
    {
        sendGet(EndPoints.MerchantApi.URL_ADDRESS);
        return this;
    }
    @Step("Verify status code")
    public GetListMerchantAddressSteps verifyStatusCode(int code)
    {
        validateStatusCode(code);
        return this;
    }
    @Step("Verify message")
    public GetListMerchantAddressSteps verifyMessage(String message)
    {
        assertEquals(message,getJsonValue("message"));
        return this;
    }
    @Step("Verify Response")
    public GetListMerchantAddressSteps verifyResponse(String name, String phoneNumber, String address1, String address2, String postcode, String city, String state, String country, String pickupInstruction, String userId)
    {
        assertEquals("["+name+"]",getJsonValue("name"));
        assertEquals("["+phoneNumber+"]",getJsonValue("phoneNumber"));
        assertEquals("["+address1+"]",getJsonValue("address1"));
        assertEquals("["+address2+"]",getJsonValue("address2"));
        assertEquals("["+postcode+"]",getJsonValue("postcode"));
        assertEquals("["+city+"]",getJsonValue("city"));
        assertEquals("["+state+"]",getJsonValue("state"));
        assertEquals("["+country+"]",getJsonValue("country"));
        assertEquals("["+pickupInstruction+"]",getJsonValue("pickupInstruction"));
        assertEquals("["+userId+"]",getJsonValue("userId"));
        return this;
    }
    @Step("get Address ID")
    public String getAddressID()
    {
        String result=getJsonValue("addressId");
        result=result.replace("[","");
        result=result.replace("]","");
        return  result;
    }
}
