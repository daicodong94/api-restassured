package my.logistika.microservices.merchant.parcelSize.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreateParcelSizeRequest {
    String parcelSize;
    String description;
    String dimensionDescription;
    String weightDescription;
    String visible;// 1= true, 0 false
}
