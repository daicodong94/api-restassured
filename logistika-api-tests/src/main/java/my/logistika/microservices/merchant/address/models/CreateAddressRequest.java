package my.logistika.microservices.merchant.address.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreateAddressRequest {
    String name;
    String phoneNumber;
    String address1;
    String address2;
    String postcode;
    String city;
    String state;
    String pickupInstruction;
    String country;
}
