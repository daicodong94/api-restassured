package my.logistika.microservices.merchant.parcelSize.steps;

import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.merchant.parcelSize.models.CreateParcelSizeRequest;
import my.logistika.microservices.merchant.parcelSize.models.CreateParcelSizeResponse;
import my.setel.core.BaseApi;

public class CreateParcelSizeSteps extends BaseApi {
    @Step("create merchant address")
    public CreateParcelSizeSteps when_created_parcel_size(CreateParcelSizeRequest createParcelSizeRequest){
           sendPost(EndPoints.MerchantApi.URL_PARCEL_SIZE,createParcelSizeRequest);
        return this;
    }

    @Step("verify a parcel size")
    public CreateParcelSizeSteps then_verify_a_parcel_size(CreateParcelSizeRequest expected, CreateParcelSizeResponse actual){

        assert expected.getParcelSize().equals(actual.getParcelSize());
        assert expected.getDescription().equals(actual.getDescription());
        assert expected.getVisible().equals(actual.getVisible());
        assert expected.getDimensionDescription().equals(actual.getDimensionDescription());
        assert expected.getWeightDescription().equals(actual.getWeightDescription());
        return this;
    }
}
