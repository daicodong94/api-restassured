package my.logistika.microservices.merchant.address.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class CreateAddressResponse {
    String addressId;
    String name;
    String phoneNumber;
    String address1;
    String address2;
    String postcode;
    String city;
    String state;
    String userId;
    String pickupInstruction;
    String country;
}
