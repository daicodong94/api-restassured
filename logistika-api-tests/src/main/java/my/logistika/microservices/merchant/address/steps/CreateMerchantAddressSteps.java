package my.logistika.microservices.merchant.address.steps;

import io.qameta.allure.Step;
import io.restassured.response.Response;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.merchant.address.models.CreateAddressRequest;
import my.logistika.microservices.merchant.address.models.CreateAddressResponse;
import my.setel.core.BaseApi;
import org.testng.Assert;

public class CreateMerchantAddressSteps extends BaseApi {

    @Step("create merchant address")
    public CreateMerchantAddressSteps when_created_merchant_address(CreateAddressRequest createAddressRequest){
     Response  res= sendPost(EndPoints.MerchantApi.URL_ADDRESS,createAddressRequest);
      //  System.out.println(res);
        return this;
    }

    @Step("verify merchant address")
    public CreateMerchantAddressSteps then_verify_merchant_address(CreateAddressRequest expected, CreateAddressResponse actual){

        assert expected.getName().equals(actual.getName());
        assert expected.getPhoneNumber().equals(actual.getPhoneNumber());
        assert expected.getCity().equals(actual.getCity());
        assert expected.getState().equals(actual.getState());
        assert expected.getAddress1().equals(actual.getAddress1());
        assert expected.getAddress2().equals(actual.getAddress2());
        assert expected.getPostcode().equals(actual.getPostcode());
        assert expected.getPickupInstruction().equals(actual.getPickupInstruction());
        assert expected.getCountry().equals(actual.getCountry());
        return this;

    }
}
