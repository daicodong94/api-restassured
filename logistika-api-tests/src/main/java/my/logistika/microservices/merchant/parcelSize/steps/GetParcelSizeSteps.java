package my.logistika.microservices.merchant.parcelSize.steps;
import io.qameta.allure.Step;
import io.restassured.response.Response;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.merchant.parcelSize.models.CreateParcelSizeRequest;
import my.logistika.microservices.merchant.parcelSize.models.CreateParcelSizeResponse;
import my.setel.core.BaseApi;
import java.util.HashMap;
import java.util.Map;

public class GetParcelSizeSteps extends BaseApi{
    @Step("get all parcel size")
    public GetParcelSizeSteps when_get_all_parcel_size()
    {
        Response res=sendGet(EndPoints.MerchantApi.URL_PARCEL_SIZE,"visible",null);
        System.out.println(res.asString());
        return this;
    }
    @Step("get parcel size by visible")
    public GetParcelSizeSteps when_get_parcel_size_by_visible(Boolean bool)
    {
        Map<String,Object> map = new HashMap<>();
        map.put("visible",bool);
        sendGet(EndPoints.MerchantApi.URL_PARCEL_SIZE,map);
        return  this;
    }
    @Step("get parcel size by parcel size id")
    public GetParcelSizeSteps when_get_parcel_size_by_id(String parcelSizeId )
    {
//        Map<String, Object> map = new HashMap<String, Object>();
//        map.put("parcelSizeId",parcelSizeId );

        sendGet(EndPoints.MerchantApi.URL_PARCEL_SIZE+"/"+parcelSizeId );
        return  this;
    }
    @Step("verify a parcel size")
    public GetParcelSizeSteps then_verify_a_parcel_size(CreateParcelSizeRequest expected, CreateParcelSizeResponse actual){

        assert expected.getParcelSize().equals(actual.getParcelSize());
        assert expected.getDescription().equals(actual.getDescription());
        assert expected.getVisible().equals(actual.getVisible());
        assert expected.getDimensionDescription().equals(actual.getDimensionDescription());
        assert expected.getWeightDescription().equals(actual.getWeightDescription());
        return this;
    }
}
