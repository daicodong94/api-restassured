package my.logistika.microservices.merchant.parcelSize.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreateParcelSizeResponse {
    String parcelSizeId;
    String parcelSize;
    String description;
    String dimensionDescription;
    String weightDescription;
    String visible;
}
