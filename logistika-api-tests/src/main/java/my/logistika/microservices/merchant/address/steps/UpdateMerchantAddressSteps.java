package my.logistika.microservices.merchant.address.steps;

import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.merchant.address.models.CreateAddressRequest;
import my.logistika.microservices.merchant.address.models.CreateAddressResponse;
import my.setel.core.BaseApi;


public class UpdateMerchantAddressSteps extends BaseApi {
    @Step("Update merchant address")
    public UpdateMerchantAddressSteps when_update_merchant_address(CreateAddressRequest valueAddressRequest, String id)
    {
        sendPut(EndPoints.UrlUpdateAddress.URL_UPDATE_ADDRESS,valueAddressRequest,"id",id);
        return this;
    }
    @Step("verify merchant address")
    public UpdateMerchantAddressSteps then_verify_merchant_address(CreateAddressRequest expected, CreateAddressResponse actual){

        assert expected.getName().equals(actual.getName());
        assert expected.getPhoneNumber().equals(actual.getPhoneNumber());
        assert expected.getCity().equals(actual.getCity());
        assert expected.getState().equals(actual.getState());
        assert expected.getAddress1().equals(actual.getAddress1());
        assert expected.getAddress2().equals(actual.getAddress2());
        assert expected.getPostcode().equals(actual.getPostcode());
        assert expected.getPickupInstruction().equals(actual.getPickupInstruction());
        assert expected.getCountry().equals(actual.getCountry());
        return this;

    }
}
