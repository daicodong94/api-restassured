package my.logistika.microservices.merchant.address.steps;
import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.setel.core.BaseApi;
import static org.testng.Assert.assertEquals;


public class DeleteMerchantAddressSteps extends BaseApi {

    @Step("Delete Merchant Address")
    public DeleteMerchantAddressSteps whenDeleteMerchantAddress(String id)
    {
        sendDelete(EndPoints.UrlUpdateAddress.URL_UPDATE_ADDRESS,"id",id);
        return this;
    }
    @Step("verify Status Code")
    public DeleteMerchantAddressSteps verifyStatusCode(int code)
    {
        validateStatusCode(code);
        return this;
    }
    @Step("verify Message")
    public DeleteMerchantAddressSteps verifyMessage(String message)
    {
        assertEquals(message,getJsonValue("message"));
        return this;
    }
    @Step("verify Response")
    public DeleteMerchantAddressSteps verifyResponse(String name, String phoneNumber, String address1, String address2, String postcode, String city, String state, String country, String pickupInstruction, String userId)
    {
        assertEquals(name,getJsonValue("name"));
        assertEquals(phoneNumber,getJsonValue("phoneNumber"));
        assertEquals(address1,getJsonValue("address1"));
        assertEquals(address2,getJsonValue("address2"));
        assertEquals(postcode,getJsonValue("postcode"));
        assertEquals(city,getJsonValue("city"));
        assertEquals(state,getJsonValue("state"));
        assertEquals(country,getJsonValue("country"));
        assertEquals(pickupInstruction,getJsonValue("pickupInstruction"));
        assertEquals(userId,getJsonValue("userId"));
        return this;
    }
}
