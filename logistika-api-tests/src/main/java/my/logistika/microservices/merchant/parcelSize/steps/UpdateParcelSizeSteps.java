package my.logistika.microservices.merchant.parcelSize.steps;

import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.merchant.parcelSize.models.CreateParcelSizeRequest;
import my.logistika.microservices.merchant.parcelSize.models.CreateParcelSizeResponse;
import my.setel.core.BaseApi;

public class UpdateParcelSizeSteps extends BaseApi {
    @Step("update parcel size")
    public UpdateParcelSizeSteps when_update_parcel_size(CreateParcelSizeRequest createParcelSizeRequest, String id)
    {
        sendPut(EndPoints.MerchantApi.URL_UPDATE_PARCEL_SIZE,createParcelSizeRequest,"parcelSizeId",id);
        return this;
    }
    @Step("verify a parcel size")
    public UpdateParcelSizeSteps then_verify_a_parcel_size(CreateParcelSizeRequest expected, CreateParcelSizeResponse actual){

        assert expected.getParcelSize().equals(actual.getParcelSize());
        assert expected.getDescription().equals(actual.getDescription());
        assert expected.getVisible().equals(actual.getVisible());
        assert expected.getDimensionDescription().equals(actual.getDimensionDescription());
        assert expected.getWeightDescription().equals(actual.getWeightDescription());
        return this;
    }
}
