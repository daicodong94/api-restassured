package my.logistika.microservices.authentication.login.steps;

import com.epam.reportportal.annotations.Step;
import my.logistika.constants.*;
import my.logistika.data.DataUser;
import my.logistika.microservices.authentication.login.models.*;
import my.setel.core.BaseApi;

public class LoginSteps extends BaseApi {
    public static String access_token;

    @Step("Verify login user successfully")
    public LoginSteps when_userLogin(DataUser user) {
        LoginRequest login = new LoginRequest(user.username, user.password);
        removeHeaders();
        sendPost(EndPoints.Authapi.URL_LOGIN,login);
        validateStatusCode(201);
        access_token = getJsonValue("id_token");
        setHeader("authorization", access_token);
        return this;
    }

    @Step("Verify login user data")
    public LoginSteps when_Login(DataUser user) {
        LoginRequest login = new LoginRequest(user.username, user.password);
        removeHeaders();
        sendPost(EndPoints.Authapi.URL_LOGIN,login);
        return this;
    }

    public LoginSteps login_to_support(DataUser user) {
        LoginRequest login = new LoginRequest(user.username, user.password);
        removeHeaders();
        sendPost(EndPoints.Authapi.URL_LOGIN,login);
        setHeader("authorization", getJsonValue("id_token"));
        return this;
    }


    @Step("Get value by field")
    public String get_value_by_field(String field)
    {
        return getJsonValue(field);
    }
    @Step("Verify statusCode")
    public LoginSteps verifyStatusCode(int code) {
        validateStatusCode(code);
        return this;
    }
    @Step("Verify message")
    public LoginSteps verifyMessage( String message) {
        assert getJsonValue("message").equalsIgnoreCase(message);
        return this;
    }
    @Step("Verify")
    public LoginSteps verifyByField(String FieldName, String value) {
        assert getJsonValue(FieldName).equalsIgnoreCase(value);
        return this;
    }
    @Step("print Message")
    public LoginSteps printMessage()
    {
        System.out.println(getJsonValue("message"));
        return this;
    }
}
