package my.logistika.microservices.authentication.fogot_password.models;

public class ForgotPasswordRequest {
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ForgotPasswordRequest() {
    }

    private String   username;
}
