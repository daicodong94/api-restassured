package my.logistika.microservices.authentication.fogot_password.steps;
import io.qameta.allure.Step;
import my.logistika.constants.*;
import my.logistika.microservices.authentication.fogot_password.models.ForgotPasswordRequest;
import my.setel.core.BaseApi;
import static org.testng.Assert.assertEquals;

public class ForgotPasswordSteps extends BaseApi {
    @Step("Forgot Password")
    public ForgotPasswordSteps whenForgotPassword(String username)
    {
        ForgotPasswordRequest forgot_passwordInput= new ForgotPasswordRequest();
        forgot_passwordInput.setUsername(username);
         sendPost(EndPoints.Authapi.URL_FORGOT_PASSWORD,forgot_passwordInput);
        return this;
    }
    @Step("verify Status Code")
    public ForgotPasswordSteps verifyStatusCode(int status)
    {
        validateStatusCode(status);
        return this;
    }
    @Step("verify Message")
    public ForgotPasswordSteps verifyMessage(String message)
    {
        assertEquals(getJsonValue("message"), message);
        return this;
    }
    @Step("verify Error")
    public ForgotPasswordSteps verifyError(String error)
    {
        assertEquals(getJsonValue("error"), error);
        return this;
    }

}
