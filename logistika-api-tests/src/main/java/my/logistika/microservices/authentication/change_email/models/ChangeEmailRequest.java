package my.logistika.microservices.authentication.change_email.models;

public class ChangeEmailRequest {
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNewEmail() {
        return newEmail;
    }

    public void setNewEmail(String newEmail) {
        this.newEmail = newEmail;
    }

    public ChangeEmailRequest() {
    }

    public ChangeEmailRequest(String username, String newEmail) {
        this.username = username;
        this.newEmail = newEmail;
    }

    private String username, newEmail;
}
