package my.logistika.microservices.authentication.login.steps;

import com.epam.reportportal.annotations.Step;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.authentication.login.models.LoginRequest;
import my.setel.core.BaseApi;

public class LoginSetelDashBoardSteps extends BaseApi {
    @Step("Verify login user successfully")
    public LoginSetelDashBoardSteps when_userLogins(String email, String password) {
        LoginRequest login = new LoginRequest(email, password);
        removeHeaders();
        sendPost(EndPoints.Authapi.URL_LOGIN_SETEL_DASHBOARD,login);
        validateStatusCode(201);
        setHeader("authorization", getJsonValue("id_token"));
        return this;
    }
}
