package my.logistika.microservices.authentication.token.steps;

import io.qameta.allure.Step;
import my.logistika.constants.*;
import my.setel.core.BaseApi;
import static org.testng.Assert.assertEquals;

public class TokenSteps extends BaseApi {
    @Step("Get Token")
    public TokenSteps whenGetToken() {
        sendGet(EndPoints.Authapi.URL_TOKEN);
        return this;
    }
    @Step("verify Status Code")
    public TokenSteps verifyStatusCode(int code) {
        validateStatusCode(code);
        return this;
    }
    @Step("verify Message")
    public TokenSteps verifyMessage(String message) {
        assertEquals(getJsonValue("message"), message);
        return this;
    }
}
