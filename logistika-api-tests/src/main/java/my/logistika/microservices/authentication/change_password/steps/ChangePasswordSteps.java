package my.logistika.microservices.authentication.change_password.steps;
import io.qameta.allure.Step;
import io.restassured.response.Response;
import my.logistika.constants.*;
import my.logistika.microservices.authentication.change_password.models.ChangePasswordRequest;
import my.setel.core.BaseApi;

import static org.testng.Assert.assertEquals;

public class ChangePasswordSteps extends BaseApi {

    Response res;
    @Step("")
    public ChangePasswordSteps whenChangePassword(String username, String password, String newPassword, String confirmPassword)
    {
        ChangePasswordRequest change_password_input= new ChangePasswordRequest(username,password,newPassword,confirmPassword);
        res= sendPut(EndPoints.Authapi.URL_CHANGE_PASSWORD,change_password_input);
        return this;
    }
    @Step("verify Status Code")
    public ChangePasswordSteps verifyStatusCode(int code) {
        validateStatusCode(code);
        return this;
    }
    @Step("verify Message")
    public ChangePasswordSteps verifyMessage(String message) {
        assertEquals(getJsonValue("message"), message);
        return this;
    }
    @Step("verify Is Success")
    public ChangePasswordSteps verifyIsSuccess(String isSuccess) {
        assertEquals(getJsonValue("isSuccess"), isSuccess);
        return this;
    }
    @Step("verify Data")
    public ChangePasswordSteps verifyData(String data) {
        System.out.print(res.asString());
        assertEquals(data,getJsonValue("data") );
        return this;
    }
    @Step("verify Error")
    public ChangePasswordSteps verifyError(String name) {
        assertEquals(getJsonValue("error"), name);
        return this;
    }
}
