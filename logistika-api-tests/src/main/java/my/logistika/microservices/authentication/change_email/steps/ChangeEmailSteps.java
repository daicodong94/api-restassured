package my.logistika.microservices.authentication.change_email.steps;
import io.qameta.allure.Step;
import my.logistika.microservices.authentication.change_email.models.ChangeEmailRequest;
import my.logistika.constants.EndPoints;
import my.setel.core.BaseApi;

import static org.testng.Assert.assertEquals;

public class ChangeEmailSteps extends BaseApi {
    @Step("Reset Email")
    public ChangeEmailSteps whenResetEmail(String  user_name, String newEmail)
    {
        ChangeEmailRequest change_email_input = new ChangeEmailRequest(user_name, newEmail);
        sendPut(EndPoints.Authapi.URL_CHANGE_EMAIL,change_email_input);
        return this;
    }
    @Step("verify Status Code")
    public ChangeEmailSteps verifyStatusCode(int code) {
        validateStatusCode(code);
        return this;
    }
    @Step("verify Message")
    public ChangeEmailSteps verifyMessage(String message) {
        assertEquals(getJsonValue("message"), message);
        return this;
    }
    @Step("verify Error")
    public ChangeEmailSteps verifyError(String name) {
        assertEquals(getJsonValue("name"), name);
        return this;
    }
}
