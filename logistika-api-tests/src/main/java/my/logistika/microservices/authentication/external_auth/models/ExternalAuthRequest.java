package my.logistika.microservices.authentication.external_auth.models;

public class ExternalAuthRequest {
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNewEmail() {
        return newEmail;
    }

    public void setNewEmail(String newEmail) {
        this.newEmail = newEmail;
    }

    public ExternalAuthRequest() {
    }

    public ExternalAuthRequest(String username, String newEmail) {
        this.username = username;
        this.newEmail = newEmail;
    }

    private String username, newEmail;
}
