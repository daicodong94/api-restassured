package my.logistika.microservices.authentication.login.models;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LoginResponse {
    String token_type;
    String id_token;
    String refresh_token;
    String user_id;
    String expiredIn;
    String role;
}
