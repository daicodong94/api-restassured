package my.logistika.microservices.authentication.refesh_token.steps;
import io.qameta.allure.Step;
import my.logistika.constants.*;
import my.logistika.data.DataUser;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.authentication.login.steps.LoginSteps;
import my.logistika.microservices.authentication.refesh_token.models.RefreshTokenRequest;
import my.setel.core.BaseApi;
import static org.testng.Assert.assertEquals;

public class RefreshTokenSteps extends BaseApi {

@Step("Refresh Token")
    public RefreshTokenSteps whenRefreshToken(String refreshToken) {
        RefreshTokenRequest refresh_token_input = new RefreshTokenRequest(refreshToken);
         sendPost(EndPoints.Authapi.URL_REFRESH_TOKEN, refresh_token_input);
        return this;
    }
    @Step("Refresh Token")
    public RefreshTokenSteps whenRefreshToken(DataUser user) {
        LoginSteps loginSteps = new LoginSteps();
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        String refreshToken= loginSteps.when_userLogin(user).get_value_by_field("refresh_token");
        RefreshTokenRequest refresh_token_input = new RefreshTokenRequest(refreshToken);
         sendPost(EndPoints.Authapi.URL_REFRESH_TOKEN, refresh_token_input);
        return this;
    }
    @Step("verify Status Code")
    public RefreshTokenSteps verifyStatusCode(int statuscode) {
        validateStatusCode(statuscode);
        return this;
    }
    @Step("verify Message")
    public RefreshTokenSteps verifyMessage(String userid) {
        assertEquals(getJsonValue("message"), userid);
        return this;
    }
    @Step("verify User ID")
    public RefreshTokenSteps verifyUserID(String Message) {
        assertEquals(getJsonValue("user_id"), Message);
        return this;
    }
    @Step("verify Error")
    public RefreshTokenSteps verifyError(String error) {
        assertEquals(getJsonValue("error"), error);
        return this;
    }

}
