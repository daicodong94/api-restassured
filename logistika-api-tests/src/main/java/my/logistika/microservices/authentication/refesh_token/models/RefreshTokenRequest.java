package my.logistika.microservices.authentication.refesh_token.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RefreshTokenRequest {

    private String refreshToken;

}
