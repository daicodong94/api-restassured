package my.logistika.microservices.consignment.getShipmentDetails.models;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
 public class DetailShipmentResponseDto {
     String id;
     String shipmentId;
     String workflowStatus;
     String workflowStatusForUser;
     String noOfParcels;
     String deliveryRegion;
     String parcelSize;
     String instructions;
     String receiverName;
     String receiverAddress;
     String pickupStation;
     String intermediateStation;
     String deliveryStation;
     String currentOwner;
     String assignedDriver;
     String pickupOperatorPhoneNumber;
     String assignedDriverPhoneNumber;
     String deliveryOperatorPhoneNumber;
     String merchant;
     String merchantCode;
     String merchantUserId;
     String senderPhoneNumber;
     String merchantReferenceId;
     String parcelWeight;
     String parcelHeight;
     String parcelWidth;
     String parcelLength;
     String receiverAddress1;
     String receiverAddress2;
     String receiverPostalCode;
     String receiverCity;
     String receiverState;
     String receiverCountry;
     String receiverPhoneNumber;
     String receiverEmail;
     String senderAddress1;
     String senderAddress2;
     String senderPostalCode;
     String senderCity;
     String senderState;
     String senderCountry;
     String senderReceiverStationMmdKey;
     String deliveredAt;
     String lastStatusUpdatedAt;
     String createdBy;
     String signature;
     String deliveredTo;
     String proofOfDelivery;
     String failedDeliveryAttempts;
     String failedDeliveryReason;
     String currentLocation;
     String initialPickupType;
     String pickupVehicleType;
     String deliveryOption;
     String trackingToken;
     String shippingLabel;
     String qrCode;
     String shippingProvider;
     String serviceType;
     String isReceiverOfficeAddress;
     String createdAt;
     String updatedAt;
     String shipmentRowId;
     List<WorkflowStatusForUserLog> workflowStatusForUserLog = null;

}

@Data
@AllArgsConstructor
class WorkflowStatusForUserLog {

     String workflowStatusForUser;
     String currentLocation;
     String updatedAt;

}