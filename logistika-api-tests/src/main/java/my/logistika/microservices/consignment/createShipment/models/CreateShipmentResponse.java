package my.logistika.microservices.consignment.createShipment.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
 public class CreateShipmentResponse{
     String id;
     String merchantReferenceId;
     String noOfParcels;
     String parcelWeight;
     String parcelLength;
     String parcelWidth;
     String parcelHeight;
     String parcelSize;
     String receiverName;
     String receiverPhoneNumber;
     String receiverAddress1;
     String receiverAddress2;
     String receiverPostalCode;
     String receiverCity;
     String receiverState;
     String instructions;
     String isReceiverOfficeAddress;
     String receiverAddress;
     String receiverEmail;
     String merchant;
     String initialPickupType;
     String senderAddress1;
     String senderAddress2;
     String senderCity;
     String senderState;
     String senderPostalCode;
     String senderPhoneNumber;
     String deliveryRegion;
     String pickupStation;
     String deliveryStation;
     String shippingProvider;
     String serviceType;
     String merchantUserId;
     String pickupVehicleType;
     String shipmentId;
     String trackingToken;
     String workflowStatusForUser;
     String deliveryOption;
     String createdAt;
     String updatedAt;

}
