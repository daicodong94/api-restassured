package my.logistika.microservices.consignment.createShipment.models;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
 public class CreateShipmentDTO {

     String merchantReferenceId;
     String noOfParcels;
     String parcelWeight;
     String parcelLength;
     String parcelWidth;
     String parcelHeight;
     String parcelSize;
     String receiverName;
     String receiverPhoneNumber;
     String receiverAddress1;
     String receiverAddress2;
     String receiverPostalCode;
     String receiverCity;
     String receiverState;
     String instructions;
     String isReceiverOfficeAddress;
     String receiverAddress;
     String receiverEmail;
     String merchant;
     String merchantPhoneNumber;
     String merchantUserIdReferenceId;
     String initialPickupType;
     String senderAddress1;
     String senderAddress2;
     String senderCountry;
     String senderCity;
     String senderState;
     String senderPostalCode;
     String senderPhoneNumber;
     String workflowStatus;
     String merchantUserId;

}
