package my.logistika.microservices.consignment.reverseShipment.steps;

import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.setel.core.BaseApi;

public class CreateReverseShipmentSteps extends BaseApi {

    @Step("When a post request is made to create reverse shipment")
    public CreateReverseShipmentSteps when_a_post_request_is_made_to_create_reverse_shipment(String shipmentId){
        sendPost(EndPoints.consignmentApi.URL_CREATE_REVERSE_SHIPMENT.replace("{id}",shipmentId));
        return this;
    }

}
