package my.logistika.microservices.consignment.createShipment.steps;

import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.consignment.createShipment.models.*;
import my.logistika.microservices.consignment.createShipment.models.CreateShipment;
import my.logistika.microservices.delivery.constants.ParcelSize;
import my.setel.core.BaseApi;
import my.setel.utils.CommonUtils;
import my.setel.utils.LoggerUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CreateShipmentSteps extends BaseApi {

    @Step("When a POST request is made to shipments endpoint")
    public CreateShipmentSteps when_a_POST_request_is_made_to_shipments_endpoint(CreateShipment createShipmentRequest) {
       sendPost(EndPoints.consignmentApi.URL_CREATE_SHIPMENT,createShipmentRequest);
       return this;
    }

    @Step("Then shipment must be created")
    public CreateShipmentSteps then_shipment_must_be_created(Map<String,String> expected, CreateShipmentResponse shipmentResponseList){
        LoggerUtils.getInstance().info("shipment created with Id "+shipmentResponseList.getShipmentId());
        LoggerUtils.getInstance().info("Shipment created with work flow status"+shipmentResponseList.getWorkflowStatusForUser());
        assert expected.get("workflowStatusForUser").equals(shipmentResponseList.getWorkflowStatusForUser());
        assert expected.get("merchantReferenceId").equals(shipmentResponseList.getMerchantReferenceId());
        return this;
    }

    @Step("When a shipment is created successfully")
    public CreateShipmentSteps createShipment(String senderPostalCode, String receiverPostalCode, String pickupType, String expectedWorkFlowStatusforUser) {
        String refId = CommonUtils.getRandomFiveCharsString();
        CreateShipmentDTO createShipmentDTO = new CreateShipmentDTO(
                refId, "1", "2", "5", "4", "3", ParcelSize.S.name(), "SHIVA",
                "+601111111111", "Test dr 44", "", receiverPostalCode, "",
                "Kuala Lumpur", "optional delivery instructions", "0", "", "",
                "Sravan Mer", "+60999999999", "", pickupType,
                "address 1 mer", "address 2 mer", "", "", "Kuala Lumpur",
                senderPostalCode, "+609999999999", "", ApiUserRegistry.MERCHANT.userId);
        List<CreateShipmentDTO> list = new ArrayList<>();
        list.add(createShipmentDTO);
        CreateShipment createShipmentRequest = new CreateShipment(list);
        ShipmentResponse shipmentResponse = (ShipmentResponse) when_a_POST_request_is_made_to_shipments_endpoint(createShipmentRequest)
                .validateResponse(201)
                .saveResponseObject(ShipmentResponse.class);
        Map<String,String> expectedMap = new HashMap<>();
        expectedMap.put("workflowStatusForUser",expectedWorkFlowStatusforUser);
        expectedMap.put("merchantReferenceId",refId);
        then_shipment_must_be_created(expectedMap, shipmentResponse.getShipments().get(0));
        return this;
    }

    @Step("When a shipment is created successfully")
    public CreateShipmentSteps createShipment(CreateShipmentDTO createShipmentDTO, Map<String, String> expectedShipmentData) {
        List<CreateShipmentDTO> list = new ArrayList<>();
        list.add(createShipmentDTO);
        CreateShipment createShipmentRequest = new CreateShipment(list);
        ShipmentResponse shipmentResponse = (ShipmentResponse) when_a_POST_request_is_made_to_shipments_endpoint(createShipmentRequest)
                .validateResponse(201)
                .saveResponseObject(ShipmentResponse.class);
        then_shipment_must_be_created(expectedShipmentData, shipmentResponse.getShipments().get(0));
        return this;
    }
}
