package my.logistika.microservices.consignment.constants;

public class ConsignmentApiShipmentStatus {
    public static final String CREATED="Created";
    public static final String PENDING_DROP_OFF="Pending drop Off";
    public static final String PENDING_PICKUP ="Pending Pickup";
    public static final String PENDING_RETURN ="pending Return";
    public static final String IN_TRANSIT="In Transit";
    public static final String DELIVERY_FAILED="Delivery Failed";
    public static final String COMPLETED="Completed";
    public static final String ON_HOLD="On Hold";
    public static final String CANCELLED="Cancelled";
    public static final String RETURNED="Returned";
    public static final String READY_FOR_COLLECTION="Ready for collection";
    public static final String OUT_FOR_DELIVERY="Out for delivery";
}
