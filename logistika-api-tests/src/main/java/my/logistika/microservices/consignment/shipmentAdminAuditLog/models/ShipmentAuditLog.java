package my.logistika.microservices.consignment.shipmentAdminAuditLog.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class ShipmentAuditLog {
    public String  shipmentId;
    public String  createdOn;
    public String eventType;
    public String customDate;
    public String updatedByName;

}
