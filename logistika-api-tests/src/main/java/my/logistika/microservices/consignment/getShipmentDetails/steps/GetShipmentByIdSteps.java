package my.logistika.microservices.consignment.getShipmentDetails.steps;

import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.setel.core.BaseApi;
import my.setel.utils.LoggerUtils;
import org.testng.Assert;

import java.util.Map;

public class GetShipmentByIdSteps extends BaseApi {

    @Step("When a GET request is made to get shipments endpoint")
    public GetShipmentByIdSteps when_a_GET_request_is_made_to_shipments_endpoint(String id) {
        sendGet(EndPoints.consignmentApi.URL_GET_SHIPMENT_BY_ID, "id", id);
        return this;
    }

    @Step("Then shipment details must be returned")
    public GetShipmentByIdSteps then_shipment_details_must_be_returned(String shipmentId) {
        assert getJsonValue("shipmentId").equals(shipmentId);
        return this;
    }

    @Step("Then shipment details must be updated on api consignment")
    public GetShipmentByIdSteps then_shipment_details_must_be_updated_on_api_consignment(String id, Map<String, String> expectedUpdates) {
        if (expectedUpdates.size() == 0) {
            throw new AssertionError("expected map is empty. It should contain elements to be validated");
        }
        long timeout = System.currentTimeMillis() + 30000;
        OUTER: while (System.currentTimeMillis() < timeout) {
            sendGet(EndPoints.consignmentApi.URL_GET_SHIPMENT_BY_ID, "id", id);
            validateStatusCode(200);
            int i=0;
            for (Map.Entry<String, String> entry : expectedUpdates.entrySet()) {
                if (!getJsonValue(entry.getKey()).equalsIgnoreCase(entry.getValue())) {
                    if (System.currentTimeMillis() >= timeout) {
                        LoggerUtils.getInstance().info(getJsonAsString());
                        Assert.assertEquals(getJsonValue(entry.getKey()),entry.getValue());
                    } else {
                        break;
                    }
                } else {
                    i++;
                    if (i==expectedUpdates.size()){
                        LoggerUtils.getInstance().info(getJsonAsString());
                        break OUTER;
                    }
                }
                }
            }
        return this;
    }

    @Step("Then shipment details must be returned on ADMIN api consignment")
    public GetShipmentByIdSteps then_shipment_details_must_be_returned_on_ADMIN_api_consignment(String id, Map<String, String> expectedUpdates) {
        if (expectedUpdates.size() == 0) {
            throw new AssertionError("expected map is empty. It should contain elements to be validated");
        }
        long timeout = System.currentTimeMillis() + 30000;
        OUTER: while (System.currentTimeMillis() < timeout) {
            sendGet(EndPoints.consignmentApi.URL_ADMIN_GET_SHIPMENT_BY_ID, "id", id);
            validateStatusCode(200);
            int i=0;
            for (Map.Entry<String, String> entry : expectedUpdates.entrySet()) {
                if (!getJsonValue(entry.getKey()).equalsIgnoreCase(entry.getValue())) {
                    if (System.currentTimeMillis() >= timeout) {
                        LoggerUtils.getInstance().info(getJsonAsString());
                        Assert.assertEquals(getJsonValue(entry.getKey()),entry.getValue());
                    } else {
                        break;
                    }
                } else {
                    i++;
                    if (i==expectedUpdates.size()){
                        LoggerUtils.getInstance().info(getJsonAsString());
                        break OUTER;
                    }
                }
            }
        }
        return this;
    }
}
