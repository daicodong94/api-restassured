package my.logistika.microservices.consignment.shipmentAdminAuditLog.steps;

import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.consignment.shipmentAdminAuditLog.models.ShipmentAuditLog;
import my.setel.core.BaseApi;

import java.util.HashMap;
import java.util.Map;

public class ShipmentAuditLogSteps extends BaseApi {
    @Step("When get shipment audit log by shipment Id")
    public ShipmentAuditLogSteps when_get_shipment_audit_log_by_shipment_Id(String shipmentId, String eventType,String createdOn,String customDate,String updatedByName) {
        Map<String,Object> map=new HashMap();
        map.put("eventType",eventType);
        map.put("createdOn",createdOn);
        map.put("customDate",customDate);
        map.put("updatedByName",updatedByName);
        sendGet(EndPoints.consignmentApi.URL_GET_SHIPMENT_AUDIT_LOG_BY_SHIPMENT_ID.replace("{shipmentId}",shipmentId),map );
        return this;
    }
}
