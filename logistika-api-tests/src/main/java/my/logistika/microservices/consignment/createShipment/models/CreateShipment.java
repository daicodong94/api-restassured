package my.logistika.microservices.consignment.createShipment.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class CreateShipment {
     List<CreateShipmentDTO> data = null;
}
