package my.logistika.microservices.consignment.getShipmentDetails.steps;
import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.data.user.ApiUserRegistry;
import my.logistika.microservices.authentication.login.steps.LoginSteps;
import my.setel.core.BaseApi;
import my.setel.utils.LoggerUtils;
import org.testng.Assert;
import java.util.HashMap;
import java.util.Map;

public class GetShipmentDetailsByTrackingToken extends BaseApi {

    LoginSteps loginSteps = new LoginSteps();

    @Step("When a GET request is made to get shipment details endpoint")
    public GetShipmentDetailsByTrackingToken when_a_GET_request_is_made_to_shipment_details_endpoint(String shipmentId, String trackingToken) {
        removeHeaders();
        loginSteps.when_userLogin(ApiUserRegistry.MERCHANT);
        Map<String, Object> queryMap = new HashMap<>();
        queryMap.put("shipmentid",shipmentId);
        queryMap.put("trackingtoken",trackingToken);
        sendGet(EndPoints.consignmentApi.URL_GET_SHIPMENT_DETAILS, queryMap);
        return this;
    }

    @Step("Then shipment details must be updated on api consignment")
    public GetShipmentDetailsByTrackingToken then_shipment_details_must_be_updated_on_consignment_service(String shipmentId, String trackingToken, Map<String, String> expectedUpdates) {
        if (expectedUpdates.size() == 0) {
            Assert.fail("expected map is empty. It should contain elements to be validated");
        }
        long timeout = System.currentTimeMillis() + 30000;
        OUTER:
        while (System.currentTimeMillis() < timeout) {
            when_a_GET_request_is_made_to_shipment_details_endpoint(shipmentId, trackingToken);
            int i = 0;
            for (Map.Entry<String, String> entry : expectedUpdates.entrySet()) {
                if (!getJsonValue(entry.getKey()).equalsIgnoreCase(entry.getValue())) {
                    if (System.currentTimeMillis() >= timeout) {
                        Assert.assertEquals(getJsonValue(entry.getKey()), entry.getValue());
                    } else {
                        break;
                    }
                } else {
                    i++;
                    if (i == expectedUpdates.size()) {
                        LoggerUtils.getInstance().info(getJsonAsString());
                        break OUTER;
                    }
                }
            }
        }
        return this;
    }
}
