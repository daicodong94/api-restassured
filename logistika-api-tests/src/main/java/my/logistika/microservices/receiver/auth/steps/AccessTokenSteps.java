package my.logistika.microservices.receiver.auth.steps;

import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.receiver.auth.models.AccessTokenReqBody;
import my.setel.core.BaseApi;

import static my.logistika.constants.APIErrorMessage.Error.*;
import static my.logistika.constants.APIErrorMessage.Shipment.*;
import static my.logistika.constants.APIFieldName.*;
import static my.logistika.constants.APIFieldName.Response.*;


public class AccessTokenSteps extends BaseApi {
    @Step("Call API create access token")
    public AccessTokenSteps given_create_access_token(String code){
        removeHeaders();
        sendPost(EndPoints.ReceiverApi.URL_ACCESS_TOKEN,new AccessTokenReqBody(code));
        return this;
    }

    @Step("Call API create access token with empty body")
    public AccessTokenSteps given_create_access_token_with_empty_body(){
        removeHeaders();
        sendPost(EndPoints.ReceiverApi.URL_ACCESS_TOKEN,"{}");
        return this;
    }

    @Step("Call API create access token with code is integer")
    public AccessTokenSteps given_create_access_token_with_type_of_code_is_integer(Integer code_value){
        removeHeaders();
        sendPost(EndPoints.ReceiverApi.URL_ACCESS_TOKEN,"{ \"code\": "+code_value+" }");
        return this;
    }

    @Step("Call API create access token with code is boolean")
    public AccessTokenSteps given_create_access_token_with_type_of_code_is_boolean(Boolean code_value){
        removeHeaders();
        sendPost(EndPoints.ReceiverApi.URL_ACCESS_TOKEN,"{ \"code\": "+code_value+" }");
        return this;
    }

    @Step("Verify statusCode")
    public AccessTokenSteps then_verify_status_code(int code) {
        validateStatusCode(code);
        return this;
    }

    @Step("Verify errorCode")
    public AccessTokenSteps then_verify_error_code(String error) {
        assert getJsonValue(ERROR_CODE).contains(error);
        return this;
    }

    @Step("Verify error")
    public AccessTokenSteps then_verify_error(String error) {
        assert getJsonValue(ERROR).contains(error);
        return this;
    }
    
    @Step("Verify statusCode on Response")
    public AccessTokenSteps then_verify_response_status_code(int code) {
        assert Integer.parseInt(getJsonValue(STATUS_CODE))==code;
        return this;
    }

    @Step("Verify errorMessage")
    public AccessTokenSteps then_verify_error_message(String error) {
        assert getJsonAsString().contains(error);
        return this;
    }

    @Step("Get the returned access token")
    public String then_get_the_returned_access_token(){
        return getJsonValue(ACCESS_TOKEN);
    }

    @Step("Get the returned refresh token")
    public String then_get_the_returned_refresh_token(){
        return getJsonValue(REFRESH_TOKEN);
    }

    @Step("Verify response body when get access token successfully")
    public AccessTokenSteps then_verify_get_access_token_successfully(){
        assert !getJsonValue(ACCESS_TOKEN).isEmpty();
        assert !getJsonValue(REFRESH_TOKEN).isEmpty();
        assert AUTHORIZATION_TYPE.equals(getJsonValue(TOKEN_TYPE));
        String time = getJsonValue(EXPIRES_IN);
        int expires_time = Integer.parseInt(time);
        assert expires_time > 0;
        return this;
    }

    @Step("Verify response body when code is empty")
    public AccessTokenSteps then_verify_response_body_when_code_is_empty(){
        then_verify_error_message(REQUIRE_CODE);
        then_verify_error(BAD_REQUEST);
        then_verify_response_status_code(STATUS_CODE_400);
        return this;
    }

    @Step("Verify response body when code is null")
    public AccessTokenSteps then_verify_response_body_when_code_is_null(){
        then_verify_error_message(REQUIRE_CODE);
        then_verify_error_message(REQUIRE_CODE_TYPE);
        then_verify_error(BAD_REQUEST);
        then_verify_response_status_code(STATUS_CODE_400);
        return this;
    }

    @Step("Verify response body when enter invalid type of code")
    public AccessTokenSteps then_verify_response_body_when_enter_invalid_type_of_code(){
        then_verify_error_message(REQUIRE_CODE_TYPE);
        then_verify_error(BAD_REQUEST);
        then_verify_response_status_code(STATUS_CODE_400);
        return this;
    }

    @Step("Verify authorization code not found")
    public AccessTokenSteps then_verify_returned_authorization_code_not_found(){
        then_verify_error_message(AUTHORIZATION_CODE_NOT_FOUND);
        then_verify_error_code(ERROR_CODE_400);
        then_verify_response_status_code(STATUS_CODE_400);
        return this;
    }
}
