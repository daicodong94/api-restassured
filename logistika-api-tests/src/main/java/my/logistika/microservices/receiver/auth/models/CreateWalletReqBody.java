package my.logistika.microservices.receiver.auth.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreateWalletReqBody {
    String pin;
}
