package my.logistika.microservices.receiver.delivery.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DeliveryCustomerFeedbackReqBody {
    String  trackingToken;
    Integer rating;
    String  feedback;
}
