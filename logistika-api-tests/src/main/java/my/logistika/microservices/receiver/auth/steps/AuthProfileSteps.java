package my.logistika.microservices.receiver.auth.steps;

import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.setel.core.BaseApi;

import static my.logistika.constants.APIErrorMessage.Error.*;
import static my.logistika.constants.APIFieldName.*;
import static my.logistika.constants.APIFieldName.Header.HEADER_AUTHORIZATION;
import static my.logistika.constants.APIFieldName.Response.*;


public class AuthProfileSteps extends BaseApi {
    @Step("Call API get auth profile with authorization")
    public AuthProfileSteps given_get_auth_profile_with_authorization(String accessToken){
        removeHeaders();
        setHeader(HEADER_AUTHORIZATION,AUTHORIZATION_TYPE+" "+accessToken);
        sendGet(EndPoints.ReceiverApi.URL_AUTH_PROFILE);
        return this;
    }

    @Step("Call API get auth profile without authorization")
    public AuthProfileSteps given_get_auth_profile_without_authorization(){
        removeHeaders();
        sendGet(EndPoints.ReceiverApi.URL_AUTH_PROFILE);
        return this;
    }

    @Step("Verify statusCode")
    public AuthProfileSteps then_verify_status_code(int code) {
        validateStatusCode(code);
        return this;
    }

    @Step("Verify errorCode")
    public AuthProfileSteps then_verify_error_code(String error) {
        assert getJsonValue(ERROR_CODE).contains(error);
        return this;
    }
    
    @Step("Verify statusCode on Response")
    public AuthProfileSteps then_verify_response_status_code(int code) {
        assert Integer.parseInt(getJsonValue(STATUS_CODE))==code;
        return this;
    }

    @Step("Verify errorMessage")
    public AuthProfileSteps then_verify_error_message(String error) {
        assert getJsonAsString().contains(error);
        return this;
    }

    @Step("Verify response body when get access token successfully")
    public AuthProfileSteps then_verify_get_access_token_successfully(String phone){
        assert !getJsonValue(USER_ID).isEmpty();
        assert phone.equals(getJsonValue(IDENTIFIER));
        return this;
    }

    @Step("Verify unauthorized")
    public AuthProfileSteps then_verify_returned_unauthorized(){
        then_verify_error_message(UNAUTHORIZED);
        then_verify_error_code(ERROR_CODE_401);
        then_verify_response_status_code(STATUS_CODE_401);
        return this;
    }
}
