package my.logistika.microservices.receiver.delivery.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class CustomerFeedbackResponse {
    Integer statusCode;
    String message;
    String error;
}
