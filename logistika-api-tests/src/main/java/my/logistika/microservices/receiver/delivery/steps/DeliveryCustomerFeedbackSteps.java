package my.logistika.microservices.receiver.delivery.steps;

import io.qameta.allure.Step;
import my.logistika.constants.APIErrorMessage;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.receiver.auth.models.AccessTokenReqBody;
import my.logistika.microservices.receiver.auth.steps.AccessTokenSteps;
import my.logistika.microservices.receiver.delivery.models.DeliveryCustomerFeedbackReqBody;
import my.setel.core.BaseApi;
//import org.apache.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

import static my.logistika.constants.APIErrorMessage.Error.*;
import static my.logistika.constants.APIErrorMessage.Delivery.*;
import static my.logistika.constants.APIFieldName.AUTHORIZATION_TYPE;
import static my.logistika.constants.APIFieldName.Header.HEADER_AUTHORIZATION;
import static my.logistika.constants.APIFieldName.Response.*;

public class DeliveryCustomerFeedbackSteps extends BaseApi {
    @Step("Call API add customer feedback ")
    public DeliveryCustomerFeedbackSteps add_customer_feedback(String trackingToken, Integer rate, String feedback  ){
        sendPost(EndPoints.ReceiverApi.URL_DELIVERY_CUSTOMER_FEEDBACK, new DeliveryCustomerFeedbackReqBody(trackingToken, rate, feedback));
        return this;
    }
    @Step("Call API add customer feedback with access token ")
    public DeliveryCustomerFeedbackSteps add_customer_feedback_with_access_token(String accesstoken, String trackingToken, Integer rate, String feedback  ){
        setHeader(HEADER_AUTHORIZATION, AUTHORIZATION_TYPE+""+accesstoken);
        sendPost(EndPoints.ReceiverApi.URL_DELIVERY_CUSTOMER_FEEDBACK, new DeliveryCustomerFeedbackReqBody(trackingToken, rate, feedback));
        return this;
    }
    @Step("Call API add customer feedback with rating is double type ")
    public DeliveryCustomerFeedbackSteps add_customer_feedback_double(String trackingToken, double rate, String feedback  ){
        sendPost(EndPoints.ReceiverApi.URL_DELIVERY_CUSTOMER_FEEDBACK, "{\"trackingToken\": "+trackingToken+", \"rating\": "+rate+", \"feedback\": "+feedback+"}");
        return this;
    }
    @Step("Call API add customer feedback with req body ")
    public DeliveryCustomerFeedbackSteps add_customer_feedback_with_req_body(Map<String, Object> queryParams){
        sendPost(EndPoints.ReceiverApi.URL_DELIVERY_CUSTOMER_FEEDBACK, queryParams);
        return this;
    }
    @Step("Call API create access token")
    public DeliveryCustomerFeedbackSteps create_access_token(String code){
        removeHeaders();
        sendPost(EndPoints.ReceiverApi.URL_ACCESS_TOKEN, new AccessTokenReqBody(code));
        return this;
    }
    @Step("Get the return access token")
    public String get_access_token(){
        return getJsonValue(ACCESS_TOKEN);
    }
    @Step("Verify statusCode")
    public DeliveryCustomerFeedbackSteps then_verify_status_code(int code){
        validateStatusCode(code);
        return this;
    }
    @Step("Verify errorCode")
    public DeliveryCustomerFeedbackSteps then_verify_error_code(String error){
        assert getJsonValue(ERROR_CODE).contains(error);
        return this;
    }
    @Step("Verify error")
    public DeliveryCustomerFeedbackSteps then_verify_error(String error){
        assert getJsonValue(ERROR).contains(error);
        return this;
    }
    @Step("Verify statusCode on Response")
    public DeliveryCustomerFeedbackSteps then_verify_statusCode_on_Response(int code){
        assert Integer.parseInt(getJsonValue(STATUS_CODE))==code;
        return this;
    }
    @Step("Verify message when Update feedback successfully")
    public DeliveryCustomerFeedbackSteps then_verify_message_update_feedback_successfully(){
        assert getJsonValue(MESSAGE).equals(APIErrorMessage.Delivery.UPDATE_FEEDBACK_SUCCESSFULLY);
        return this;
    }
    @Step("Verify message when Update feedback unsuccessfully")
    public DeliveryCustomerFeedbackSteps then_verify_message_update_feedback_unsuccessfully(){
        assert getJsonValue(MESSAGE).contains(APIErrorMessage.Delivery.UPDATE_FEEDBACK_UNSUCCESSFULLY);
        return this;
    }
    @Step("Verify message invalid trackingToken")
    public DeliveryCustomerFeedbackSteps then_verify_message_invalid_tracking_token(){
        assert getJsonValue(MESSAGE).contains(APIErrorMessage.Delivery.INVALID_TRACKING_TOKEN);
        return this;
    }
    @Step("Verify message non existing valid shipment status")
    public DeliveryCustomerFeedbackSteps then_verify_message_non_existing_valid_shipment_status(){
        assert getJsonValue(MESSAGE).contains(APIErrorMessage.Delivery.NON_EXISTING_VALID_SHIPMENT_STATUS);
        return this;
    }
    @Step("Verify message on response")
    public DeliveryCustomerFeedbackSteps then_verify_message_on_response(String message){
        assert getJsonValue(MESSAGE).contains(message);
        return this;
    }
    @Step("Verify errorMessage")
    public DeliveryCustomerFeedbackSteps then_verify_error_message(String error) {
        assert getJsonAsString().contains(error);
        return this;
    }
    @Step("Verify errorMessage on array")
    public DeliveryCustomerFeedbackSteps then_verify_error_message_array(String message) {
        String Response = String.valueOf(getJsonArray(this.getResponse()).get(0));
        assert Response.contains(message);
        return this;
    }
    @Step("Verify response body when update feedback successfully")
    public DeliveryCustomerFeedbackSteps then_verify_response_body_update_feedback_successfully(){
        then_verify_statusCode_on_Response(STATUS_CODE_201);
        then_verify_message_update_feedback_successfully();
        return this;
    }
    @Step("Verify response body when update feedback unsuccessfully")
    public DeliveryCustomerFeedbackSteps then_verify_response_body_update_feedback_unsuccessfully(){
        then_verify_statusCode_on_Response(STATUS_CODE_400);
        then_verify_error_message(BAD_REQUEST);
        then_verify_error(BAD_REQUEST);
        then_verify_message_update_feedback_unsuccessfully();
        return this;
    }
    @Step("Verify response body when send feedback with value of rate > 10")
    public DeliveryCustomerFeedbackSteps then_verify_response_body_when_send_feedback_with_value_of_rate_greater_than_10(){
        then_verify_statusCode_on_Response(STATUS_CODE_400);
        then_verify_error_message(REQUIRE_RATE_GREATER_THAN_10);
        then_verify_error(BAD_REQUEST);
        return this;
    }
    @Step("Verify response body when send feedback with value of rate < 0")
    public DeliveryCustomerFeedbackSteps then_verify_response_body_when_send_feedback_with_value_of_rate_less_than_0(){
        then_verify_statusCode_on_Response(STATUS_CODE_400);
        then_verify_error_message(REQUIRE_RATE_LESS_THAN_0);
        then_verify_error(BAD_REQUEST);
        return this;
    }
    @Step("Verify Response body when send feedback with invalid value of tracking token")
    public DeliveryCustomerFeedbackSteps then_verify_response_body_when_send_feedback_with_invalid_value_of_tracking_token(){
        then_verify_statusCode_on_Response(STATUS_CODE_400);
        then_verify_message_on_response(INVALID_TRACKING_TOKEN);
        then_verify_error(BAD_REQUEST);
        return this;
    }
}