package my.logistika.microservices.receiver.shipments.steps;

import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.setel.core.BaseApi;

import java.util.Map;

import static my.logistika.constants.APIErrorMessage.Error.*;
import static my.logistika.constants.APIErrorMessage.Shipment.*;
import static my.logistika.constants.APIFieldName.AUTHORIZATION_TYPE;
import static my.logistika.constants.APIFieldName.Header.HEADER_AUTHORIZATION;
import static my.logistika.constants.APIFieldName.Response.*;


public class ShipmentDetailSteps extends BaseApi {
    @Step("Call API get shipment details with authorization")
    public ShipmentDetailSteps given_get_shipment_details_with_authorization(String accessToken, Map<String, Object> queryParams){
        removeHeaders();
        setHeader(HEADER_AUTHORIZATION,AUTHORIZATION_TYPE+" "+accessToken);
        sendGet(EndPoints.ReceiverApi.URL_GET_SHIPMENT_DETAILS,queryParams);
        return this;
    }

    @Step("Call API get shipment details without authorization")
    public ShipmentDetailSteps given_get_shipment_details_without_authorization(Map<String, Object> queryParams){
        removeHeaders();
        sendGet(EndPoints.ReceiverApi.URL_GET_SHIPMENT_DETAILS,queryParams);
        return this;
    }

    @Step("Verify statusCode")
    public ShipmentDetailSteps then_verify_status_code(int code) {
        validateStatusCode(code);
        return this;
    }

    @Step("Verify errorCode")
    public ShipmentDetailSteps then_verify_error_code(String error) {
        assert getJsonValue(ERROR_CODE).contains(error);
        return this;
    }

    @Step("Verify error")
    public ShipmentDetailSteps then_verify_error(String error) {
        assert getJsonValue(ERROR).contains(error);
        return this;
    }
    
    @Step("Verify statusCode on Response")
    public ShipmentDetailSteps then_verify_response_status_code(Integer code) {
        assert Integer.parseInt(getJsonValue(STATUS_CODE))==code;
        return this;
    }

    @Step("Verify errorMessage")
    public ShipmentDetailSteps then_verify_error_message(String error) {
        assert getJsonAsString().contains(error);
        return this;
    }

    @Step("Verify response body when get shipment details successfully")
    public ShipmentDetailSteps then_verify_get_shipment_details_successfully(String shipmentId, String trackingToken, String workflowStatus, String receiverPhone) {
        assert shipmentId.equals(getJsonValue("shipmentId"));
        assert trackingToken.equals(getJsonValue("trackingToken"));
        assert workflowStatus.equals(getJsonValue("workflowStatusForUser"));
        assert receiverPhone.equals(getJsonValue("receiverPhoneNumber"));
        return this;
    }

    @Step("Verify response body is shipment not found")
    public ShipmentDetailSteps then_verify_response_body_is_shipment_not_found(){
        then_verify_error_message(SHIPMENT_NOT_FOUND);
        then_verify_error_code(ERROR_CODE_404);
        then_verify_response_status_code(STATUS_CODE_404);
        return this;
    }

    @Step("Verify response body when value is empty")
    public ShipmentDetailSteps then_verify_response_body_when_value_is_empty(){
        then_verify_error_message(REQUIRE_VALUE);
        then_verify_error(BAD_REQUEST);
        then_verify_response_status_code(STATUS_CODE_400);
        return this;
    }

    @Step("Verify response body when value is null")
    public ShipmentDetailSteps then_verify_response_body_when_value_is_null(){
        then_verify_error_message(REQUIRE_VALUE);
        then_verify_error_message(REQUIRE_VALUE_TYPE);
        then_verify_error(BAD_REQUEST);
        then_verify_response_status_code(STATUS_CODE_400);
        return this;
    }

    @Step("Verify response body when do not send type")
    public ShipmentDetailSteps then_verify_response_body_when_do_not_send_type(){
        then_verify_error_message(REQUIRE_TYPE);
        then_verify_error_message(REQUIRE_VALUE_OF_TYPE);
        then_verify_error(BAD_REQUEST);
        then_verify_response_status_code(STATUS_CODE_400);
        return this;
    }

    @Step("Verify response body when enter invalid value of type field")
    public ShipmentDetailSteps then_verify_response_body_when_enter_invalid_value_of_type_field(){
        then_verify_error_message(REQUIRE_VALUE_OF_TYPE);
        then_verify_error(BAD_REQUEST);
        then_verify_response_status_code(STATUS_CODE_400);
        return this;
    }

    @Step("Verify unauthorized")
    public ShipmentDetailSteps then_verify_returned_unauthorized(){
        then_verify_error_message(UNAUTHORIZED);
        then_verify_error_code(ERROR_CODE_401);
        then_verify_response_status_code(STATUS_CODE_401);
        return this;
    }
}
