package my.logistika.microservices.receiver.shipments.enums;

import java.util.*;

import static java.util.Collections.*;

public enum ShipmentHistoryGetFields {
    noOfParcels,
    parcelWeight,
    parcelLength,
    parcelWidth,
    parcelHeight,
    parcelSize,
    receiverName,
    receiverPhoneNumber,
    receiverAddress1,
    receiverAddress2,
    receiverPostalCode,
    receiverCity,
    receiverState,
    instructions,
    receiverAddress,
    receiverEmail,
    merchant,
    initialPickupType,
    senderAddress1,
    senderAddress2,
    senderCountry,
    senderCity,
    senderState,
    senderPostalCode,
    senderPhoneNumber,
    workflowStatus,
    merchantUserId;

    private static List<ShipmentHistoryGetFields> VALUES =  unmodifiableList(Arrays.asList(values()));
    private static final Random RANDOM = new Random();

    public static List<String> getRandomValue(int size) {
        List<String> randomValues = new ArrayList<>();
        while (randomValues.size() != size){
            int randomIndex = RANDOM.nextInt(VALUES.size());
            if (!randomValues.contains(String.valueOf(VALUES.get(randomIndex)))) {
                randomValues.add(String.valueOf(VALUES.get(randomIndex)));
            }
        }
        return randomValues;
    }

    public static String toString(List<String> input) {
        StringBuilder random = new StringBuilder();
        for (int i = 0; i < input.size(); i++) {
            random.append(input.get(i));
            if (i < input.size() - 1) random.append(",");
        }
        return random.toString();
    }
}
