package my.logistika.microservices.receiver.NextPublicHolidaysCount.models;

import com.atlassian.httpclient.api.HttpStatus;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NextPublicHolidaysCountResponse {
    HttpStatus statusCode;
    int count;
}
