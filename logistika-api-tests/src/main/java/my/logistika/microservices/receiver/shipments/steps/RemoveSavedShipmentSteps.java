package my.logistika.microservices.receiver.shipments.steps;

import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.setel.core.BaseApi;

import static my.logistika.constants.APIErrorMessage.Error.*;
import static my.logistika.constants.APIErrorMessage.Shipment.*;
import static my.logistika.constants.APIFieldName.AUTHORIZATION_TYPE;
import static my.logistika.constants.APIFieldName.Header.HEADER_AUTHORIZATION;
import static my.logistika.constants.APIFieldName.Response.*;


public class RemoveSavedShipmentSteps extends BaseApi {
    @Step("Call API post delete saved shipment with authorization")
    public RemoveSavedShipmentSteps given_delete_remove_saved_shipment_with_authorization(String accessToken, String shipmentId) {
        removeHeaders();
        setHeader(HEADER_AUTHORIZATION, AUTHORIZATION_TYPE + " " + accessToken);
        sendDelete(EndPoints.ReceiverApi.URL_DELETE_REMOVE_SAVED_SHIPMENT,"shipmentId",shipmentId);
        return this;
    }

    @Step("Call API post delete saved shipment with authorization and multi shipmentId")
    public RemoveSavedShipmentSteps given_delete_remove_saved_shipment_with_authorization_and_multi_shipmentId(String accessToken, String shipmentId) {
        removeHeaders();
        setHeader(HEADER_AUTHORIZATION, AUTHORIZATION_TYPE + " " + accessToken);
        String shipmentId2 = shipmentId.substring(0,10);
        int lastId = Integer.parseInt(shipmentId.substring(10))-1;
        String multi_shipmentId = shipmentId+","+shipmentId2+lastId;
        sendDelete(EndPoints.ReceiverApi.URL_DELETE_REMOVE_SAVED_SHIPMENT,"shipmentId",multi_shipmentId);
        return this;
    }

    @Step("Call API post delete saved shipment without authorization")
    public RemoveSavedShipmentSteps given_delete_remove_saved_shipment_without_authorization(String shipmentId) {
        removeHeaders();
        sendDelete(EndPoints.ReceiverApi.URL_DELETE_REMOVE_SAVED_SHIPMENT,"shipmentId",shipmentId);
        return this;
    }

    @Step("Verify statusCode")
    public RemoveSavedShipmentSteps then_verify_status_code(int code) {
        validateStatusCode(code);
        return this;
    }

    @Step("Verify errorCode")
    public RemoveSavedShipmentSteps then_verify_error_code(String error) {
        assert getJsonValue(ERROR_CODE).contains(error);
        return this;
    }

    @Step("Verify error")
    public RemoveSavedShipmentSteps then_verify_error(String error) {
        assert getJsonValue(ERROR).contains(error);
        return this;
    }

    @Step("Verify statusCode on Response")
    public RemoveSavedShipmentSteps then_verify_response_status_code(Integer code) {
        assert Integer.parseInt(getJsonValue(STATUS_CODE)) == code;
        return this;
    }

    @Step("Verify errorMessage")
    public RemoveSavedShipmentSteps then_verify_error_message(String error) {
        assert getJsonAsString().contains(error);
        return this;
    }

    @Step("Verify response body when delete saved shipment successfully")
    public RemoveSavedShipmentSteps then_verify_response_body_remove_saved_shipment_successfully(String phoneNumber, String shipmentId) {
        assert "OK".equals(getJsonValue("message"));
        assert !getJsonValue("data._id").isEmpty();
        assert !getJsonValue("data.createdAt").isEmpty();
        assert !getJsonValue("data.updatedAt").isEmpty();
        assert getJsonValue("data.createdAt").equals(getJsonValue("data.updatedAt"));
        assert shipmentId.equals(getJsonValue("data.shipmentId"));
        assert phoneNumber.equals(getJsonValue("data.identifier"));
        return this;
    }

    @Step("Verify response body when saved shipment not found")
    public RemoveSavedShipmentSteps then_verify_response_body_when_saved_shipment_not_found(){
        then_verify_error_message(SAVED_SHIPMENT_NOT_FOUND);
        then_verify_error_code(ERROR_CODE_404);
        then_verify_response_status_code(STATUS_CODE_404);
        return this;
    }

    @Step("Verify response body when send request without authorization")
    public RemoveSavedShipmentSteps then_verify_returned_unauthorized(){
        then_verify_error_message(UNAUTHORIZED);
        then_verify_error_code(ERROR_CODE_401);
        then_verify_response_status_code(STATUS_CODE_401);
        return this;
    }
}
