package my.logistika.microservices.receiver.auth.steps;

import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.receiver.auth.models.RefreshTokenReqBody;
import my.setel.core.BaseApi;

import static my.logistika.constants.APIErrorMessage.Error.*;
import static my.logistika.constants.APIErrorMessage.Shipment.*;
import static my.logistika.constants.APIFieldName.*;
import static my.logistika.constants.APIFieldName.Response.*;


public class RefreshTokenSteps extends BaseApi {
    @Step("Call API create refresh token")
    public RefreshTokenSteps given_create_refresh_token(String refreshToken){
        removeHeaders();
        sendPost(EndPoints.ReceiverApi.URL_REFRESH_TOKEN,new RefreshTokenReqBody(refreshToken));
        return this;
    }

    @Step("Call API create refresh token with empty body")
    public RefreshTokenSteps given_create_refresh_token_with_empty_body(){
        removeHeaders();
        sendPost(EndPoints.ReceiverApi.URL_REFRESH_TOKEN,"{}");
        return this;
    }

    @Step("Call API create refresh token with type of value is integer")
    public RefreshTokenSteps given_create_refresh_token_with_type_of_value_is_integer(Integer refreshToken_value){
        removeHeaders();
        sendPost(EndPoints.ReceiverApi.URL_REFRESH_TOKEN,"{ \"refreshToken\": "+refreshToken_value+" }");
        return this;
    }


    @Step("Call API create refresh token with type of value is integer")
    public RefreshTokenSteps given_create_refresh_token_with_type_of_value_is_boolean(Boolean refreshToken_value){
        removeHeaders();
        sendPost(EndPoints.ReceiverApi.URL_REFRESH_TOKEN,"{ \"refreshToken\": "+refreshToken_value+" }");
        return this;
    }

    @Step("Verify statusCode")
    public RefreshTokenSteps then_verify_status_code(int code) {
        validateStatusCode(code);
        return this;
    }

    @Step("Verify errorCode")
    public RefreshTokenSteps then_verify_error_code(String error) {
        assert getJsonValue(ERROR_CODE).contains(error);
        return this;
    }

    @Step("Verify error")
    public RefreshTokenSteps then_verify_error(String error) {
        assert getJsonValue(ERROR).contains(error);
        return this;
    }

    @Step("Verify statusCode on Response")
    public RefreshTokenSteps then_verify_response_status_code(int code) {
        assert Integer.parseInt(getJsonValue(STATUS_CODE))==code;
        return this;
    }

    @Step("Verify errorMessage")
    public RefreshTokenSteps then_verify_error_message(String error) {
        assert getJsonAsString().contains(error);
        return this;
    }

    @Step("Verify response body when get refresh token successfully")
    public RefreshTokenSteps then_verify_get_refresh_token_successfully(){
        assert !getJsonValue(ACCESS_TOKEN).isEmpty();
        assert !getJsonValue(REFRESH_TOKEN).isEmpty();
        assert AUTHORIZATION_TYPE.equals(getJsonValue(TOKEN_TYPE));
        String time = getJsonValue(EXPIRES_IN);
        int expires_time = Integer.parseInt(time);
        assert expires_time > 0;
        return this;
    }

    @Step("Verify response body when code is empty")
    public RefreshTokenSteps then_verify_response_body_when_refresh_token_is_empty(){
        then_verify_error_message(REQUIRE_REFRESH_TOKEN);
        then_verify_error(BAD_REQUEST);
        then_verify_response_status_code(STATUS_CODE_400);
        return this;
    }

    @Step("Verify response body when code is null")
    public RefreshTokenSteps then_verify_response_body_when_refresh_token_is_null(){
        then_verify_error_message(REQUIRE_REFRESH_TOKEN);
        then_verify_error_message(REQUIRE_REFRESH_TOKEN_TYPE);
        then_verify_error(BAD_REQUEST);
        then_verify_response_status_code(STATUS_CODE_400);
        return this;
    }

    @Step("Verify response body when enter invalid type of refresh token")
    public RefreshTokenSteps then_verify_response_body_when_enter_invalid_type_of_refresh_token(){
        then_verify_error_message(REQUIRE_REFRESH_TOKEN_TYPE);
        then_verify_error(BAD_REQUEST);
        then_verify_response_status_code(STATUS_CODE_400);
        return this;
    }

    @Step("Verify response body when have server error")
    public RefreshTokenSteps then_verify_returned_internal_server_error(){
        then_verify_error_message(INTERNAL_SERVER_ERROR);
        then_verify_error_code(ERROR_CODE_500);
        then_verify_response_status_code(STATUS_CODE_500);
        return this;
    }
}
