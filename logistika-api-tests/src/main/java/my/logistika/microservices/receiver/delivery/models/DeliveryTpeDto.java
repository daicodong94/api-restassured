package my.logistika.microservices.receiver.delivery.models;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DeliveryTpeDto {
    String type;
    String queryBy;
    String value;
}
