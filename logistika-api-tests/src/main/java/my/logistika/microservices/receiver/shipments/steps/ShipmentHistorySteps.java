package my.logistika.microservices.receiver.shipments.steps;

import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.receiver.shipments.models.Constants;
import my.setel.core.BaseApi;
import org.testng.Assert;

import java.util.*;

import static my.logistika.constants.APIErrorMessage.Error.*;
import static my.logistika.constants.APIErrorMessage.Shipment.*;
import static my.logistika.constants.APIFieldName.Response.*;
import static my.logistika.microservices.receiver.shipments.models.Constants.receiverState;


public class ShipmentHistorySteps extends BaseApi {
    @Step("Call API get shipment history without authorization")
    public ShipmentHistorySteps given_get_shipment_history_without_authorization(Map<String, Object> queryParams){
        removeHeaders();
        sendGet(EndPoints.ReceiverApi.URL_GET_SHIPMENT_HISTORY,queryParams);
        return this;
    }

    @Step("Verify statusCode")
    public ShipmentHistorySteps then_verify_status_code(int code) {
        validateStatusCode(code);
        return this;
    }

    @Step("Verify errorCode")
    public ShipmentHistorySteps then_verify_error_code(String error) {
        assert getJsonValue(ERROR_CODE).contains(error);
        return this;
    }

    @Step("Verify error")
    public ShipmentHistorySteps then_verify_error(String error) {
        assert getJsonValue(ERROR).contains(error);
        return this;
    }
    
    @Step("Verify statusCode on Response")
    public ShipmentHistorySteps then_verify_response_status_code(int code) {
        assert Integer.parseInt(getJsonValue(STATUS_CODE))==code;
        return this;
    }

    @Step("Verify errorMessage")
    public ShipmentHistorySteps then_verify_error_message(String error) {
        assert getJsonAsString().contains(error);
        return this;
    }

    @Step("Verify response body when get shipment history successfully")
    public ShipmentHistorySteps then_verify_get_shipment_history_successfully(String workflowStatus) {
        String Response = String.valueOf(getJsonArray(this.getResponse()).get(0));
        assert !getJsonValue("updated_at").isEmpty();
        assert !getJsonValue("currentLocation").isEmpty();
        assert !getJsonValue("workflowStatusForUser").isEmpty();
        assert Response.contains(workflowStatus);
        return this;
    }

    @Step("Verify response body when get shipment history successfully with selected fields")
    public ShipmentHistorySteps then_verify_get_shipment_history_successfully_with_selected_field(List<String> selectedFields, Map<String,String> expected) {
        String Response = String.valueOf(getJsonArray(this.getResponse()).get(0));
        assert !Response.contains("updated_at");
        assert !Response.contains("currentLocation");
        assert !Response.contains("workflowStatusForUser");
        for (String selectedField : selectedFields) {
            String responseField = String.valueOf(selectedField);
            String responseValue = (getJsonArray(this.getResponse()).get(0).getAsJsonObject().get(responseField)).getAsString();
            String expectedValue = expected.get(responseField);
            Assert.assertEquals(responseValue, expectedValue);
        }
        return this;
    }

    public Map<String, String> mapFields(){
        Map<String,String> expectedMap = new HashMap<>();
        expectedMap.put("noOfParcels", Constants.noOfParcels);
        expectedMap.put("parcelWeight", Constants.parcelWeight);
        expectedMap.put("parcelLength", Constants.parcelLength);
        expectedMap.put("parcelWidth", Constants.parcelWidth);
        expectedMap.put("parcelHeight", Constants.parcelHeight);
        expectedMap.put("parcelSize", Constants.parcelSize);
        expectedMap.put("receiverName", Constants.receiverName);
        expectedMap.put("receiverPhoneNumber", Constants.receiverPhoneNumber);
        expectedMap.put("receiverAddress1", Constants.receiverAddress1);
        expectedMap.put("receiverAddress2", Constants.receiverAddress2);
        expectedMap.put("receiverPostalCode", Constants.receiverPostalCode);
        expectedMap.put("receiverCity", Constants.receiverCity);
        expectedMap.put("receiverState", receiverState);
        expectedMap.put("instructions", Constants.instructions);
        expectedMap.put("isReceiverOfficeAddress", Constants.isReceiverOfficeAddress);
        expectedMap.put("receiverAddress", Constants.receiverAddress1 + " " + Constants.receiverAddress2 + " " + Constants.receiverPostalCode + " " + Constants.receiverCity + " " + receiverState);
        expectedMap.put("receiverEmail", Constants.receiverEmail);
        expectedMap.put("merchant", Constants.merchant);
        expectedMap.put("merchantPhoneNumber", Constants.merchantPhoneNumber);
        expectedMap.put("merchantUserIdReferenceId", Constants.merchantUserIdReferenceId);
        expectedMap.put("initialPickupType", Constants.initialPickupType);
        expectedMap.put("senderAddress1", Constants.senderAddress1);
        expectedMap.put("senderAddress2", Constants.senderAddress2);
        expectedMap.put("senderCountry", Constants.senderCountry);
        expectedMap.put("senderCity", Constants.senderCity);
        expectedMap.put("senderState", Constants.senderState);
        expectedMap.put("senderPostalCode", Constants.senderPostalCode);
        expectedMap.put("senderPhoneNumber", Constants.senderPhoneNumber);
        expectedMap.put("workflowStatus", "Created");
        expectedMap.put("merchantUserId", Constants.merchantUserId);
        return expectedMap;
    }

    @Step("Verify response body is shipment not found")
    public ShipmentHistorySteps then_verify_response_body_is_shipment_not_found(){
        then_verify_error_message(SHIPMENT_NOT_FOUND);
        then_verify_error_code(ERROR_CODE_404);
        then_verify_response_status_code(STATUS_CODE_404);
        return this;
    }

    @Step("Verify response body when value is empty")
    public ShipmentHistorySteps then_verify_response_body_when_value_is_empty(){
        then_verify_error_message(REQUIRE_VALUE);
        then_verify_error(BAD_REQUEST);
        then_verify_response_status_code(STATUS_CODE_400);
        return this;
    }

    @Step("Verify response body when value is null")
    public ShipmentHistorySteps then_verify_response_body_when_value_is_null(){
        then_verify_error_message(REQUIRE_VALUE);
        then_verify_error_message(REQUIRE_VALUE_TYPE);
        then_verify_error(BAD_REQUEST);
        then_verify_response_status_code(STATUS_CODE_400);
        return this;
    }

    @Step("Verify response body when do not send type")
    public ShipmentHistorySteps then_verify_response_body_when_do_not_send_type(){
        then_verify_error_message(REQUIRE_TYPE);
        then_verify_error_message(REQUIRE_VALUE_OF_TYPE);
        then_verify_error(BAD_REQUEST);
        then_verify_response_status_code(STATUS_CODE_400);
        return this;
    }

    @Step("Verify response body when do not send type and value")
    public ShipmentHistorySteps then_verify_response_body_when_do_not_send_type_and_value(){
        then_verify_error_message(REQUIRE_TYPE);
        then_verify_error_message(REQUIRE_VALUE_OF_TYPE);
        then_verify_error_message(REQUIRE_VALUE);
        then_verify_error_message(REQUIRE_VALUE_TYPE);
        then_verify_error(BAD_REQUEST);
        then_verify_response_status_code(STATUS_CODE_400);
        return this;
    }

    @Step("Verify response body when enter invalid value of type field")
    public ShipmentHistorySteps then_verify_response_body_when_enter_invalid_value_of_type_field(){
        then_verify_error_message(REQUIRE_VALUE_OF_TYPE);
        then_verify_error(BAD_REQUEST);
        then_verify_response_status_code(STATUS_CODE_400);
        return this;
    }
}
