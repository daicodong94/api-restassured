package my.logistika.microservices.receiver.delivery.steps;
import com.google.gson.Gson;
import io.qameta.allure.Step;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.receiver.auth.steps.AccessTokenSteps;
import my.logistika.microservices.receiver.auth.steps.AuthProfileSteps;
import my.logistika.microservices.receiver.delivery.models.DeliveryTpeDto;
import my.setel.core.BaseApi;
import my.setel.utils.Logger;
import my.setel.utils.LoggerUtils;
import org.apache.http.Header;
import org.apache.http.HttpStatus;

public class DeliveryTypeSteps extends BaseApi {

    @Step("When a receiver updates delivery type")
    public DeliveryTypeSteps when_a_receiver_updates_delivery_type(String deliveryOption, String queryBy, String trackingToken){
        LoggerUtils.getInstance().info(new Gson().toJson(new DeliveryTpeDto(deliveryOption,queryBy,trackingToken)));
        removeHeaders();
        setHeader("Content-Type", ContentType.JSON);
        sendPost(EndPoints.ReceiverApi.URL_DELIVERY_TYPE, new DeliveryTpeDto(deliveryOption,queryBy,trackingToken));
        return this;
    }

    @Step("the delivery type update must be successful on recevier api")
    public DeliveryTypeSteps then_delivery_type_must_be_updated_on_receiver(){
        validateStatusCode(HttpStatus.SC_OK);
        assert getJsonValue("message").equals("Delivery Type Updated!");
        return this;
    }
}
