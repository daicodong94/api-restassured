package my.logistika.microservices.receiver.shipments.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SavedShipmentReqBody {
    String type;
    String status;
    String limit;
    String offset;
}
