package my.logistika.microservices.receiver.auth.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;

@Data
@AllArgsConstructor
public class CallAuthorizeReqBody {
    ArrayList scopes;
    String clientId;
    String redirectUri;
}
