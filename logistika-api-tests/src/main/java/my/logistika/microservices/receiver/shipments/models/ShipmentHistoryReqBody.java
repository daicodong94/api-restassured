package my.logistika.microservices.receiver.shipments.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ShipmentHistoryReqBody {
    String type;
    String value;
    String selectedFields;
}
