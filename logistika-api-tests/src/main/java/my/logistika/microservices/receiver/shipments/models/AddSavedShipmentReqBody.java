package my.logistika.microservices.receiver.shipments.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AddSavedShipmentReqBody {
    String shipmentId;
}
