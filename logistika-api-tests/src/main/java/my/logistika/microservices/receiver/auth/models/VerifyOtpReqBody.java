package my.logistika.microservices.receiver.auth.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class VerifyOtpReqBody {
    String phone;
    String otp;
}
