package my.logistika.microservices.receiver.shipments.steps;

import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.setel.core.BaseApi;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static my.logistika.constants.APIErrorMessage.Error.*;
import static my.logistika.constants.APIErrorMessage.Shipment.*;
import static my.logistika.constants.APIFieldName.AUTHORIZATION_TYPE;
import static my.logistika.constants.APIFieldName.Header.HEADER_AUTHORIZATION;
import static my.logistika.constants.APIFieldName.Response.*;


public class SavedShipmentSteps extends BaseApi {
    @Step("Call API get shipment history with authorization")
    public SavedShipmentSteps given_get_saved_shipment_with_authorization(String accessToken, Map<String, Object> queryParams) {
        removeHeaders();
        setHeader(HEADER_AUTHORIZATION, AUTHORIZATION_TYPE + " " + accessToken);
        sendGet(EndPoints.ReceiverApi.URL_GET_SAVED_SHIPMENT, queryParams);
        return this;
    }

    @Step("Call API get shipment history without authorization")
    public SavedShipmentSteps given_get_saved_shipment_without_authorization(Map<String, Object> queryParams) {
        removeHeaders();
        sendGet(EndPoints.ReceiverApi.URL_GET_SAVED_SHIPMENT, queryParams);
        return this;
    }

    @Step("Verify statusCode")
    public SavedShipmentSteps then_verify_status_code(int code) {
        validateStatusCode(code);
        return this;
    }

    @Step("Verify errorCode")
    public SavedShipmentSteps then_verify_error_code(String error) {
        assert getJsonValue(ERROR_CODE).contains(error);
        return this;
    }

    @Step("Verify error")
    public SavedShipmentSteps then_verify_error(String error) {
        assert getJsonValue(ERROR).contains(error);
        return this;
    }

    @Step("Verify statusCode on Response")
    public SavedShipmentSteps then_verify_response_status_code(Integer code) {
        assert Integer.parseInt(getJsonValue(STATUS_CODE)) == code;
        return this;
    }

    @Step("Verify errorMessage")
    public SavedShipmentSteps then_verify_error_message(String error) {
        assert getJsonAsString().contains(error);
        return this;
    }

    @Step("Verify response body when get my shipment successfully with status")
    public SavedShipmentSteps then_verify_response_body_get_my_shipment_successfully_with_status(String workflowStatus) {
        assert "20".equals(getJsonValue("data.limit"));
        assert "0".equals(getJsonValue("data.offset"));
        int total_data = Integer.parseInt(getJsonValue("data.total"));
        if (total_data > 0) {
            for (int i = 0; i < getJsonArraySize("data.data"); i++) {
                String getWorkflowStatus = getJsonValue("data.data[" + i + "].workflowStatusForUser");
                assert getWorkflowStatus.equals(workflowStatus);
                if (workflowStatus.equals("Completed") || workflowStatus.equals("Cancelled") || workflowStatus.equals("Returned")) {
                    String updateAt = getJsonValue("data.data[" + i + "].updated_at");
                    then_verify_update_date_not_past_14days(updateAt);
                } else {
                    String updateAt = getJsonValue("data.data[" + i + "].updated_at");
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                    String now = LocalDateTime.now().format(formatter);
                    LocalDateTime updateDate = LocalDateTime.parse(updateAt, formatter);
                    LocalDateTime currentDate = LocalDateTime.parse(now, formatter);
                    currentDate = currentDate.minusDays(14L);
                    int index = currentDate.compareTo(updateDate);
                    assert index > 0 || index <= 0;
                }
            }
        }
        return this;
    }

    @Step("Verify response body when get shipment history successfully with status")
    public SavedShipmentSteps then_verify_response_body_get_shipment_history_successfully_with_status(String workflowStatus) {
        assert "20".equals(getJsonValue("data.limit"));
        assert "0".equals(getJsonValue("data.offset"));
        assert Integer.parseInt(getJsonValue("data.total")) > 0;
        for (int i = 0; i < getJsonArraySize("data.data"); i++) {
            String getWorkflowStatus = getJsonValue("data.data[" + i + "].workflowStatusForUser");
            assert getWorkflowStatus.equals(workflowStatus);
            if (workflowStatus.equals("Completed") || workflowStatus.equals("Cancelled") || workflowStatus.equals("Returned")) {
                String updateAt = getJsonValue("data.data[" + i + "].updated_at");
                then_verify_update_date_past_14days(updateAt);
            }
        }
        return this;
    }

    @Step("Verify response body when get my shipment successfully")
    public SavedShipmentSteps then_verify_response_body_get_my_shipment_successfully() {
        assert "20".equals(getJsonValue("data.limit"));
        assert "0".equals(getJsonValue("data.offset"));
        assert Integer.parseInt(getJsonValue("data.total")) > 0;
        for (int i = 0; i < getJsonArraySize("data.data"); i++) {
            String workflowStatus = getJsonValue("data.data[" + i + "].workflowStatusForUser");
            if (workflowStatus.equals("Completed") || workflowStatus.equals("Cancelled")) {
                String updateAt = getJsonValue("data.data[" + i + "].updated_at");
                then_verify_update_date_not_past_14days(updateAt);
            } else {
                String updateAt = getJsonValue("data.data[" + i + "].updated_at");
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                String now = LocalDateTime.now().format(formatter);
                LocalDateTime updateDate = LocalDateTime.parse(updateAt, formatter);
                LocalDateTime currentDate = LocalDateTime.parse(now, formatter);
                currentDate = currentDate.minusDays(14L);
                int index = currentDate.compareTo(updateDate);
                assert index > 0 || index <=0;
            }
        }
        return this;
    }

    @Step("Verify response body when get shipment history successfully")
    public SavedShipmentSteps then_verify_response_body_get_shipment_history_successfully() {
        assert "20".equals(getJsonValue("data.limit"));
        assert "0".equals(getJsonValue("data.offset"));
        assert Integer.parseInt(getJsonValue("data.total")) > 0;
        assert Integer.parseInt(getJsonValue("data.total")) > 0;
        for (int i = 0; i < getJsonArraySize("data.data"); i++) {
            String getWorkflowStatus = getJsonValue("data.data[" + i + "].workflowStatusForUser");
            assert getWorkflowStatus.equals("Completed") || getWorkflowStatus.equals("Cancelled") || getWorkflowStatus.equals("Returned");
            String updateAt = getJsonValue("data.data[" + i + "].updated_at");
            then_verify_update_date_past_14days(updateAt);
        }
        return this;
    }

    @Step("Verify response body when get my shipment successfully with limit")
    public SavedShipmentSteps then_verify_response_body_get_my_shipment_successfully_with_limit(Integer limit) {
        assert limit.equals(Integer.parseInt(getJsonValue("data.limit")));
        assert "0".equals(getJsonValue("data.offset"));
        assert Integer.parseInt(getJsonValue("data.total")) > 0;
        assert getJsonArraySize("data.data") <= limit;
        for (int i = 0; i < getJsonArraySize("data.data"); i++) {
            String workflowStatus = getJsonValue("data.data["+i+"].workflowStatusForUser");
            if (workflowStatus.equals("Completed") || workflowStatus.equals("Cancelled") || workflowStatus.equals("Returned")) {
                String updateAt = getJsonValue("data.data["+i+"].updated_at");
                then_verify_update_date_not_past_14days(updateAt);
            }
        }
        return this;
    }

    @Step("Get the total data")
    public Integer get_the_total_data(){
        return Integer.parseInt(getJsonValue("data.total"));
    }

    @Step("Verify response body when get my shipment successfully with limit and offset")
    public SavedShipmentSteps then_verify_response_body_get_my_shipment_successfully_with_offset_is_total_minus_one(Integer limit, Integer offset) {
        assert limit.equals(Integer.parseInt(getJsonValue("data.limit")));
        assert offset.equals(Integer.parseInt(getJsonValue("data.offset")));
        assert Integer.parseInt(getJsonValue("data.total")) > 0;
        String workflowStatus = getJsonValue("data.data[0].workflowStatusForUser");
        if (workflowStatus.equals("Completed") || workflowStatus.equals("Cancelled") || workflowStatus.equals("Returned")) {
            String updateAt = getJsonValue("data.data[0].updated_at");
            then_verify_update_date_not_past_14days(updateAt);
        }
        return this;
    }

    public String get_random_status_saved_history() {
        List<String> givenList = Arrays.asList("Completed","Cancelled","Returned");
        Random rand = new Random();
        return givenList.get(rand.nextInt(givenList.size()));
    }

    public String get_random_status() {
        List<String> givenList = Arrays.asList("Created","In Transit","Out for delivery","Transferred to 3PL","Ready for collection",
                "Pending Pickup","Pending drop Off","Delivery failed","Pending Return","On hold","Completed","Cancelled","Returned");
        Random rand = new Random();
        return givenList.get(rand.nextInt(givenList.size()));
    }

    @Step("Verify response body when do not send type")
    public SavedShipmentSteps then_verify_response_body_when_do_not_send_type(){
        then_verify_error_message(REQUIRE_TYPE);
        then_verify_error_message(REQUIRE_VALUE_OF_TYPE);
        then_verify_error(BAD_REQUEST);
        then_verify_response_status_code(STATUS_CODE_400);
        return this;
    }

    @Step("Verify response body when enter invalid value of type field")
    public SavedShipmentSteps then_verify_response_body_when_enter_invalid_value_of_type_field(){
        then_verify_error_message(REQUIRE_VALUE_OF_TYPE);
        then_verify_error(BAD_REQUEST);
        then_verify_response_status_code(STATUS_CODE_400);
        return this;
    }

    @Step("Verify response body when enter invalid limit type")
    public SavedShipmentSteps then_verify_response_body_when_enter_invalid_limit_type(){
        then_verify_error_message(INVALID_LIMIT_TYPE);
        then_verify_error(BAD_REQUEST);
        then_verify_response_status_code(STATUS_CODE_400);
        return this;
    }

    @Step("Verify response body when enter invalid offset_type")
    public SavedShipmentSteps then_verify_response_body_when_enter_invalid_offset_type(){
        then_verify_error_message(INVALID_OFFSET_TYPE);
        then_verify_error(BAD_REQUEST);
        then_verify_response_status_code(STATUS_CODE_400);
        return this;
    }

    @Step("Verify response body when enter invalid status")
    public SavedShipmentSteps then_verify_response_body_when_enter_invalid_status(){
        then_verify_error_message(INVALID_STATUS);
        then_verify_error(BAD_REQUEST);
        then_verify_response_status_code(STATUS_CODE_400);
        return this;
    }

    @Step("Verify response body when enter expired access token")
    public SavedShipmentSteps then_verify_response_body_when_enter_expired_access_token(){
        then_verify_error_message(UNAUTHORIZED);
        then_verify_error_code(ERROR_CODE_401);
        then_verify_response_status_code(STATUS_CODE_401);
        return this;
    }

    public SavedShipmentSteps then_verify_update_date_not_past_14days(String updateAt){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String now = LocalDateTime.now().format(formatter);
        LocalDateTime updateDate = LocalDateTime.parse(updateAt, formatter);
        LocalDateTime currentDate = LocalDateTime.parse(now, formatter);
        currentDate = currentDate.minusDays(14L);
        int index = currentDate.compareTo(updateDate);
        assert index <= 0;
        return this;
    }

    public SavedShipmentSteps then_verify_update_date_past_14days(String updateAt){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String now = LocalDateTime.now().format(formatter);
        LocalDateTime updateDate = LocalDateTime.parse(updateAt, formatter);
        LocalDateTime currentDate = LocalDateTime.parse(now, formatter);
        currentDate = currentDate.minusDays(14L);
        int index = currentDate.compareTo(updateDate);
        assert index > 0;
        return this;
    }
}
