package my.logistika.microservices.receiver.NextPublicHolidaysCount.steps;
import com.google.gson.JsonObject;
import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;
import my.logistika.microservices.receiver.NextPublicHolidaysCount.models.NextPublicHolidaysCountResponse;
import my.setel.core.BaseApi;
import org.testng.Assert;

import java.util.Map;

public class NextPublicHolidaysCountSteps extends BaseApi {

    @Step("when next public holidays count for given date is requested")
    public NextPublicHolidaysCountSteps when_holiday_count_is_requested(Map<String, Object> queryParams) {
        sendGet(EndPoints.ReceiverApi.NEXT_PUBLIC_HOLIDAYS_COUNT, queryParams);
        return this;
    }

    @Step("then number of holidays after current date is returned")
    public NextPublicHolidaysCountSteps then_expected_holidays_count_is_returned(int expectedCount, NextPublicHolidaysCountResponse actualResponseObject) {
        int actualCount = actualResponseObject.getCount();
        Assert.assertEquals(expectedCount,actualCount);
        return this;
    }



}
