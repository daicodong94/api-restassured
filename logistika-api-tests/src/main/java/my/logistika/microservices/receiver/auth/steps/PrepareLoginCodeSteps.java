package my.logistika.microservices.receiver.auth.steps;
import io.qameta.allure.Step;
import my.logistika.constants.EndPoints;

import my.logistika.microservices.receiver.auth.models.*;
import my.setel.core.BaseApi;

import java.util.ArrayList;

import static my.logistika.constants.APIFieldName.*;
import static my.logistika.constants.APIFieldName.Response.*;
import static my.logistika.constants.APIFieldName.Header.*;
import static my.logistika.microservices.receiver.auth.models.Constants.*;

public class PrepareLoginCodeSteps extends BaseApi {

    @Step("Call API send otp when login to Setel app")
    public PrepareLoginCodeSteps given_send_otp(String phone){
        removeHeaders();
        sendPost(EndPoints.sandBoxSetelApi.URL_SEND_OTP,new SendOtpReqBody(phone));
        return this;
    }

    @Step("Call API verify otp when login to Setel app")
    public PrepareLoginCodeSteps then_verify_otp(String phone, String otp){
        removeHeaders();
        sendPost(EndPoints.sandBoxSetelApi.URL_VERIFY_OTP,new VerifyOtpReqBody(phone,otp));
        return this;
    }

    @Step("Call API drop in")
    public PrepareLoginCodeSteps given_drop_in(String phoneToken, String otpToken, String phone){
        removeHeaders();
        setHeader(HEADER_PHONE_TOKEN,phoneToken);
        setHeader(HEADER_OTP_TOKEN,otpToken);
        sendPost(EndPoints.sandBoxSetelApi.URL_DROPIN,new DropInReqBody(phone));
        return this;
    }

    @Step("Call API authorize")
    public PrepareLoginCodeSteps given_authorize(String accessToken){
        removeHeaders();
        setHeader(HEADER_AUTHORIZATION,AUTHORIZATION_TYPE+" "+accessToken);
        ArrayList<String> scopes = new ArrayList<>();
        scopes.add(SCOPE_VALUE);
        sendPost(EndPoints.sandBoxSetelApi.URL_AUTHORIZE,new CallAuthorizeReqBody(scopes,CLIENT_ID,REDIRECT_URI));
        return this;
    }

    @Step("Verify statusCode")
    public PrepareLoginCodeSteps then_verify_status_code(int code) {
        validateStatusCode(code);
        return this;
    }

    @Step("Verify get otp successfully")
    public PrepareLoginCodeSteps then_verify_get_otp_successfully(){
        assert !getJsonValue(OTP_TOKEN).isEmpty();
        return this;
    }

    @Step("Get the returned phoneToken")
    public String then_get_the_returned_phoneToken(){
        return getJsonValue(PHONE_TOKEN);
    }

    @Step("Get the returned otpToken")
    public String then_get_the_returned_otpToken(){
        return getJsonValue(OTP_TOKEN);
    }

    @Step("Get the returned login access token")
    public String then_get_the_returned_login_access_token(){
        return getJsonValue(ACCESS_TOKEN);
    }

    @Step("Get the returned code")
    public String then_get_the_returned_code(){
        return getJsonValue(CODE);
    }
}
