package my.logistika.ultils;

import java.time.LocalDate;

public class DateCalculator {

    public LocalDate today() {
        LocalDate today = LocalDate.now();
        return today;
    }

    public LocalDate tomorrow() {
        LocalDate today = LocalDate.now();
        LocalDate tomorrow = today.plusDays(1);
        return tomorrow;
    }

    public LocalDate getDate(long dayToAdd) {
        LocalDate today = LocalDate.now();
        LocalDate expectedDate = today.plusDays(dayToAdd);
        return expectedDate;
    }
}
