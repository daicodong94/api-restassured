package my.logistika.data;

import com.google.gson.annotations.SerializedName;

public class DataUser {
    @SerializedName("username")
    public String username;
    @SerializedName("password")
    public String password;
    @SerializedName("userId")
    public String userId;
    @SerializedName("code")
    public String code;
}
