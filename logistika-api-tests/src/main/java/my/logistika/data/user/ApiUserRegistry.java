package my.logistika.data.user;

import com.google.gson.JsonObject;
import my.logistika.data.DataUser;
import my.setel.utils.JsonUtils;
import my.setel.utils.ObjectMappingUtils;

public class ApiUserRegistry {

    public static DataUser MERCHANT;
    public static DataUser MERCHANT_1;
    public static DataUser MERCHANT_2;
    public static DataUser FMD_DJY052_KV;
    public static DataUser LMD_DJY052_KV;
    public static DataUser HO_DJY052_KV;
    public static DataUser MMD_DJY052_TDY053_KV;
    public static DataUser MMD_DJY052_TMS051_KV;
    public static DataUser superAdmin;
    public static DataUser OPS_SUPPORT;
    public static DataUser FMD_TDY053_KV;
    public static DataUser LMD_TDY053_KV;
    public static DataUser HO_TDY053_KV;

    public static DataUser ADMIN_PORTAL;
    public static DataUser MMD_TDY053_LDC081;
    public static DataUser MMD_LDC081_MTR080;

    public static DataUser HO_BAJ121_PNG;
    public static DataUser FMD_BAJ121_PNG;
    public static DataUser LMD_BAJ121_PNG;
    public static DataUser CUSTOMER_GLOBAL;
    public static DataUser ROUTING_DELIVERY;

    public static DataUser HO_TEST011;
    public static DataUser FMD_TEST011;
    public static DataUser LMD_TEST011;


    public ApiUserRegistry() {
        JsonObject map = null;
        if (System.getProperty("logistikaAPI.baseEnv").contains("staging")) {
            map = JsonUtils.readJsonFile("staging/staging_api_user_data.json");
        } else if (System.getProperty("logistikaAPI.baseEnv").contains("dev")) {
            map = JsonUtils.readJsonFile("dev/dev_api_user_data.json");
        } else if (System.getProperty("logistikaAPI.baseEnv").contains("prod")) {
            map = JsonUtils.readJsonFile("prod/api_prod_user_data.json");
        }
        MERCHANT = (DataUser) ObjectMappingUtils.parseJsonToModel(map.getAsJsonObject("merchant").toString(), DataUser.class);

        MERCHANT_1 = (DataUser) ObjectMappingUtils.parseJsonToModel(map.getAsJsonObject("merchant1").toString(), DataUser.class);
        MERCHANT_2 = (DataUser) ObjectMappingUtils.parseJsonToModel(map.getAsJsonObject("merchant2").toString(), DataUser.class);

        FMD_DJY052_KV=(DataUser) ObjectMappingUtils.parseJsonToModel(map.getAsJsonObject("FMD_DJY052").toString(),DataUser.class);
        LMD_DJY052_KV= (DataUser) ObjectMappingUtils.parseJsonToModel(map.getAsJsonObject("LMD_DJY052").toString(),DataUser.class);
        HO_DJY052_KV= (DataUser) ObjectMappingUtils.parseJsonToModel(map.getAsJsonObject("HO_DJY052").toString(),DataUser.class);
        MMD_DJY052_TDY053_KV=(DataUser) ObjectMappingUtils.parseJsonToModel(map.getAsJsonObject("MMD_DJY052_TDY053").toString(),DataUser.class);
        superAdmin=(DataUser) ObjectMappingUtils.parseJsonToModel(map.getAsJsonObject("superAdmin").toString(),DataUser.class);
        OPS_SUPPORT=(DataUser) ObjectMappingUtils.parseJsonToModel(map.getAsJsonObject("Ops_support").toString(),DataUser.class);
        FMD_TDY053_KV=(DataUser) ObjectMappingUtils.parseJsonToModel(map.getAsJsonObject("FMD_TDY053").toString(),DataUser.class);
        LMD_TDY053_KV= (DataUser) ObjectMappingUtils.parseJsonToModel(map.getAsJsonObject("LMD_TDY053").toString(),DataUser.class);
        HO_TDY053_KV= (DataUser) ObjectMappingUtils.parseJsonToModel(map.getAsJsonObject("HO_TDY053").toString(),DataUser.class);
        ADMIN_PORTAL = (DataUser) ObjectMappingUtils.parseJsonToModel(map.getAsJsonObject("tigersheet").toString(),DataUser.class);
        MMD_TDY053_LDC081= (DataUser) ObjectMappingUtils.parseJsonToModel(map.getAsJsonObject("MMD_TDY053_LDC081").toString(),DataUser.class);
        MMD_LDC081_MTR080= (DataUser) ObjectMappingUtils.parseJsonToModel(map.getAsJsonObject("MMD_LDC081_MTR080").toString(),DataUser.class);
        CUSTOMER_GLOBAL=(DataUser) ObjectMappingUtils.parseJsonToModel(map.getAsJsonObject("customerGlobal").toString(),DataUser.class);

        ROUTING_DELIVERY= (DataUser) ObjectMappingUtils.parseJsonToModel(map.getAsJsonObject("ROUTING_DELIVERY").toString(),DataUser.class);
        HO_TEST011=(DataUser) ObjectMappingUtils.parseJsonToModel(map.getAsJsonObject("HO_TEST011").toString(),DataUser.class);
        FMD_TEST011=(DataUser) ObjectMappingUtils.parseJsonToModel(map.getAsJsonObject("FMD_TEST011").toString(),DataUser.class);
        LMD_TEST011=(DataUser) ObjectMappingUtils.parseJsonToModel(map.getAsJsonObject("LMD_TEST011").toString(),DataUser.class);
    }
}
